<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteSericeProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','Auth\LoginController@showLoginForm');

Auth::routes();

Route::group(['middleware' => 'auth'],function(){

	Route::resource('dashboards', 'DashboardsController');


	Route::group(['prefix' => 'payrolls/admin/filemanagers'],function(){

		Route::get('benefits/getItem','BenefitsController@getItem');
		Route::post('benefits/delete','BenefitsController@delete');
		Route::resource('benefits','BenefitsController');

		Route::get('adjustments/getItem','AdjustmentsController@getItem');
		Route::resource('adjustments','AdjustmentsController');

		Route::get('deductions/getItem','DeductionsController@getItem');
		Route::post('deductions/delete','DeductionsController@delete');
		Route::resource('deductions','DeductionsController');

		Route::get('loans/getItem','LoansController@getItem');
		Route::post('loans/delete','LoansController@delete');
		Route::resource('loans','LoansController');

		Route::get('responsibilitiescenter/getItem','ResponsibilitiesCenterController@getItem');
		Route::post('responsibilitiescenter/delete','ResponsibilitiesCenterController@delete');
		Route::resource('responsibilitiescenter','ResponsibilitiesCenterController');

		Route::get('projects/getItem','ProjectsController@getItem');
		Route::post('projects/delete','ProjectsController@delete');
		Route::resource('projects','ProjectsController');

		Route::get('banks/getItem','BanksController@getItem');
		Route::post('banks/delete','BanksController@delete');
		Route::resource('banks','BanksController');

		Route::get('bankbranches/getItem','BankBranchesController@getItem');
		Route::resource('bankbranches','BankBranchesController');

		Route::get('pagibig/showPolicy','PagibigController@showPolicy');
		Route::get('pagibig/getItem','PagibigController@getItem');
		Route::post('pagibig/delete','PagibigController@delete');
		Route::resource('pagibig','PagibigController');

		Route::get('philhealths/showPolicy','PhilhealthsController@showPolicy');
		Route::get('philhealths/getItem','PhilhealthsController@getItem');
		Route::post('philhealths/delete','PhilhealthsController@delete');
		Route::resource('philhealths','PhilhealthsController');

		Route::get('providentfundpolicies/getItem','ProvidentFundPoliciesController@getItem');
		Route::resource('providentfundpolicies','ProvidentFundPoliciesController');

		Route::get('salariesgrade/getSgstep','SalariesGradeController@getSgstep');
		Route::resource('salariesgrade','SalariesGradeController');

		Route::post('taxes/storeTaxstatus','TaxesController@storeTaxstatus');
		Route::get('taxes/showTaxstatus','TaxesController@showTaxstatus');
		Route::post('taxes/storeAnnualTaxPolicy','TaxesController@storeAnnualTaxPolicy');
		Route::get('taxes/showAnnualTaxPolicy','TaxesController@showAnnualTaxPolicy');
		Route::post('taxes/storeTaxpolicy','TaxesController@storeTaxpolicy');
		Route::get('taxes/showTaxpolicy','TaxesController@showTaxpolicy');

		Route::get('taxes/getTaxtable','TaxesController@getTaxtable');
		Route::get('taxes/getTaxstatus','TaxesController@getTaxstatus');
		Route::get('taxes/getTaxannual','TaxesController@getTaxannual');
		Route::get('taxes/getTaxpolicy','TaxesController@getTaxpolicy');

		Route::post('taxes/delete','TaxesController@delete');
		Route::resource('taxes','TaxesController');

		Route::get('wagerates/getItem','WageRatesController@getItem');
		Route::resource('wagerates','WageRatesController');

		Route::get('gsis/getItem','GsisController@getItem');
		Route::post('gsis/delete','GsisController@delete');
		Route::resource('gsis','GsisController');

		Route::get('jobgrades/getJgstep','GsisController@getJgstep');
		Route::resource('jobgrades','JobGradesController');

		Route::resource('divisions','DivisionsController');
		Route::resource('offices','OfficesController');
		Route::resource('employeestatus','EmployeeStatusController');
		Route::resource('positions','PositionsController');
		Route::resource('position_items','PositionItemsController');
		Route::post('rates/delete','RatesController@delete');
		Route::resource('rates','RatesController');
		Route::post('travelrates/delete','TravelRateController@delete');
		Route::resource('travelrates','TravelRateController');
	});

	Route::group(['prefix' => 'payrolls/reports/remittances'],function(){
		Route::resource('gsisremittances','GsisRemittancesController');
		Route::resource('ecip','EcipRemittancesController');
		Route::resource('pagibigremittances','PagibigRemittancesController');
		Route::resource('philhealth','PhilhealthRemittancesController');
	});

	Route::group(['prefix' => 'payrolls/reports/othercompensations'],function(){

		Route::get('communicationexpense/getEmployeeinfo','CommunicationExpenseReportController@getEmployeeinfo');
		Route::resource('communicationexpense','CommunicationExpenseReportController');

		Route::get('eme/getEmployeeinfo','EmeReportsController@getEmployeeinfo');
		Route::resource('eme','EmeReportsController');

		Route::get('cashmidyearbonus/getEmployeeinfo','CashGiftsAndMidYearReportController@getEmployeeinfo');
		Route::resource('cashmidyearbonus','CashGiftsAndMidYearReportController');
	});

	Route::group(['prefix' => 'payrolls/reports'], function(){

		Route::resource('loansreport','LoansReportController');
		Route::get('payslips/getSearchby','PayslipsController@getSearchby');
		Route::get('payslips/getPayslip','PayslipsController@getPayslip');
		Route::resource('payslips','PayslipsController');
		Route::resource('government','GovernmentsReportController');
		Route::resource('banksreport','BanksReportController');
		Route::get('performancesreport/getEmployeeinfo','PerformancesReportController@getEmployeeinfo');
		Route::resource('performancesreport','PerformancesReportController');
		Route::resource('joborder','JobOrderReportsController');
		Route::get('overtime/getOvertimeReport','OvertimeReportsController@getOvertimeReport');
		Route::resource('overtime','OvertimeReportsController');
		Route::resource('rata','RataReportsController');
		Route::resource('generalpayroll','GeneralPayrollReportController');
		Route::resource('contractofservice','ContractOfServiceController');
		Route::resource('employeeentitledmidyearbonus','EmployeeEntitledMidYearBonusController');
		Route::resource('employeeentitledpei','EmployeeEntitledPEIController');
		Route::resource('employeeentitledriceallowance','EmployeeEntitledRiceAllowanceController');
		Route::resource('employeeentitledyearendbonus','EmployeeEntitledYearEndBonusController');
		Route::resource('employeeentitledanniversarybonus','EmployeeEntitledAnniversaryBonusController');
		Route::resource('employeeentitleduniformallowance','EmployeeEntitledUniformAllowanceController');
		Route::resource('employeeentitledhonoraria','EmployeeEntitledHonorariaController');
		Route::resource('gippayrollworksheet','GIPPayrollWorksheetReportsController');
		Route::resource('deductedloanreports','DeductedLoanReportsController');
		Route::resource('loandetailsreports','LoanDetailsReportsController');
		Route::resource('gsisloanreports','GSISLoanReportsController');
		Route::resource('gipdeductionreports','GIPDeductionReportsController');
		Route::resource('specialpayrollreports','SpecialPayrollReportsController');
		Route::resource('cosgeneralpayrollreports','COSGeneralPayrollReportsController');
		Route::resource('cashadvancereports','CashAdvanceReportsController');
		Route::resource('certificate_of_employment','COEController');
		Route::resource('coec_report','COECController');

	});

	Route::group(['prefix'=>'payrolls/otherpayrolls'],function(){
		Route::get('initialsalaries/getInitialSalary','InitialSalariesController@getInitialSalary');
		Route::resource('initialsalaries','InitialSalariesController');
		Route::get('lastsalaries/getLastSalary','LastSalariesController@getLastSalary');
		Route::resource('lastsalaries','LastSalariesController');
		Route::get('cancelsalaries/getCancelSalary','CancelSalariesController@getCancelSalary');
		Route::resource('cancelsalaries','CancelSalariesController');

		// TRAVEL ALLOWANCE
		Route::get('leavemonetizations/getLeaveMonetization','LeaveMonetizationTransactionsController@getLeaveMonetization');
		Route::get('leavemonetizations/showLeaveMonetizationDatatable','LeaveMonetizationTransactionsController@showLeaveMonetizationDatatable');
		Route::post('leavemonetizations/processLeaveMonetization','LeaveMonetizationTransactionsController@processLeaveMonetization');
		Route::post('leavemonetizations/deleteLeaveMonetization','LeaveMonetizationTransactionsController@deleteLeaveMonetization');
		Route::resource('leavemonetizations','LeaveMonetizationTransactionsController');
	});

	Route::group(['prefix' => 'payrolls/specialpayrolls'],function(){

				// RATA PAYROLL
		Route::get('ratatransactions/getSearchby','RataTransactionsController@getSearchby');
		Route::get('ratatransactions/getRataInfo','RataTransactionsController@getRataInfo');
		Route::post('ratatransactions/storeRata','RataTransactionsController@storeRata');
		Route::post('ratatransactions/processRata','RataTransactionsController@processRata');
		Route::post('ratatransactions/deleteRata','RataTransactionsController@deleteRata');
		Route::resource('ratatransactions/showRataDatatable','RataTransactionsController@showRataDatatable');
		Route::resource('ratatransactions/filter','RataTransactionsController@filter');
		Route::resource('ratatransactions','RataTransactionsController');

		// EME PAYROLL
		Route::get('emetransactions/getEmeInfo','EmeTransactionsController@getEmeInfo');
		Route::get('emetransactions/showEmeDatatable','EmeTransactionsController@showEmeDatatable');
		Route::post('emetransactions/processEme','EmeTransactionsController@processEme');
		Route::post('emetransactions/deleteEme','EmeTransactionsController@deleteEme');
		Route::resource('emetransactions','EmeTransactionsController');

		// COMMUNICATION ALLOTMENT
		Route::get('communicationtransactions/getCeaInfo','CommunicationTransactionsController@getCeaInfo');
		Route::get('communicationtransactions/showCeaDatatable','CommunicationTransactionsController@showCeaDatatable');
		Route::post('communicationtransactions/processCea','CommunicationTransactionsController@processCea');
		Route::post('communicationtransactions/deleteCea','CommunicationTransactionsController@deleteCea');
		Route::resource('communicationtransactions','CommunicationTransactionsController');

		// PERFORMANCE INCENTIVE
		Route::get('peitransactions/getPeiInfo','PerformanceIncentiveTransactionsController@getPeiInfo');
		Route::get('peitransactions/showPeiDatatable','PerformanceIncentiveTransactionsController@showPeiDatatable');
		Route::post('peitransactions/processPei','PerformanceIncentiveTransactionsController@processPei');
		Route::post('peitransactions/deletePei','PerformanceIncentiveTransactionsController@deletePei');
		Route::resource('peitransactions','PerformanceIncentiveTransactionsController');

		// MID YEAR BONUS
		Route::get('midyeartransactions/getMidYear','MidYearBonusTransactionsController@getMidYear');
		Route::get('midyeartransactions/showMidYearDatatable','MidYearBonusTransactionsController@showMidYearDatatable');
		Route::post('midyeartransactions/processMidYear','MidYearBonusTransactionsController@processMidYear');
		Route::post('midyeartransactions/deleteMidYear','MidYearBonusTransactionsController@deleteMidYear');
		Route::resource('midyeartransactions','MidYearBonusTransactionsController');

		// CASH GIFT AND YEAR END BONUS
		Route::get('cgyetransactions/getCgyeInfo','CashGiftAndYearEndTransactionsController@getCgyeInfo');
		Route::get('cgyetransactions/showCgyeDatatable','CashGiftAndYearEndTransactionsController@showCgyeDatatable');
		Route::post('cgyetransactions/processCgye','CashGiftAndYearEndTransactionsController@processCgye');
		Route::post('cgyetransactions/deleteCgye','CashGiftAndYearEndTransactionsController@deleteCgye');
		Route::resource('cgyetransactions','CashGiftAndYearEndTransactionsController');

		// UNIFORM ALLOWANCE
		Route::get('uniformtransactions/getUniform','UniformTransactionsController@getUniform');
		Route::get('uniformtransactions/showUniformDatatable','UniformTransactionsController@showUniformDatatable');
		Route::post('uniformtransactions/processUniform','UniformTransactionsController@processUniform');
		Route::post('uniformtransactions/deleteUniform','UniformTransactionsController@deleteUniform');
		Route::post('uniformtransactions/updateUniform','UniformTransactionsController@updateUniform');
		Route::resource('uniformtransactions','UniformTransactionsController');

		// RICE ALLOWANCE
		Route::get('ricetransactions/getRice','RiceTransactionsController@getRice');
		Route::get('ricetransactions/showRiceDatatable','RiceTransactionsController@showRiceDatatable');
		Route::post('ricetransactions/processRice','RiceTransactionsController@processRice');
		Route::post('ricetransactions/deleteRice','RiceTransactionsController@deleteRice');
		Route::resource('ricetransactions','RiceTransactionsController');

		// CASH GIFT AND YEAR END BONUS
		Route::get('pbbtransactions/getPbbInfo','PerformanceBasicBonusTransactionsController@getPbbInfo');
		Route::get('pbbtransactions/showPbbDatatable','PerformanceBasicBonusTransactionsController@showPbbDatatable');
		Route::post('pbbtransactions/processPbb','PerformanceBasicBonusTransactionsController@processPbb');
		Route::post('pbbtransactions/deletePbb','PerformanceBasicBonusTransactionsController@deletePbb');
		Route::resource('pbbtransactions','PerformanceBasicBonusTransactionsController');

		// ANNIVERSARY BONUS
		Route::get('anniversarytransactions/getAnniversary','AnniversaryTransactionsController@getAnniversary');
		Route::get('anniversarytransactions/showAnniversaryDatatable','AnniversaryTransactionsController@showAnniversaryDatatable');
		Route::post('anniversarytransactions/processAnniversary','AnniversaryTransactionsController@processAnniversary');
		Route::post('anniversarytransactions/deleteAnniversary','AnniversaryTransactionsController@deleteAnniversary');
		Route::resource('anniversarytransactions','AnniversaryTransactionsController');

		// TRAVEL ALLOWANCE
		Route::get('traveltransactions/getTravelAllowance','TravelRateTransactionsController@getTravelAllowance');
		Route::get('traveltransactions/showTravelAllowanceDatatable','TravelRateTransactionsController@showTravelAllowanceDatatable');
		Route::post('traveltransactions/processTravelAllowance','TravelRateTransactionsController@processTravelAllowance');
		Route::post('traveltransactions/deleteTravelAllowance','TravelRateTransactionsController@deleteTravelAllowance');
		Route::resource('traveltransactions','TravelRateTransactionsController');


		// OTHER BENEFITS
		Route::get('otherbenefits/getOtherBenefits','OtherBenefitsController@getOtherBenefits');
		Route::get('otherbenefits/showOtherBenefits','OtherBenefitsController@showOtherBenefits');
		Route::post('otherbenefits/processOtherBenefits','OtherBenefitsController@processOtherBenefits');
		Route::post('otherbenefits/deleteOtherBenefits','OtherBenefitsController@deleteOtherBenefits');
		Route::resource('otherbenefits','OtherBenefitsController');

		// HONORARIAS
		Route::get('honorarias/getHonoraria','HonorariaTransactionsController@getHonoraria');
		Route::get('honorarias/showHonoraria','HonorariaTransactionsController@showHonoraria');
		Route::post('honorarias/processHonoraria','HonorariaTransactionsController@processHonoraria');
		Route::post('honorarias/deleteHonoraria','HonorariaTransactionsController@deleteHonoraria');
		Route::resource('honorarias','HonorariaTransactionsController');

	});


	Route::group(['prefix' => 'payrolls'], function(){

		// EMPLOYEE FILE
		Route::get('admin/employees_payroll_informations/getJgstep','EmployeePayrollInformationsController@getJgstep');
		Route::get('admin/employees_payroll_informations/getItem','EmployeePayrollInformationsController@getItem');
		Route::get('admin/employees_payroll_informations/getSgstep','EmployeePayrollInformationsController@getSgstep');
		Route::get('admin/employees_payroll_informations/getEmployeesinfo','EmployeePayrollInformationsController@getEmployeesinfo');
		Route::get('admin/employees_payroll_informations/getSearchby','EmployeePayrollInformationsController@getSearchby');
		Route::get('admin/employees_payroll_informations/filter','EmployeePayrollInformationsController@filter');
		Route::get('admin/employees_payroll_informations/showBenefitinfo','EmployeePayrollInformationsController@showBenefitinfo');
		Route::get('admin/employees_payroll_informations/showSalaryinfo','EmployeePayrollInformationsController@showSalaryinfo');
		Route::get('admin/employees_payroll_informations/showDeductioninfo','EmployeePayrollInformationsController@showDeductioninfo');
		Route::get('admin/employees_payroll_informations/showLoaninfo','EmployeePayrollInformationsController@showLoaninfo');
		Route::get('admin/employees_payroll_informations/computeEmployeeInfo','EmployeePayrollInformationsController@computeEmployeeInfo');
		Route::post('admin/employees_payroll_informations/storeBenefitinfo','EmployeePayrollInformationsController@storeBenefitinfo');
		Route::post('admin/employees_payroll_informations/deleteBenefitinfo','EmployeePayrollInformationsController@deleteBenefitinfo');
		Route::post('admin/employees_payroll_informations/deleteLoanInfo','EmployeePayrollInformationsController@deleteLoanInfo');
		Route::post('admin/employees_payroll_informations/deleteDeductInfo','EmployeePayrollInformationsController@deleteDeductInfo');
		Route::post('admin/employees_payroll_informations/storeSalaryinfo','EmployeePayrollInformationsController@storeSalaryinfo');
		Route::post('admin/employees_payroll_informations/storeDeductioninfo','EmployeePayrollInformationsController@storeDeductioninfo');
		Route::post('admin/employees_payroll_informations/storeLoaninfo','EmployeePayrollInformationsController@storeLoaninfo');
		Route::post('admin/employees_payroll_informations/storeNonPlantilla','EmployeePayrollInformationsController@storeNonPlantilla');
		Route::resource('admin/employees_payroll_informations','EmployeePayrollInformationsController');

		Route::resource('admin/employeesetup','EmployeeSetupController');

		Route::get('admin/annualtaxsetup/getAnnualTaxPolicy','AnnualTaxSetupController@getAnnualTaxPolicy');
		Route::get('admin/annualtaxsetup/getEmployeesinfo','AnnualTaxSetupController@getEmployeesinfo');
		Route::post('admin/annualtaxsetup/deleteTax','AnnualTaxSetupController@deleteTax');
		Route::resource('admin/annualtaxsetup','AnnualTaxSetupController');

		Route::get('admin/importmodule/getProgress','ImportsController@getProgress');
		Route::get('admin/importmodule/getErrorImport','ImportsController@getErrorImport');
		Route::post('admin/importmodule/fileUpload','ImportsController@fileUpload');
		Route::post('admin/importmodule/storeValidated','ImportsController@storeValidated');
		Route::resource('admin/importmodule','ImportsController');

		Route::post('admin/users/deleteUser','UsersController@deleteUser');
		Route::resource('admin/users','UsersController');


		Route::resource('otherpayrolls','OtherPayrollsController');
		Route::resource('filemanagers','FileManagersController');

		Route::post('transactions/deletePayroll','TransactionsController@deletePayroll');
		Route::post('transactions/processPayroll','TransactionsController@processPayroll');
		Route::post('transactions/updatePayroll','TransactionsController@updatePayroll');
		Route::get('transactions/showLoaninfo','TransactionsController@showLoaninfo');
		Route::get('transactions/showDeductioninfo','TransactionsController@showDeductioninfo');
		Route::get('transactions/showBenefitinfo','TransactionsController@showBenefitinfo');
		Route::get('transactions/getSearchby','TransactionsController@getSearchby');
		Route::get('transactions/searchName','TransactionsController@searchName');
		Route::get('transactions/filter','TransactionsController@filter');
		Route::get('transactions/getEmployeesinfo','TransactionsController@getEmployeesinfo');
		Route::post('transactions/storeNewDeduction','TransactionsController@storeNewDeduction');
		Route::post('transactions/storeBenefitinfo','TransactionsController@storeBenefitinfo');
		Route::post('transactions/deleteLoan','TransactionsController@deleteLoan');
		Route::post('transactions/deleteBenefit','TransactionsController@deleteBenefit');
		Route::post('transactions/deleteDeduction','TransactionsController@deleteDeduction');
		Route::resource('transactions','TransactionsController');

		Route::resource('reports','ReportsController');

		Route::resource('admin/payrollconfigurations','PayrollConfigurationsController');
		Route::get('admin/previousemployer/getPreviousEmployer','PreviousEmployerController@getPreviousEmployer');
		Route::resource('admin/previousemployer','PreviousEmployerController');
		Route::get('admin/beginningbalances/getBeginningBalances','BeginningBalancesController@getBeginningBalances');
		Route::resource('admin/beginningbalances','BeginningBalancesController');

		Route::get('overtimepay/getOvertimeInfo','OvertimePaysController@getOvertimeInfo');
		Route::post('overtimepay/deleteOvertime','OvertimePaysController@deleteOvertime');
		Route::post('overtimepay/storeOvertimeInfo','OvertimePaysController@storeOvertimeInfo');
		Route::resource('overtimepay','OvertimePaysController');
		Route::post('nonplantilla/processPayroll','NonPlantillaController@processPayroll');
		Route::get('nonplantilla/getEmployeesinfo','NonPlantillaController@getEmployeesinfo');
		Route::resource('nonplantilla','NonPlantillaController');

		Route::get('admin/longevitysetup/getLongevityPay','LongevityPayController@getLongevityPay');
		Route::resource('admin/longevitysetup','LongevityPayController');

		Route::get('admin/loanshistory/getLoansHistory','LoansAndDeductionsHistoryController@getLoansHistory');
		Route::resource('admin/loanshistory','LoansAndDeductionsHistoryController');

		Route::resource('admin/access_modules','AccessModuleController');

	});


});


