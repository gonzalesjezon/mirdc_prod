<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Benefit extends Model
{
    protected $primaryKey = 'id';
    protected $table = "pms_benefits";
    protected $fillable = [
    	'code',
    	'name',
    	'de_minimis_type',
    	'computation_type',
    	'amount',
    	'payroll_group',
    	'tax_type',
    	'itr_classification',
    	'include_computation',
        'alphalist_classification',
    	'remarks',

    ];

    public function benefitinfo(){
        return $this->belongsTo('App\BenefitInfo');
    }

}
