<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccessRight extends Model
{
    protected $primaryKey = 'id';

    protected $table = 'pms_access_rights';

    protected $fillable = [
		'access_module_id',
		'module_id',
		'to_view',
    ];

    public function module(){
    	return $this->belongsTo('App\Module','module_id');
    }

    public function access_module(){
    	return $this->belongsTo('App\AccessModule','access_module_id');
    }
}
