<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rata extends Model
{
    protected $table = 'pms_rata';
    protected $fillable = [
    	'employee_id',
        'employee_number',
    	'office_id',
    	'position_item_id',
    	'department_id',
		'number_of_actual_work' ,
		'number_of_leave_field' ,
		'number_of_work_days' ,
		'number_of_used_vehicles' ,
		'percentage_of_rata' ,
		'percentage_of_rata_value',
		'representation_amount',
		'transportation_amount',
		'year',
		'month',
		'created_by',
		'updated_by',
    ];

    public function leaveinfo(){
    	return $this->hasMany('App\Leave','rata_id','id');
    }

    public function offices(){
    	return $this->belongsTo('App\Office','office_id');
    }

    public function positionitems(){
    	return $this->belongsTo('App\PositionItem','position_item_id');
    }

    public function departments(){
    	return $this->belongsTo('App\Department','department_id');
    }
    public function employees(){
    	return $this->belongsTo('App\Employee','employee_id');
    }

    public function leave(){
        return $this->hasMany('App\Leave','rata_id','id');
    }
    public function salaryinfo(){
        return $this->belongsTo('App\SalaryInfo','employee_id')->with('salarygrade');
    }
}
