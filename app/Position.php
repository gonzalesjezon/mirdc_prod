<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{

	protected $primaryKey = 'RefId';
    protected $table = 'position';
    protected $fillable = [
    	'Code',
    	'Name',
        'created_by',
        'updated_by'

    ];

}
