<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $primaryKey = 'id';

    protected $table = 'pms_modules';

    protected $fillable = [
    	'module_name',
    	'description'
    ];
}
