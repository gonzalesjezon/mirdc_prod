<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BenefitInfoTransaction extends Model
{
    protected $table = 'pms_benefitsinfo_transactions';
    protected $fillable = [
        'employee_id',
    	'benefit_id',
        'benefit_info_id',
        'amount',
        'no_of_months_entitled',
        'sala_absent',
        'sala_absent_amount',
        'sala_undertime',
        'sala_undertime_amount',
        'days_present',
        'benefit_rate',
        'used_amount',
        'ta_covered_date',
        'year',
        'month',
        'status',
        'created_by',
        'updated_by'
    ];


    public function benefitinfo(){
        return $this->belongsTo('App\BenefitInfo','benefit_info_id')->with('benefits');
    }

    public function employees(){
        return $this->belongsTo('App\Employee','employee_id','employee_id');
    }

    public function  employeeinformation(){
        return $this->belongsTo('App\EmployeeInformation','employee_id','employee_id');
    }

    public function  salaryinfo(){
        return $this->belongsTo('App\SalaryInfo','employee_id','employee_id');
    }

    public function benefits(){
        return $this->belongsTo('App\Benefit','benefit_id');
    }

    public function transaction(){
        return $this->belongsTo('App\Transaction');
    }
}
