<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreviousEmployer extends Model
{
    protected $table = 'pms_previous_employer';
    protected $fillable = [

		'employee_id',
		'gross_taxable_income',
		'thirteen_month_pay',
		'tax_withheld',
		'mandatory_deduction',
		'as_of_date',
		'created_by'

    ];
}
