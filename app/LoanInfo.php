<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanInfo extends Model
{
    protected $table = 'pms_loansinfo';
    protected $fillable = [
        'employee_id',
        'loan_id',
        'loan_total_amount',
        'loan_total_balance',
        'loan_amount',
        'loan_date_granted',
        'start_date',
        'end_date',
        'date_terminated',
        'terminated'

    ];

    public function loans(){
        return $this->belongsTo('App\Loan','loan_id');
    }

    public function employees(){
        return $this->belongsTo('App\Employee','employee_id');
    }

}
