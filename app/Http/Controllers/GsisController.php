<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Input;
use App\GsisPolicy;

class GsisController extends Controller
{

    function __construct(){
        $this->controller = $this;
        $this->title = 'GSIS Policy';
        $this->module_prefix = 'payrolls/admin/filemanagers';
        $this->module = 'gsis';
        $this->table = 'gsis_policies';
    }

    public function index(){
        $response = array(
            'controller'    => $this->controller,
            'title'         => $this->title,
            'module'        => $this->module,
            'module_prefix' => $this->module_prefix,
        );

        return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function store(Request $request){



        $gsis = new GsisPolicy;

        // $gsis->ee_percentage = 0.09;
        // $gsis->er_percentage = 0.12;

        if(isset($request->gsis_id)){

            $gsis = GsisPolicy::find((int)$request->gsis_id);

            $gsis->fill($request->all())->save();

            $response =  json_encode(['status'=>true,'response' => 'Update Successfully!']);

        }else{

            $this->validate(request(),[
                'policy_name'           => 'required',
                // 'pay_period'            => 'required',
                'deduction_period'      => 'required',
                'policy_type'           => 'required',
                'based_on'              => 'required',
                // 'computation'           => 'required',
            ]);

            $gsis->fill($request->all())->save();

            $response =  json_encode(['status'=>true,'response' => 'Save Successfully!']);

        }

        return $response;

    }

    public function show(){

        $q = Input::get('q');
        $limit = Input::get('limit');
        if(empty($limit)){ $limit  =  10; }

        $data = $this->get_records($limit,$q);

        $response = array(
                        'data'          => $data,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix,
                        'cols'          => $this->table_columns($this->table)

                        );
        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);

    }


    private function get_records($limit,$q){

        $cols = ['policy_name','pay_period','deduction_period','policy_type'];


        $query = GsisPolicy::where(function($query) use($cols,$q){

                $query = $query->where(function($qry) use($q, $cols){
                    foreach ($cols as $key => $value) {
                        $qry->orWhere($value,'like','%'.$q.'%');
                    }
                });
        });
        $response = $query->paginate($limit);

        return $response;

    }

    public function getItem(){
        $id = Input::get('id');

        $query = GsisPolicy::where('id',$id)->first();

        return json_encode($query);
    }

    public function delete(){
        $data = Input::all();
        $id   = $data['id'];

        $benefit = new GsisPolicy;
        $benefit->destroy($id);

        return json_encode(['status'=>true]);
    }
}
