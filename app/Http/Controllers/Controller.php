<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use DB;
use Auth;
use App\TaxTable;
use App\AccessRight;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function table_columns($table){

        $res = array();
        $data = DB::select('SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = "default" AND TABLE_NAME="'.$table.'"');
        foreach ($data as $key => $value) {
        	$notInArray = array('id','created_at','updated_at');
        	// echo
            if(!in_array($value->column_name,$notInArray)){
                array_push($res,$value->column_name);
            }
        }
        return $res;

    }

    public function countDays($year, $month, $ignore) {
        $count = 0;
        $counter = mktime(0, 0, 0, $month, 1, $year);
        while (date("n", $counter) == $month) {
            if (in_array(date("w", $counter), $ignore) == false) {
                $count++;
            }
            $counter = strtotime("+1 day", $counter);
        }
        return $count-1;
    }

    public function getWorkingDays($startDate,$endDate){
        $start = strtotime($startDate);
        $end = strtotime($endDate);

        if($start > $end){
            $working_days = 0;
        }else{
            $no_days = 0;
            $weekends = 0;
            while ($start <= $end) {
                $no_days++; // no of days in given interval
                $what_day = date('N',$start);
                if($what_day>6){ // 6 and 7 are weekend
                    $weekends++;
                }
                $start+=86400; // +1 day
            }
        }
        $working_days = $no_days - $weekends;

        return $working_days;

    }

    public function computeTax($gross_salary){

        $taxtable = new TaxTable;

        $prescribeTax        = 0;
        $prescribePercentage = 0;
        $clRange             = 0;
        $taxDue              = 0;
        $grossTaxable        = 0;
        $taxPercentage       = 0;

        $taxtable = $taxtable
        ->get()->toArray();

        $data['monthlyCL'] =  [
            0,
            $taxtable[1]['salary_bracket_level2'],
            $taxtable[1]['salary_bracket_level3'],
            $taxtable[1]['salary_bracket_level4'],
            $taxtable[1]['salary_bracket_level5'],
            $taxtable[1]['salary_bracket_level6']
        ];

        $data['percent'] = [0,.20,.25,.30,.32,.35];
        $data['below']  = [0,20833.33,33333.33,66666.66,166666.66,666666.66];
        $data['above'] = [20832,33332,66666,166666,666666,9999999] ;

        foreach ($data['monthlyCL'] as $key => $value) {

            if($gross_salary > $data['below'][$key] && $gross_salary < $data['above'][$key]){
                $prescribeTax = $value;
                $prescribePercentage = $data['percent'][$key];
                $clRange = $data['below'][$key];
            }
        }

        if($gross_salary < 20833){
            $clRange = 0;
            $prescribePercentage = 0;
        }

        $grossTaxable = $gross_salary - $clRange;
        $taxPercentage = $grossTaxable * $prescribePercentage;
        $taxDue = $taxPercentage + $prescribeTax;

        return $taxDue;
    }

    public function computePhic($basic_amount){

        $basicAmount = $basic_amount;
        $ee_share = 0;
        $er_share = 0;
        $return = [];

        if($basicAmount >= 40000){
            $ee_share = 550;
            $er_share = 550;
        }else{

            $salary = $basicAmount * 0.0275;
            $salary = round($salary,2); // round to two decimal
            $empShare  = $salary / 2;
            $er_share  = round($empShare,2); // round to two decimal
            $ee_share = $salary - $er_share;
        }

        $return['ee_share'] = $ee_share;
        $return['er_share'] = $er_share;

        return $return;
    }

    public function computeGsis($basic_amount){

        $return = [];
        $ee_share = $basic_amount * .09;
        $er_share = $basic_amount * .12;

        $return['ee_share'] = $ee_share;
        $return['er_share'] = $er_share;

        return $return;
    }

    public function latestYear(){
        return date('Y');
    }

    public function earliestYear(){
        return 1950;
    }

    public function access_rights($access,$access_type_id){

        $res =  AccessRight::with('access_module','module')
                                ->whereHas('module',function($q) use($access){
                                    $q->where('module_name','like',$access);
                                })
                                ->where('access_module_id',$access_type_id)
                                ->first();

        if($res){
            return true;
        }else{
            return false;
        }
    }

    public function check($module){
        if($this->access_rights($module,Auth::User()->access_type_id) == false && Auth::User()->id != 1){
            return redirect('404')->send();
        }
    }
}
