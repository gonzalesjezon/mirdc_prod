<?php

namespace App\Http\Controllers;

use App\Module;
use App\AccessModule;
use App\AccessRight;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

use Auth;

class AccessModuleController extends Controller
{

    function __construct(){
        $this->title            = 'Access Module';
        $this->module           = 'access_modules';
        $this->module_prefix    = 'payrolls/admin';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $this->check($this->module);

        $perPage = 100;
        $access_modules = AccessModule::latest()
        ->paginate($perPage);

        return view($this->module_prefix.'.access_modules.index',[
            'access_modules'    => $access_modules,
            'module'            => $this->module,
            'module_prefix'     => $this->module_prefix,
            'title'             => $this->title,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $this->check($this->module);

        $modules = Module::orderBy('description','asc')->getModels();

        return view($this->module_prefix.'.access_modules.create',[
            'module'            => $this->module,
            'title'             => $this->title,
            'modules'           => $modules,
            'module_prefix'     => $this->module_prefix
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'access_name' => 'required'
        ]);

        $access_module = AccessModule::find($request->id);
        $message = 'Update Successfully.';
        if(empty($access_module)){
            $access_module = new AccessModule;
            $message = 'Save Successfully.';
        }

        $access_module->fill($request->all());
        if($message == 'Update Successfully.'){
            $access_module->updated_by = Auth::id();
        }else{
            $access_module->created_by = Auth::id();
        }

        if($access_module->save()){
            $access_right = $this->storeAccessRight($request,$access_module->id);
        }

        return json_encode(['status' => true, 'response' => $message]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->check($this->module);

        $access_module = AccessModule::find($id);
        $access_right = [];
        if(count($access_module->access_rights) > 0){
            foreach ($access_module->access_rights as $key => $value) {
                $access_right[$value->module_id] = $value->id;
            }
        }

        return view($this->module_prefix.'.access_modules.edit',[
            'title'             => $this->title,
            'access_module'     => $access_module,
            'access_right'      => $access_right,
            'modules'           => Module::orderBy('description','asc')->getModels(),
            'module_prefix'     => $this->module_prefix,
            'module'            => $this->module,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        AccessModule::destroy($id);
        AccessRight::where('access_module_id',$id)->delete();
        return redirect($this->module_prefix.'/access_modules')->with('success', 'Access module was successfully deleted!');
    }

    public static function storeAccessRight($request,$access_module_id){

        foreach ($request->access_right as $key => $value) {


            $access_right = AccessRight::find(@$request->access_id[$key]);

            if(empty($access_right)){
                $access_right = new AccessRight;
            }

            $access_right->access_module_id = $access_module_id;
            $access_right->module_id = $key;
            $access_right->to_view = (isset($value)) ? 1 :0;

            if($access_right->exists()){
                $access_right->updated_by = Auth::id();
            }else{
                $access_right->created_by = Auth::id();
            }
            $access_right->save();
        }
    }
}
