<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Auth;
use App\PostedReport;
use App\SpecialPayrollTransaction;
class SpecialPayrollReportsController extends Controller
{
    function __construct(){
		$this->title = 'OTHER PAYROLL REPORT';
    	$this->module = 'specialpayrollreports';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;
	}

	public function index(){

        $rType = ['midyearbonus','yearendbonus','pbb','pei','ab','otherbenefits','cashgift','ua','ra'];

        $posted = PostedReport::whereIn('report_type',$rType)->latest()->get();

    	$response = array(
                        'posted'            => $posted,
    					'module'            => $this->module,
    					'controller'        => $this->controller,
                        'module_prefix'     => $this->module_prefix,
    					'title'		        => $this->title,
                        'months'            => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m'),
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }


    public function show(){

        $data = Input::all();

        $year   	   = @$data['year'];
        $month  	   = @$data['month'];
        $payroll_type  = @$data['payroll_type'];

        if($payroll_type == 'cashgift'){
            $payroll_type = 'yearendbonus';
        }

        $transaction =  new SpecialPayrollTransaction;

        $query = $transaction
        ->with('divisions','employees')
        ->where('year',$year)
        ->where('month',$month)
        ->where('status',$payroll_type)
        ->get();

        $payroll = [];
        foreach ($query as $key => $value) {
        	$payroll[@$value->divisions->Code][$key] = $value;
        }

        return json_encode($payroll);

    }

     /*
    * Post Special Payroll
    */
    public function store(Request $request){

        $this->validate($request,[
            'payroll_type' => 'required'
        ]);


        $ifPosted = PostedReport::where('year',$request->year)
        ->where('month',$request->month)
        // ->where('pay_period',$request->pay_period)
        ->where('report_type',$request->payroll_type)
        ->first();

        if(isset($ifPosted)){
            $response = json_encode(['status'=>false,'response'=> 'Already posted.']);
        }else{

            $hasTransaction = SpecialPayrollTransaction::where('year',$request->year)
            ->where('status',$request->payroll_type)
            ->where('month',$request->month)->get();

            if(count($hasTransaction) > 0){

                // $signatory = new Signatory;
                // $signatory->fill($request->all());
                // $signatory->created_by = Auth::id();

                // if($signatory->save()){
                    $posted = new PostedReport;
                    $posted->fill($request->all());
                    // $posted->signatory_id   = $signatory->id;
                    $posted->report_type    = $request->payroll_type;
                    $posted->created_by     = Auth::id();

                    if($posted->save()){
                        $transaction = SpecialPayrollTransaction::where('year',$request->year)
                        ->where('month',$request->month)
                        ->where('status',$request->payroll_type)
                        ->update(['posted' => 1]);
                    }
                // }

                $response = json_encode(['status'=>true,'response'=> 'Report posted successfully.']);
            }else{
                $response = json_encode(['status'=>false,'response'=> 'No transaction Found.']);
            }
        }

        return $response;
    }
}
