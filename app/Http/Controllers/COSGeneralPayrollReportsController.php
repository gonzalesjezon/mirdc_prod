<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Auth;
use App\PostedReport;
use App\EmployeeStatus;
use App\EmployeeInformation;
use App\NonPlantillaTransaction;
use App\ResponsibilityCenter;
class COSGeneralPayrollReportsController extends Controller
{
    function __construct(){
		$this->title = 'CONTRACTUAL GENERAL PAYROLL REPORT';
    	$this->module = 'cosgeneralpayrollreports';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;
	}

	public function index(){

        $posted = PostedReport::where('report_type','cos_gp')->latest()->get();
    	$response = array(
    					'module'            => $this->module,
    					'controller'        => $this->controller,
                        'module_prefix'     => $this->module_prefix,
    					'title'		        => $this->title,
                        'rcenter'           => ResponsibilityCenter::orderBy('name','asc')->get(),
                        'months'            => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m'),
                       'posted'             => $posted

    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }


    public function show(){

        $data = Input::all();

        $year   	   = @$data['year'];
        $month  	   = @$data['month'];
        $sub_period    = @$data['sub_period'];
        $rcenter       = @$data['rcenter'];

        $transaction =  new NonPlantillaTransaction;
        $status 	 = 	new EmployeeStatus;

        $query = $transaction
        ->with([
            'nonplantilla',
            'employees',
            'rcenter',
            'project' => function($qry){
                $qry->orderBy('name','asc');
            }
        ])
        ->where('year',$year)
        ->where('month',$month)
        ->where('responsibility_id',$rcenter)
        ->where('sub_pay_period',$sub_period)
        ->get();

        $payroll = [];
        foreach ($query as $key => $value) {
        	$payroll[@$value->rcenter->name][@$value->project->name][$key] = $value;
        }

        return json_encode($payroll);

    }

     /*
    * Post COS General Payroll
    */
    public function store(Request $request){

        $this->validate($request,[
            'pay_period'        => 'required',
            'responsibility_id' => 'required'
        ]);

        $ifPosted = PostedReport::where('year',$request->year)
        ->where('month',$request->month)
        ->where('pay_period',$request->pay_period)
        ->where('report_type','cos_gp')
        ->first();

        if(isset($ifPosted)){
            $response = json_encode(['status'=>false,'response'=> 'Already posted.']);
        }else{

            $hasTransaction = NonPlantillaTransaction::where('year',$request->year)
            ->where('pay_period',$request->pay_period)
            ->where('month',$request->month)->get();

            if(count($hasTransaction) > 0){

                // $signatory = new Signatory;
                // $signatory->fill($request->all());
                // $signatory->created_by = Auth::id();

                // if($signatory->save()){
                    $posted = new PostedReport;
                    $posted->fill($request->all());
                    // $posted->signatory_id   = $signatory->id;
                    $posted->report_type    = 'cos_gp';
                    $posted->created_by     = Auth::id();

                    if($posted->save()){
                        $transaction = NonPlantillaTransaction::where('year',$request->year)
                        ->where('month',$request->month)
                        ->update(['posted' => 1]);
                    }
                // }

                $response = json_encode(['status'=>true,'response'=> 'Report posted successfully.']);
            }else{
                $response = json_encode(['status'=>false,'response'=> 'No transaction Found.']);
            }
        }

        return $response;
    }
}
