<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\NonPlantillaTransaction;
use App\NonPlantillaEmployeeInfo;
use App\EmployeeStatus;
use App\EmployeeInformation;
use App\Employee;
use App\AttendanceInfo;
class JobOrderReportsController extends Controller
{
    function __construct(){
		$this->title = 'JOB ORDER';
    	$this->module = 'joborder';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;
	}

	public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m')
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }


    public function show(){

        $data = Input::all();

        $year = $data['year'];
        $month = $data['month'];

        $transaction         = new NonPlantillaTransaction;
        $employeeinfo        = new NonPlantillaEmployeeInfo;
        $empstatus           = new EmployeeStatus;
        $employeeinformation = new EmployeeInformation;

        $empstatus = $empstatus
        ->where('Code','JO')
        ->select('RefId')
        ->first();

        $employee_id = $employeeinformation
        ->where('employee_status_id',$empstatus->RefId)
        ->select('employee_id')
        ->get()
        ->toArray();

        $query['transaction'] = $transaction
        ->with([
                'nonplantilla',
                'divisions',
                'overtime'=>function($qry) use($year,$month){
                    $qry->where('year',$year)
                        ->where('month',$month);
                },
                'employees',
                'positionitems',
                'positions',
                'offices'
        ])
        ->whereIn('employee_id',$employee_id)
		->where('year',$year)
		->where('month',$month)
		->get();

        return json_encode($query);
    }
}
