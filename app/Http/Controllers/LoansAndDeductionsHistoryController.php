<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\EmployeeStatus;
use App\EmployeeInformation;
use App\LoanInfo;
use App\DeductionInfoTransaction;
use Input;
class LoansAndDeductionsHistoryController extends Controller
{
    function __construct(){
    	$this->title = 'LOANS AND DEDUCTION HISTORY';
    	$this->module = 'loanshistory';
        $this->module_prefix = 'payrolls/admin';
    	$this->controller = $this;

    }

    public function index(){

    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q = Input::get('q');
        $empstatus = Input::get('empstatus');
        $category = Input::get('category');
        $emp_type = Input::get('emp_type');
        $searchby = Input::get('searchby');

        $data = "";

        $data = $this->searchName($q);

        if(isset($empstatus) || isset($category)){
            $data = $this->filter($empstatus,$category,$emp_type,$searchby);

        }

        $response = array(
                        'data'          => $data,
                        'title'         => $this->title,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix
                    );

        return view($this->module_prefix.'.'.$this->module.'.namelist',$response);

    }

    public function searchName($q){
        $cols = ['lastname','firstname','id'];

        $employee 		 	 = new Employee;
        $employeeinformation = new EmployeeInformation;
        $employee_status 	 = new EmployeeStatus;

        $status_id = $employee_status
		        ->where('category',1)
		        ->select('id')
		        ->get()->toArray();

        $employeeinfo_id =  $employeeinformation
				        ->whereIn('employee_status_id',$status_id)
				        ->select('employee_id')
				        ->get()
				        ->toArray();

        $query = $employee
		        ->whereIn('id',$employeeinfo_id)
		        ->where(function($query) use($cols,$q){
			        $query = $query->where(function($qry) use($q, $cols){
			            foreach ($cols as $key => $value) {
			                $qry->orWhere($value,'like','%'.$q.'%');
			            }
			        });
			    });

        $response = $query->where('active',1)->orderBy('lastname','asc')->get();

        return $response;

    }

    public function filter($empstatus,$category,$emp_type,$searchby){


    	$employeestatus 	 = new EmployeeStatus;
    	$employeeinformation = new EmployeeInformation;
    	$employee 			 = new Employee;


        $empstatus_id = [];
        switch($empstatus){
            case 'plantilla':
                $empstatus_id = $employeestatus->where('category',1)->select('id')->get()->toArray();
            break;

            case 'nonplantilla':
                $empstatus_id = $employeestatus->where('category',0)->select('id')->get()->toArray();
            break;
        }

        $employeesRefId = [];
        $employeesRefId = $employeeinformation->select('employee_id')
                            ->where(function($qry) use($category,$searchby){
                                switch ($searchby) {
                                    case 'company':
                                        $qry =  $qry->where('company_id',$category);
                                        break;
                                    case 'position':
                                        $qry =  $qry->where('position_id',$category);
                                        break;
                                    case 'division':
                                        $qry =  $qry->where('division_id',$category);
                                        break;
                                    case 'office':
                                        $qry =  $qry->where('office_id',$category);
                                        break;
                                    case 'department':
                                        $qry =  $qry->where('department_id',$category);
                                        break;
                                }

                            });
                if(count($empstatus_id) > 0){
                    $employeesRefId = $employeesRefId->whereIn('employee_status_id',$empstatus_id);
                }
                $employeesRefId = $employeesRefId->get()->toArray();

        $query = $employee->whereIn('id',$employeesRefId)->where('active',1)->orderBy('lastname','asc')->get();

        return $query;


    }

    public function showLoansHistory(){



        $response       = array(
                            'controller'    => $this->controller,
                            'module'        => $this->module,
                            'module_prefix' => $this->module_prefix,
                        );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);
    }


    public function getLoansHistory(){
        $data = Input::all();

        $employee_id = $data['employee_id'];

        $loanshistory =  new LoanInfo;

        $query = $loanshistory
        ->with('loans')
        ->where('employee_id',$employee_id)
        ->get();


        return json_encode(['loanshistory'=>$query]);
    }
}
