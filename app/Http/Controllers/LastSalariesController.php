<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Employee;
use App\EmployeeStatus;
use App\EmployeeInformation;
use App\OtherPayroll;
use Auth;
class LastSalariesController extends Controller
{
    function __construct(){
    	$this->title = 'LAST SALARY';
    	$this->module = 'lastsalaries';
        $this->module_prefix = 'payrolls/otherpayrolls';
    	$this->controller = $this;

    }

    public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m')
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q = Input::get('q');
        $empstatus = Input::get('empstatus');
        $category = Input::get('category');
        $emp_type = Input::get('emp_type');
        $searchby = Input::get('searchby');

        $data = "";

        $data = $this->searchName($q);

        if(isset($empstatus) || isset($category)){
            $data = $this->filter($empstatus,$category,$emp_type,$searchby);

        }

        $response = array(
                        'data'          => $data,
                        'title'         => $this->title,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix
                    );

        return view($this->module_prefix.'.'.$this->module.'.namelist',$response);

    }

    public function searchName($q){
        $cols = ['lastname','firstname','id'];

        $query = Employee::where(function($query) use($cols,$q){

                        $query = $query->where(function($qry) use($q, $cols){
                            foreach ($cols as $key => $value) {
                                $qry->orWhere($value,'like','%'.$q.'%');
                            }
                        });
                    });


        $response = $query->where('active',1)->orderBy('lastname','asc')->get();

        return $response;

    }

    public function filter($empstatus,$category,$emp_type,$searchby){

        $plantilla = ['Permanent','Temporary','Coterminous','Termer','Probationary','Casual','Contractual'];

        $nonplantilla = ['Job Order','Contract of Service'];

        $empstatus_id = [];
        switch($empstatus){
            case 'plantilla':
                $empstatus_id = EmployeeStatus::whereIn('name',$plantilla)->select('id')->get()->toArray();
            break;

            case 'nonplantilla':
                $empstatus_id = EmployeeStatus::whereIn('name',$nonplantilla)->select('id')->get()->toArray();
            break;
        }

        $employeesRefId = [];
        $employeesRefId = EmployeeInformation::select('employee_id')
                            ->where(function($qry) use($category,$searchby){
                                switch ($searchby) {
                                    case 'company':
                                        $qry =  $qry->where('company_id',$category);
                                        break;
                                    case 'position':
                                        $qry =  $qry->where('position_id',$category);
                                        break;
                                    case 'division':
                                        $qry =  $qry->where('division_id',$category);
                                        break;
                                    case 'office':
                                        $qry =  $qry->where('office_id',$category);
                                        break;
                                    case 'department':
                                        $qry =  $qry->where('department_id',$category);
                                        break;
                                }

                            });
                if(count($empstatus_id) > 0){
                    $employeesRefId = $employeesRefId->whereIn('employee_status_id',$empstatus_id);
                }
                $employeesRefId = $employeesRefId->get()->toArray();

        $query = Employee::whereIn('id',$employeesRefId)->where('active',1)->orderBy('lastname','asc')->get();

        return $query;


    }

    public function store(Request $request){

    	$otherpayroll = new OtherPayroll;

    	 $basic_salary_amount   = ($request->basic_salary_amount) ? str_replace(',', '', $request->basic_salary_amount) : 0;
    	 $pera_amount     		= ($request->pera_amount) ? str_replace(',', '', $request->pera_amount) : 0;
    	 $gross_salary_amount   = ($request->gross_salary_amount) ? str_replace(',', '', $request->gross_salary_amount) : 0;
    	 $gsis_premium_amount   = ($request->gsis_premium_amount) ? str_replace(',', '', $request->gsis_premium_amount) : 0;
    	 $philhealth_amount     = ($request->philhealth_amount) ? str_replace(',', '', $request->philhealth_amount) : 0;
    	 $hdmf_amount     		= ($request->hdmf_amount) ? str_replace(',', '', $request->hdmf_amount) : 0;
    	 $hdmf_mpl_amount     	= ($request->hdmf_mpl_amount) ? str_replace(',', '', $request->hdmf_mpl_amount) : 0;
    	 $withholding_amount    = ($request->witholding_tax_amount) ? str_replace(',', '', $request->witholding_tax_amount) : 0;
    	 $total_deductions_amount   = ($request->total_deductions_amount) ? str_replace(',', '', $request->total_deductions_amount) : 0;
    	 $net_amount     			= ($request->net_amount) ? str_replace(',', '', $request->net_amount) : 0;
    	 $gross_taxable_amount     			= ($request->gross_taxable_amount) ? str_replace(',', '', $request->gross_taxable_amount) : 0;
// dd($request->all());
    	if(isset($request->otherpayroll_id)){

    		$otherpayroll = $otherpayroll->find($request->otherpayroll_id);

    		$otherpayroll->employee_id 				= $request->employee_id;
            $otherpayroll->employee_number          = $request->employee_number;
			$otherpayroll->basic_salary_amount 		= $basic_salary_amount;
			$otherpayroll->pera_amount 				= $pera_amount;
			$otherpayroll->gross_salary_amount 		= $gross_salary_amount;
			$otherpayroll->gsis_premium_amount 		= $gsis_premium_amount;
			$otherpayroll->philhealth_amount 		= $philhealth_amount;
			$otherpayroll->hdmf_amount 				= $hdmf_amount;
			$otherpayroll->hdmf_mpl_amount 			= $hdmf_mpl_amount;
            $otherpayroll->process_date             = $request->process_date;
			$otherpayroll->withholding_amount 		= $withholding_amount;
			$otherpayroll->total_deductions_amount 	= $total_deductions_amount;
			$otherpayroll->net_amount 				= $net_amount;
			$otherpayroll->lwop 					= $request->lwop;
			$otherpayroll->gross_taxable_amount 	= $gross_taxable_amount;
            $otherpayroll->year                     = $request->year;
            $otherpayroll->month                    = $request->month;
            $otherpayroll->status                   = 'lastsalary';
			$otherpayroll->updated_by 				= Auth::User()->id;

			$otherpayroll->save();

			$response = json_encode(['status'=>true, 'response'=>'Update Successfully!']);

    	}else{

    		$otherpayroll->employee_id 				= $request->employee_id;
			$otherpayroll->basic_salary_amount 		= $basic_salary_amount;
            $otherpayroll->employee_number          = $request->employee_number;
			$otherpayroll->pera_amount 				= $pera_amount;
			$otherpayroll->gross_salary_amount 		= $gross_salary_amount;
			$otherpayroll->gsis_premium_amount 		= $gsis_premium_amount;
			$otherpayroll->philhealth_amount 		= $philhealth_amount;
			$otherpayroll->hdmf_amount 				= $hdmf_amount;
			$otherpayroll->hdmf_mpl_amount 			= $hdmf_mpl_amount;
			$otherpayroll->withholding_amount 		= $withholding_amount;
			$otherpayroll->total_deductions_amount 	= $total_deductions_amount;
            $otherpayroll->process_date             = $request->process_date;
            $otherpayroll->lwop 					= $request->lwop;
            $otherpayroll->year                     = $request->year;
            $otherpayroll->month                    = $request->month;
            $otherpayroll->gross_taxable_amount 	= $gross_taxable_amount;
            $otherpayroll->status                   = 'lastsalary';
			$otherpayroll->net_amount 				= $net_amount;
			$otherpayroll->updated_by 				= Auth::User()->id;

			$otherpayroll->save();

			$response = json_encode(['status'=>true, 'response'=>'Save Successfully!']);
    	}

    	return $response;

    }

    public function showLastSalaries(){

        $otherpayrolls    = OtherPayroll::with('employees')->get();

        $response       = array(
                            'otherpayrolls'   => $otherpayrolls,
                            'controller'    => $this->controller,
                            'module'        => $this->module,
                            'module_prefix' => $this->module_prefix,
                        );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);
    }

    public function getLastSalary(){
    	$data = Input::all();
    	$otherpayroll = new OtherPayroll;

    	$query = $otherpayroll->where('employee_id',$data['id'])->where('status','lastsalary')->with('employees')->get();

    	return json_encode($query);
    }
}
