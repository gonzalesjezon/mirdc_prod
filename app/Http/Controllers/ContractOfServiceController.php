<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\NonPlantillaTransaction;
use App\NonPlantillaEmployeeInfo;
use App\EmployeeStatus;
use App\EmployeeInformation;
use App\Employee;
use App\AttendanceInfo;
class ContractOfServiceController extends Controller
{
   function __construct(){
		$this->title = 'CONTRACT OF SERVICE';
    	$this->module = 'contractofservice';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;
	}

	public function index(){

    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m')
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }


    public function show(){

        $data = Input::all();

        $year = $data['year'];
        $month = $data['month'];

        $transaction         = new NonPlantillaTransaction;
        $employeeinfo        = new NonPlantillaEmployeeInfo;
        $employeeinformation = new EmployeeInformation;
        $employeestatus      = new EmployeeStatus;

        $status = $employeestatus
        ->where('Code','COS')
        ->select('RefId')
        ->first();

        $employee_id = $employeeinformation
        ->where('employee_status_id',$status->RefId)
        ->select('employee_id')
        ->get()->toArray();

        $query = $transaction
        ->with([
                'nonplantilla',
                'departments' =>function($qry){
                	$qry->orderBy('name','asc');
                },
                'divisions',
                'overtime'=>function($qry) use($year,$month){
                    $qry->where('year',$year)
                        ->where('month',$month);
                },
                'employees',
                'positionitems',
                'positions',
                'offices'
            ])
        ->whereIn('employee_id',$employee_id)
        ->whereNotNull('actual_basicpay_amount')
		->where('year',$year)
		->where('month',$month)
		->get();

        if(count($query) > 0){
            $data = [];
            foreach ($query as $key => $value) {

                if($value->department_id !== null){
                    $data[$value->departments->name][$key] = $value;

                }

            }
        }else{
            $data = [];
        }

        return json_encode($data);
    }
}
