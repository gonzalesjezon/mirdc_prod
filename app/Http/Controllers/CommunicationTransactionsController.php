<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Auth;
use App\Employee;
use App\EmployeeStatus;
use App\Benefit;
use App\BenefitInfo;
use App\SpecialPayrollTransaction;
use App\EmployeeInformation;
use App\PositionItem;
class CommunicationTransactionsController extends Controller
{
    function __construct(){
    	$this->title = 'COMMUNICATION TRANSACTIONS';
    	$this->controller = $this;
    	$this->module = 'communicationtransactions';
        $this->module_prefix = 'payrolls/specialpayrolls';
    }

    public function index(){


    	$response = array(
           'title' 	        	=> $this->title,
           'controller'         => $this->controller,
           'module'	        	=> $this->module,
           'module_prefix'      => $this->module_prefix,

       );

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q 			= Input::get('q');
        $year 		= Input::get('year');
        $month 		= Input::get('month');
        $check_cea 	= Input::get('check_cea');
        $checkcea 	= Input::get('checkcea');
        $data = "";

        $data = $this->searchName($q,$check_cea);

        if(isset($year) || isset($month) || isset($checkcea)){
            $data = $this->filter($year,$month,$checkcea);
        }


        $response = array(
            'data'          => $data,
            'title'         => $this->title,
            'controller'    => $this->controller,
            'module'        => $this->module,
            'module_prefix' => $this->module_prefix
        );

        return view($this->module_prefix.'.'.$this->module.'.namelist',$response);

    }

    public function searchName($q,$checkcea){

        $employee 	 	  = new Employee;
        $benefit  	 	  = new Benefit;
        $benefitinfo 	  = new BenefitInfo;
        $transaction 	  = new SpecialPayrollTransaction;

        $cols = ['lastname','firstname','id'];

        $arrBenefit  = $benefit->where('code',['CEA'])->select('id')->get()->toArray();
        $arrEmployee = $benefitinfo->whereIn('benefit_id',$arrBenefit)->select('employee_id')->get()->toArray();
        $employee_id = $transaction->whereIn('employee_id',$arrEmployee)->select('employee_id')->get()->toArray();

        $query = [];
        switch ($checkcea) {
            case 'wcea':
                $query = $employee->whereIn('id',$employee_id);
                break;

            default:
                $query = $employee->whereNotIn('id',$employee_id)->whereIn('id',$arrEmployee);
                break;
        }

      	$query = $query->where(function($query) use($cols,$q){
            $query = $query->where(function($qry) use($q, $cols){
                foreach ($cols as $key => $value) {
                    $qry->orWhere($value,'like','%'.$q.'%');
                }
            });

        });

        $response = $query->orderBy('lastname','asc')->get();

        return $response;
    }

    public function filter($year,$month,$checkcea){


        $employee           = new Employee;
        $benefit  	 	  	= new Benefit;
        $benefitinfo 	  	= new BenefitInfo;
        $transaction        = new SpecialPayrollTransaction;

        $arrBenefit 	 = $benefit->whereIn('code',['CEA'])->select('id')->get()->toArray();
        $arrEmployee 	 = $benefitinfo->whereIn('benefit_id',$arrBenefit)->select('employee_id')->get()->toArray();

        $query = [];
        $response = "";

        switch ($checkcea) {
            case 'wcea':

                $query =  $transaction->select('employee_id');

                    if(isset($year)){
                        $query = $query->where('year',$year);

                    }
                    if(isset($month)){
                        $query = $query->where('month',$month);
                    }

                    $query = $query->get()->toArray();

                    $response = $employee->whereIn('id',$query)
                                        ->orderBy('lastname','asc')->get();


                break;

            case 'wocea':

                 $query =  $transaction->select('employee_id');


                    if(isset($year)){
                        $query = $query->where('year',$year);

                    }
                    if(isset($month)){
                        $query = $query->where('month',$month);
                    }

                    $query = $query->get()->toArray();

                    $response = $employee->whereIn('id',$arrEmployee)
                                        ->whereNotIn('id',$query)
                                        ->orderBy('lastname','asc')->get();

                break;
        }
        return $response;
    }


    public function processCea(Request $request){
    	$data = Input::all();

    	$benefitinfo 		 = new BenefitInfo;
    	$employeeinformation = new EmployeeInformation;
        $positionitem 		 = new PositionItem;
        $benefit 			 = new Benefit;

        $benefit_id =  $benefit->whereIn('code',['CEA'])->select('id')->get()->toArray();

    	foreach ($data['list_id'] as $key => $value) {

	    	if(isset($value)){
                $data_cea   = new SpecialPayrollTransaction;
                $data_cea = $data_cea->where('employee_id',$value)->where('year',$data['year'])->where('month',$data['month'])->first();

                if(!isset($data_cea)){
            		$benefit_info 	= $benefitinfo->where('employee_id',$value)
        											    		->whereIn('benefit_id',$benefit_id)
        											    		->select('id')
        											    		->orderBy('benefit_id','asc')
        											    		->get();

            		$employeeinfo 	= $employeeinformation->where('employee_id',$value)->select('office_id','position_item_id')->get();

                    foreach ($benefit_info as $key => $val) {

                        $cea = new SpecialPayrollTransaction;

                        $cea->employee_id       = $value;
                        $cea->office_id         = @$employeeinfo[0]->office_id;
                        $cea->position_item_id  = @$employeeinfo[1]->position_item_id;
                        $cea->benefit_info_id   = $val->id;
                        $cea->year              = $data['year'];
                        $cea->month             = $data['month'];
                        $cea->created_by        = Auth::User()->id;
                        $cea->save();

                    }

                }

	    	}
    	}

    	$response = json_encode(['status'=>true,'response'=>'Process Successfully!']);

    	return $response;
    }

    public function showCeaDatatable(){

    	$response = array(
            'title'             => $this->title,
            'controller'        => $this->controller,
            'module'            => $this->module,
            'module_prefix'     => $this->module_prefix,

        );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);
    }

    public function getCeaInfo(){
    	$data = Input::all();

    	$cea =  new SpecialPayrollTransaction;

    	$query = $cea->with(['positionitems'=>function($qry){
		    		$qry->with('positions');
			    	},'offices','benefitinfo'=>function($qry){
			    		$qry->with('benefits');
			    	}])->where('year',$data['year'])
			    	->where('month',$data['month'])
			    	->where('employee_id',@$data['employee_id'])->get();

    	return json_encode($query);
    }

    public function deleteCea(){
        $data = Input::all();

        $rata = new SpecialPayrollTransaction;

        foreach ($data['empid'] as $key => $value) {

            $rata->where('employee_id',$data['empid'][$key])
            ->where('month',$data['month'])
            ->where('year',$data['year'])
            ->delete();

        }

        return json_encode(['status'=>true,'response'=>'Delete Successfully']);
    }
}
