<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Loans;
use App\Imports\LoansImport;
use Input;
use Session;
use Excel;
class ImportsController extends Controller
{
    function __construct(){
    	$this->title = 'IMPORT MODULE';
    	$this->controller = $this;
    	$this->module = 'importmodule';
        $this->module_prefix = 'payrolls/admin';
    }

     public function index(){

        $response = array(
                        'module'        => $this->module,
                        'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
                        'title'         => $this->title
                        );

        return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

     /** IMPORT FUNCTIONS
      /**
     * Stone file uploader
     * @param  FileUploadRequest $request
     * @return [json]
     */
    public function fileUpload(Request $request){
        // getting all of the post data
        try{
            $file = Input::file('file');
            $disk = "public_folder";
            $destination_path = "uploads/files";
            $ext = strtolower($file->getClientOriginalExtension());

            $filename = 'files'.date('Y-m-d').uniqid().'.'.$ext;
            $upload_success = $file->move($destination_path, $filename);

            if($upload_success){
               Session::forget('excelPath');
               Session::put('excelPath',$destination_path.'/'.$filename);
               // $this->ReadExcel($destinationPath.'/'.$filename);
               $filePath = $destination_path.'/'.$filename;

                // === PARAMETERS ===
               // import ex(new LoansImport), filePath ,$disk, $readerType
              Excel::import(new LoansImport,$filePath,$disk);

            }
        }
        catch( \Exception $e){
            dd($e);
        }
    }

   //  public function ReadExcel($excelPath){
   //      try{
   //          Excel::load($excelPath, function($reader) {
   //          //    // Getting all results
   //              $results = $reader->get();
   //              echo json_encode($results[0]);

   //          });
   //      }catch(\Exception $e){
   //          echo 'jay0';
   //          dd($e);
   //      }
   //  }


   // public function store(){
   //      $path = Session::get('excelPath');
   //                  // return 'ob_get_level';
   //      Excel::load($path, function($reader) {
   //          header('Content-type: text/html; charset=utf-8');
   //          if (ob_get_level() == 0) ob_start();
   //          // header('Cache-Control: no-cache');
   //          Session::forget('noErrorData');
   //          Session::forget('errorData');
   //          $_fields = Input::all();
   //          $errorData = [];
   //          $noErrorData = [];
   //          Session::put('fields',$_fields);

   //          $results = $reader->all();  // get the item on excel

   //          $uploadedCounter = 1;
   //          foreach ($results as $key => $value) {
   //              $errorCounter = 0;
   //               $errorCounter = $this->validateFields($_fields, $value, $errorCounter, $key);
   //              echo $uploadedCounter / count($results) * 100 . '-' . $uploadedCounter. ' out of ' . count($results) . '|';

   //              $uploadedCounter++;
   //              // Session::put('progress',round($progress));
   //              if($errorCounter > 0){
   //                  $errorData[$key] = $value->toArray();
   //                  Session::put('errorData',$errorData);
   //                  $noerror = true;
   //                  ob_clean();

   //              }else{
   //                   // echo '12';
   //                  usleep(1000);
   //                  ob_flush();
   //                  flush();

   //                  $noErrorData[$key] = $value->toArray();
   //                  Session::put('noErrorData',$noErrorData);

   //              }

   //          }
   //              ob_end_flush();

   //      });

   //  }

   //  public function storeValidated(){
   //        $data = Session::get('noErrorData');
   //        $fields = Session::get('fields');

   //        if (ob_get_level() == 0) ob_start();

   //        $uploadedCounter = 1;

   //        foreach ($data as $key => $value) {

   //          echo $uploadedCounter / count($data) * 100 . '-' . $uploadedCounter. ' out of ' . count($data) . '|';

   //          $uploadedCounter++;
   //          $_data = $this->assignMappedKeys($value,$fields);
   //          $_data['subscriber_id'] = Auth::User()->subscriber_id;

   //          // foreach ($_data as $key => $value) {
   //          //     $if_exists = PurchaseOrderDetail::where('po_number',$_data['po_number'])->first();
   //          //     if(!$if_exists){

   //                usleep(1000);
   //                ob_flush();
   //                flush();

   //                Loans::create($_data);

   //          //     }
   //          // }
   //      }
   //       ob_end_flush();
   //  }


   //  private function validateFields($fields, $value, $errorCounter, $key){
   //      foreach ($fields as $field_key => $field_value) {

   //          switch ($field_value) {
   //              case 'ignore': break;
   //              default:
   //                  if(empty($value[$field_key])){
   //                      $errorCounter++;
   //                  }
   //                  break;
   //           }
   //      }

   //      return $errorCounter;
   //  }

   //  public function getErrorImport(){
   //      $errorExcel = Session::get('errorData');
   //      $fields = Session::get('fields');
   //      // header('Content-type: text/html; charset=utf-8');
   //      if (ob_get_level() == 0) ob_start();
   //      $value['subscriber_id'] = '';
   //      $errorCounter = 0;
   //      if($errorExcel){
   //          foreach ($errorExcel as $key => $value) {
   //              // unset($fields['location_id']);
   //              $errorCounter++;
   //              ob_flush();
   //              flush();
   //              usleep(1000);
   //          }
   //              echo $errorCounter;
   //      }

   //       ob_end_flush();
   //  }

   //  public function getProgress(){
   //      return @Session::get('progress');
   //  }

   //  private function ignoredFields($fields){
   //      $data = [];
   //      foreach ($fields as $key => $value) {
   //          if($value == 'ignore' || $value == 'pos_required_fields'){
   //              array_push($data, $key);
   //          }
   //      }

   //      return $data;
   //  }

   //  private function posFields($_data, $fields){
   //      $data = [];
   //      foreach ($fields as $key => $value) {
   //          if($value == 'pos_required_fields'){
   //              if($_data[$key] > 0){
   //                  array_push($data, str_replace('_', ' ', $key));
   //              }
   //          }
   //      }

   //      return $data;
   //  }

   //   private function assignMappedKeys($data, $fields){
   //      foreach ($data as $key => $value) {
   //           $hold[$fields[$key]] = $value;
   //      }
   //      return $hold;
   //  }


}
