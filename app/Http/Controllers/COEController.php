<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Employee;
use App\EmployeeInfo;
use Input;
class COEController extends Controller
{
    function __construct(){
        $this->title = 'Certificate Of Employment';
        $this->module = 'certificate_of_employment';
        $this->module_prefix = 'payrolls/reports';
        $this->controller = $this;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = array(
                        'module'        => $this->module,
                        'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
                        'title'         => $this->title,
                        'employees'     => Employee::orderBy('lastname','asc')->get()
                        );

        return view($this->module_prefix.'.'.$this->module,$response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Input::all();

        $employeeinfo   = new EmployeeInfo;

        $year        = $data['year'];
        $month       = $data['month'];
        $employee_id = $data['id'];

        $query = $employeeinfo
        ->with('employees','benefitinfo','employee_information')
        ->where('employee_id',$employee_id)
        ->first();

        return json_encode($query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
