<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Input;
use App\Benefit;

class BenefitsController extends Controller
{
    function __construct(){
    	$this->controller = $this;
    	$this->title = 'BENEFITS';
    	$this->module_prefix = 'payrolls/admin/filemanagers';
    	$this->module = 'benefits';
        $this->table = 'benefits';
    }

    public function index(){
    	$response = array(
    		'controller'	=> $this->controller,
    		'title'			=> $this->title,
    		'module'		=> $this->module,
    		'module_prefix'	=> $this->module_prefix,
    	);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function store(Request $request){

        $benefit = Benefit::find($request->benefit_id);

        $message = 'Update Successfully.';
        if(empty($benefit)){

            $this->validate($request,[
                'code'                  => 'required',
                'name'                  => 'required',
            ]);

            $benefit = new Benefit;
            $message = 'Save Successfully.';
        }

        $benefit->fill($request->all());
        $benefit->save();

        return json_encode(['status'=>true,'response' => $message]);

    }


    public function show(){

        $q = Input::get('q');
        $limit = Input::get('limit');
        // if(empty($limit)){ $limit  =  10; }

        $data = $this->get_records($q);

        $response = array(
                        'data'          => $data,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix,
                        'cols'          => $this->table_columns($this->table)

                        );
        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);

    }


    private function get_records($q){

        $cols = ['code','name','tax_type','payroll_group'];


        $query = Benefit::where(function($query) use($cols,$q){

                $query = $query->where(function($qry) use($q, $cols){
                    foreach ($cols as $key => $value) {
                        $qry->orWhere($value,'like','%'.$q.'%');
                    }
                });
        });
        $response = $query->get();

        return $response;

    }

    public function getItem(){
        $id = Input::get('id');

        $query = Benefit::where('id',$id)->first();

        return json_encode($query);
    }

    public function delete(){
        $data = Input::all();
        $id   = $data['id'];

        $benefit = new Benefit;
        $benefit->destroy($id);

        return json_encode(['status'=>true]);
    }
}
