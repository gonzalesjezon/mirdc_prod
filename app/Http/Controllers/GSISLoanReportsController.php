<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\EmployeeInfo;
use App\EmployeeInformation;
use App\EmployeeStatus;
use App\Employee;
class GSISLoanReportsController extends Controller
{
    function __construct(){
		$this->title = 'GSIS REMITTANCE REPORT';
    	$this->module = 'gsisloanreports';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;
	}

	public function index(){

		$employee = new Employee;
		$employeeinformation = new EmployeeInformation;
        $employeestatus      = new EmployeeStatus;


		$status = $employeestatus
        ->where('category',1)
        ->select('RefId')
        ->get()->toArray();

        $employee_id = $employeeinformation
        ->whereIn('employee_status_id',$status)
        ->select('employee_id')
        ->get()->toArray();

        $employee = $employee
        ->whereIn('id',$employee_id)
        ->where('active',1)
        ->where('with_setup',1)
        ->orderBy('lastname','asc')
        ->get();

    	$response = array(
    					'employee' 		=> $employee,
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m')
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }


    public function show(){

        $data = Input::all();

        $year = $data['year'];
        $month = $data['month'];

        $transaction         = new EmployeeInfo;
        $employeeinformation = new EmployeeInformation;
        $employeestatus      = new EmployeeStatus;

        $status = $employeestatus
        ->where('category',1)
        ->select('RefId')
        ->get()->toArray();

        $employee_id = $employeeinformation
        ->whereIn('employee_status_id',$status)
        ->select('employee_id')
        ->get()->toArray();

        $query = $transaction
        ->with([
        		'loanstransaction' => function($qry){
        			$qry = $qry->with([
        				'loans' => function($qry){
        					$qry->where('loan_type','GSIS');
        				}
        			]);
        		},
        		'salaryinfo',
        		'employees',
            ])
        ->whereIn('employee_id',$employee_id)
		->get();


        return json_encode($query);
    }
}
