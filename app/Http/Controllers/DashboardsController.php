<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Employee;
use App\EmployeeStatus;
use App\EmployeeInformation;
class DashboardsController extends Controller
{
    function __construct(){
    	$this->title = "DASHBOARD";
    	$this->controller = $this;
        $this->module_prefix  = 'payrolls';
        $this->module = 'dashboards';
    }

    public function index(){

        $employestatus        = new EmployeeStatus;
        $emplpoyeeinformation = new EmployeeInformation;
        $employee             = new Employee;

        $code = ['P','T','CON','COS','JO'];

        $data['permanent'] = $employestatus->where('Code',$code[0])->first();
        $data['mds'] = $employestatus->where('Code',$code[1])->first();
        $data['con'] = $employestatus->where('Code',$code[2])->first();
        $data['cos'] = $employestatus->where('Code',$code[3])->first();
        $data['jo'] = $employestatus->where('Code',$code[4])->first();

        $employee = $employee->where('active',1)->select('id')->get()->toArray();

        $employees = $emplpoyeeinformation
        ->WhereIn('employee_id',$employee)
        ->get();

        $permanent = $emplpoyeeinformation
        ->where('employee_status_id',@$data['permanent']->RefId)
        ->get();

        $mds = $emplpoyeeinformation
        ->where('employee_status_id',@$data['mds']->RefId)
        ->get();

        $con = $emplpoyeeinformation
        ->where('employee_status_id',@$data['con']->RefId)
        ->get();

        $cos = $emplpoyeeinformation
        ->where('employee_status_id',@$data['cos']->RefId)
        ->get();

        $temp = $emplpoyeeinformation
        ->where('employee_status_id',@$data['jo']->RefId)
        ->get();


    	$response = array(
                        'allemployees'=> count(@$employee),
                        'permanent'  => count(@$permanent),
                        'mds'         => count(@$mds),
                        'con'        => count(@$con),
                        'cos'        => count(@$cos),
                        'jo'        => count(@$temp),
    					'title'      => $this->title,
    					'controller' => $this->controller,
                        'module'     => $this->module,

    				 	);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q = Input::get('q');

        $data = $this->searchName($q);

        $response = array(
                        'data'          => $data,
                        'title'         => $this->title,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix
                    );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);

    }

    public function searchName($q){

        $cols = ['lastname','firstname'];

        $employee_status      = new EmployeeStatus;
        $employee             = new Employee;
        $employeeinformation  = new EmployeeInformation;

        $query = $employee->where(function($query) use($cols,$q){
            $query = $query->where(function($qry) use($q, $cols){
                foreach ($cols as $key => $value) {
                    $qry->orWhere($value,'like','%'.$q.'%');
                }
            });

        });

        $response = $query->with('employeeinformation','salaryinfo')
        ->where('active',1)
        ->orderBy('lastname','asc')
        ->get();

        return $response;
    }
}
