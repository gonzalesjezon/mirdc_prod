<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Project;
use Input;
use Auth;

class ProjectsController extends Controller
{

    function __construct(){
        $this->controller = $this;
        $this->title = 'Projects';
        $this->module_prefix = 'payrolls/admin/filemanagers';
        $this->module = 'projects';
        $this->table = 'pms_projects';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::latest()
        ->paginate(30);

        $response = array(
            'projects'      => $projects,
            'controller'    => $this->controller,
            'title'         => $this->title,
            'module'        => $this->module,
            'module_prefix' => $this->module_prefix,
        );

        return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $projects = Project::find($request->id);
        $message = 'Update Successfully.';
        if(empty($projects)){
            $projects = new Project;
            $this->validate($request,[
                // 'code'                  => 'required|unique:pms_projects',
                'name'                  => 'required',
            ]);
            $message = 'Save Successfully.';
        }

        $projects->fill($request->all());
        if($message == 'Update Successfully.'){
            $projects->updated_by = Auth::id();
        }else{
            $projects->created_by = Auth::id();
        }
        $projects->save();

        return json_encode(['status'=>true,'response' => $message ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     public function getItem(){
        $id = Input::get('id');

        $query = Project::where('id',$id)->first();

        return json_encode($query);
    }

    public function delete(){
        $data = Input::all();
        $id   = $data['id'];

        $benefit = new Project;
        $benefit->destroy($id);

        return json_encode(['status'=>true]);
    }
}
