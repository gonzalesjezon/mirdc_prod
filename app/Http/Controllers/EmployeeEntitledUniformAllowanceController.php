<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\SpecialPayrollTransaction;
class EmployeeEntitledUniformAllowanceController extends Controller
{
    function __construct(){
		$this->title = 'EMPLOYEE ENTITLED TO UNIFORM ALLOWANCE';
    	$this->module = 'employeeentitleduniformallowance';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;
	}

	public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m')
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }


    public function show(){

        $data = Input::all();

        $transaction    =  new SpecialPayrollTransaction;

        $year = $data['year'];
        $month = $data['month'];

        $query = $transaction
        ->with('employees','benefitinfo')
        ->where('year',$year)
        ->where('month',$month)
        ->where('status','ua')
        // ->whereNotNull('cost_uniform_amount')
        ->orderBy('created_at','desc')
        ->get();

        $arr = [];
        if(isset($query)){

            foreach ($query as $key => $value) {

                $data2 = $transaction
                ->with('employees','benefitinfo')
                ->where('year',$year)
                ->where('month',$month)
                ->where('employee_id',$value->employee_id)
                ->where('status','ua')
                ->orderBy('created_at','desc')
                ->first();

                if(isset($value->division_id)){
                    $arr[$value->divisions->Name][$key] = $data2;
                }
                if(isset($value->office_id)){

                    $arr[$value->offices->Name][$key] = $data2;
                }
            }

        }




        return json_encode($arr);
    }
}
