<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Transaction;
use App\Loan;
use App\LoanInfo;
class EcipRemittancesController extends Controller
{
    function __construct(){
		$this->title = 'OTHER REMITTANCES';
    	$this->module = 'ecip';
        $this->module_prefix = 'payrolls/reports/remittances';
    	$this->controller = $this;
	}

	public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m')
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }


   public function show(){

        $q = Input::all();

        $loan_id = Loan::where('code','EA')->select('id')->first();

        $employee_id = Transaction::where('year',$q['year'])
                            ->where('month',$q['month'])->select('employee_id')->get()->toArray();

         $query = LoanInfo::with('employees')->where('loan_id',$loan_id->id)->whereIn('employee_id',$employee_id)->get();

        return json_encode($query);
    }
}
