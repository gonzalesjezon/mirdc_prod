<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Input;
use Crypt;
use App\Employee;
use App\GsisPolicy;
use App\PhilhealthPolicy;
use App\PagibigPolicy;
use App\TaxPolicy;
use App\Bank;
use App\BankBranch;
use App\Department;
use App\Company;
use App\Position;
use App\Office;
use App\Division;
use App\EmployeeStatus;
use App\EmployeeInformation;
use App\Benefit;
use App\PositionItemSetup;
use App\TaxTable;
use App\WageRate;
use App\BenefitInfo;
use App\SalaryInfo;
use App\LoanInfo;
use App\DeductionInfo;
use App\Loan;
use App\Deduction;
use App\NonPlantillaEmployeeInfo;
use App\NonPlantillaTransaction;
use Carbon\Carbon;
use App\AttendanceInfo;
use Session;

class NonPlantillaController extends Controller
{

    function __construct(){
        $this->title = 'NON PLANTILLA';
        $this->module = 'nonplantilla';
        $this->module_prefix = 'payrolls';
        $this->controller = $this;

    }

    public function index(){

        $benefit        = Benefit::orderBy('name','asc')->get();
        $loans          = Loan::orderBy('name','asc')->get();
        $deduction      = Deduction::orderBy('name','asc')->get();
        $empstatus      = EmployeeStatus::where('category',0)->get();


        $response = array(
                        'empstatus'     => $empstatus,
                        'benefit'       => $benefit,
                        'loans'         => $loans,
                        'deductions'     => $deduction,
                        'module'        => $this->module,
                        'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
                        'title'         => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m')
                        );

        return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function showBenefitinfo(){


        $response       = array(
                            'controller'    => $this->controller,
                            'module'        => $this->module,
                            'module_prefix' => $this->module_prefix,
                        );

        return view($this->module_prefix.'.'.$this->module.'.benefitdatatable',$response);
    }

    public function showLoaninfo(){


        $response       = array(
                            'controller'    => $this->controller,
                            'module'        => $this->module,
                            'module_prefix' => $this->module_prefix,
                        );

        return view($this->module_prefix.'.'.$this->module.'.loandatatable',$response);
    }

     public function showDeductioninfo(){


        $response       = array(
                            'controller'    => $this->controller,
                            'module'        => $this->module,
                            'module_prefix' => $this->module_prefix,
                        );

        return view($this->module_prefix.'.'.$this->module.'.deductdatatable',$response);
    }

    public function show(){
        $q                   = Input::get('q');
        $year                = Input::get('year');
        $month               = Input::get('month');
        $selectedYear        = Input::get('selected_year');
        $selectedMonth       = Input::get('selected_month');

        // filter
        $_employee_status    = Input::get('_employee_status');
        $_check_payroll      = Input::get('_check_payroll');
        $_sub_period          = Input::get('_sub_period');

        $check_payroll       = Input::get('check_payroll');
        $employee_status     = Input::get('employee_status');
        $sub_period          = Input::get('sub_period');

        if(!isset($_check_payroll) && !isset($check_payroll)){
            $_check_payroll = 'wopayroll';
            $check_payroll = 'wopayroll';
        }

        $data = $this->searchName($q,$check_payroll,$employee_status,$selectedYear,$selectedMonth,$sub_period);

        if(isset($year) || isset($month) || isset($_check_payroll) || isset($_employee_status) || isset($_sub_period)){
            $data = $this->filter($year,$month,$_check_payroll,$_employee_status,$_sub_period);
        }
        $response = array(
                        'data'          => $data,
                        'title'         => $this->title,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix
                    );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);
    }

    public function searchName($q,$checkpayroll,$employee_status,$year,$month,$sub_period){

        $employeestatus              = new EmployeeStatus;
        $employee_information        = new EmployeeInformation;
        $employee                    = new Employee;
        $nonplantilla_employee_info  = new NonPlantillaEmployeeInfo;
        $transaction                 = new NonPlantillaTransaction;

        $cols = ['lastname','firstname','middlename'];

        $empstatus_id = $employeestatus
        ->where('category',0)
        ->where('RefId',$employee_status)
        ->select('RefId')
        ->get()
        ->toArray();

        $employee_info_id = $employee_information
        ->whereIn('employee_status_id',$empstatus_id)
        ->select('employee_id')
        ->get()->toArray();

        $nonplantilla_employee_id =  $nonplantilla_employee_info
        ->select('employee_id')
        ->whereNotNull('daily_rate_amount')
        ->get()->toArray();

       $transaction = $transaction
       ->select('employee_id')
       ->where('year',$year)
       ->where('month',$month)
       ->where('sub_pay_period',$sub_period)
       ->get()
       ->toArray();

       $query = [];
        switch ($checkpayroll) {
            case 'wpayroll':

               $query = $employee->whereIn('id',$transaction);

                break;

            case 'wopayroll':

                $query = $employee
                ->whereNotIn('id',$transaction)
                ->whereIn('id',$nonplantilla_employee_id);

                break;

            default:
                // $employee_id = $transaction->select('employee_id')->get()->toArray();
                $query = $employee->whereIn('id',$employee_info_id);

                break;
        }


        $query = $query->where(function($qry) use($q, $cols){
            foreach ($cols as $key => $value) {
                $qry->orWhere($value,'like','%'.$q.'%');
            }
        });
        $response = $query->where('active',1)->orderBy('lastname','asc')->get();


        return $response;
    }

    public function filter($year,$month,$checkpayroll,$employee_status,$sub_period){


        $employeestatus              = new EmployeeStatus;
        $employee_information        = new EmployeeInformation;
        $nonplantilla                = new NonPlantillaTransaction;
        $employee                    = new Employee;
        $nonplantilla_employee_info  = new NonPlantillaEmployeeInfo;

        // --GET EMPLOYEE STATUS ID EX(JOB ORDER)
        $empstatus_id = $employeestatus
        ->where('category',0)
        ->where('RefId',$employee_status)
        ->select('RefId')
        ->get()
        ->toArray();

        $employee_id  = $employee_information->whereIn('employee_status_id',$empstatus_id)->select('employee_id')->get()->toArray();
        $nonplantilla_employee_id =  $nonplantilla_employee_info->select('employee_id')->whereNotNull('daily_rate_amount')->get()->toArray();

        $query = [];
        $response = "";

        $query = $nonplantilla->select('employee_id')
            ->whereIn('employee_status_id',$empstatus_id)
            ->where('year',$year)
            ->where('month',$month)
            ->where('sub_pay_period',$sub_period)
            ->get()->toArray();

        switch ($checkpayroll) {
            case 'wpayroll':

                    $response = $employee->where('active',1)->whereIn('id',$query)->orderBy('lastname','asc')->get();
                break;

            case 'wopayroll':

                    $response = $employee->whereIn('id',$employee_id)
                                        ->whereIn('id',$nonplantilla_employee_id)
                                        ->whereNotIn('id',$query)
                                        ->where('active',1)
                                        ->orderBy('lastname','asc')->get();

                break;
        }
        return $response;
    }

     public function getSearchby(){
        $q = Input::get('q');

        $query = "";
        switch ($q) {
            case 'company':
                $query = Company::orderBy('name','asc')->get();
                break;
            case 'department':
                $query = Department::orderBy('name','asc')->get();
                break;
            case 'office':
                $query = Office::orderBy('name','asc')->get();
                break;
            case 'division':
                $query = Division::orderBy('name','asc')->get();
                break;
            case 'position':
                $query = Position::orderBy('name','asc')->get();
                break;

            default:
                # code...
                break;
        }

        return json_encode($query);
    }

    public function getEmployeesinfo(){

        $data = Input::all();

        $transaction  = new NonPlantillaTransaction;
        $employeeinfo = new NonPlantillaEmployeeInfo;

        $query['transaction'] = $transaction
                            ->with('employees')
                            ->where('year',@$data['year'])
                            ->where('month',@$data['month'])
                            ->where('sub_pay_period',@$data['sub_period'])
                            ->where('employee_id',@$data['id'])->first();


        $query['employeeinfo'] = $employeeinfo
                                ->with('tax_policy_ewt','tax_policy_pwt')
                                ->where('employee_id',@$data['id'])
                                ->first();


        // $query['benefitinfo'] = BenefitInfo::with(['benefits' => function($qry) {$qry->where('name','PERA');} ])->where('employeeinfo_id',$query['employeeinfo']['id'])
        //                                 ->get();

        // $query['deductioninfo'] = DeductionInfo::with('deductions')->where('employeeinfo_id',$query['employeeinfo']['id'])
        //                                 ->get();

        // $query['loaninfo'] = LoanInfo::with('loans')->where('employeeinfo_id',$query['employeeinfo']['id'])
                                        // ->get();

        return json_encode($query);
    }

    public function processPayroll() {
        $data = Input::all();

        $year            = $data['year'];
        $month           = $data['month'];
        $semi_pay_period = @$data['summary']['semi_pay_period'];
        $emp_code        = $data['emp_code'];
        $transactions =  new NonPlantillaTransaction;

        if(isset($data['transaction_id'])){

            //ATTENDACE

            // TRANSACTIONS
            $actual_basicpay     = ($data['summary']['actual_basicpay']) ? str_replace(',', '', $data['summary']['actual_basicpay']) : 0;
            $adjust_basicpay     = ($data['summary']['adjust_basicpay']) ? str_replace(',', '', $data['summary']['adjust_basicpay']) : 0;
            $total_basicpay      = ($data['summary']['total_basicpay']) ? str_replace(',', '', $data['summary']['total_basicpay']) : 0;

            $actual_absences     = ($data['summary']['actual_absences']) ? str_replace(',', '', $data['summary']['actual_absences']) : 0;
            $adjust_absences     = ($data['summary']['adjust_absences']) ? str_replace(',', '', $data['summary']['adjust_absences']) : 0;
            $total_absences      = ($data['summary']['total_absences']) ? str_replace(',', '', $data['summary']['total_absences']) : 0;

            // $actual_tardines     = ($data['summary']['actual_tardines']) ? str_replace(',', '', $data['summary']['actual_tardines']) : 0;
            // $adjust_tardines     = ($data['summary']['total_tardines']) ? str_replace(',', '', $data['summary']['adjust_tardines']) : 0;
            // $total_tardines      = ($data['summary']['total_tardines']) ? str_replace(',', '', $data['summary']['total_tardines']) : 0;

            // $actual_undertime     = ($data['summary']['actual_undertime']) ? str_replace(',', '', $data['summary']['actual_undertime']) : 0;
            // $adjust_undertime     = ($data['summary']['adjust_undertime']) ? str_replace(',', '', $data['summary']['adjust_undertime']) : 0;
            // $total_undertime      = ($data['summary']['total_undertime']) ? str_replace(',', '', $data['summary']['total_undertime']) : 0;
            $overpayment_amount   = ($data['summary']['overpayment_amount']) ? str_replace(',', '', $data['summary']['overpayment_amount']) : 0;
            $underpayment_amount   = ($data['summary']['underpayment_amount']) ? str_replace(',', '', $data['summary']['underpayment_amount']) : 0;
            $other_deduction_amount   = ($data['summary']['other_deduction_amount']) ? str_replace(',', '', $data['summary']['other_deduction_amount']) : 0;

            $basic_net_pay      = ($data['summary']['basic_net_pay']) ? str_replace(',', '', $data['summary']['basic_net_pay']) : 0;
            // $net_deduction      = ($data['summary']['net_deduction']) ? str_replace(',', '', $data['summary']['net_deduction']) : 0;
            $gross_pay      = ($data['summary']['gross_pay']) ? str_replace(',', '', $data['summary']['gross_pay']) : 0;
            $gross_taxable_pay      = ($data['summary']['gross_taxable_pay']) ? $data['summary']['gross_taxable_pay'] : 0;
            $net_pay      = ($data['summary']['net_pay']) ? str_replace(',', '', $data['summary']['net_pay']) : 0;
            $tax_rate_amount_one      = ($data['summary']['tax_rate_amount_one']) ? str_replace(',', '', $data['summary']['tax_rate_amount_one']) : 0;
            $tax_rate_amount_two      = ($data['summary']['tax_rate_amount_two']) ? str_replace(',', '', $data['summary']['tax_rate_amount_two']) : 0;

            $actual_workdays  = ($data['attendance']['actual_workdays']) ? $data['attendance']['actual_workdays'] : 0;
            $actual_absent  = ($data['attendance']['actual_absences']) ? $data['attendance']['actual_absences'] : 0;
            // $actual_tard  = ($data['attendance']['actual_tardines']) ? $data['attendance']['actual_tardines'] : 0;
            // $actual_under = ($data['attendance']['actual_undertime']) ? $data['attendance']['actual_undertime'] : 0;
            // $adjust_workdays  = ($data['attendance']['adjust_workdays']) ? $data['attendance']['adjust_workdays'] : 0;
            // $adjust_absent  = ($data['attendance']['adjust_absences']) ? $data['attendance']['adjust_absences'] : 0;
            // $adjust_tard  = ($data['attendance']['adjust_tardines']) ? $data['attendance']['adjust_tardines'] : 0;
            // $adjust_under = ($data['attendance']['adjust_undertime']) ? $data['attendance']['adjust_undertime'] : 0;

            $transactions = NonPlantillaTransaction::find($data['transaction_id']);

            // Tax Computation
            $diffAmount          = $total_basicpay - $total_absences;
            $employeeinfo        = NonPlantillaEmployeeInfo::where('employee_id',$transactions->employee_id)->first();

            $ewtRate             = TaxPolicy::where('id',$employeeinfo->ewt_policy_id)->select('job_grade_rate')->first();
            $pwtRate             = TaxPolicy::where('id',$employeeinfo->pwt_policy_id)->select('job_grade_rate')->first();

            $result            = $this->compute_tax($employeeinfo->tax_policy_id,$employeeinfo,$diffAmount,$ewtRate,$pwtRate,$transactions->employee_id);

            $taxAmountOne      = $result['tax_amount_one'];
            $taxAmountTwo      = $result['tax_amount_two'];
            $threshHoldBalance = $result['threshold_balance'];

            $net_pay = $diffAmount - $taxAmountOne - $taxAmountTwo;


            $transactions->actual_workdays  = $actual_workdays;
            $transactions->actual_absences  = $actual_absent;
            // $transactions->actual_tardiness = $actual_tard;
            // $transactions->actual_undertime = $actual_under;
            // $transactions->adjust_workdays  = $adjust_workdays;
            // $transactions->adjust_absences  = $adjust_absent;
            // $transactions->adjust_tardiness = $adjust_tard;
            // $transactions->adjust_undertime = $adjust_under;


            $transactions->actual_basicpay_amount     = $actual_basicpay;
            $transactions->adjust_basicpay_amount     = $adjust_basicpay;
            $transactions->total_basicpay_amount      = $total_basicpay;
            $transactions->actual_absences_amount     = $actual_absences;
            $transactions->adjust_absences_amount     = $adjust_absences;
            $transactions->total_absences_amount      = $total_absences;
            // $transactions->actual_undertime_amount    = $actual_tardines;
            // $transactions->adjust_tardiness_amount    = $adjust_tardines;
            // $transactions->total_tardiness_amount     = $total_tardines;
            // $transactions->actual_undertime_amount    = $actual_undertime;
            // $transactions->adjust_undertime_amount    = $adjust_undertime;
            // $transactions->total_undertime_amount     = $total_undertime;
            $transactions->overpayment_amount         = $overpayment_amount;
            $transactions->underpayment_amount        = $underpayment_amount;
            $transactions->other_deduction_amount     = $other_deduction_amount;
            $transactions->basic_net_pay              = $basic_net_pay;
            $transactions->pay_period                 = @$pay_period;
            $transactions->sub_pay_period             = @$semi_pay_period;
            $transactions->tax_rate_amount_one        = @$taxAmountOne;
            $transactions->tax_rate_amount_two        = @$taxAmountTwo;
            $transactions->threshhold_balance         = @$threshHoldBalance;
            // $transactions->net_deduction              = $net_deduction;
            $transactions->gross_pay                  = $gross_pay;
            $transactions->gross_taxable_pay          = $gross_taxable_pay;
            $transactions->net_pay                    = $net_pay;

            $transactions->save();


            $response = json_encode(['status'=>true,'response'=>'Payroll Updated Successfully!']);

        }else{

            $ctr = 0;
            $error = 0;
            $error_id = [];
            foreach ($data['empid'] as $key => $_id) {

                if(isset($_id)){
                    $check = NonPlantillaEmployeeInfo::where('employee_id',$_id)->whereNotNull('daily_rate_amount')->first();
                    if($check === null){
                        $error++;
                        $error_id[] = $_id;
                    }
                }
            }

            $y = date('Y', strtotime($year));
            $m = date('m', strtotime($month));

            $cosDays = cal_days_in_month(CAL_GREGORIAN,$m,$y); // gsis days
            $totalCosDays = $cosDays;
            $joDays = 22;

            switch ($semi_pay_period) {
                case 'firsthalf':
                    $cosDays = round(($cosDays / 2) - 1);
                    $joDays = $joDays / 2;
                    break;

                default:
                    $cosDays = $cosDays / 2;
                    $joDays = $joDays / 2;
                    break;
            }

            $workdays = 0;
            if($error == 0){
                foreach ($data['empid'] as $key => $_id) {
                    if(isset($_id)){

                        $employeeinformation = EmployeeInformation::where('employee_id',$_id)->first();
                        $employeeinfo        = NonPlantillaEmployeeInfo::where('employee_id',$_id)->first();

                        $tax_policy_id       = $employeeinfo->tax_policy_id;
                        $ewt_policy_id       = $employeeinfo->ewt_policy_id;
                        $pwt_policy_id       = $employeeinfo->pwt_policy_id;
                        $monthly             = $employeeinfo->monthly_rate_amount;
                        $daily               = $employeeinfo->daily_rate_amount;

                        $monthly_rate_amount = 0;
                        switch ($emp_code) {
                            case 'COS':
                                $temp_daily = $monthly / $totalCosDays;
                                $monthly_rate_amount = $temp_daily * $cosDays;
                                $workdays = $cosDays;
                                break;
                            case 'JO':
                                $monthly_rate_amount = $daily * $joDays;
                                $workdays = $joDays;
                                break;
                            default:
                                $monthly_rate_amount = $daily * 22;
                                $workdays = 22;
                                break;
                        }

                        $annual_rate_amount  = $monthly_rate_amount * 12;

                        $ewtRate = TaxPolicy::where('id',$ewt_policy_id)->select('job_grade_rate')->first();
                        $pwtRate = TaxPolicy::where('id',$pwt_policy_id)->select('job_grade_rate')->first();

                        $taxAmountOne        = 0;
                        $taxAmountTwo        = 0;
                        $threshHoldBalance   = 0;

                        $result            = $this->compute_tax($tax_policy_id,$employeeinfo,$monthly_rate_amount,$ewtRate,$pwtRate,$_id);
                        $taxAmountOne      = $result['tax_amount_one'];
                        $taxAmountTwo      = $result['tax_amount_two'];
                        $threshHoldBalance = $result['threshold_balance'];

                        $netPay = $monthly_rate_amount - $taxAmountOne - $taxAmountTwo;

                        $transactions =  new NonPlantillaTransaction;

                        $transactions->employee_id              = $_id;
                        $transactions->employeeinfo_id          = $employeeinfo->id;
                        $transactions->division_id              = $employeeinformation->division_id;
                        $transactions->company_id               = $employeeinformation->company_id;
                        $transactions->position_item_id         = $employeeinformation->position_item_id;
                        $transactions->office_id                = $employeeinformation->office_id;
                        $transactions->responsibility_id        = $employeeinfo->responsibility_id;
                        $transactions->project_id               = $employeeinfo->project_id;
                        $transactions->employee_status_id       = $employeeinformation->employee_status_id;
                        $transactions->position_id              = $employeeinformation->position_id;
                        $transactions->employee_number          = @$employeeinformation->employee_number;
                        $transactions->actual_workdays          = $workdays;
                        $transactions->sub_pay_period           = $semi_pay_period;
                        $transactions->total_basicpay_amount    = $monthly_rate_amount;
                        $transactions->actual_basicpay_amount   = $monthly_rate_amount;
                        $transactions->monthly_rate_amount      = $monthly_rate_amount;
                        $transactions->gross_pay                = $monthly_rate_amount;
                        $transactions->annual_rate_amount       = $annual_rate_amount;
                        $transactions->tax_rate_amount_one      = $taxAmountOne;
                        $transactions->tax_rate_amount_two      = $taxAmountTwo;
                        $transactions->threshhold_balance       = $threshHoldBalance;
                        $transactions->gross_taxable_pay        = $netPay;
                        $transactions->net_pay                  = $netPay;
                        $transactions->year                     = $year;
                        $transactions->month                    = $month;

                        $transactions->save();
                        $ctr++;
                    }
                }
                $response = json_encode(['status'=>true,'response'=>'Processed Successfully! <br>'.$ctr.' Records Saved']);
            }else{
                $error_id = implode(',', $error_id);
                $response = json_encode(['status'=>false,'response'=>'Save Failed! <br>'.$error.' Records <br>No Employee Setup <br>Employee ID <br>['.$error_id.']']);
            }
        }

        return $response;
    }

    public function compute_tax($tax_policy_id,$employeeinfo,$monthly_rate_amount,$ewtRate,$pwtRate,$_id){
        $taxAmountOne       = 0;
        $taxAmountTwo       = 0;
        $threshHoldBalance  = 0;

        $arr = [];
        switch ($tax_policy_id) {
            // Inputted policy
            case '1':
                $taxAmountOne = $employeeinfo->inputted_amount;
                break;

            // Standard Policy
            case '2':
                $taxAmountOne = $monthly_rate_amount * @$ewtRate->job_grade_rate;
                $taxAmountTwo = $monthly_rate_amount * @$pwtRate->job_grade_rate;
                break;

            // Sworn declaration policy
            case '3':

                $prevThreshold = NonPlantillaTransaction::where('employee_id',$_id)
                ->select('threshhold_balance')
                ->orderBy('created_at','desc')
                ->first();

                if(isset($prevThreshold)){
                    $threshHoldBalance = $prevThreshold->threshhold_balance - $monthly_rate_amount;

                }else{

                    $threshhold_amount = $employeeinfo->threshhold_amount;

                    if($threshhold_amount != 0){
                        $threshHoldBalance = $threshhold_amount - $monthly_rate_amount;
                    }else{
                        $threshHoldBalance = 250000;
                        $threshHoldBalance = $threshHoldBalance - $monthly_rate_amount;
                    }
                }

                //* tax amount is less than 0 then (difference * 8%) = tax amount
                if($threshHoldBalance < 0){
                    $taxAmountOne = abs($threshHoldBalance) * 0.08;
                    $threshHoldBalance = 0;
                }

                break;
        }

        $arr['tax_amount_one']    = $taxAmountOne;
        $arr['tax_amount_two']    = $taxAmountTwo;
        $arr['threshold_balance'] = $threshHoldBalance;
        return $arr;
    }


    public function destroy(){
        $data = Input::all();

        $transactions = new NonPlantillaTransaction;

        foreach ($data['empid'] as $key => $value) {

            $transactions->where('employee_id',$data['empid'][$key])
                                ->where('month',$data['month'])
                                ->where('year',$data['year'])
                                ->where('sub_pay_period',$data['sub_period'])
                                ->delete();

        }

        return json_encode(['status'=>true,'response'=>'Delete Successfully']);
    }

}
