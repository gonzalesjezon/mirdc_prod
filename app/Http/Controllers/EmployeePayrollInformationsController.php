<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Input;
use Crypt;
use App\Employee;
use App\GsisPolicy;
use App\PhilhealthPolicy;
use App\PagibigPolicy;
use App\TaxPolicy;
use App\Bank;
use App\BankBranch;
use App\Department;
use App\Company;
use App\Position;
use App\Office;
use App\Division;
use App\EmployeeStatus;
use App\EmployeeInformation;
use App\Benefit;
use App\PositionItemSetup;
use App\TaxTable;
use App\WageRate;
use App\BenefitInfo;
use App\SalaryInfo;
use App\LoanInfo;
use App\DeductionInfo;
use App\Loan;
use App\Deduction;
use App\EmployeeInfo;
use App\SalaryGrade;
use App\PositionItem;
use App\JobGrade;
use App\NonPlantillaEmployeeInfo;
use App\Rate;
use App\Project;
use App\ResponsibilityCenter;

use Carbon\Carbon;
use Auth;
class EmployeePayrollInformationsController extends Controller
{

    function __construct(){
        $this->module_prefix = 'payrolls/admin';
        $this->module = 'employees_payroll_informations';
        $this->title = 'EMPLOYEE FILE';
        $this->controller  = $this;


    }

    public function index(){

        $gsis           = GsisPolicy::get();
        $pagibig        = PagibigPolicy::get();
        $philhealth     = PhilhealthPolicy::get();
        $bank           = Bank::get();
        $benefit        = Benefit::whereNull('remarks')->get();
        $taxperiod      = TaxTable::get();
        $wagerate       = WageRate::get();
        $loans          = Loan::get();
        $deductions     = Deduction::get();
        $sg_data        = SalaryGrade::get();
        $positionitem   = PositionItem::orderBy('name','asc')->get();
        $position       = Position::orderBy('Name','asc')->get();
        $jg_data        = JobGrade::get();
        $cos_tax_policy = TaxPolicy::whereNotNull('job_grade_rate')->orderBy('policy_name','asc')->get();
        $tax_policy     = TaxPolicy::whereNull('job_grade_rate')->get();
        $hp_rate        = Rate::where('status','hp')->orderBy('rate','asc')->get();
        $lp_rate        = Rate::where('status','lp')->orderBy('rate','asc')->get();
        $rcenter        = ResponsibilityCenter::orderBy('name','asc')->get();
        $projects       = Project::orderBy('name','asc')->get();

    	$response = array(
                        'hp_rate'       => $hp_rate,
                        'lp_rate'       => $lp_rate,
                        'cos_tax_policy' => $cos_tax_policy,
                        'rcenter'       => $rcenter,
                        'projects'      => $projects,
                        'jg_data'       => $jg_data,
                        'sg_data'       => $sg_data,
                        'positionitem'  => $positionitem,
                        'position'      => $position,
                        'deductions'    => $deductions,
                        'loans'         => $loans,
                        'wagerate'      => $wagerate,
                        'taxperiod'     => $taxperiod,
                        'bank'          => $bank,
                        'philhealth'    => $philhealth,
                        'pagibig'       => $pagibig,
                        'gsis'          => $gsis,
                        'benefit'       => $benefit,
                        'tax_policy'    => $tax_policy,
    					'title' 		=> $this->title,
    					'controller'	=> $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix
                    );

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function showBenefitinfo(){

        $benefitinfo    = BenefitInfo::with('benefits')->get();

        $response       = array(
                            'benefitinfo'   => $benefitinfo,
                            'controller'    => $this->controller,
                            'module'        => $this->module,
                            'module_prefix' => $this->module_prefix,
                        );

        return view($this->module_prefix.'.'.$this->module.'.benefitinfo_datatable',$response);
    }

    public function showLoaninfo(){

        $loaninfo = LoanInfo::with('loans')->get();
        $response = array(
                        'loaninfo'   => $loaninfo,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix,
                    );
        return view($this->module_prefix.'.'.$this->module.'.loaninfo_datatable',$response);
    }


     public function showSalaryinfo(){

        $salaryinfo   = SalaryInfo::with('salarygrade','jobgrade')->get();
        $response     = array(
                        'salaryinfo'   => $salaryinfo,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix,
                    );
        return view($this->module_prefix.'.'.$this->module.'.salaryinfo_datatable',$response);
    }

     public function showDeductioninfo(){

        $deductioninfo   = DeductionInfo::with('deductions')->get();
        $response        = array(
                            'deductioninfo'   => $deductioninfo,
                            'controller'    => $this->controller,
                            'module'        => $this->module,
                            'module_prefix' => $this->module_prefix,
                        );
        return view($this->module_prefix.'.'.$this->module.'.deductioninfo_datatable',$response);
    }


    public function store(Request $request){

        if(!isset($request->employee_id)){

            $response = json_encode(['status'=>false,'response'=>'No employee selected']);

        }else{

            $this->validate($request,[
                'pagibig_contribution'      => 'required',
                'philhealth_contribution'   => 'required',
                'gsis_contribution'         => 'required',
             ]);

            $pagibig_contribution       =   (isset($request->pagibig_contribution) ? str_replace(',', '', $request->pagibig_contribution) : 0.00);
            $philhealth_contribution    = (isset($request->philhealth_contribution) ? str_replace(',', '', $request->philhealth_contribution) : 0.00);
            $gsis_contribution          = (isset($request->gsis_contribution) ? str_replace(',', '', $request->gsis_contribution) : 0.00);
            $tax_contribution           = (isset($request->tax_contribution) ? str_replace(',', '', $request->tax_contribution) : 0.00);
            $pagibig2                   = (isset($request->pagibig2) ? str_replace(',', '', $request->pagibig2) : 0.00);
            $tax_bracket_amount         = (isset($request->tax_bracket_amount) ? str_replace(',', '', $request->tax_bracket_amount) : 0.00);
            $pagibig_personal                = (isset($request->pagibig_personal) ? str_replace(',', '', $request->pagibig_personal) : 0.00);

            $tax_inexcess                    = (isset($request->tax_inexcess) ? str_replace(',', '', $request->tax_inexcess) : 0.00);
            $er_pagibig_share                = (isset($request->er_pagibig_share) ? str_replace(',', '', $request->er_pagibig_share) : 0.00);
            $er_gsis_share                   = (isset($request->er_gsis_share) ? str_replace(',', '', $request->er_gsis_share) : 0.00);
            $er_philhealth_share             = (isset($request->er_philhealth_share) ? str_replace(',', '', $request->er_philhealth_share) : 0.00);
            $overtime_balance_amount         = (isset($request->overtime_balance_amount) ? str_replace(',', '', $request->overtime_balance_amount) : 0.00);
            $daily_rate_amount         = (isset($request->daily_rate_amount) ? str_replace(',', '', $request->daily_rate_amount) : 0);

            $monthly_rate_amount         = (isset($request->monthly_rate_amount) ? str_replace(',', '', $request->monthly_rate_amount) : 0);
            $annual_rate_amount         = (isset($request->annual_rate_amount) ? str_replace(',', '', $request->annual_rate_amount) : 0);

            $employee_info = EmployeeInfo::find($request->id);

            if(empty($employee_info)){
                $employee_info = new EmployeeInfo;
            }

            $employee_info->fill($request->all());
            $employee_info->daily_rate_amount        = $daily_rate_amount;
            $employee_info->monthly_rate_amount      = $monthly_rate_amount;
            $employee_info->pagibig_contribution     = $pagibig_contribution;
            $employee_info->annual_rate_amount       = $annual_rate_amount;
            $employee_info->philhealth_contribution  = $philhealth_contribution;
            $employee_info->gsis_contribution        = $gsis_contribution;
            $employee_info->tax_contribution         = $tax_contribution;
            $employee_info->pagibig2                 = $pagibig2;
            $employee_info->tax_bracket_amount       = $tax_bracket_amount;
            $employee_info->tax_inexcess             = $tax_inexcess;
            $employee_info->pagibig_personal         = $pagibig_personal;
            $employee_info->er_pagibig_share         = $er_pagibig_share;
            $employee_info->er_gsis_share            = $er_gsis_share;
            $employee_info->er_philhealth_share      = $er_philhealth_share;
            $employee_info->overtime_balance_amount  = $overtime_balance_amount;

            if($employee_info->exists()){
                $employee_info->updated_by = Auth::id();
                $response = json_encode(['status' => true, 'response' => 'Updated Successfully!','employeeload'=>true]);
            }else{
                $employee_info->created_by = Auth::id();
                $response = json_encode(['status' => true, 'response' => 'Save Successfully!','employeeload'=>true]);
            }

            $employee_info->save();

        }

        return $response;


    }

    public function show(){

        $q               = Input::get('q');
        $empstatus       = Input::get('empstatus');
        $employee_status = Input::get('employee_status');
        $category        = Input::get('category');
        $emp_type        = Input::get('emp_type');
        $searchby        = Input::get('searchby');

        $data = "";

        $data = $this->searchName($q,$employee_status);

        if(isset($empstatus) || isset($category)){
            $data = $this->filter($empstatus,$category,$emp_type,$searchby);

        }

        $response = array(
                        'data'          => $data,
                        'title'         => $this->title,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix
                    );

        return view($this->module_prefix.'.'.$this->module.'.namelist',$response);

    }

    public function searchName($q,$empstatus){
        $cols = ['lastname','firstname'];

        $employee_status      = new EmployeeStatus;
        $employee             = new Employee;
        $employeeinformation  = new EmployeeInformation;
        $empstatus_id = [];
        $employee_id = [];


        $query = [];
        switch($empstatus){
            case 'plantilla':
                $empstatus_id = $employee_status
                ->where('category',1)
                ->select('RefId')
                ->get()
                ->toArray();

                $employee_id =  $employeeinformation->whereIn('employee_status_id',$empstatus_id)->select('employee_id')->get()->toArray();
                $query = $employee->whereIn('id',$employee_id);
            break;

            case 'nonplantilla':
                $empstatus_id = $employee_status
                ->where('category',0)
                ->select('RefId')
                ->get()
                ->toArray();

                $employee_id =  $employeeinformation->whereIn('employee_status_id',$empstatus_id)->select('employee_id')->get()->toArray();
                $query = $employee->whereIn('id',$employee_id);
            break;

            default:

                $employee_id =  $employeeinformation->select('employee_id')->get()->toArray();
                $query = $employee->whereIn('id',$employee_id);

            break;
        }

        $query = $query
            ->where('active',1)
            ->where(function($query) use($cols,$q){
            $query = $query->where(function($qry) use($q, $cols){
                foreach ($cols as $key => $value) {
                    $qry->orWhere($value,'like','%'.$q.'%');
                }
            });

        })->orderBy('lastname','asc')->get();

        return $query;

    }

    public function filter($empstatus,$category,$emp_type,$searchby){

        $employee_status     = new EmployeeStatus;
        $employeeinformation = new EmployeeInformation;
        $employee            = new Employee;
        $employeeinfo        = new EmployeeInfo;

        $employeeinfo = $employeeinfo->select('employee_id')->get()->toArray();

        $empstatus_id = [];
        switch($empstatus){
            case 'plantilla':
                $empstatus_id = $employee_status->where('category',1)->select('RefId')->get()->toArray();
            break;

            case 'nonplantilla':
                $empstatus_id = $employee_status->where('category',0)->select('RefId')->get()->toArray();
            break;
        }

        $employeesRefId = [];
        $employeesRefId = $employeeinformation->select('employee_id')
                            ->where(function($qry) use($category,$searchby){
                                switch ($searchby) {
                                    case 'company':
                                        $qry =  $qry->where('company_id',$category);
                                        break;
                                    case 'position':
                                        $qry =  $qry->where('position_id',$category);
                                        break;
                                    case 'division':
                                        $qry =  $qry->where('division_id',$category);
                                        break;
                                    case 'office':
                                        $qry =  $qry->where('office_id',$category);
                                        break;
                                    case 'department':
                                        $qry =  $qry->where('department_id',$category);
                                        break;
                                }

                            });
                if(count($empstatus_id) > 0){
                    $employeesRefId = $employeesRefId->whereIn('employee_status_id',$empstatus_id);
                }
                $employeesRefId = $employeesRefId->get()->toArray();

        $query = $employee
            ->whereIn('id',$employeesRefId)
            ->where('active',1)
            ->orderBy('lastname','asc')->get();

        return $query;


    }


    public function getSearchby(){
        $q = Input::get('q');

        $query = "";
        switch ($q) {
            case 'company':
                $query = Company::orderBy('Name','asc')->get();
                break;
            case 'department':
                $query = Department::orderBy('Name','asc')->get();
                break;
            case 'office':
                $query = Office::orderBy('Name','asc')->get();
                break;
            case 'division':
                $query = Division::orderBy('Name','asc')->get();
                break;
            case 'position':
                $query = Position::orderBy('Name','asc')->get();
                break;

            default:
                # code...
                break;
        }

        return json_encode($query);
    }

    public function getEmployeesinfo(){
        $data = Input::all();

        $employee_id     = @$data['id'];

        $employeeinformation = new EmployeeInformation;
        $employeeinfo        = new EmployeeInfo;
        $loaninfo            = new LoanInfo;
        $deductioninfo       = new DeductionInfo;
        $benefitinfo         = new BenefitInfo;
        $salaryinfo          = new SalaryInfo;
        $nonplantilla        = new NonPlantillaEmployeeInfo;
        $taxable             = new TaxTable;

        $data['employeeinfo'] = $employeeinformation
        ->with([
            'employees',
            'divisions',
            'positions',
            'employeestatus',
            'offices'])
        ->where('employee_id',$employee_id)
        ->first();

        $data['pmsemployeeinfo'] = $employeeinfo
        ->with('employees','gsispolicy','pagibigpolicy','philhealthpolicy','taxpolicy','banks','wages','providentfunds')
        ->where('employee_id',$employee_id)
        ->first();

        if(isset($data['pmsemployeeinfo'])){

            $data['loaninfo'] = $loaninfo
            ->with('loans')
            ->where('employee_id',$employee_id)

            ->get();

            $data['deductioninfo'] = $deductioninfo
            ->with('deductions')
            ->where('employee_id',$employee_id)

            ->get();

            $data['benefitinfo'] = $benefitinfo
            ->with('benefits')
            ->where('employee_id',$employee_id)

            ->get();
        }


        $effectivity_date = Carbon::now()->toDateString();

        $data['salaryinfo'] = $salaryinfo
        ->with('salarygrade','jobgrade')
        ->where('employee_id',$employee_id)
        ->where('salary_effectivity_date','<=',$effectivity_date)
        ->first();

        $data['salarylist'] = $salaryinfo
        ->with('salarygrade','jobgrade')
        ->where('employee_id',$employee_id)
        ->get();

        $data['nonplantilla'] = $nonplantilla
        ->where('employee_id',$employee_id)
        ->first();

        $data['cos_days'] = cal_days_in_month(CAL_GREGORIAN,date('m'),date('Y'));

        $taxtable = $taxable
        ->get()->toArray();


        $data['cl'] = [
            'monthlyCL' => [
                            '0'.'-'.'0',
                            $taxtable[1]['salary_bracket_level2'].'-'.'.20',
                            $taxtable[1]['salary_bracket_level3'].'-'.'.25',
                            $taxtable[1]['salary_bracket_level4'].'-'.'.30',
                            $taxtable[1]['salary_bracket_level5'].'-'.'.32',
                            $taxtable[1]['salary_bracket_level6'].'-'.'.35'
                        ]
            ];

        $data['first']  = ['0','20833','33333','66667','166667','666667'];
        $data['second'] = ['20832','33332','66666','166666','666666','9999999'] ;


        return json_encode($data);
    }


    public function storeBenefitinfo(Request $request){

        $this->validate($request,[
            'benefit_id'                => 'required',
            'start_date'                => 'required',
        ]);

        $amount = (isset($request->benefit_amount)) ? str_replace(',', '', $request->benefit_amount) : 0;
        $lp_basic_amount = (isset($request->lp_basic_salary)) ? str_replace(',', '', $request->lp_basic_salary) : 0;

        $benefitinfo = BenefitInfo::find($request->id);
        $message = 'Update Successfully!';

        if(empty($benefitinfo)){
            $benefitinfo = new BenefitInfo;
            $message = 'Save Successfully!';
        }


        $benefitinfo->fill($request->all());
        $benefitinfo->benefit_amount    = $amount;
        $benefitinfo->lp_basic_salary   = $lp_basic_amount;
        $benefitinfo->benefit_rate      = ($request->hp_rate) ? $request->hp_rate : $request->lp_rate;
        $benefitinfo->terminated        = ($request->terminated) ? 1 : 0;
        if($message == 'Update Successfully.'){
            $benefitinfo->updated_by = Auth::id();
        }else{
            $benefitinfo->created_by = Auth::id();
        }

        $benefitinfo->save();

        $data = BenefitInfo::with('benefits')
        ->where('employee_id',$request->employee_id)
        ->orderBy('start_date','asc')
        ->get();

        $response = json_encode(['status'=> true,'response'=> $message,'benefitinfo'=> $data, 'myform'=>'myForm3']);

        return $response;
    }


    public function storeLoaninfo(Request $request){

        $this->validate($request,[
            'loan_id'                => 'required',
            'start_date'             => 'required',
            'loan_amount'            => 'required',
        ]);


        $amount = (isset($request->loan_amount)) ? str_replace(',', '', $request->loan_amount) : 0;
        $loan_total_amount = (isset($request->loan_total_amount)) ? str_replace(',', '', $request->loan_total_amount) : 0;
        $loan_total_balance = (isset($request->loan_total_balance)) ? str_replace(',', '', $request->loan_total_balance) : 0;

        $message     = "";
        $loaninfo = LoanInfo::find($request->id);
        $message = 'Update Successfully.';
        if(empty($loaninfo)){
            $loaninfo = new LoanInfo;
            $message = 'Save Successfully.';
        }


        $loaninfo->fill($request->all());
        $loaninfo->loan_amount        = $amount;
        $loaninfo->loan_total_amount  = $loan_total_amount;
        $loaninfo->loan_total_balance = $loan_total_balance;
        $loaninfo->terminated         = ($request->terminated) ? 1 : 0;
        if($message == 'Update Successfully.'){
            $loaninfo->updated_by = Auth::id();
        }else{
            $loaninfo->created_by = Auth::id();
        }

        $loaninfo->save();

        $data = LoanInfo::with('loans')
        ->where('employee_id',$request->employee_id)
        ->orderBy('start_date','asc')
        ->get();

        $response = json_encode(['status'=> true,'response'=> $message,'loaninfo'=> $data, 'myform'=>'myForm4']);

        return $response;
    }


    public function storeDeductioninfo(Request $request){

        $this->validate($request,[
            'deduction_id'           => 'required',
            'start_date'             => 'required',
            'deduction_amount'       => 'required',
        ]);


        $amount = (isset($request->deduction_amount)) ? str_replace(',', '', $request->deduction_amount) : 0;
        $deduction_info = DeductionInfo::find($request->id);
        $message = 'Update Successfully.';

        if(empty($deduction_info)){
            $deduction_info = new DeductionInfo;
            $message = 'Save Successfully.';
        }


        $deduction_info->fill($request->all());
        $deduction_info->deduction_amount     = $amount;
        $deduction_info->terminated         = ($request->terminated) ? 1 : 0;

        if($message == 'Update Successfully.'){
            $deduction_info->updated_by = Auth::id();
        }else{
            $deduction_info->created_by = Auth::id();
        }
        $deduction_info->save();

        $data = DeductionInfo::with('deductions')
        ->where('employee_id',$request->employee_id)
        ->orderBy('start_date','asc')
        ->get();

        $response = json_encode(['status'=> true,'response'=> $message,'deductioninfo'=> $data, 'myform'=>'myForm5']);

        return $response;
    }

    public function storeSalaryinfo(Request $request){

        if(!isset($request->employee_id)){

            $response = json_encode(['status'=>false,'response'=>'No employee selected']);

        }else{


            $salaryInfo = new SalaryInfo;

            $basic_amount_one      = (isset($request->basic_amount_one) ? str_replace(',', '', $request->basic_amount_one) : 0.00 );
            $basic_amount_two      = (isset($request->basic_amount_two) ? str_replace(',', '', $request->basic_amount_two) : 0.00);
            $salarygrade_id        =  (isset($request->salarygrade_id) ? $request->salarygrade_id : NULL);
            $jobgrade_id           = (isset($request->jobgrade_id) ? $request->jobgrade_id : NULL);
            $position_id           =  (isset($request->position_id) ? $request->position_id : NULL);
            $positionitem_id       =  (isset($request->positionitem_id) ? $request->positionitem_id : NULL);
            $step_inc              =  (isset($request->jgstep_inc) ? $request->jgstep_inc : NULL);

            if(isset($request->sgjginfo_id)){

                $salaryInfo = SalaryInfo::find($request->sgjginfo_id);

                $salaryInfo->salarygrade_id          = $salarygrade_id;
                $salaryInfo->jobgrade_id             = $jobgrade_id;
                $salaryInfo->employee_id             = $request->employee_id;
                $salaryInfo->salary_effectivity_date = $request->salary_effectivity_date;
                $salaryInfo->salary_description      = $request->salary_description;
                $salaryInfo->employee_number         = $request->employee_number;
                $salaryInfo->basic_amount_one        = $basic_amount_one;
                $salaryInfo->basic_amount_two        = $basic_amount_two;
                $salaryInfo->positionitem_id         = $positionitem_id;
                $salaryInfo->position_id             = $position_id;
                $salaryInfo->step_inc                = $step_inc;

                $salaryInfo->save();

                $response = json_encode(['status'=>true,'response'=>'Update Successfully!','salaryinfo'=> SalaryInfo::with('salarygrade','jobgrade')->where('employee_id',$request->employee_id)->orderBy('created_at','asc')->get(), 'myform'=>'myForm2']);

            }else{

                $request->validate([
                    'salary_effectivity_date'  => 'required',
                    'salary_description'       => 'required',
                ]);

                $salaryInfo->salarygrade_id          = $salarygrade_id;
                $salaryInfo->jobgrade_id             = $jobgrade_id;
                $salaryInfo->employee_id             = $request->employee_id;
                $salaryInfo->salary_effectivity_date = $request->salary_effectivity_date;
                $salaryInfo->salary_description      = $request->salary_description;
                $salaryInfo->basic_amount_one        = $basic_amount_one;
                $salaryInfo->employee_number         = $request->employee_number;
                $salaryInfo->basic_amount_two        = $basic_amount_two;
                $salaryInfo->positionitem_id         = $positionitem_id;
                $salaryInfo->position_id             = $position_id;
                $salaryInfo->step_inc                = $step_inc;

                $salaryInfo->save();

                $response = json_encode(['status'=>true,'response'=>'Save Successfully!','salaryinfo'=> SalaryInfo::with('salarygrade','jobgrade')->where('employee_id',$request->employee_id)->orderBy('created_at','asc')->get(), 'myform'=>'myForm2']);

            }
        }

        return $response;
    }


     public function getSgstep(){

        $id = Input::get('id');
        $step_no = Input::get('step_no');

        $amount = SalaryGrade::where('id',$id)->first([$step_no])->toArray();

        return $amount;
    }

    public function getJgstep(){

        $id = Input::get('id');
        $step_no = Input::get('step_no');

        $amount = JobGrade::where('id',$id)->first([$step_no])->toArray();

        return $amount;
    }

    public function getItem(){
        $id = Input::get('id');

        $query = PositionItemSetup::where('id',$id)->first();

        return json_encode($query);
    }

    //  STORE NON PLANTILLA
    public function storeNonPlantilla(Request $request){

        $this->validate($request,[
            'daily_rate_amount'     => 'required',
        ]);

        $monthly_rate_amount = ($request->monthly_rate_amount) ? str_replace(',', '', $request->monthly_rate_amount) : 0;
        $daily_rate_amount = ($request->daily_rate_amount) ? str_replace(',', '', $request->daily_rate_amount) : 0;
        $inputted_amount = ($request->inputted_amount) ? str_replace(',', '', $request->inputted_amount) : 0;
        $overtime_balance_amount = ($request->overtime_balance_amount) ? str_replace(',', '', $request->overtime_balance_amount) : 0;
        $threshhold_amount = ($request->threshhold_amount) ? str_replace(',', '', $request->threshhold_amount) : 0;


        $nonplantilla = NonPlantillaEmployeeInfo::find($request->id);
        $message = 'Update Successfully.';
        if(empty($nonplantilla)){
            $nonplantilla = new NonPlantillaEmployeeInfo;
            $message = 'Save Successfully.';
        }

        $nonplantilla->fill($request->all());
        $nonplantilla->monthly_rate_amount      = $monthly_rate_amount;
        $nonplantilla->daily_rate_amount        = $daily_rate_amount;
        $nonplantilla->overtime_balance_amount  = $overtime_balance_amount;
        $nonplantilla->inputted_amount          = $inputted_amount;
        $nonplantilla->threshhold_amount        = $threshhold_amount;
        $nonplantilla->save();


        return json_encode(['status'=> true, 'response'=> $message]);

    }

    public function deleteDeductInfo(){
        $data = Input::all();

        $employee_id    = $data['employee_id'];
        $id   = $data['id'];

        $deduct_info = new DeductionInfo;

        $deduct_info->destroy($id);

        $deduct_info = $deduct_info
        ->with('deductions')
        ->where('employee_id',$employee_id)
        ->get();

        return json_encode(['status'=>'deductions','data'=>$deduct_info]);

    }

    public function deleteBenefitinfo(){
        $data = Input::all();

        $employee_id    = $data['employee_id'];
        $id             = $data['id'];

        $benefit_info = new BenefitInfo;
        $benefit_info->destroy($id);

        $benefit_info = $benefit_info->with('benefits')
        ->where('employee_id',$employee_id)
        ->get();

        return json_encode(['status'=>'benefits','data'=>$benefit_info]);

    }

    public function deleteLoanInfo(){
        $data = Input::all();

        $employee_id    = $data['employee_id'];
        $id             = $data['id'];

        $loan_info = new LoanInfo;
        $loan_info->destroy($id);

        $loan_info = $loan_info->with('loans')->where('employee_id',$employee_id)->get();

        return json_encode(['status'=>'loans','data'=>$loan_info]);

    }

    public function computeEmployeeInfo(){

        $data = Input::all();

        $employeeinfo        = new EmployeeInfo;
        $employee            = new Employee;
        $employeeinformation = new EmployeeInformation;
        $phicpolicy          = new PhilhealthPolicy;
        $pagibigpolicy       = new PagibigPolicy;
        $philhealthpolicy    = new PhilhealthPolicy;
        // $responsibilities    = new ResponsibilityCenter;
        $employeestatus      = new EmployeeStatus;
        $gsispolicy          = new GsisPolicy;
        $taxpolicy           = new TaxPolicy;
        $benefit             = Benefit::whereIn('code',['PERA','SA','LA'])->get();

        $status = $employeestatus
        ->where('Code','P')
        ->first();

        $employee = $employee
        ->whereIn('id',$data['empid'])
        ->where('active',1)
        ->select('id')
        ->get()->toArray();

        $query = $employeeinformation
        ->whereIn('employee_id',$employee)
        ->where('employee_status_id',$status->RefId)
        ->get();

        $philhealthpolicy = $philhealthpolicy
        ->where('policy_name','STANDARD POLICY')
        ->first();

        $pagibigpolicy = $pagibigpolicy
        ->where('policy_name','STANDARD POLICY')
        ->first();

        $gsispolicy = $gsispolicy
        ->where('policy_name','STANDARD POLICY')
        ->first();

        $taxpolicy = $taxpolicy
        ->where('policy_name','ANNUAL POLICY')
        ->first();

        foreach ($query as $key => $value) {
            $employeeinfo        = new EmployeeInfo;
            $salaryinfo          = new SalaryInfo;

            // Setup PERA and SALA
            foreach ($benefit as $k => $val) {
                $benefitinfo = BenefitInfo::where('benefit_id',$val->id)
                ->where('employee_id',$value->employee_id)
                ->first();

                if(empty($benefitinfo)){
                    $benefitinfo = new BenefitInfo;
                }else{
                    $benefitinfo = BenefitInfo::find($benefitinfo->id);
                }

                $benefitinfo->benefit_id    = $val->id;
                $benefitinfo->employee_id   = $value->employee_id;

                switch ($val->code) {
                    case 'PERA':
                        $benefitinfo->benefit_amount = 2000;
                        break;
                }
                $benefitinfo->start_date = date('Y-m-d');
                $benefitinfo->created_by = Auth::id();
                $benefitinfo->save();
            }

            $emp = Employee::find($value->employee_id);

            $salaryinfo = $salaryinfo
            ->where('employee_id',@$value->employee_id)
            ->orderBy('salary_effectivity_date','desc')
            ->first();

            if(isset($salaryinfo)){

                $basicOne = @$salaryinfo->basic_amount_one;
                $basicTwo = @$salaryinfo->basic_amount_two;

                $monthlyRateAmount = $basicOne + $basicTwo;
                $dailyRateAmount  = 0;
                if(isset($monthlyRateAmount)){
                    $dailyRateAmount = $monthlyRateAmount / 22;
                }else{
                    dd('No Salary Info '.$value->employee_id);
                }

                $annualRateAmount = $monthlyRateAmount * 12;

                $phic = $this->computePhic($monthlyRateAmount);
                $gsis = $this->computeGsis($monthlyRateAmount);

                $phic_ee_share = $phic['ee_share'];
                $phic_er_share = $phic['er_share'];
                $gsis_ee_share = $gsis['ee_share'];
                $gsis_er_share = $gsis['er_share'];
                $pagibig_ee_share = 100;

                $totalDeduction = $phic_ee_share + $gsis_ee_share + $pagibig_ee_share;
                $grossSalary = $monthlyRateAmount - $totalDeduction;

                $tax = $this->computeTax($grossSalary);

                $empinfo = $employeeinfo
                ->where('employee_id',$value->employee_id)
                ->first();

                if(isset($empinfo)){

                    $employeeinfo = $employeeinfo->find($empinfo->id);

                    $employeeinfo->employee_id                  = $value->employee_id;
                    $employeeinfo->gsispolicy_id                = $gsispolicy->id;
                    $employeeinfo->pagibigpolicy_id             = $pagibigpolicy->id;
                    $employeeinfo->philhealthpolicy_id          = $philhealthpolicy->id;
                    $employeeinfo->taxpolicy_id                 = $taxpolicy->id;
                    // $employeeinfo->employee_status_id           = $value->employee_status_id;
                    $employeeinfo->daily_rate_amount            = $dailyRateAmount;
                    $employeeinfo->monthly_rate_amount          = $monthlyRateAmount;
                    $employeeinfo->annual_rate_amount           = $annualRateAmount;
                    $employeeinfo->pagibig_contribution         = $pagibig_ee_share;
                    $employeeinfo->philhealth_contribution      = $phic_ee_share;
                    $employeeinfo->gsis_contribution            = $gsis_ee_share;
                    // $employeeinfo->tax_contribution             = $tax;
                    $employeeinfo->er_pagibig_share             = $pagibig_ee_share;
                    $employeeinfo->er_philhealth_share          = $phic_er_share;
                    $employeeinfo->er_gsis_share                = $gsis_er_share;
                    $employeeinfo->overtime_balance_amount      = $annualRateAmount;

                    $employeeinfo->save();

                }else{
                    $employeeinfo->employee_number              = $emp->employee_number;
                    $employeeinfo->employee_id                  = $value->employee_id;
                    $employeeinfo->gsispolicy_id                   = $gsispolicy->id;
                    $employeeinfo->pagibigpolicy_id                = $pagibigpolicy->id;
                    $employeeinfo->philhealthpolicy_id             = $philhealthpolicy->id;
                    $employeeinfo->taxpolicy_id                    = $taxpolicy->id;
                    // $employeeinfo->employee_status_id           = $value->employee_status_id;
                    $employeeinfo->daily_rate_amount            = $dailyRateAmount;
                    $employeeinfo->monthly_rate_amount          = $monthlyRateAmount;
                    $employeeinfo->annual_rate_amount           = $annualRateAmount;
                    $employeeinfo->pagibig_contribution         = $pagibig_ee_share;
                    $employeeinfo->philhealth_contribution      = $phic_ee_share;
                    $employeeinfo->gsis_contribution            = $gsis_ee_share;
                    // $employeeinfo->tax_contribution             = $tax;
                    $employeeinfo->er_pagibig_share             = $pagibig_ee_share;
                    $employeeinfo->er_philhealth_share          = $phic_er_share;
                    $employeeinfo->er_gsis_share                = $gsis_er_share;
                    $employeeinfo->overtime_balance_amount      = $annualRateAmount;

                    $employeeinfo->save();

                }
            }
            }


        return json_encode(['status' => true, 'response' => 'Update Successfully']);

    }

}
