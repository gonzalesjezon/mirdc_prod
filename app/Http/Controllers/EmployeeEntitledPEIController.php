<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\SpecialPayrollTransaction;
class EmployeeEntitledPEIController extends Controller
{
    function __construct(){
		$this->title = 'EMPLOYEE ENTITLED TO PEI';
    	$this->module = 'employeeentitledpei';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;
	}

	public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m')
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }


    public function show(){

        $data = Input::all();

        $transaction    =  new SpecialPayrollTransaction;

        $year = $data['year'];
        $month = $data['month'];

        $query = $transaction
        ->with('employees')
        ->where('year',$year)
        ->where('month',$month)
        ->where('status','pei')
        ->get();

        $arr = [];

        foreach ($query as $key => $value) {
        	if(isset($value->division_id)){
        		$arr[$value->divisions->Name][$key] = $value;
        	}
        	if(isset($value->office_id)){
        		$arr[$value->offices->Name][$key] = $value;
        	}
        }


        return json_encode($arr);
    }
}
