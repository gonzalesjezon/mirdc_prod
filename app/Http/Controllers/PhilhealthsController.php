<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use Input;
use App\PhilhealthTable;
use App\PhilhealthPolicy;

class PhilhealthsController extends Controller
{

    function __construct(){
        $this->controller = $this;
        $this->title = 'PHILHEALTH POLICY';
        $this->module_prefix = 'payrolls/admin/filemanagers';
        $this->module = 'philhealths';

    }

    public function index(){
        $response = array(
            'controller'    => $this->controller,
            'title'         => $this->title,
            'module'        => $this->module,
            'module_prefix' => $this->module_prefix,
        );

        return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }


    public function store(Request $request){

        $philhealth_policy = new PhilhealthPolicy;
        $philhealth_policy->below = 0.0275;
        $philhealth_policy->above = 1100;

        if(isset($request->policy_id)){

            $philhealth_policy = PhilhealthPolicy::find((int)$request->policy_id);

            $philhealth_policy->fill($request->all())->save();

            $response =  json_encode(['status'=>true,'response' => 'Update Successfully!']);

        }else{

            $this->validate(request(),[
                'policy_name'           => 'required',
                'pay_period'            => 'required',
                'deduction_period'      => 'required',
                'policy_type'           => 'required',
                'based_on'              => 'required',
            ]);

            $philhealth_policy->fill($request->all())->save();

            $response =  json_encode(['status'=>true,'response' => 'Save Successfully!']);
        }

        return $response;

    }


    public function showPolicy(){

        $q = Input::get('q');
        // $limit = Input::get('limit');
        // if(empty($limit)){ $limit  =  10; }

        $data = $this->get_philpolicy_records($q);

        $response = array(
                        'data'          => $data,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix,

                        );

        return view($this->module_prefix.'.'.$this->module.'.datatablepolicy',$response);

    }


    private function get_philpolicy_records($q){

        $cols = ['policy_name','pay_period','deduction_period','policy_type'];

        $query = PhilhealthPolicy::where(function($query) use($cols,$q){

                $query = $query->where(function($qry) use($q, $cols){
                    foreach ($cols as $key => $value) {
                        $qry->orWhere($value,'like','%'.$q.'%');
                    }
                });
        });
        $response = $query->get();

        return $response;

    }

    public function getItem(){
        $id = Input::get('id');

        $query = PhilhealthPolicy::where('id',$id)->first();

        return json_encode($query);
    }

    public function delete(){
        $data = Input::all();
        $id   = $data['id'];

        $benefit = new PhilhealthPolicy;
        $benefit->destroy($id);

        return json_encode(['status'=>true]);
    }
}
