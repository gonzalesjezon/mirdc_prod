<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\LoanInfoTransaction;
use App\EmployeeInformation;
use App\Transaction;
use App\EmployeeStatus;
use App\Loan;
use App\Employee;
class LoanDetailsReportsController extends Controller
{
    function __construct(){
		$this->title = 'LOAN DETAILS REPORT';
    	$this->module = 'loandetailsreports';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;
	}

	public function index(){

		$employee = new Employee;
		$employeeinformation = new EmployeeInformation;
        $employeestatus      = new EmployeeStatus;


		$status = $employeestatus
        ->where('category',1)
        ->select('RefId')
        ->get()->toArray();

        $employee_id = $employeeinformation
        ->whereIn('employee_status_id',$status)
        ->select('employee_id')
        ->get()->toArray();

        $employee = $employee
        ->whereIn('id',$employee_id)
        ->where('active',1)
        ->where('with_setup',1)
        ->orderBy('lastname','asc')
        ->get();

    	$response = array(
    					'employee' 		=> $employee,
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m')
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }


    public function show(){

        $data = Input::all();

        $year   = $data['year'];
        $month  = $data['month'];
        $empid  = $data['id'];

        $transaction         = new LoanInfoTransaction;
        $loans               = new Loan;

        $loans = $loans
        ->where('loan_type','GSIS')
        ->select('id')
        ->get()->toArray();

        $query = $transaction
        ->with([
        		'payrollinfo',
        		'employees',
        		'loaninfo',
                'loans',
                'employeeinformation'
            ])
        ->whereIn('loan_id',$loans)
        ->where('employee_number',$empid)
		->where('year',$year)
		->get();

        $loan = [];
        foreach ($query as $key => $value) {
            $loan[$value->loans->name][$key] = $value;
        }


        return json_encode($loan);
    }
}
