<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ResponsibilityCenter;

use Input;
use Auth;
class ResponsibilitiesCenterController extends Controller
{

    function __construct(){
        $this->controller = $this;
        $this->title = 'RESPONSIBILITY CENTER';
        $this->module_prefix = 'payrolls/admin/filemanagers';
        $this->module = 'responsibilitiescenter';
        $this->table = 'pms_responsibility_center';
    }

    public function index(){

        $rcenter = ResponsibilityCenter::latest()
        ->paginate(30);

        $response = array(
            'rcenter'       => $rcenter,
            'controller'    => $this->controller,
            'title'         => $this->title,
            'module'        => $this->module,
            'module_prefix' => $this->module_prefix,
        );

        return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function store(Request $request){


        $rcenter = ResponsibilityCenter::find($request->id);
        $message = 'Update Successfully.';
        if(empty($rcenter)){
            $rcenter = new ResponsibilityCenter;
            $this->validate($request,[
                'code'                  => 'required|unique:pms_responsibility_center',
                'name'                  => 'required',
            ]);
            $message = 'Save Successfully.';
        }

        $rcenter->fill($request->all());
        if($message == 'Update Successfully.'){
            $rcenter->updated_by = Auth::id();
        }else{
            $rcenter->created_by = Auth::id();
        }
        $rcenter->save();

        return json_encode(['status'=>true,'response' => $message ]);
    }

    public function getItem(){
        $id = Input::get('id');

        $query = ResponsibilityCenter::where('id',$id)->first();

        return json_encode($query);
    }

    public function delete(){
        $data = Input::all();
        $id   = $data['id'];

        $benefit = new ResponsibilityCenter;
        $benefit->destroy($id);

        return json_encode(['status'=>true]);
    }
}