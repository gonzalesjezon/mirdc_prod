<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Input;
use App\LoanInfoTransaction;
use App\EmployeeInformation;
use App\EmployeeStatus;
class DeductedLoanReportsController extends Controller
{
    function __construct(){
		$this->title = 'DEDUCTED LOANS REPORT';
    	$this->module = 'deductedloanreports';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;
	}

	public function index(){

    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m')
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }


    public function show(){

        $data = Input::all();

        $year = $data['year'];
        $month = $data['month'];

        $transaction         = new LoanInfoTransaction;
        $employeeinformation = new EmployeeInformation;
        $employeestatus      = new EmployeeStatus;

        $status = $employeestatus
        ->where('category',1)
        ->select('RefId')
        ->get()->toArray();

        $employee_id = $employeeinformation
        ->whereIn('employee_status_id',$status)
        ->select('employee_id')
        ->get()->toArray();

        $query = $transaction
        ->with([
        		'employeeinformation',
        		'employees',
        		'loans',
            ])
        ->whereIn('employee_id',$employee_id)
		->where('year',$year)
		->where('month',$month)
		->get();

        $loan = [];
        foreach ($query as $key => $value) {
            $loan[$value->employee_number][$key] = $value;
        }


        return json_encode($loan);
    }
}
