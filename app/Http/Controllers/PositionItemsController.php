<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Auth;
use App\Position;
use App\PositionItem;
class PositionItemsController extends Controller
{
    function __construct(){
    	$this->title 		 = 'POSITION ITEM SETUP';
    	$this->module_prefix = 'payrolls/admin/filemanagers';
    	$this->module 		 = 'position_items';
    	$this->controller 	 = $this;
    }

    public function index(){

    	$response = array(
    					'positions'			=> Position::orderBy('name','asc')->get(),
						'module'        	=> $this->module,
						'controller'    	=> $this->controller,
		                'module_prefix' 	=> $this->module_prefix,
						'title'		    	=> $this->title
					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q = Input::get('q');

        $data = $this->get_records($q);

        $response = array(
                        'data'          => $data,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix,
                    );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);

    }

    private function get_records($q){

        $cols = ['code','name'];

        $query = PositionItem::with('positions')->where(function($query) use($cols,$q){

                $query = $query->where(function($qry) use($q, $cols){
                    foreach ($cols as $key => $value) {
                        $qry->orWhere($value,'like','%'.$q.'%');
                    }
                });
        });
        $response = $query->orderBy('name','asc')->get();

        return $response;

    }

    public function store(Request $request){

    	

    	$positionitems = new PositionItem;

    	if(isset($request->position_item_id)){

    		$positionitems = PositionItem::find($request->position_item_id);

	    	$positionitems->code 	   	= $request->code;
	    	$positionitems->name 	   	= $request->name;
	    	$positionitems->position_id = $request->position_id;
	    	$positionitems->updated_by 	= Auth::User()->id;

	    	$positionitems->save();

	    	$response = json_encode(['status'=>true,'response'=>'Update Successfully!']);
    	}else{

            $this->validate($request,[
                'code'  => 'required|unique:pms_position_items',
                'name'  => 'required'
            ]);

    		$positionitems->code 	   	= $request->code;
	    	$positionitems->name 	   	= $request->name;
	    	$positionitems->position_id = $request->position_id;
	    	$positionitems->created_by 	= Auth::User()->id;

	    	$positionitems->save();

	    	$response = json_encode(['status'=>true,'response'=>'Save Successfully!']);
    	}


    	return $response;
    }
}
