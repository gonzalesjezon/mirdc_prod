<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Transaction;
use App\BenefitInfo;
use App\Benefit;
class EmeReportsController extends Controller
{
    function __construct(){
		$this->title = 'EXTRAORDINARY AND MISCELLANEOUS EXPENSES';
    	$this->module = 'eme';
        $this->module_prefix = 'payrolls/reports/othercompensations';
    	$this->controller = $this;
	}

	public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }


    public function getEmployeeinfo(){

        $q = Input::all();

        $benefits = Benefit::whereIn('code',['ME','EE'])->select('id')->get()->toArray();

        $benefitinfo = BenefitInfo::select('employeeinfo_id')->whereIn('benefit_id',$benefits)->get()->toArray();

        $query = Transaction::with(['employees','positionitems'=>function($qry){ $qry->with('positions'); },'offices','salaryinfo'])->where('year',$q['year'])->where('month',$q['month'])->whereIn('employeeinfo_id',$benefitinfo);

        $query = $query->with(['employeeinfo'=>function($qry){
                                $qry = $qry->with(['benefitinfo' => function($qry){
                            	   $qry->with('benefits')->whereHas('benefits', function($qry){
                            		  $qry->whereIn('code',['ME','EE']);
                            	});
                            }]);
                        }])->orderBy('office_id','desc')->get();
        $data = [];

        foreach ($query as $key => $value) {
            $data[$value->offices->name][$key] = $value;
        }

        return json_encode($data);
    }
}
