<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Employee;
use App\LoanInfoTransaction;
use App\Transaction;
use App\EmployeeInfo;
use App\BenefitInfoTransaction;
use App\DeductionInfoTransaction;
use App\SalaryInfo;
use App\Company;
use App\Office;
use App\Division;
use App\Department;
use App\Position;
use App\AttendanceInfo;
use App\OvertimePay;
use App\EmployeeStatus;
use App\EmployeeInformation;
use App\PostedReport;

use Input;
use Auth;

class PayslipsController extends Controller
{
    function __construct(){
    	$this->title = 'PAYSLIP';
    	$this->module = 'payslips';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){

        $employee_status        = new EmployeeStatus;
        $employee_information   = new EmployeeInformation;
        $employee               = new Employee;

        $empstatus_id = $employee_status->where('category',1)->select('RefId')->get()->toArray();
        $employee_id  = $employee_information->whereIn('employee_status_id',$empstatus_id)->select('employee_id')->get()->toArray();

        $employeeinfo = $employee->whereIn('id',$employee_id)->orderBy('lastname','asc')->get();
        $posted = PostedReport::where('report_type','payslip')->latest()->get();

    	$response = array(
                        'employeeinfo'       => $employeeinfo,
    					'module'             => $this->module,
    					'controller'         => $this->controller,
                        'module_prefix'      => $this->module_prefix,
    					'title'		         => $this->title,
                        'months'             => config('params.months'),
                        'latest_year'        => $this->latestYear(),
                        'earliest_year'      => $this->earliestYear(),
                        'current_month'      => (int)date('m'),
                        'divisions'          => Division::orderBy('Name','asc')->get(),
                        'posted'             => $posted
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){
        $data = Input::all();

        $year        = @$data['year'];
        $month       = @$data['month'];
        $employee_id = @$data['id'];
        $division_id = @$data['division_id'];

        $query = Transaction::leftJoin('pms_employees as e','e.id','pms_transactions.employee_id')
                ->with([
                    'employees',
                    'positionitems',
                    'positions',
                    'offices',
                    'departments',
                    'divisions',
                    'loanTransaction' => function($qry) use($year,$month){
                        $qry = $qry->where('year',$year)->where('month',$month);
                    },
                    'deductionTransaction' => function($qry) use($year,$month){
                        $qry = $qry->where('year',$year)->where('month',$month);
                    },
                    'benefitTransaction' => function($qry) use($year,$month){
                        $qry = $qry->where('year',$year)->where('month',$month);
                    }
                ]);

        if(isset($employee_id)){
            $query = $query->where('employee_id',$employee_id);
        }

        if(isset($division_id)){
            $query = $query->where('division_id',$division_id);
        }

        $query = $query->where('year',$year)
                ->where('month',$month)
                ->orderBy('e.lastname','asc')
                ->get();

        return json_encode([
            'transaction' =>  $query
        ]);

    }

    /*
    * Post Payslip
    */
    public function store(Request $request){

        $this->validate($request,[
            'division_id' => 'required'
        ]);

        $transaction = Transaction::where('year',$request->year)
        ->where('division_id',$request->division_id)
        ->where('month',$request->month)
        ->get();

        if(count($transaction) > 0){

            foreach ($transaction as $key => $value) {

                $posted = PostedReport::where('year',$request->year)
                ->where('employee_id',$value->employee_id)
                ->where('month',$request->month)
                ->where('report_type','payslip')
                ->first();

                if(!$posted){
                    $posted = new PostedReport;
                    $posted->fill($request->all());
                    $posted->employee_id    = $value->employee_id;
                    $posted->report_type    = 'payslip';
                    $posted->created_by     = Auth::id();

                    if($posted->save()){
                        $transaction = Transaction::where('year',$request->year)
                        ->where('month',$request->month)
                        ->where('employee_id',$value->employee_id)
                        ->update(['posted' => 1]);
                    }
                }
            }

            $response = json_encode(['status'=>true,'response'=> 'Report Posted Successfully.']);
        }else{
            $response = json_encode(['status'=>false,'response'=> 'No Transaction Found.']);
        }

        return $response;
    }




}
