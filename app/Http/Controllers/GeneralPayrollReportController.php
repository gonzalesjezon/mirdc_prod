<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Auth;
use App\Transaction;
use App\PostedReport;
class GeneralPayrollReportController extends Controller
{
    function __construct(){
    	$this->title = 'GENERAL PAYROLL REPORT';
    	$this->module = 'generalpayroll';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){


        $posted = PostedReport::where('report_type','general_payroll')->latest()->get();
    	$response = array(
                        'posted'            => $posted,
    					'module'            => $this->module,
    					'controller'        => $this->controller,
                        'module_prefix'     => $this->module_prefix,
    					'title'		        => $this->title,
                        'months'            => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m')
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q = Input::all();

        $transaction = new Transaction;

        $year  = @$q['year'];
        $month = @$q['month'];

        $query = $transaction
        ->with([
            'employees',
            'positionitems',
            'positions',
            'employeeinfo',
            'longevity',
            'offices'=>function($qry){
                $qry->orderBy('name','asc');
            },
            'employeeinformation',
            'salaryinfo',
            // 'gettax' =>function($qry) use($year,$month){
            //     $qry = $qry->where('for_year',$year)
            //     ->where('for_month',$month);
            // },
            'deductionTransaction'=>function($qry) use($year,$month){
                $qry = $qry->where('year',$year)
                ->where('month',$month);
            },
            'benefitTransaction'=>function($qry) use($year,$month){
                $qry = $qry->where('year',$year)
                ->where('month',$month);
            },
            'loanTransaction' =>function($qry) use($year,$month){
                $qry = $qry->where('year',$year)
                ->where('month',$month);
            }
        ])
        ->where('year',$year)
        ->where('month',$month)
        ->get();

        // $data = [];
        // if(count($query) > 0){
        //     foreach ($query as $key => $value) {
        //         $data[$value->offices->name][$key] = $value;
        //     }

        // }else{
        //     $data = [];
        // }

        // $data3  = [];
        // $ctr    = 1;
        // $ctr2   = 1;
        // foreach ($query as $key => $value) {

        //     if($ctr2 > 2){ // page number
        //         $ctr++;
        //         $ctr2 = 1;
        //     }
        //     $data3[$ctr][$ctr2] = $value;
        //     $ctr2++;

        // }

        return json_encode($query);
    }
    /*
    * Post General Payroll
    */
    public function store(Request $request){


        $ifPosted = PostedReport::where('year',$request->year)
        ->where('month',$request->month)
        ->where('report_type','general_payroll')
        ->first();

        if(isset($ifPosted)){
            $response = json_encode(['status'=>false,'response'=> 'Already posted.']);
        }else{


            $hasTransaction = Transaction::where('year',$request->year)
            ->where('month',$request->month)->get();

            if(count($hasTransaction) > 0){

                // $signatory = new Signatory;
                // $signatory->fill($request->all());
                // $signatory->created_by = Auth::id();

                // if($signatory->save()){
                    $posted = new PostedReport;
                    $posted->fill($request->all());
                    // $posted->signatory_id   = $signatory->id;
                    $posted->report_type    = 'general_payroll';
                    $posted->created_by     = Auth::id();

                    if($posted->save()){
                        $transaction = Transaction::where('year',$request->year)
                        ->where('month',$request->month)
                        ->update(['posted' => 1]);
                    }
                // }
                $response = json_encode(['status'=>true,'response'=> 'Report posted successfully.']);
            }else{
                $response = json_encode(['status'=>false,'response'=> 'No Transaction Found']);
            }

        }

        return $response;
    }
}
