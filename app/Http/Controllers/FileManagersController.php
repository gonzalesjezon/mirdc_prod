<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FileManagersController extends Controller
{
    
    function __construct(){
    	$this->module_prefix = 'payrolls';
    	$this->module = 'filemanagers';
    	$this->title = 'FILE MANAGER';
    	$this->controller = $this;
    }

    public function index(){

    	$response = array(
    					'module' => $this->module,
    					'module_prefix' => $this->module_prefix,
    					'title'			=> $this->title,
    					'controller'	=> $this->controller, 
    				);
        
    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }
}
