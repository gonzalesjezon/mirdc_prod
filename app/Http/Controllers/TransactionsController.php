<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Input;
use Crypt;
use Auth;
use App\Employee;
use App\GsisPolicy;
use App\PhilhealthPolicy;
use App\PagibigPolicy;
use App\TaxPolicy;
use App\Bank;
use App\BankBranch;
use App\Department;
use App\Company;
use App\Position;
use App\Office;
use App\Division;
use App\EmployeeStatus;
use App\EmployeeInformation;
use App\Benefit;
use App\PositionItemSetup;
use App\TaxTable;
use App\WageRate;
use App\BenefitInfo;
use App\SalaryInfo;
use App\LoanInfo;
use App\DeductionInfo;
use App\Loan;
use App\Deduction;
use App\EmployeeInfo;
use App\Transaction;
use Carbon\Carbon;
use App\AttendanceInfo;
use App\BenefitInfoTransaction;
use App\LoanInfoTransaction;
use App\DeductionInfoTransaction;
use App\AnnualTaxRate;
use Session;
use App\Rate;
use App\LongevityPay;
use App\EmployeeCreditBalance;
use App\DTRProcess;
use App\PostedReport;

class TransactionsController extends Controller
{

    function __construct(){
    	$this->title = 'REGULAR PAYROLL';
    	$this->module = 'transactions';
        $this->module_prefix = 'payrolls';
    	$this->controller = $this;

    }

    public function index(){

        $benefit        = Benefit::where('remarks','pdiem')->orderBy('name','asc')->get();
        $loans          = Loan::orderBy('name','asc')->get();
        $deduction      = Deduction::orderBy('name','asc')->get();
        $hp_rate        = Rate::where('status','hp')->orderBy('rate','asc')->get();


    	$response = array(
                        'hp_rate'       => $hp_rate,
                        'benefit'       => $benefit,
                        'loans'         => $loans,
                        'deductions'     => $deduction,
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'divisions'          => Division::orderBy('Name','asc')->get(),
                       'current_month'      => (int)date('m')
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q              = Input::get('q');
        $year           = Input::get('year');
        $month          = Input::get('month');
        $_year          = Input::get('_year');
        $_month         = Input::get('_month');
        $subperiod      = Input::get('subperiod');
        $period         = Input::get('period');
        $checkpayroll   = Input::get('checkpayroll');
        $check_payroll  = Input::get('check_payroll');
        $divisionId     = Input::get('division');

        $data = $this->searchName($q,$check_payroll,$_year,$_month, $divisionId);

        if(isset($year) || isset($month) || isset($checkpayroll)){
            $data = $this->filter($year,$month,$checkpayroll, $divisionId);
        }

        $response = array(
                        'data'          => $data,
                        'title'         => $this->title,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix
                    );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);

    }
    
    public function searchName($q,$checkpayroll,$year,$month , $divisionId){

        $employee_status            = new EmployeeStatus;
        $employee_info              = new EmployeeInformation;
        $employee                   = new Employee;
        $salaryinfo                 = new SalaryInfo;
        $transaction                = new Transaction;

        $cols = ['lastname','firstname'];

        $empstatus_id = $employee_status->where('category',1)->select('RefId')->get()->toArray();

        $employee_info_id = EmployeeInformation::whereIn('employee_status_id',$empstatus_id);
        if($divisionId){
            $employee_info_id = $employee_info_id->where('division_id', $divisionId);

        }
        $employee_info_id = $employee_info_id->pluck('employee_id')
        ->toArray();

        // $salary_info_employee_id = $salaryinfo->select('employee_id')->get()->toArray();

        $query = [];
        switch ($checkpayroll) {
            case 'wpayroll':

               $transaction = Transaction::select('employee_id');
               if($divisionId){
                 $transaction = $transaction->where('division_id', $divisionId);
               }
               $transaction = $transaction->where('year',$year)
               ->where('month',$month)
               ->get()
               ->toArray();

               $query = $employee->whereIn('id',$transaction);

                break;

            case 'wopayroll':

               $transaction = $transaction
               ->select('employee_id')
               ->where('year',$year)
               ->where('month',$month)
               ->get()
               ->toArray();

                $query = $employee
                ->whereNotIn('id',$transaction)
                ->whereIn('id',$employee_info_id);

                break;

            default:
                // $employee_id = $transaction->select('employee_id')->get()->toArray();
                $query = $employee->whereIn('id',$employee_info_id);

                break;
        }


        $query = $query->where(function($qry) use($q, $cols){
            foreach ($cols as $key => $value) {
                $qry->orWhere($value,'like','%'.$q.'%');
            }
        });

        $response = $query->where('active',1)->orderBy('lastname','asc')->get();

        return $response;

    }

    public function filter($year,$month,$checkpayroll, $divisionId){

        $employee_status        = new EmployeeStatus;
        $employee_information   = new EmployeeInformation;
        $transaction            = new Transaction;
        $employee               = new Employee;
        $salaryinfo             = new SalaryInfo;

        $empstatus_id = $employee_status->where('category',1)->select('RefId')->get()->toArray();
        $employee_id  = EmployeeInformation::whereIn('employee_status_id',$empstatus_id);
        if($divisionId){
            $employee_id = $employee_id->where('division_id', $divisionId);
        }
        $employee_id = $employee_id->pluck('employee_id')
        ->toArray();
        
        $salary_info_employee_id = $salaryinfo->select('employee_id')->get()->toArray();

        $query = [];
        $response = "";
        switch ($checkpayroll) {
            case 'wpayroll':

                $query =  Transaction::select('employee_id');

                    if($divisionId){
                        $query = $query->where('division_id',$divisionId);   
                    }

                    if(isset($year)){
                        $query = $query->where('year',$year);

                    }
                    if(isset($month)){
                        $query = $query->where('month',$month);
                    }

                    $query = $query->get()->toArray();

                    $response = $employee->whereIn('id',$query)->where('active',1)
                                        ->orderBy('lastname','asc')->get();

                break;

            case 'wopayroll':

                 $query =  $transaction->select('employee_id');

                    if(count($empstatus_id) > 0){
                        $query = $query->whereIn('empstatus_id',$empstatus_id);
                    }

                    if(isset($year)){
                        $query = $query->where('year',$year);

                    }
                    if(isset($month)){
                        $query = $query->where('month',$month);
                    }

                    $query = $query->get()->toArray();

                    $response = $employee->whereIn('id',$employee_id)
                                        ->whereIn('id',$salary_info_employee_id)
                                        ->whereNotIn('id',$query)
                                        ->where('active',1)
                                        ->orderBy('lastname','asc')->get();

                break;
        }
        return $response;
    }

    public function showBenefitinfo(){


        $response       = array(
                            'controller'    => $this->controller,
                            'module'        => $this->module,
                            'module_prefix' => $this->module_prefix,
                        );

        return view($this->module_prefix.'.'.$this->module.'.benefitdatatable',$response);
    }

    public function showLoaninfo(){


        $response       = array(
                            'controller'    => $this->controller,
                            'module'        => $this->module,
                            'module_prefix' => $this->module_prefix,
                        );

        return view($this->module_prefix.'.'.$this->module.'.loandatatable',$response);
    }

     public function showDeductioninfo(){


        $response       = array(
                            'controller'    => $this->controller,
                            'module'        => $this->module,
                            'module_prefix' => $this->module_prefix,
                        );

        return view($this->module_prefix.'.'.$this->module.'.deductdatatable',$response);
    }



     public function getSearchby(){
        $q = Input::get('q');

        $query = "";
        switch ($q) {
            case 'company':
                $query = Company::orderBy('name','asc')->get();
                break;
            case 'department':
                $query = Department::orderBy('name','asc')->get();
                break;
            case 'office':
                $query = Office::orderBy('name','asc')->get();
                break;
            case 'division':
                $query = Division::orderBy('name','asc')->get();
                break;
            case 'position':
                $query = Position::orderBy('name','asc')->get();
                break;

            default:
                # code...
                break;
        }

        return json_encode($query);
    }

    public function getEmployeesinfo(){

        $data = Input::all();

        $employee_id = @$data['id'];
        $employee_number = @$data['employee_number'];

        $transaction    = new Transaction;
        $employeeinfo   = new EmployeeInfo;
        $salaryinfo     = new SalaryInfo;
        $benefitinfo    = new BenefitInfoTransaction;
        $deductioninfo  = new DeductionInfoTransaction;
        $loaninfo       = new LoanInfoTransaction;
        $benefit        = new Benefit;

        $query['transaction'] = $transaction->with('employees')
                            ->where('year',@$data['year'])
                            ->where('month',@$data['month'])
                            // ->where('employee_number',@$employee_number)
                            ->where('employee_id',@$employee_id)
                            ->first();

        $query['employeeinfo'] = $employeeinfo
                            ->where('employee_id',@$employee_id)
                            // ->where('employee_number',@$employee_number)
                            ->first();

        $effectivity_date = Carbon::now()->toDateTimeString();
        $query['salaryinfo'] = $salaryinfo
                            // ->where('employee_number',@$employee_number)
                            ->where('employee_id',@$employee_id)
                            ->where('salary_effectivity_date','<=',$effectivity_date)
                            ->first();

        $query['benefitinfo'] = $benefitinfo->with('benefits','benefitinfo')
                                ->where('employee_id',@$employee_id)
                                // ->where('employee_number',@$employee_number)
                                ->where('year',@$data['year'])
                                ->where('month',@$data['month'])
                                ->get();

        $query['deductioninfo'] = $deductioninfo->with('deductions','deductioninfo')
                                ->where('employee_id',@$employee_id)
                                // ->where('employee_number',@$employee_number)
                                ->where('year',@$data['year'])
                                ->where('month',@$data['month'])
                                ->get();

        $query['loaninfo'] = $loaninfo
                            ->with('loans','loaninfo')
                            // ->where('employee_number',@$employee_number)
                            ->where('employee_id',@$employee_id)
                            ->where('year',@$data['year'])
                            ->where('month',@$data['month'])
                            ->get();

        // $year  = $data['year'];
        // $month = $data['month'];

        // $y = date('Y', strtotime($year));
        // $m = date('m', strtotime($month));

        // $workdays = $this->countDays($y,$m,array(0,6));

        // $query['workdays'] = $workdays;

        return json_encode($query);
    }

    public function updatePayroll(){
        $data = Input::all();

        $year = $data['year'];
        $month = $data['month'];

        $actual_basicpay_amount     = ($data['summary']['actual_basicpay']) ? str_replace(',', '', $data['summary']['actual_basicpay']) : 0;
        $adjust_basicpay_amount     = ($data['summary']['adjust_basicpay']) ? str_replace(',', '', $data['summary']['adjust_basicpay']) : 0;
        $total_basicpay_amount      = ($data['summary']['total_basicpay']) ? str_replace(',', '', $data['summary']['total_basicpay']) : 0;

        $actual_absences_amount     = ($data['summary']['actual_absences']) ? str_replace(',', '', $data['summary']['actual_absences']) : 0;
        $adjust_absences_amount     = ($data['summary']['adjust_absences']) ? str_replace(',', '', $data['summary']['adjust_absences']) : 0;
        $total_absences_amount      = ($data['summary']['total_absences']) ? str_replace(',', '', $data['summary']['total_absences']) : 0;

        $actual_tardines_amount     = ($data['summary']['actual_tardines']) ? str_replace(',', '', $data['summary']['actual_tardines']) : 0;
        $adjust_tardines_amount     = ($data['summary']['total_tardines']) ? str_replace(',', '', $data['summary']['adjust_tardines']) : 0;
        $total_tardines_amount      = ($data['summary']['total_tardines']) ? str_replace(',', '', $data['summary']['total_tardines']) : 0;

        $actual_undertime_amount     = ($data['summary']['actual_undertime']) ? str_replace(',', '', $data['summary']['actual_undertime']) : 0;
        $adjust_undertime_amount     = ($data['summary']['adjust_undertime']) ? str_replace(',', '', $data['summary']['adjust_undertime']) : 0;
        $total_undertime_amount      = ($data['summary']['total_undertime']) ? str_replace(',', '', $data['summary']['total_undertime']) : 0;

        $actual_contribution     = ($data['summary']['actual_contribution']) ? str_replace(',', '', $data['summary']['actual_contribution']) : 0;
        $adjust_contribution     = ($data['summary']['adjust_contribution']) ? str_replace(',', '', $data['summary']['adjust_contribution']) : 0;
        $total_contribution      = ($data['summary']['total_contribution']) ? str_replace(',', '', $data['summary']['total_contribution']) : 0;

        $actual_loan     = ($data['summary']['actual_loan']) ? str_replace(',', '', $data['summary']['actual_loan']) : 0;
        $adjust_loan     = ($data['summary']['adjust_loan']) ? str_replace(',', '', $data['summary']['adjust_loan']) : 0;
        $total_loan      = ($data['summary']['total_loan']) ? str_replace(',', '', $data['summary']['total_loan']) : 0;

        $actual_otherdeduct     = ($data['summary']['actual_otherdeduct']) ? str_replace(',', '', $data['summary']['actual_otherdeduct']) : 0;
        $adjust_otherdeduct     = ($data['summary']['adjust_otherdeduct']) ? str_replace(',', '', $data['summary']['adjust_otherdeduct']) : 0;
        $total_otherdeduct      = ($data['summary']['total_otherdeduct']) ? str_replace(',', '', $data['summary']['total_otherdeduct']) : 0;

        $basic_net_pay      = ($data['summary']['basic_net_pay']) ? str_replace(',', '', $data['summary']['basic_net_pay']) : 0;
        $net_deduction      = ($data['summary']['net_deduction']) ? str_replace(',', '', $data['summary']['net_deduction']) : 0;
        $gross_pay      = ($data['summary']['gross_pay']) ? str_replace(',', '', $data['summary']['gross_pay']) : 0;
        $gross_taxable_pay      = ($data['summary']['gross_taxable_pay'])? str_replace(',', '', $data['summary']['gross_taxable_pay']) : 0;
        $net_pay      = ($data['summary']['net_pay']) ? str_replace(',', '', $data['summary']['net_pay']) : 0;

        $adjust_workdays = $data['attendance']['adjust_workdays'];
        $actual_absences = $data['attendance']['actual_absences'];
        $adjust_absences = $data['attendance']['adjust_absences'];
        $actual_tardiness = $data['attendance']['actual_tardines'];
        $adjust_tardiness = $data['attendance']['adjust_tardines'];
        $actual_undertime = $data['attendance']['actual_undertime'];
        $adjust_undertime = $data['attendance']['adjust_undertime'];

        if(isset($actual_absences)){
            $benefitinfo = new BenefitInfoTransaction;
            $benefitinfo = $benefitinfo->find($data['pera_id']);

            $old_pera_amount = $benefitinfo->amount;
            $daily_pera_amount = $old_pera_amount / 22;
            $deduct_pera_amount = $daily_pera_amount * $actual_absences;
            $new_pera_amont = $old_pera_amount - $deduct_pera_amount;
            $benefitinfo->amount = $new_pera_amont;
            $benefitinfo->save();
        }

        $salaryinfo   =  new SalaryInfo;
        $employeeinfo =  new EmployeeInfo;

        $salaryinfo = $salaryinfo
        ->where('employee_id',$data['employee_id'])
        ->first();

        $employeeinfo = $employeeinfo
        ->where('employee_id',$data['employee_id'])
        ->first();

        $dateNow = Carbon::now();

        $y = date('Y', strtotime($year));
        $m = date('m', strtotime($month));

        $daysInAMonth = cal_days_in_month(CAL_GREGORIAN,$month,$year); // gsis days

        // daily rate = monthly salary / days in a month
        // lwop days = days in a month - lwop days
        // gsis amount = lwop days * daily rate

        // ee_share = gsis amount * 9%;
        // er_share = gsis amount * 12%;

        // $workdays = 22;
        $eeShare = 0;
        $erShare = 0;
        if(isset($data['summary']['actual_absences'])){

            $dailyRate = $total_basicpay_amount / $daysInAMonth;
            $lwopDays = $daysInAMonth - $actual_absences;
            $gsisAmount = $lwopDays * $dailyRate;

            $eeShare = $gsisAmount * 0.09;
            $erShare = $gsisAmount * 0.12;

        }else{
            $eeShare = ($data['gsis_ee_share']) ? str_replace(',', '', $data['gsis_ee_share']) : 0;
            $erShare = ($data['gsis_er_share']) ? str_replace(',', '', $data['gsis_er_share']) : 0;
        }


        $transactions = Transaction::find($data['transaction_id']);

        $transactions->gsis_ee_share              = $eeShare;
        $transactions->gsis_er_share              = $erShare;
        $transactions->actual_basicpay_amount     = $actual_basicpay_amount;
        $transactions->adjust_basicpay_amount     = $adjust_basicpay_amount;
        $transactions->total_basicpay_amount      = $total_basicpay_amount;
        $transactions->actual_absences_amount     = $actual_absences_amount;
        $transactions->adjust_absences_amount     = $adjust_absences_amount;
        $transactions->total_absences_amount      = $total_absences_amount;
        $transactions->actual_tardines_amount     = $actual_tardines_amount;
        $transactions->adjust_tardines_amount     = $adjust_tardines_amount;
        $transactions->total_tardines_amount      = $total_tardines_amount;
        $transactions->actual_undertime_amount    = $actual_undertime_amount;
        $transactions->adjust_undertime_amount    = $adjust_undertime_amount;
        $transactions->total_undertime_amount     = $total_undertime_amount;
        $transactions->actual_absences_amount     = $actual_absences_amount;
        $transactions->adjust_workdays            = $adjust_workdays;
        $transactions->actual_absences            = $actual_absences;
        $transactions->adjust_absences            = $adjust_absences;
        $transactions->actual_tardiness           = $actual_tardiness;
        $transactions->adjust_tardiness           = $adjust_tardiness;
        $transactions->actual_undertime           = $actual_undertime;
        $transactions->adjust_undertime           = $adjust_undertime;
        $transactions->basic_net_pay              = $basic_net_pay;
        $transactions->actual_contribution        = $actual_contribution;
        $transactions->adjust_contribution        = $adjust_contribution;
        $transactions->total_contribution         = $total_contribution;
        $transactions->actual_loan                = $actual_loan;
        $transactions->adjust_loan                = $adjust_loan;
        $transactions->total_loan                 = $total_loan;
        $transactions->actual_otherdeduct         = $actual_otherdeduct;
        $transactions->adjust_otherdeduct         = $adjust_otherdeduct;
        $transactions->total_otherdeduct          = $total_otherdeduct;
        $transactions->net_deduction              = $net_deduction;
        $transactions->gross_pay                  = $gross_pay;
        $transactions->gross_taxable_pay          = $gross_taxable_pay;
        $transactions->net_pay                    = $net_pay;

        $transactions->save();


        $response = json_encode(['status'=>true,'response'=>'Payroll Updated Successfully!']);

        return $response;

    }

    public function processPayroll() {

        $data = Input::all();

        $transactions =  new Transaction;

        // dd($data['empid']);
        $ctr = 0;
        $error = 0;
        $error_id = [];
        foreach ($data['empid'] as $key => $_id) {

            if(isset($_id)){
                $check = DTRProcess::where('EmployeesRefId',$_id)->first();
                if($check === null){
                    $error++;
                    $empNumber = Employee::where('id',$_id)->select('employee_number')->first();
                    $error_id[$key] = $empNumber->employee_number;
                }
            }
        }

        $year  = $data['year'];
        $month = $data['month'];

        $y = date('Y', strtotime($year));
        $m = date('m', strtotime($month));

        $workdays = $this->countDays($y,$m,array(0,6));
        $daysInAMonth = cal_days_in_month(CAL_GREGORIAN,$month,$year); // gsis days
        if($workdays < 22){
            $workdays = 21;
        }

        $effectivity_date    = Carbon::now()->toDateTimeString();

        if($error == 0){
            foreach ($data['empid'] as $key => $_id) {
                if(isset($_id)){
                    $employeeinfo        = new EmployeeInfo;
                    $salaryinfo          = new SalaryInfo;
                    $employeeinformation = new EmployeeInformation;
                    $loaninfo            = new LoanInfo;
                    $taxrates            = new AnnualTaxRate;
                    $longevitypay        = new LongevityPay;

                    $employeeinfo = $employeeinfo
                    ->with('taxpolicy')
                    ->where('employee_id',$_id)
                    ->first();

                    $salaryinfo = $salaryinfo
                    ->where('salary_effectivity_date','<=',$effectivity_date)
                    ->where('employee_id',$_id)
                    ->orderBy('salary_effectivity_date','desc')
                    ->first();

                    $employeeinformation = $employeeinformation
                    ->where('employee_id',$_id)
                    ->first();
                    $dailyRateAmount = $employeeinfo->daily_rate_amount;

                    $share = $this->compute_lwop($employeeinfo,$_id,$dailyRateAmount,$y,$m,$daysInAMonth);
                    $eeShare = $share['eeShare'];
                    $erShare = $share['erShare'];
                    $lwopCounted = $share['lwopCounted'];

                    $longevitypay = $longevitypay
                    ->where('employee_id',$_id)
                    ->orderBy('longevity_date','desc')
                    ->first();

                    $totalContribution = (@$employeeinfo->pagibig_contribution + @$employeeinfo->philhealth_contribution + @$eeShare + @$employeeinfo->pagibig2 + @$employeeinfo->pagibig_personal);


                    $taxAmount = 0;
                    if(isset($employeeinfo->taxpolicy->name) === 'STANDARD POLICY'){

                        $taxAmount = @$employeeinfo->tax_contribution;

                    }else{

                        $taxrates = $taxrates
                        ->where('employee_id',$_id)
                        ->where('for_year',$year)
                        ->where('for_month',$month)
                        ->select('tax_amount')
                        ->first();

                        $taxAmount = @$taxrates->tax_amount;

                    }

                    $totaBenefit = $this->storeBenefitTransation($_id,$year,$month,$workdays,$lwopCounted);
                    $totalDeduction = $this->storeDeduction($_id,$year,$month,$employeeinformation->division_id);
                    $totalLoan = $this->storeLoaninfoTransaction($_id,$year,$month,$employeeinformation->division_id);

                    $netDeduction = $totalContribution + $totalLoan + $totalDeduction;
                    $basic_amount = $employeeinfo->monthly_rate_amount;

                    $total_absences_amount = $dailyRateAmount * $lwopCounted;
                    $actual_absences = ($lwopCounted) ? $lwopCounted : NULL;

                    $basicNetPay = $basic_amount - $total_absences_amount;

                    $gross_taxable = ((float)$basicNetPay - (float)$totalContribution);

                    $net_pay = ($gross_taxable + 2000 ) - (@$taxAmount);

                    $gross_pay = ($basicNetPay + 2000 + @$totaBenefit + @$longevitypay->longevity_amount);

                    $transactions->employee_id        = $_id;
                    $transactions->total_absences_amount  = $total_absences_amount;
                    $transactions->salaryinfo_id      = $salaryinfo->id;
                    $transactions->pagibig_share      = @$employeeinfo->pagibig_contribution;
                    $transactions->phic_share         = @$employeeinfo->philhealth_contribution;
                    $transactions->mp1_amount         = @$employeeinfo->pagibig_personal;
                    $transactions->mp2_amount         = @$employeeinfo->pagibig2;
                    $transactions->gsis_ee_share      = $eeShare;
                    $transactions->gsis_er_share      = $erShare;
                    $transactions->actual_absences    = $lwopCounted;
                    $transactions->position_id        = $employeeinformation->position_id;
                    $transactions->division_id        = $employeeinformation->division_id;
                    $transactions->company_id         = $employeeinformation->company_id;
                    $transactions->position_item_id   = $employeeinformation->position_item_id;
                    $transactions->office_id          = $employeeinformation->office_id;
                    $transactions->department_id      = $employeeinformation->department_id;
                    $transactions->empstatus_id       = $employeeinformation->employee_status_id;
                    $transactions->actual_workdays    = $workdays;
                    $transactions->tax_amount         = @$taxAmount;
                    $transactions->total_contribution = $totalContribution;
                    $transactions->ecc_amount         = 100;
                    $transactions->total_loan         = (float)$totalLoan;
                    $transactions->total_otherdeduct  = (float)$totalDeduction;
                    $transactions->net_deduction      = $netDeduction;
                    $transactions->basic_net_pay      = (float)$basicNetPay;
                    $transactions->actual_basicpay_amount = (float)$basic_amount;
                    $transactions->total_basicpay_amount  = (float)$basic_amount;
                    $transactions->net_pay            = $net_pay;
                    $transactions->gross_taxable_pay  = $gross_taxable;
                    $transactions->gross_pay          = $gross_pay;
                    $transactions->year               = $year;
                    $transactions->month              = $month;

                    Transaction::create($transactions->toArray());

                    $ctr++;
                }
            }
            $response = json_encode(['status'=>true,'response'=>'Processed Successfully! <br>'.$ctr.' Records Saved']);
        }else{
            $error_id = implode(',', $error_id);
            $response = json_encode(['status'=>false,'response'=>'Save Failed! <br>'.$error.' Records <br>No DTR Processed <br>Employee ID <br>['.$error_id.']']);
        }

        return $response;
    }

    public function storeBenefitTransation($employee_id,$year,$month,$workdays,$lwop_for_the_month){

        $benefitInfo = new BenefitInfo;
        $benefit     = new Benefit;

        $code = ['SA','LA','RATA1','RATA2','HP','LP','PERA'];

        $benefit = $benefit
        ->whereIn('code',$code)
        ->select('id')
        ->get()->toArray();

        $query = $benefitInfo
        ->with('benefits')
        ->where('employee_id',$employee_id)
        ->whereIn('benefit_id',$benefit)
        ->where('start_date','<=',date('Y-m-d'))
        ->whereNull('date_terminated')
        ->whereNull('end_date')
        ->where('terminated',0)
        ->get();

        $sa_amount = 150 * $workdays;
        $la_amount = (500 / $workdays) * $workdays;

        $amount = 0;
        foreach ($query as $key => $value) {

            $transaction = new BenefitInfoTransaction;

            $benefit_amount = 0;
            switch ($value->benefits->code) {
                case 'SA':
                    $benefit_amount = $sa_amount;
                    break;
                case 'LA':
                    $benefit_amount = $la_amount;
                    break;
                case 'PERA':

                    if(isset($lwop_for_the_month)){
                        $dailyPeraAmount    = $value->benefit_amountt / 22;
                        $lessPeraAmount     = $dailyPeraAmount * $lwop_for_the_month;
                        $newPeraAmount      = $value->benefit_amountt - $lessPeraAmount;
                        $benefit_amount     = $newPeraAmount;
                    }else{
                        $benefit_amount = $value->benefit_amount;
                    }

                default:
                    $benefit_amount = $value->benefit_amount;
                    break;
            }

            $transaction->benefit_info_id   = $value->id;
            $transaction->benefit_id        = $value->benefit_id;
            $transaction->employee_id       = $value->employee_id;
            $transaction->amount            = $benefit_amount;
            $transaction->days_present      = $workdays;
            $transaction->benefit_rate      = $value->benefit_rate;
            $transaction->year              = $year;
            $transaction->month             = $month;
            $transaction->created_by        = Auth::User()->id;

            $transaction->save();

            $amount += (float)$benefit_amount;
        }

        return $amount;

    }

    public function storeLoaninfoTransaction($employee_id,$year,$month,$division_id){
        $loaninfo =  new LoanInfo;

        $query = $loaninfo
        ->where('employee_id',$employee_id)
        ->where('start_date','<=',date('Y-m-d'))
        ->whereNull('date_terminated')
        ->whereNull('end_date')
        ->where('terminated',0)
        ->get();

        $amount = 0;

        foreach ($query as $key => $value) {

            $transaction =  new LoanInfoTransaction;

            $transaction->employee_id        = $employee_id;
            $transaction->division_id        = $division_id;
            $transaction->loan_info_id       = $value->id;
            $transaction->loan_id            = $value->loan_id;
            $transaction->amount             = $value->loan_amount;
            $transaction->year               = $year;
            $transaction->month              = $month;
            $transaction->created_by         = Auth::User()->id;
            $transaction->save();

            $amount += (float)$value->loan_amount;
        }


        return $amount;

    }

    public function storeDeduction($employee_id,$year,$month,$division_id){

        $deductionInfo  =  new DeductionInfo;

        $query = $deductionInfo
        ->where('employee_id',$employee_id)
        ->where('start_date','<=',date('Y-m-d'))
        ->whereNull('date_terminated')
        ->whereNull('end_date')
        ->where('terminated',0)
        ->get();

        $amount = 0;
        foreach ($query as $key => $value) {
            $transaction    =  new DeductionInfoTransaction;
            $transaction->employee_id        = $employee_id;
            $transaction->division_id        = $division_id;
            $transaction->deduction_info_id  = $value->id;
            $transaction->deduction_id       = $value->deduction_id;
            $transaction->amount             = $value->deduction_amount;
            $transaction->status             = 'deductions';
            $transaction->year               = $year;
            $transaction->month              = $month;
            $transaction->created_by         = Auth::User()->id;
            $transaction->save();

            $amount += (float)$value->deduction_amount;
        }

        return $amount;

    }


    public function deletePayroll(){
        $data = Input::all();

        $transactions   = new Transaction;
        $attendance     = new AttendanceInfo;
        $benefitinfo    = new BenefitInfoTransaction;
        $deductioninfo  = new DeductionInfoTransaction;
        $loaninfo       = new LoanInfoTransaction;

        $posted = PostedReport::where('year',$data['year'])
        ->where('month',$data['month'])
        ->first();

        if(isset($posted)){
            $response = json_encode(['status'=>false,'response'=>'Transaction cannot be deleted cause its already posted.' ]);
        }else{

            foreach ($data['empid'] as $key => $value) {

                $transactions->where('employee_id',$data['empid'][$key])
                                    ->where('month',$data['month'])
                                    ->where('year',$data['year'])
                                    ->delete();
                $attendance->where('employee_id',$data['empid'][$key])
                                    ->where('month',$data['month'])
                                    ->where('year',$data['year'])
                                    ->delete();
                $deductioninfo->where('employee_id',$data['empid'][$key])
                                    ->where('month',$data['month'])
                                    ->where('year',$data['year'])
                                    ->delete();
                $benefitinfo->where('employee_id',$data['empid'][$key])
                                    ->where('month',$data['month'])
                                    ->where('year',$data['year'])
                                    ->delete();

                $loaninfo->where('employee_id',$data['empid'][$key])
                                    ->where('month',$data['month'])
                                    ->where('year',$data['year'])
                                    ->delete();

            }
            $response = json_encode(['status'=>true,'response'=>'Delete Successfully']);
        }

        return $response;
    }


    public function storeBenefitinfo(Request $request){

        if(!isset($request->employee_id)){

            $response = json_encode(['status'=>false,'response'=>'No employee selected']);

        }else{

            $transaction = BenefitInfoTransaction::find($request->id);
            $message = 'Update Successfully.';
            if(empty($transaction)){
                $this->validate($request,[
                    'benefit_id' => 'required',
                    'amount'     => 'required'
                ]);
                $message = 'Save Successfully.';
                $transaction = new BenefitInfoTransaction;
                $transaction->fill($request->all());
                $transaction->amount = str_replace(',', '', $request->amount);
            }else{
                switch ($request->benefit_status) {
                    case 'SA':
                        $transaction->days_present            = $request->days_present_sala;
                        $transaction->amount                  = $request->benefit_amount_sala;
                        $transaction->sala_absent             = $request->sala_absent;
                        $transaction->sala_absent_amount      = $request->sala_absent_amount;
                        $transaction->sala_undertime          = $request->sala_undertime;
                        $transaction->sala_undertime_amount   = $request->sala_undertime_amount;
                        break;
                    case 'LA':
                        $transaction->days_present            = $request->days_present_sala;
                        $transaction->amount                  = $request->benefit_amount_sala;
                        $transaction->sala_absent             = $request->sala_absent;
                        $transaction->sala_absent_amount      = $request->sala_absent_amount;
                        $transaction->sala_undertime          = $request->sala_undertime;
                        $transaction->sala_undertime_amount   = $request->sala_undertime_amount;
                        break;

                    case 'HP':
                        $transaction->days_present    = $request->days_present_hp;
                        $transaction->amount          = $request->benefit_amount_hp;
                        $transaction->benefit_rate    = $request->benefit_rate;
                        break;

                    default:
                        $transaction->days_present    = $request->used_count;
                        $transaction->used_amount     = $request->used_amount;
                        $transaction->ta_covered_date = $request->ta_covered_date;
                        break;
                }

                $transaction->updated_by     = Auth::User()->id;
            }



            $transaction->save();

            $query = $transaction
            ->with('benefitinfo','benefits')
            ->where('year',$request->year)
            ->where('month',$request->month)
            ->where('employee_id',$request->employee_id)
            ->get();

            $response = json_encode(['status'=>true,'response'=> $message,'benefitinfo'=> $query, 'myform'=>'myForm3']);
        }

        return $response;

    }

    public function deleteLoan(){
        $data = Input::all();

        $transaction = new LoanInfoTransaction;

        $query = $transaction
        ->where('id',$data['id'])
        ->delete();

        $data2['loaninfo'] = $transaction
                ->with('loans','loaninfo')
                ->where('employee_id',@$data['employee_id'])
                ->where('year',@$data['year'])
                ->where('month',@$data['month'])
                ->get();

        return json_encode(['status'=>'loans', 'data'=> $data2]);
    }

    public function deleteDeduction(){
        $data = Input::all();

        $transaction = new DeductionInfoTransaction;

        $query['deductioninfo'] = $transaction
        ->where('id',$data['id'])
        ->delete();

        $data2['deductioninfo'] = $transaction
                ->with('deductions','deductioninfo')
                ->where('employee_id',@$data['employee_id'])
                ->where('year',@$data['year'])
                ->where('month',@$data['month'])
                ->get();

        return json_encode(['status'=>'deductions', 'data'=> $data2]);
    }

    public function deleteBenefit(){
        $data = Input::all();

        $transaction = new BenefitInfoTransaction;

        $query = $transaction
        ->where('id',$data['id'])
        ->delete();

        $data2['benefitinfo'] = $transaction
                ->with('benefits','benefitinfo')
                ->where('employee_id',@$data['employee_id'])
                ->where('year',@$data['year'])
                ->where('month',@$data['month'])
                ->get();

        return json_encode(['status'=>'benefits', 'data'=> $data2]);
    }

    public function storeNewDeduction(Request $request){

        $this->validate($request,[
            'select_deduction'  => 'required',
            'deduction_amount'  => 'required',
            'select_pay_period' => 'required',
            'date_start'        => 'required'
        ]);

        $amount = (isset($request->deduction_amount)) ? str_replace(',', '', $request->deduction_amount) : 0;
        $deductioninfo = DeductionInfo::find($request->id);
        if(empty($deductioninfo)){
            $deductioninfo =  new DeductionInfo;
        }

        $deductioninfo->employee_id         = $request->employee_id;
        $deductioninfo->deduction_id        = $request->select_deduction;
        $deductioninfo->deduct_amount       = $amount;
        $deductioninfo->deduct_date_start   = $request->date_start;

        if($deductioninfo->save()){

            $transaction = DeductionInfoTransaction::where('deduction_info_id',$deductioninfo->id)->first();

            if(empty($check)){
                $transaction  = new DeductionInfoTransaction;
            }

            $transaction->employee_id       = $request->employee_id;
            $transaction->deduction_id      = $request->select_deduction;
            $transaction->deduction_info_id = $deductioninfo->id;
            $transaction->amount            = $amount;
            $transaction->status            = 'deductions';
            $transaction->year              = $request->year;
            $transaction->month             = $request->month;
            $transaction->save();
        }

        $query = $transaction
        ->with('deductions','deductioninfo')
        ->where('employee_id',$request->employee_id)
        ->where('year',$request->year)
        ->where('month',$request->month)
        ->get();


        return json_encode(['status'=>true,'response'=>'Save Successfully!', 'deductioninfo'=>$query ]);


    }

    public function compute_lwop($employeeinfo,$_id,$dailyRateAmount,$y,$m,$daysInAMonth)
    {
        $arr = [];
        $dtrprocess          = new DTRProcess;
        $creditbalance       = new EmployeeCreditBalance;

        $creditbalance = $creditbalance
        ->where('EmployeesRefId',$_id)
        ->where('NameCredits','VL')
        ->where('EffectivityYear',$y)
        ->first();

        // ================ LWOP =================== //

        $begBalAsOfDate = @$creditbalance->BegBalAsOfDate;

        $begbalMonth = date('m', strtotime($begBalAsOfDate));

        $dtrprocess = $dtrprocess
        ->where('EmployeesRefId',$_id)
        ->where('Year',$y)
        ->whereBetween('Month',array($begbalMonth,$m))
        ->get();


        $vl_balance = @$creditbalance->BeginningBalance;

        $undertime = 0;
        $tardiness = 0;
        $absent    = 0;
        $vl_used   = 0;
        $vl_earned = 0;
        $new_vl    = 0;
        $lwop_for_the_month = 0;

        foreach ($dtrprocess as $key2 => $value) {
            $deduction = 0;

            $tardiness  = ($value->Tardy_Deduction_EQ > 0) ? $value->Tardy_Deduction_EQ : 0 ;
            $undertime  = ($value->Undertime_Deduction_EQ > 0) ? $value->Undertime_Deduction_EQ : 0 ;
            $absent     = ($value->Total_Absent_Count > 0) ? $value->Total_Absent_Count : 0;
            $vl_used    = ($value->VL_Used > 0) ? $value->VL_Used : 0;
            $vl_earned  = ($value->VL_Earned > 0) ? $value->VL_Earned : 0;

            $deduction = $tardiness + $undertime + $absent;
            $new_vl = ($vl_earned + $vl_balance) - $deduction;

            if($new_vl < 0){
                $lwop_for_the_month = $new_vl;
                $vl_balance = 0;
            }else{
                $vl_balance = $new_vl;
            }
        }

           // ============ LWOP COMPUTATION ===========================
          // vl_used + tardiness + undertime + absent = total deduction
         // (vl_earned + vl_balance) - total deduction
        // ===========================================================

        $hourRateAmount  = $dailyRateAmount / 8;
        $minutRateAmount = $hourRateAmount / 60;


        $eeShare = 0;
        $erShare = 0;

        $lwopDays = 0;
        $lwopCounted = 0;
        if($lwop_for_the_month == 0){
            $lwopDays = 0;
            $eeShare = $employeeinfo->gsis_contribution;
            $erShare = $employeeinfo->er_gsis_share;
        }else{

            $lwopCounted = abs($lwop_for_the_month);

            $lwopDays = $daysInAMonth - $lwopCounted;

            $gsisAmount = $lwopDays * $dailyRateAmount;
            $eeShare = $gsisAmount * 0.09;
            $erShare = $gsisAmount * 0.12;
        }

        $arr['eeShare'] = $eeShare;
        $arr['erShare'] = $erShare;
        $arr['lwopCounted'] = $lwopCounted;

        return $arr;

    }

}
