<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Transaction;
class CashGiftsAndMidYearReportController extends Controller
{
    function __construct(){
		$this->title = 'CASH GIFT AND MID YEAR BONUS';
    	$this->module = 'cashmidyearbonus';
        $this->module_prefix = 'payrolls/reports/othercompensations';
    	$this->controller = $this;
	}

	public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }


    public function getEmployeeinfo(){

        $q = Input::all();

        $query = Transaction::with(['employees','positionitems'=>function($qry){ $qry->with('positions'); },'offices','salaryinfo','employeeinformation'])->where('year',$q['year'])->where('month',$q['month']);

        $query = $query->with(['employeeinfo'=>function($qry){
                                $qry = $qry->with(['benefitinfo' => function($qry){
                            	   $qry->with('benefits')->whereHas('benefits', function($qry){
                            		  $qry->where('code','CG');
                            	});
                            }]);
                        }])->orderBy('office_id','desc')->get();
        $data = [];
        if(count($query[0]->employeeinfo->benefitinfo) > 0){

            foreach ($query as $key => $value) {

                $data[$value->offices->name][$key] = $value;
            }

        }else{

            $data = [];
        }

        return json_encode($data);
    }
}
