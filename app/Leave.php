<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leave extends Model
{
	protected $table = 'pms_leave';
	protected $fillable = [

		'employee_id',
		'employee_number',
		'rata_id',
		'leave_type',
		'leave_date',
		'number_of_leave_field',
		'updated_by'

	];
}
