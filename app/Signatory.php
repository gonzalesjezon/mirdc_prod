<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Signatory extends Model
{
    protected $primaryKey = 'id';

    protected $table = 'pms_signatory';

    protected $fillable = [

		'signatory_one',
		'signatory_two',
		'signatory_three',
		'signatory_four',
		'signatory_five',
		'signatory_six',
		'add_degree'

    ];
}
