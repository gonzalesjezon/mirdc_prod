<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeCreditBalance extends Model
{
    protected $primaryKey = 'RefId';
    protected $table = 'employeescreditbalance';
    protected $fillable = [

    	'EmployeesRefId',
    	'NameCredits',
    	'EffectivityYear',
    	'BeginningBalance',
    	'Total_Absent_Count',
    	'BegBalAsOfDate',
    ];
}
