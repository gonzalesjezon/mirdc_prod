<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'pms_projects';
    protected $fillable = [
        'code',
        'name',
    ];
}
