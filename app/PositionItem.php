<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PositionItem extends Model
{

    protected $table = 'pms_position_items';
    protected $fillable = [

		'code',
		'name',
		'position_id',
		'position_level_id',
		'position_classification_id',
		'created_by',
		'updated_by'

    ];

    public function positions(){
    	return $this->belongsTo('App\Position','position_id');
    }

}
