<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeInfo extends Model
{
    protected $table = 'pms_payroll_information';
    protected $fillable = [
        'bp_no',
        'crn',
		'employee_id',
		'gsispolicy_id',
		'pagibigpolicy_id',
		'philhealthpolicy_id',
		'taxpolicy_id',
		'bank_id',
		'wagestatus_id',
		'providentfund_id',
		'atm_no',
        'daily_rate_amount',
        'monthly_rate_amount',
        'annual_rate_amount',
		'pagibig_contribution',
		'philhealth_contribution',
		'gsis_contribution',
		'tax_contribution',
		'pagibig2',
        'pagibig_personal',
		'tax_payperiod',
		'tax_bracket',
		'tax_bracket_amount',
		'tax_inexcess',
        'union_dues',
        'er_pagibig_share',
        'er_philhealth_share',
        'er_gsis_share',
        'mid_year_bonus',
        'end_year_bonus',
        'tax_id_number',
        'overtime_balance_amount'

    ];

    public function employees(){
        return $this->belongsTo('App\Employee','employee_id');
    }
    public function gsispolicy(){
    	return $this->belongsTo('App\GsisPolicy','gsispolicy_id');
    }
    public function pagibigpolicy(){
    	return $this->belongsTo('App\PagibigPolicy','pagibigpolicy_id');
    }
    public function philhealthpolicy(){
    	return $this->belongsTo('App\PhilhealthPolicy','philhealthpolicy_id');
    }
    public function taxpolicy(){
    	return $this->belongsTo('App\TaxPolicy','taxpolicy_id');
    }

    public function banks(){
    	return $this->belongsTo('App\Bank','bank_id');
    }

    // public function bankbranches(){
    // 	return $this->belongsTo('App\BankBranch','bankbranch_id');
    // }

    public function wages(){
    	return $this->belongsTo('App\WageRate','wagestatus_id');
    }

    public function providentfunds(){
    	return $this->belongsTo('App\ProvidentFundPolicy','providentfund_id');
    }

    public function salaryinfo(){
        return $this->belongsTo('App\SalaryInfo','employee_id');
    }

    public function transactions(){
        return $this->belongsTo('App\Transaction','employee_id');
    }

     public function benefitinfo(){
        return $this->hasMany('App\BenefitInfo','employee_id','employee_id')->with('benefits');
    }
    public function loaninfo(){
        return $this->hasMany('App\LoanInfo','employee_id','id');
    }

    public function payrolldeduction(){
        return $this->hasMany('App\DeductionInfo','employee_id','id');
    }

    public function deductioninfo(){
        return $this->belongsTo('App\DeductionInfo','employee_id','id')->with('deductions');
    }

    public function mwssLoaninfo(){
         return $this->belongsTo('App\LoanInfo','employee_id','employee_id');
    }
    public function mwssDeductionInfo(){
         return $this->belongsTo('App\DeductionInfo','employee_id','employee_id');
    }

    public function loanstransaction(){
        return $this->hasMany('App\LoanInfoTransaction','employee_id','employee_id');
    }

    public function employee_information(){
        return $this->belongsTo('App\EmployeeInformation','employee_id','employee_id')
                ->with('positions','employeestatus','offices','divisions');
    }

}
