<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanInfoTransaction extends Model
{
    protected $table 	= 'pms_loaninfo_transactions';
    protected $fillable = [
		'employee_id',
		'loan_id',
        'division_id',
		'loan_info_id',
		'amount',
        'year',
        'month',
		'created_by',
		'updated_by',
    ];

    public function employees(){
        return $this->belongsTo('App\Employee','employee_id');
    }

    public function loanInfo(){
    	return $this->belongsTo('App\LoanInfo','loan_info_id')->with('loans');
    }
    public function loans(){
    	return $this->belongsTo('App\Loan','loan_id');
    }

    public function divisions(){
        return $this->belongsTo('App\Division','division_id');
    }

    public function payrollinfo(){
        return $this->belongsTo('App\EmployeeInfo','employee_id','employee_id');
    }

    public function employeeinformation(){
        return $this->belongsTo('App\EmployeeInformation','employee_id','employee_id')->with('positions');
    }
}
