<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BenefitInfo extends Model
{
    protected $table = 'pms_benefitsinfo';
    protected $fillable = [

        'employee_id',
        'benefit_id',
        'benefit_amount',
        'days_present',
        'benefit_rate',
        'lp_basic_salary',
        'lp_tranche',
        'start_date',
        'end_date',
        'date_terminated',
        'terminated',
        'number_of_years'

    ];

    public function benefits(){
    	return $this->belongsTo('App\Benefit','benefit_id');
    }

    public function employees(){
        return $this->belongsTo('App\Employee','employee_id');
    }


}
