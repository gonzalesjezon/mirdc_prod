<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PositionItemSetup extends Model
{
    protected $table = 'pms_positionitem_setup';
    protected $fillable = [
        'employee_id',
        'employee_number',
    	'positionitem_id',
    	'position_id',
        'jobgrade_id',
    	'salarygrade_id',
    	'step_inc',
    	'amount'
    ];


    public function positionitems(){
    	return $this->belongsTo('App\PositionItem','positionitem_id');
    }
    public function positions(){
    	return $this->belongsTo('App\Position','position_id');
    }
    public function salarygrade(){
    	return $this->belongsTo('App\SalaryGrade','salarygrade_id');
    }
    public function jobgrade(){
        return $this->belongsTo('App\JobGrade','jobgrade_id');
    }
}
