<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeductionInfo extends Model
{
    protected $table = 'pms_deductioninfo';
    protected $fillable = [
        'employee_id',
    	'deduction_id',
    	'deduction_amount',
    	'start_date',
    	'end_date',
    	'date_terminated',
        'terminated',
    ];

    public function deductions(){
        return $this->belongsTo('App\Deduction','deduction_id');
    }

    public function employees(){
        return $this->belongsTo('App\Employee','employee_id');
    }
}
