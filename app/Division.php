<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Division extends Model
{

	protected $primaryKey = 'RefId';
    protected $table = 'division';
    protected $fillable = [
    	'Code',
    	'Name',

    ];

}
