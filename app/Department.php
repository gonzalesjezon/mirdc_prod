<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{

	protected $primaryKey = 'id';
    protected $table = 'pms_departments';
    protected $fillable = [
        'code',
        'name',
    ];
}
