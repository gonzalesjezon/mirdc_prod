<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecialPayrollTransaction extends Model
{
    protected $table = 'pms_specialpayroll_transactions';
    protected $fillable = [

		'employee_id',
        'employee_number',
		'benefit_info_id',
		'position_item_id',
        'division_id',
		'office_id',
		'year',
		'month',
        'percentege',
        'no_of_months_entitled',
        'cost_uniform_amount',
        'cash_gift_amount',
        'tax_amount',
        'deduction_amount',
        'amount',
		'created_by',
		'updated_by',
        'status',

    ];

    public function employees(){
    	return $this->belongsTo('App\Employee','employee_id');
    }
    public function benefitinfo(){
    	return $this->belongsTo('App\BenefitInfo','benefit_info_id')->with('benefits');
    }
    public function offices(){
    	return $this->belongsTo('App\Office','office_id');
    }
    public function positions(){
        return $this->belongsTo('App\Position','position_id');
    }
    public function positionitems(){
    	return $this->belongsTo('App\PositionItem','position_item_id');
    }
    public function divisions(){
        return $this->belongsTo('App\Division','division_id');
    }
}
