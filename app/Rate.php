<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $table = 'pms_rates';
    protected $fillable = [
		'rate',
		'status',
		'created_by',
		'updated_by',
    ];
}
