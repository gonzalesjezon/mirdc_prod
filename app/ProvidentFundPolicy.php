<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProvidentFundPolicy extends Model
{
    protected $table = 'pms_providentfund';

    protected $fillable = [
    	'policy_name',
    	'deduction_period',
    	'policy_type',
    	'pay_period',
    	'based_on',
    	'ee_share',
    	'er_share'
    ];
}
