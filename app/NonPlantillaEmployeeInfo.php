<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NonPlantillaEmployeeInfo extends Model
{
    protected $table = 'pms_nonplantilla_employeeinfo';
    protected $fillable = [
		'employee_id',
        'employee_number',
		'bank_id',
        'tax_policy_id',
        'ewt_policy_id',
        'pwt_policy_id',
        'atm_no',
        'project_id',
        'responsibility_id',
        'daily_rate_amount',
        'monthly_rate_amount',
        'overtime_balance_amount',
        'tax_id_number',
        'inputted_amount',
        'threshhold_amount',
    ];

    public function tax_policy(){
        return $this->belongsTo('App\TaxPolicy','tax_policy_id');
    }
    public function banks(){
        return $this->belongsTo('App\Bank','jo_bank_id');
    }
    public function tax_policy_ewt(){
        return $this->belongsTo('App\TaxPolicy','ewt_policy_id');
    }
    public function tax_policy_pwt(){
        return $this->belongsTo('App\TaxPolicy','pwt_policy_id');
    }
    public function employees(){
        return $this->belongsTo('App\Employee','employee_id');
    }

    public function project(){
        return $this->belongsTo('App\Project');
    }

    public function rcenter(){
        return $this->belongsTo('App\ResponsibilityCenter');
    }
}
