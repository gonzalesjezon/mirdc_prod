<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/style2.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery-ui.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery-ui.structure.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery-ui.theme.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/select2.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/tip.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.dataTables.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/all.css')}}">



	<script src="{{ asset('js/jquery-3.3.1.js')}}"></script>
	<script src="{{ asset('js/jquery.dataTables.min.js') }} "></script>
	<script src="{{ asset('js/dataTables.bootstrap.min.js') }} "></script>

	<script type="text/javascript">

		base_url = "<?php echo URL::to('/'); ?>";
		module = "<?php echo '/'.@$module; ?>";
		module_prefix = "<?php echo '/'.@$module_prefix; ?>";
		console.log(base_url+module_prefix+module);
	</script>
	<title>{!! $title !!}</title>

</head>
<body >
	<div class="main_container">
		<section class="content-header">
				@include('elements.header')

		</section>
		<section class="main-content" style="background: #fff">
			@include('elements.sidemenu')
			<div class="container-fluid web-content" id="mainScreen">
		        <div class="row">
		        	<!-- <div class="col-md-12"> -->
		        		<div class="panel-top">
				           <div class="col-md-6">
				              <a href="javascript:void(0)" class="mbar" id="titleBarIcons" onclick="openNav();"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>&nbsp;&nbsp;{{ $title }}
				          </div>
				          <!--  <div class="col-md-6 text-right">
					           	Today is <span id="TimeDate">{{ date('Y-m-d H:i:s') }}</span>
				           </div> -->

		        		</div>

		        	<!-- </div> -->
		        </div>
				@yield('content')
			</div>
		</section>
		<section class="content-footer ">
				<div class="col-md-12">
					@include('elements.footer')
				</div>
		</section>
	</div>


        @yield('js-logic1')
</body>
</html>

<script src="{{ asset('js/jquery-ui.js') }} "></script>
<script src="{{ asset('js/jquery.form.js')}}"></script>
<script src="{{ asset('js/js6_jquery_utilities.js')}}"></script>
<script src="{{ asset('js/init.js') }} "></script>
<script src="{{ asset('js/myjs.js') }} "></script>
<script src="{{ asset('js/myjs2.js') }} "></script>
<script src="{{ asset('js/myjs3.js') }} "></script>
<script src="{{ asset('js/select2.min.js') }} "></script>
<script src="{{ asset('js/icheck.min.js')}}"></script>
<script src="{{ asset('js/bootstrap.min.js') }} "></script>
<script src="{{ asset('js/sweetalert.min.js') }} "></script>
<script src="{{ asset('js/all.js') }} "></script>

