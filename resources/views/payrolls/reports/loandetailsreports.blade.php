@extends('app-reports')


@section('reports-content')

<link rel="stylesheet" type="text/css" media="print" href="{{ asset('css/printlandscapetwo.css') }}">
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>Select Employee</td>
		</tr>
		<tr>
			<td>
				<select class="form-control select2" name="employee_id" id="employee_id">
					<option value=""></option>
					@foreach($employee as $key => $value)
					<option value="{{ $value->id }}" data-empnumber="{{ $value->employee_number }}">{{ ucwords(strtolower($value->lastname)) }} {{ ucwords(strtolower($value->firstname)) }} {{ ucwords(strtolower($value->middlename)) }}</option>
					@endforeach
				</select>
			</td>
		</tr>
		<tr>
			<td>Select Year</td>
		</tr>
		<tr>
			<td>
				<select class="employee-type form-control font-style2 select2" id="select_year" name="year">
					@foreach( range($latest_year,$earliest_year) as $i)
					<option value="{{$i}}">{{$i}}</option>
					@endforeach
				</select>
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:100%;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports">
	       		<div class="row">
	       			<div class="col-md-5 text-left">
   						<img src="{{ url('images/mirdc_logo.gif') }}" style="height: 80px;">
   						<span style="font-weight: bold;">Metals Industry Research and Development Center</span>
   					</div>
   					<!-- <div class="col-md-12 text-center" style="font-weight: bold;margin-left: 20px;padding-top: 15px;">
   						Metals Industry Research and Development Center <br>
   						<span style="padding-left: 25px;">General Santos Ave., Bicutan, Taguig City</span>
   					</div> -->
	       		</div>
	       		<br>
	       		<div class="row">
	       			<div class="col-md-12 text-center" style="font-weight: bold">
	       				<span>
	       					EMPLOYEE LOAN DETAILS
	       				</span>
	       			</div>
	       		</div>
   				<table class="table" style="border: 2px solid #5a5a5a;width: 1360px;">
   					<thead style="border: 2px solid #5a5a5a">
   						<tr class="text-center" style="font-weight: bold;">
   							<td rowspan="2">EMP NO</td>
   							<td rowspan="2">NAME</td>
   							<td rowspan="2">POSITION</td>
   							<td rowspan="2">TYPE OF LOAN</td>
   							<td rowspan="2">START DATE</td>
   							<td rowspan="2">END DATE</td>
   							<td rowspan="2">LOAN AMOUNT</td>
   							<td colspan="3">DEDUCTIONS</td>
   							<td rowspan="2">BALANCE</td>
   						</tr>
   						<tr class="text-center" style="font-weight: bold;">
   							<td>YEAR</td>
   							<td>MONTH</td>
   							<td>AMOUNT</td>
   						</tr>
   					</thead>
   					<tbody id="tbl_content"></tbody>
   				</table>
	       </div>
	 	</div>
	</div>
</div>
<!-- 0.328571 -->
@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');

	var months ={
			1:'January',
			2:'February',
			3:'March',
			4:'April',
			5:'May',
			6:'June',
			7:'July',
			8:'August',
			9:'September',
			10:'October',
			11:'November',
			12:'December',
		}
	var empNumber;
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();
		empNumber = $(this).find(':selected').data('empnumber');

	})
	var _payPeriod;
	var _semiPayPeriod;
	$(document).on('change','#pay_period',function(){
		_payPeriod = $(this).find(':selected').val();
		switch(_payPeriod){
			case 'semimonthly':
				$('#semi_pay_period').removeClass('hidden');
			break;
			default:
				$('#semi_pay_period').addClass('hidden');
			break;
		}
	});

	$(document).on('change','#semi_pay_period',function(){
		_semiPayPeriod = $(this).find(':selected').val();
	})

$(document).on('click','#print',function(){
	$('#reports').printThis();
});

$(document).on('click','#preview',function(){

	year = (_Year) ? _Year : '';
	month = (_Month) ? _Month : '';
	emp_type = (_emp_type) ? _emp_type : '';
	emp_status = (_emp_status) ? _emp_status : '';
	month = (_Month) ? _Month : '';
	category = (_searchvalue) ? _searchvalue : '';
	searchby = (_searchby) ? _searchby : '';

	if(!year){
		swal({
			  title: "Select Year First!",
			  type: "warning",
			  showCancelButton: false,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Yes",
			  closeOnConfirm: false

		});
	}else{
		$.ajax({
			url:base_url+module_prefix+module+'/show',
			data:{
				'id':empNumber,
				'year':year,
				'month':month,
				'emp_type':emp_type,
				'emp_status':emp_status,
				'category':category,
				'searchby':searchby,
			},
			type:'GET',
			dataType:'JSON',
			success:function(data){
				console.log(data)
				if(data !== null){
					arr = [];
					ctr = 0;

					netconsoLoanAmount  = 0;
					netpolicyLoanAmount = 0;
					netemergLoan 		= 0;
					neteducLoan 		= 0;
					nethdmfLoan 		= 0;
					netmplLoan 			= 0;
					netlbMobile 		= 0;
					netmembaiLoan 		= 0;
					netprovidentLoan 	= 0;
					netTotalLoans 		= 0;
					var employeeNumber;
					var Position;
					var fullName;
					employees = [];

					loanName = [];
					$.each(data,function(k,v){

						loanDeductionAmount = 0;

						firstname = (v[0] !== undefined) ? v[0].employees.firstname : '';
						lastname = (v[0] !== undefined) ? v[0].employees.lastname : '';
						middlename = (v[0] !== undefined) ? v[0].employees.middlename : '';
						employee_number = (v[0] !== undefined) ? v[0].employees.employee_number : '';
						position = (v[0] !== undefined) ? v[0].employeeinformation.positions.Name : '';

						middlename = (middlename !== null) ? middlename : '';

						fullname = lastname+' '+firstname+' '+middlename;

						loanName[ctr] = k;
						$.each(v,function(key,val){

							consoLoanAmount  = 0;
							policyLoanAmount = 0;
							emergLoan 		 = 0;
							educLoan 		 = 0;
							hdmfLoan 		 = 0;
							mplLoan 		 = 0;
							lbMobile 		 = 0;
							membaiLoan 		 = 0;
							providentLoan 	 = 0;

							year = val.year;
							month = val.month;
							loanAmount = val.amount;

							loanDeductionAmount += parseFloat(loanAmount);

							loan_amount = (loanAmount) ? commaSeparateNumber(parseFloat(loanAmount).toFixed(2)) : '';

							loan_name = (loanName[key] !== undefined) ? loanName[key] : '';

							arr += '<tr>';
							if(employeeNumber !== employee_number){
								employeeNumber = employee_number;
								arr += '<td class="text-center">'+employee_number+'</td>';
							}else{
								arr += '<td class="text-center"></td>';
							}

							if(fullName !== fullname){
								fullName = fullname;
								arr += '<td class="text-left">'+fullname+'</td>';
							}else{
								arr += '<td class="text-left"></td>';
							}

							if(Position !== position){
								Position = position;
								arr += '<td class="text-center">'+position+'</td>';;
							}else{
								arr += '<td class="text-left"></td>';
							}
							arr += '<td class="text-center">'+loan_name+'</td>';
							arr += '<td class="text-center"></td>';
							arr += '<td class="text-center"></td>';
							arr += '<td class="text-right"></td>';
							arr += '<td class="text-right">'+year+'</td>'; // year
							arr += '<td class="text-right">'+month+'</td>'; // month
							arr += '<td class="text-right">'+loan_amount+'</td>'; // amount
							arr += '<td class="text-right"></td>'; // BALANCE
							arr += '</tr>';

						});

						loan_deduction_amount = (loanDeductionAmount) ? commaSeparateNumber(parseFloat(loanDeductionAmount).toFixed(2)) : '';

						arr += '<tr>';
						arr += '<td colspan="7" class="text-center">TOTAL DEDUCTIONS</td>';
						arr += '<td colspan="3" class="text-right">'+loan_deduction_amount+'</td>';
						arr += '<td class="text-right"></td>'; // BALANCE
						arr += '</tr>'
					ctr++;
					});
					ctr = 0;

					days = daysInMonth(_Month,_Year)

					if(_payPeriod == 'monthly'){
						_coveredPeriod =  months[_Month]+' 1-'+days+', '+_Year;
					}else{
						switch(_semiPayPeriod){
							case 'firsthalf':
								_coveredPeriod =  months[_Month]+' 1-15, '+_Year;
							break;
							default:
								_coveredPeriod = months[_Month]+' 16-'+days+', '+_Year;
							break;
						}
					}

					$('.covered_date').text( months[_Month]+' '+_Year);

					$('#tbl_content').html(arr);

					$('#btnModal').trigger('click');

				}else{
					swal({
						title: "No Records Found",
						type: "warning",
						showCancelButton: false,
						confirmButtonClass: "btn-danger",
						confirmButtonText: "Yes",
						closeOnConfirm: false
					});
				}
			}
		})
	}


});

function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}
})
</script>
@endsection