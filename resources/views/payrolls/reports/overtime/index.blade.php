@extends('app-reports')


@section('reports-content')
<head>
<style type="text/css">
.table>tbody>tr>td{
	line-height: 0.428571;
}
</style>
</head>

<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td ><span><b>Employee Name</b></span></td>
		</tr>
		<tr>
			<td>
				<select class="form-control select2" name="employee_id" id="employee_id" style="margin:auto;width: 100%;">
					<option value=""></option>
					@foreach($employeeinfo as $value)
					<option value="{{ $value->id }}">{{ $value->lastname }} {{ $value->firstname }} {{ $value->middlename }}</option>
					@endforeach
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Covered Date</b></span>
					</div>

				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select class="form-control select2" name="month" id="select_month">
								<option value=""></option>
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control select2" name="year" id="select_year">
								<option value=""></option>
							</select>
						</div>

					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Pay Period</b></span>
					</div>

				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select id="pay_period" class="form-control font-style2" name="pay_period">
								<option value=""></option>
								<option value="semimonthly">Semi Monthly</option>
								<option value="monthly">Monthly</option>
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control font-style2 hidden" id="semi_pay_period" name="semi_pay_period">
								<option value=""></option>
								<option value="firsthalf">First Half</option>
								<option value="secondhalf">Second Half</option>
							</select>
						</div>

					</div>
				</div>
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="width: 700px;">
	    <div class="mypanel border0" style="height:100%;width:100%;overflow-y:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid ">
	       		<div class="hidden" id="nonplantilla">
	       			@include('payrolls.reports.overtime.nonplantilla')
	       		</div>
	       		<div class="hidden" id="plantilla">
	       			@include('payrolls.reports.overtime.plantilla')
	       		</div>
	       </div>
	 	</div>
	</div>
</div>
<!-- 0.328571 -->
@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
        year += '<option value='+y+'>'+y+'</option>';
	}
    $('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
    for ( m =  0; m <= month.length - 1; m++) {
    	mArr += '<option '+month[m]+'>'+month[m]+'</option>';
    }
    $('#select_month').html(mArr);


// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$('#select_year').select2({
		allowClear:true,
	    placeholder: "Year",
	});

	$('#select_month').select2({
		allowClear:true,
	    placeholder: "Month",
	});

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

var _payPeriod;
var _semiPayPeriod;
$(document).on('change','#pay_period',function(){
	_payPeriod = $(this).find(':selected').val();
	switch(_payPeriod){
		case 'semimonthly':
			$('#semi_pay_period').removeClass('hidden');
		break;
		default:
			$('#semi_pay_period').addClass('hidden');
		break;
	}
});
$(document).on('change','#semi_pay_period',function(){
	_semiPayPeriod = $(this).find(':selected').val();
})

$(document).on('click','#preview',function(){

	year = (_Year) ? _Year : '';
	month = (_Month) ? _Month : '';
	emp_type = (_emp_type) ? _emp_type : '';
	emp_status = (_emp_status) ? _emp_status : '';
	month = (_Month) ? _Month : '';
	category = (_searchvalue) ? _searchvalue : '';
	searchby = (_searchby) ? _searchby : '';

	if(!year || !month || !_empid){
		swal({
			  title: "Select Year, Month and Employee First!",
			  type: "warning",
			  showCancelButton: false,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Yes",
			  closeOnConfirm: false

		});
	}else{
		$.ajax({
			url:base_url+module_prefix+module+'/getOvertimeReport',
			data:{
				'id':_empid,
				'year':year,
				'month':month,
				'emp_type':emp_type,
				'emp_status':emp_status,
				'category':category,
				'searchby':searchby,
				'pay_period':_payPeriod,
				'sub_pay_period':_semiPayPeriod,
			},
			type:'GET',
			dataType:'JSON',
			success:function(data){
				console.log(data)
				$('#plantilla').addClass('hidden');
				$('#nonplantilla').addClass('hidden');
				if(data.nonplantilla_employee !== null){
					fullname = data.nonplantilla_employee.employees.lastname+' '+data.nonplantilla_employee.employees.firstname+' '+data.nonplantilla_employee.employees.middlename;
					$('#employee_name').text(fullname);

					daily_rate = (data.nonplantilla_employee.daily_rate_amount) ? commaSeparateNumber(parseFloat(data.nonplantilla_employee.daily_rate_amount).toFixed(2)) : '';
					$('#daily_rate').text((daily_rate));

					// monthly_rate = data.nonplantilla_employee.monthly_rate_amount;
					// $('#basic_pay').text(commaSeparateNumber(monthly_rate));

					number_of_working_days = data.attendanceinfo.actual_workdays;

					$('#no_of_working_days').text(number_of_working_days);


					regular_ot = (data.overtimeinfo !== null) ?  data.overtimeinfo.actual_regular_overtime : 0;
					weekend_ot = (data.overtimeinfo !== null) ?  data.overtimeinfo.actual_special_overtime : 0;

					regular_hours = (data.overtimeinfo !== null) ?  data.overtimeinfo.actual_regular_overtime : 0;
					weekend_hours = (data.overtimeinfo !== null) ?  data.overtimeinfo.actual_special_overtime : 0;

					regular_ot_amount = (data.overtimeinfo !== null) ?  data.overtimeinfo.total_regular_amount : 0;
					weekend_ot_amount = (data.overtimeinfo !== null) ?  data.overtimeinfo.total_special_amount : 0;


					tax_amount_one = (data.nonplantilla_employee.tax_rate_amount) ? parseFloat(data.nonplantilla_employee.tax_rate_amount) : 0;
					if(data.nonplantilla_employee.taxpolicy_id !==  null){
						tax_policy_one = data.nonplantilla_employee.taxpolicy_one.policy_name.split(' ');

						switch(tax_policy_one[0]){
							case 'EWT':
								$('.ewtax').text(tax_policy_one[1]+' %');
								percentage_one = (tax_policy_one[1]) ? tax_policy_one[1] : ' ';
								$('#ewtax_percentage').text(percentage_one);
								$('#ewtax_amount').text(commaSeparateNumber(parseFloat(tax_amount_one).toFixed(2)) );
							break;
							case 'PWT':
								$('#pwtax').text(tax_policy_one[1]);
								percentage_one = (tax_policy_one[1]) ? tax_policy_one[1] : ' ';
								$('#pwtax_percentage').text(percentage_one);
								$('#pwtax_amount').text(commaSeparateNumber(parseFloat(tax_amount_one).toFixed(2)));
							break;
						}
					}

					tax_amount_two = (data.nonplantilla_employee.tax_rate_amount_two) ? parseFloat(data.nonplantilla_employee.tax_rate_amount_two) : 0;
					if(data.nonplantilla_employee.taxpolicy_two !==  null){
						tax_policy_two = data.nonplantilla_employee.taxpolicy_two.policy_name.split(' ');

						switch(tax_policy_two[0]){
							case 'EWT':
								$('.ewtax').text(tax_policy_two[1]+' %');
								percentage_two = (tax_policy_two[1]) ? tax_policy_two[1] : ' ';
								$('#ewtax_percentage').text(percentage_two);
								$('#ewtax_amount').text(commaSeparateNumber(parseFloat(tax_amount_two).toFixed(2)));
							break;
							case 'PWT':
								$('#pwtax').text(tax_policy_two[1]);
								percentage_two = (tax_policy_two[1]) ? tax_policy_two[1] : ' ';
								$('#pwtax_percentage').text(percentage_two);
								$('#pwtax_amount').text(commaSeparateNumber(parseFloat(tax_amount_two).toFixed(2)));
							break;
						}
					}

					regular_ot_less_percent_amount = (parseFloat(regular_ot_amount)*(tax_policy_one[1]/100));
					weekend_ot_less_percent_amount = (parseFloat(weekend_ot_amount)*(tax_policy_one[1]/100));

					sub_total_regular_and_weekend_amount = (parseFloat(regular_ot_amount) + parseFloat(weekend_ot_amount));
					sub_total_regular_and_weekend_less_percent_amount = (parseFloat(regular_ot_less_percent_amount) + parseFloat(weekend_ot_less_percent_amount));

					net_pay = (parseFloat(sub_total_regular_and_weekend_amount) - parseFloat(sub_total_regular_and_weekend_less_percent_amount));

					regular_ot_amount = (regular_ot_amount !== 0) ? commaSeparateNumber(parseFloat(regular_ot_amount).toFixed(2)) : '';
					weekend_ot_amount = (weekend_ot_amount !== 0) ? commaSeparateNumber(parseFloat(weekend_ot_amount).toFixed(2)) : '';
					regular_ot_less_percent_amount = (regular_ot_less_percent_amount !== 0) ? commaSeparateNumber(parseFloat(regular_ot_less_percent_amount).toFixed(2)) : '';
					weekend_ot_less_percent_amount = (weekend_ot_less_percent_amount !== 0) ? commaSeparateNumber(parseFloat(weekend_ot_less_percent_amount).toFixed(2)) : '';
					sub_total_regular_and_weekend_amount = (sub_total_regular_and_weekend_amount !== 0) ? commaSeparateNumber(parseFloat(sub_total_regular_and_weekend_amount).toFixed(2)) : '';
					sub_total_regular_and_weekend_less_percent_amount = (sub_total_regular_and_weekend_less_percent_amount !== 0) ? commaSeparateNumber(parseFloat(sub_total_regular_and_weekend_less_percent_amount).toFixed(2)) : '';
					net_pay = (net_pay !== 0) ? commaSeparateNumber(parseFloat(net_pay).toFixed(2)) : '';

					$('#regular_hours').text(regular_hours);
					$('#weekend_hours').text(weekend_hours);
					$('#regular_ot_amount').text(regular_ot_amount);
					$('#weekend_ot_amount').text(weekend_ot_amount);
					$('#regular_ot_less_percent_amount').text(regular_ot_less_percent_amount);
					$('#weekend_ot_less_percent_amount').text(weekend_ot_less_percent_amount);
					$('#sub_total_regular_and_weekend_amount').text(sub_total_regular_and_weekend_amount);
					$('#sub_total_regular_and_weekend_less_percent_amount').text(sub_total_regular_and_weekend_less_percent_amount);
					$('#bank_account_number').text(data.nonplantilla_employee.atm_no);
					$('#net_pay').text(net_pay);
					$('#nonplantilla').removeClass('hidden');
					$('#btnModal').trigger('click');
				}else{

					fullname = data.plantilla_employee.employees.lastname+' '+data.plantilla_employee.employees.firstname+' '+data.plantilla_employee.employees.middlename;
					$('#fullname').text(fullname);

					daily_rate = (data.plantilla_employee.daily_rate_amount) ? commaSeparateNumber(parseFloat(data.plantilla_employee.daily_rate_amount).toFixed(2)) : '';
					$('#dailyrate').text((daily_rate));

					// monthly_rate = data.nonplantilla_employee.monthly_rate_amount;
					// $('#basic_pay').text(commaSeparateNumber(monthly_rate));

					numberofworkingdays = data.attendanceinfo.actual_workdays;

					$('#noofworkingdays').text(numberofworkingdays);


					regularot = (data.overtimeinfo !== null) ?  data.overtimeinfo.actual_regular_overtime : 0;
					weekendot = (data.overtimeinfo !== null) ?  data.overtimeinfo.actual_special_overtime : 0;

					regularhours = (data.overtimeinfo !== null) ?  data.overtimeinfo.actual_regular_overtime : 0;
					weekendhours = (data.overtimeinfo !== null) ?  data.overtimeinfo.actual_special_overtime : 0;

					regularotamount = (data.overtimeinfo !== null) ?  data.overtimeinfo.total_regular_amount : 0;
					weekendotamount = (data.overtimeinfo !== null) ?  data.overtimeinfo.total_special_amount : 0;


					taxamountone = (data.plantilla_employee.tax_contribution) ? parseFloat(data.plantilla_employee.tax_contribution) : 0;
					subtotalregularandweekendamount = (regularotamount + weekendotamount);

					netpay = (parseFloat(subtotalregularandweekendamount) - parseFloat(taxamountone));

					regularotamount = (regularotamount !== 0) ? commaSeparateNumber(parseFloat(regularotamount).toFixed(2)) : '';
					weekendotamount = (weekendotamount !== 0) ? commaSeparateNumber(parseFloat(weekendotamount).toFixed(2)) : '';
					subtotalregularandweekendamount = (subtotalregularandweekendamount !== 0) ? commaSeparateNumber(parseFloat(subtotalregularandweekendamount).toFixed(2)) : '';
					netpay = (netpay !== 0) ? commaSeparateNumber(parseFloat(Math.abs(netpay)).toFixed(2)) : '';

					$('#regularhours').text(regularhours);
					$('#weekendhours').text(weekendhours);
					$('#regularotamount').text(regularotamount);
					$('#weekendotamount').text(weekendotamount);
					$('#gross_pay').text(subtotalregularandweekendamount);
					$('#bankaccountnumber').text(data.plantilla_employee.atm_no);
					$('#netpay').text(netpay);
					$('#plantilla').removeClass('hidden');
					$('#btnModal').trigger('click');

				}
			}
		})
	}


});
})
</script>
@endsection