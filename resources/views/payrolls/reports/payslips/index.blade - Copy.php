@extends('app-reports')

@section('reports-content')
<link rel="stylesheet" type="text/css" media="print" href="{{ asset('css/printportrait.css') }}">
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td ><span><b>Employee Name</b></span>
				<select class="form-control select2" name="employee_id" id="employee_id" style="margin:auto;width: 100%;">
						<option value=""></option>
						@foreach($employeeinfo as $value)
						<option value="{{ $value->id }}">{{ $value->lastname }} {{ $value->firstname }} {{ $value->middlename }}</option>
						@endforeach
					</select>
			</td>
		</tr>
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Covered Date</b></span>
					</div>
				</div>
				<div class="row" style="margin-right: -5px;margin-left: -5px;">
					@include('payrolls.includes._months-year')
				</div>
			</td>
		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<a class="btn btn-xs btn-danger preview">Preview</a>
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0">
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="panel-body" id="reports">
		       <div class="row">
		       		<div class="col-md-12">
	      				<div class="row" style="margin-left: 10px;">
							<div class="col-md-12">
								<img src="{{ url('images/mirdc_logo.gif') }}" style="height: 40px;display: inline-block;position: relative;">
								<span><b>METAL INDUSTRY AND RESEARCH AND DEVELOPMENT</b></span>
							</div>
						</div>
						<br>
   						<div class="row" style="margin-left: 10px;">
       						<div class="col-md-2" style="font-weight: bold;">
       							Employee Name <span style="padding-left: 15px">:</span>
       						</div>
   							<div class="col-md-4 text-left">
   								<span id="employee_name"></span>
   							</div>
   							<div class="col-md-2"  style="font-weight: bold;">
   								Employee No<span style="padding-left: 15px">:</span>
   							</div>
   							<div class="col-md-4"><span id="employee_number"></span></div>
   						</div>
   						<div class="row" style="margin-left: 10px;">
   							<div class="col-md-2" style="font-weight: bold;">
   								Position <span style="margin-left: 15px">:</span>
   							</div>
   							<div class="col-md-4"><span id="position"></span></div>
   							<div class="col-md-2" style="font-weight: bold;">
   								Division <span style="margin-left: 15px">:</span>
   							</div>
   							<div class="col-md-4"><span id="division"></span></div>
   						</div>

	       				<div class="reports-body" style="margin: auto;width: 95%;padding-top: 25px;">

	       				</div>
	       				<br>
	       				<div class="row" style="margin-left: 10px;">
	       					<div class="col-md-6">
	       						<div class="footer-left-message">
		       						<i>
		       							This is a computer generated document and does
										not require any signature if without alterations
		       						</i>

	       						</div>
	       					</div>
	       				</div>
	       			</div>
		       </div>
	       </div>

	    </div>
	 </div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();
	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$('#select_year').trigger('change');
	$('#select_month').trigger('change');
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$(document).on('change','#select_searchvalue',function(){
		_searchvalue = "";
		_searchvalue = $(this).find(':selected').val();

	})

	$(document).on('change','#emp_status',function(){
		_emp_status = "";
		_emp_status = $(this).find(':selected').val();

	})
	$(document).on('change','#emp_type',function(){
		_emp_type = "";
		_emp_type = $(this).find(':selected').val();

	})
	$(document).on('change','#searchby',function(){
		_searchby = "";
		_searchby = $(this).find(':selected').val();

	})


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});

var months ={
	1:'January',
	2:'February',
	3:'March',
	4:'April',
	5:'May',
	6:'June',
	7:'July',
	8:'August',
	9:'September',
	10:'October',
	11:'November',
	12:'December',
}

$(document).on('click','#print',function(){
	$('#reports').printThis();
});

$(document).on('click','.preview',function(){

	year = (_Year) ? _Year : '';
	empid = (_empid) ? _empid : '';
	month = (_Month) ? _Month : '';
	emp_type = (_emp_type) ? _emp_type : '';
	emp_status = (_emp_status) ? _emp_status : '';
	month = (_Month) ? _Month : '';
	category = (_searchvalue) ? _searchvalue : '';
	searchby = (_searchby) ? _searchby : '';

	if(!empid || !year || !month){

		swal({
			  title: "Select year, month, and employee first!",
			  type: "warning",
			  showCancelButton: false,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Yes",
			  closeOnConfirm: false

		});

	}else{
		$.ajax({
			url:base_url+module_prefix+module+'/getPayslip',
			data:{
				'id':empid,
				'year':year,
				'month':month,
				'emp_type':emp_type,
				'emp_status':emp_status,
				'category':category,
				'searchby':searchby,
			},
			type:'GET',
			dataType:'JSON',
			success:function(data){
				console.log(data)
				if(data.transaction !== null){

					arr = [];

					// ===============
					// HEADER
					// ===============

					arr +=  '<div class="row" style="border: 2px solid #000;">';
					arr +=	'<div class="col-md-12 text-center" style="font-weight: bold;">EMPLOYEE PAYSLIP</div>';
					arr +=  '</div>';

					arr +=  '<div class="row" style="padding-top: 10px;">';
					arr +=	'<div class="col-md-3 text-center" style="font-weight: bold;border: 1px solid #000;">BASIC SALARY</div>';
					// arr +=	'<div class="col-md-1">&nbsp;</div>';
					arr +=	'<div class="col-md-3 text-center" style="font-weight: bold;border: 1px solid #000;">BENEFITS</div>';
					// arr +=	'<div class="col-md-1">&nbsp;</div>';
					arr +=	'<div class="col-md-6 text-center" style="font-weight: bold;border: 1px solid #000;">'
					arr +=  'DEDUCTIONS';
					arr +=	'</div>';
					arr +=	'</div>';

					firstname = (data.transaction.employees.firstname !== null) ? data.transaction.employees.firstname : '';
					lastname = (data.transaction.employees.lastname !== null) ? data.transaction.employees.lastname : '';
					middlename = (data.transaction.employees.middlename !== null) ? data.transaction.employees.middlename : '';
					employee_number = (data.transaction.employees.employee_number !== null) ? data.transaction.employees.employee_number : '';
					position = (data.employeeinformation.positions !== null) ? data.employeeinformation.positions.Name : '';
					division = (data.employeeinformation.divisions !== null) ? data.employeeinformation.divisions.Name : '';

					basic_amount = (data.transaction.total_basicpay_amount) ? data.transaction.total_basicpay_amount : 0;

					gsis_amount = (data.transaction.gsis_ee_share !== null) ? data.transaction.gsis_ee_share : 0;
					pagibig_amount = (data.transaction.pagibig_share !== null) ? data.transaction.pagibig_share : 0;
					philhealth_amount = (data.transaction.phic_share !== null) ? data.transaction.phic_share : 0;
					tax_amount = (data.transaction.tax_amount !== null) ? data.transaction.tax_amount : 0;
					total_loan_amount = (data.transaction.total_loan !== null) ? data.transaction.total_loan : 0;

					// ===============
					// BENEFIT INFO
					// ===============
					transportation_amount = 0;
					representation_amount = 0;
					hazard_amount 		  = 0;
					sa_amount 		  	  = 0;
					la_amount 		  	  = 0;
					rata_amount 		  = 0;
					salaAbsentAmount 	  = 0;
					saUndertimeAmount	  = 0;
					laUndertimeAmount	  = 0;
					longevityAmount 	  = 0;
					pera_amount 		  = 0;
					pdiemAmount 		  = 0;
					coveredDate 		  = "";
					if(data.benefitinfo.length !== 0){
						$.each(data.benefitinfo,function(k,v){
							switch(v.benefits.code){
								case 'RATA1':
									transportation_amount = (v.amount !== null) ? v.amount : 0;
									break;
								case 'RATA2':
									representation_amount = (v.amount !== null) ? v.amount : 0;
									usedAmount = (v.used_amount !== null) ? v.used_amount : 0;
									coveredDate = (v.ta_covered_date !== null) ? v.ta_covered_date : "";
									break;
								case 'HP':
									hazard_amount = (v.amount !== null) ? v.amount : 0;
									break;
								case 'SA':
									sa_amount = (v.amount !== null) ? v.amount : 0;
									salaAbsentAmount = (v.sala_absent_amount) ? v.sala_absent_amount : 0;
									saUndertimeAmount = (v.sala_undertime_amount) ? v.sala_undertime_amount : 0;
									break;
								case 'LA':
									la_amount = (v.amount !== null) ? v.amount : 0;
									laUndertimeAmount = (v.sala_undertime_amount) ? v.sala_undertime_amount : 0;
									break;
								case 'PERA':
									pera_amount = (v.amount !== null) ? v.amount : 0;
									break;
								case 'LP':
									longevityAmount = (v.amount !== null) ? v.amount : 0;
									break;
								case 'PDIEM':
									pdiemAmount = (v.amount !== null) ? v.amount : 0;
									break;
							}
						})
					}


					salaAmount = parseFloat(sa_amount) + parseFloat(la_amount);

					rata_amount = (parseFloat(transportation_amount) + parseFloat(representation_amount));

					gross_income_amount = (parseFloat(basic_amount) + parseFloat(pera_amount) + parseFloat(rata_amount) + parseFloat(hazard_amount) + parseFloat(salaAmount) + parseFloat(longevityAmount) + parseFloat(pdiemAmount));

					total_deduction_amount = (parseFloat(gsis_amount) + parseFloat(pagibig_amount) + parseFloat(philhealth_amount) + parseFloat(tax_amount) + parseFloat(total_loan_amount) + parseFloat(salaAbsentAmount) + parseFloat(saUndertimeAmount) + Number(laUndertimeAmount) + Number(usedAmount));

					salaUndertimeAmount = Number(saUndertimeAmount) + Number(laUndertimeAmount);

					net_income = (parseFloat(gross_income_amount) - parseFloat(total_deduction_amount));
					net_half = (parseFloat(net_income)/2);


					basic_amount = (basic_amount !== 0 )? commaSeparateNumber(parseFloat(basic_amount).toFixed(2)) : '';
					pera_amount = (pera_amount !== 0 )? commaSeparateNumber(parseFloat(pera_amount).toFixed(2)) : '';
					rata_amount = (rata_amount !== 0 ) ? commaSeparateNumber(parseFloat(rata_amount).toFixed(2)) : '';
					hazard_amount = (hazard_amount !== 0 ) ? commaSeparateNumber(parseFloat(hazard_amount).toFixed(2)) : '';
					sala_amount = (salaAmount !== 0 ) ? commaSeparateNumber(parseFloat(salaAmount).toFixed(2)) : '';
					sala_absent_amount = (salaAbsentAmount !== 0 ) ? commaSeparateNumber(parseFloat(salaAbsentAmount).toFixed(2)) : '';
					sala_undertime_amount = (salaUndertimeAmount !== 0 ) ? commaSeparateNumber(parseFloat(salaUndertimeAmount).toFixed(2)) : '';
					gsis_amount 	= (gsis_amount !== 0) ? commaSeparateNumber(parseFloat(gsis_amount).toFixed(2)) : '';
					pagibig_amount 	= (pagibig_amount !== 0) ? commaSeparateNumber(parseFloat(pagibig_amount).toFixed(2)) : '';
					philhealth_amount 	= (philhealth_amount !== 0) ? commaSeparateNumber(parseFloat(philhealth_amount).toFixed(2)) : '';
					tax_amount 	= (tax_amount !== 0) ? commaSeparateNumber(parseFloat(tax_amount).toFixed(2)) : '';
					gross_income_amount 	= (gross_income_amount !== 0) ? commaSeparateNumber(parseFloat(gross_income_amount).toFixed(2)) : '';
					net_income 	= (net_income !== 0) ? commaSeparateNumber(parseFloat(net_income).toFixed(2)) : '';
					net_half 	= (net_half !== 0) ? commaSeparateNumber(parseFloat(net_half).toFixed(2)) : '';
					total_deduction_amount 	= (total_deduction_amount !== 0) ? commaSeparateNumber(parseFloat(total_deduction_amount).toFixed(2)) : '';
					longevity_amount 	= (longevityAmount !== 0) ? commaSeparateNumber(parseFloat(longevityAmount).toFixed(2)) : '';

					pdiem_amount 	= (pdiemAmount !== 0) ? commaSeparateNumber(parseFloat(pdiemAmount).toFixed(2)) : '';

					arr += '<div class="row" style="margin-top:10px;">';
					arr +=		'<div class="col-md-3">';
					arr +=			'<div class="col-md-4">BASIC</div>';
					arr +=			'<div class="col-md-4 text-center">:</div>';
					arr +=			'<div class="col-md-4 text-right">'+basic_amount+'</div>';
					arr +=		'</div>';
					arr +=		'<div class="col-md-3">';
					arr +=			'<div class="col-md-6">SALA</div>';
					arr +=			'<div class="col-md-3 text-center">:</div>';
					arr +=			'<div class="col-md-3 text-right">'+sala_amount+'</div>';
					arr +=		'</div>';
					arr +=		'<div class="col-md-3">';
					arr +=			'<div class="col-md-6">GSIS</div>';
					arr +=			'<div class="col-md-2 text-center">:</div>';
					arr +=			'<div class="col-md-4 text-right">'+gsis_amount+'</div>';
					arr +=		'</div>';
					arr +=		'<div class="col-md-3">';
					arr +=			'<div class="col-md-6">Withholding Tax</div>';
					arr +=			'<div class="col-md-2 text-center">:</div>';
					arr +=			'<div class="col-md-4 text-right">'+tax_amount+'</div>';
					arr +=		'</div>';
					arr += '</div>';

					arr += '<div class="row">';
					arr +=		'<div class="col-md-3">';
					arr +=			'<div class="col-md-4">PERA</div>';
					arr +=			'<div class="col-md-4 text-center">:</div>';
					arr +=			'<div class="col-md-4 text-right">'+pera_amount+'</div>';
					arr +=		'</div>';
					arr +=		'<div class="col-md-3">';
					arr +=			'<div class="col-md-6">HAZARD PAY</div>';
					arr +=			'<div class="col-md-3 text-center">:</div>';
					arr +=			'<div class="col-md-3 text-right">'+hazard_amount+'</div>';
					arr +=		'</div>';
					arr +=		'<div class="col-md-3">';
					arr +=			'<div class="col-md-6">Pagibig</div>';
					arr +=			'<div class="col-md-2 text-center">:</div>';
					arr +=			'<div class="col-md-4 text-right">'+pagibig_amount+'</div>';
					arr +=		'</div>';
					arr +=		'<div class="col-md-3">';
					arr +=			'<div class="col-md-6">Philhealth</div>';
					arr +=			'<div class="col-md-2 text-center">:</div>';
					arr +=			'<div class="col-md-4 text-right">'+philhealth_amount+'</div>';
					arr +=		'</div>';
					arr += '</div>';

					arr += '<div class="row">';
					arr +=		'<div class="col-md-3">';
					arr +=			'<div class="col-md-4">RATA</div>';
					arr +=			'<div class="col-md-4 text-center">:</div>';
					arr +=			'<div class="col-md-4 text-right">'+rata_amount+'</div>';
					arr +=		'</div>';
					arr +=		'<div class="col-md-3">';
					arr +=			'<div class="col-md-6">Longetivity Pay</div>';
					arr +=			'<div class="col-md-3 text-center">:</div>';
					arr +=			'<div class="col-md-3 text-right">'+longevity_amount+'</div>';
					arr +=		'</div>';
					arr +=		'<div class="col-md-3">';
					arr +=			'<div class="col-md-6">Absent SALA</div>';
					arr +=			'<div class="col-md-2 text-center">:</div>';
					arr +=			'<div class="col-md-4 text-right">'+sala_absent_amount+'</div>';
					arr +=		'</div>';
					if(pdiemAmount){
					arr +=		'<div class="col-md-3">';
					arr +=			'<div class="col-md-6">PDIEM</div>';
					arr +=			'<div class="col-md-2 text-center">:</div>';
					arr +=			'<div class="col-md-4 text-right">'+pdiem_amount+'</div>';
					arr +=		'</div>';
					}
					arr += '</div>';

					arr += '<div class="row">';
					arr +=		'<div class="col-md-3">';
					arr +=		'</div>';
					arr +=		'<div class="col-md-3">';
					arr +=		'</div>';
					arr +=		'<div class="col-md-3">';
					arr +=			'<div class="col-md-6">Undertime SALA</div>';
					arr +=			'<div class="col-md-2 text-center">:</div>';
					arr +=			'<div class="col-md-4 text-right">'+sala_undertime_amount+'</div>';
					arr +=		'</div>';
					arr += '</div>';

					if(usedAmount !== 0){
						arr += '<div class="row">';
						arr +=		'<div class="col-md-3">';
						arr +=		'</div>';
						arr +=		'<div class="col-md-3">';
						arr +=		'</div>';
						arr +=		'<div class="col-md-3">';
						arr +=		'</div>';
						arr +=		'<div class="col-md-3">';
						arr +=			'<div class="col-md-6">TA ('+coveredDate+')</div>';
						arr +=			'<div class="col-md-2 text-center">:</div>';
						arr +=			'<div class="col-md-4 text-right">'+commaSeparateNumber(parseFloat(usedAmount).toFixed(2))+'</div>';
						arr +=		'</div>';
						arr += '</div>';
					}



					if(data.loaninfo.length !== 0){
						$.each(data.loaninfo,function(k,v){
							arr += '<div class="row">';
							arr +=		'<div class="col-md-3">';
							arr +=		'</div>';
							arr +=		'<div class="col-md-3">';
							arr +=		'</div>';
							arr +=		'<div class="col-md-3">';
							arr +=		'</div>';
							arr +=		'<div class="col-md-3">';
							arr +=			'<div class="col-md-6">'+v.loans.name+'</div>';
							arr +=			'<div class="col-md-2 text-center">:</div>';
							arr +=			'<div class="col-md-4 text-right">'+commaSeparateNumber(parseFloat(v.amount).toFixed(2))+'</div>';
							arr +=		'</div>';
							arr += '</div>';
						})
					}


					arr += '<div class="row"  style="font-weight:bold;padding-top:50px;">';
					arr +=		'<div class="col-md-3">';
					arr +=		'</div>';
					arr +=		'<div class="col-md-3">';
					arr +=			'<div class="col-md-6">GROSS INCOME</div>';
					arr +=			'<div class="col-md-2 text-center">:</div>';
					arr +=			'<div class="col-md-4 text-right">'+gross_income_amount+'</div>';
					arr +=		'</div>';
					arr +=		'<div class="col-md-3">';
					arr +=		'</div>';
					arr +=		'<div class="col-md-3">';
					arr +=			'<div class="col-md-6">TOTAL DEDUCTIONS</div>';
					arr +=			'<div class="col-md-2 text-center">:</div>';
					arr +=			'<div class="col-md-4 text-right">'+total_deduction_amount+'</div>';
					arr +=		'</div>';
					arr += '</div>';

					arr += '<div class="row"  style="font-weight:bold;padding-top:50px;">';
					arr +=		'<div class="col-md-4">';
					arr +=			'<div class="col-md-6">NET INCOME</div>';
					arr +=			'<div class="col-md-1 text-center">:</div>';
					arr +=			'<div class="col-md-4 text-right">'+months[_Month]+' '+_Year+'</div>';
					arr +=		'</div>';
					arr +=		'<div class="col-md-2">';
					arr +=			'<div class="col-md-6 text-right"></div>';
					arr +=			'<div class="col-md-1 text-center"></div>';
					arr +=			'<div class="col-md-5 text-right">'+net_income+'</div>';
					arr +=		'</div>';
					arr += '</div>';
					arr += '<div class="row"  style="font-weight:bold;padding-top:50px;">';
					arr +=		'<div class="col-md-4">';
					arr +=			'<div class="col-md-6">NET HALF</div>';
					arr +=			'<div class="col-md-1 text-center">:</div>';
					arr +=			'<div class="col-md-4 text-right"></div>';
					arr +=		'</div>';
					arr +=		'<div class="col-md-2">';
					arr +=			'<div class="col-md-6 text-right"></div>';
					arr +=			'<div class="col-md-1 text-center"></div>';
					arr +=			'<div class="col-md-5 text-right">'+net_half+'</div>';
					arr +=		'</div>';
					arr += '</div>';



					$('.reports-body').html(arr);

					$('#employee_name').text(lastname+' '+firstname+' '+middlename);
					$('#employee_number').text(employee_number);
					$('#position').text(position);
					$('#division').text(division);

					$('#btnModal').trigger('click');

				}else{
					swal({
						title: "No Records Found",
						type: "warning",
						showCancelButton: false,
						confirmButtonClass: "btn-danger",
						confirmButtonText: "Yes",
						closeOnConfirm: false
					});
				}
			}
		})
	}



});

})
</script>
@endsection