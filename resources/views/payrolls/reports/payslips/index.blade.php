@extends('app-reports')

@section('reports-content')
<link rel="stylesheet" type="text/css" media="print" href="{{ asset('css/printportrait.css') }}">
<div class="loan-reports">
	<label>{{ $title }}</label>
		<div class="panel panel-default">
			<div class="panel-body">
				@include('payrolls.reports.includes._table-posted-payslip')
				<form action="{{ url($module_prefix.'/'.$module) }}" method="post" id="form">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="row p-2">
						<div class="col-md-12">
							<span><b>Division</b></span>
							<select class="form-control select2" name="division_id" id="division_id" style="margin:auto;width: 100%;">
								<option value=""></option>
								@foreach($divisions as $value)
								<option value="{{ $value->RefId }}">{{ $value->Name }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="row p-2">
						<div class="col-md-12">
							<span><b>Employee Name</b></span>
							<select class="form-control select2" name="employee_id" id="employee_id" style="margin:auto;width: 100%;">
								<option value=""></option>
								@foreach($employeeinfo as $value)
								<option value="{{ $value->id }}">{{ $value->lastname }} {{ $value->firstname }} {{ $value->middlename }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<span class="font-weight-bold col-md-12 pl-0 ml-0">Covered Date</span>
					<div class="row p-3">
						@include('payrolls.includes._months-year')
					</div>
					<br>
					@include('payrolls.reports.includes._post-button')
				</form>
			</div>
		</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size" style="width: 90% !important;height: 90% !important;">
	    <div class="mypanel border0">
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="panel-body-report p-6" id="reports" style="font-size: 8pt !important;"></div>
	    </div>
	 </div>
</div>

@endsection


@section('js-logic2')
@include('payrolls.reports.includes._post-button-script')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();
	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$('#select_year').trigger('change');
	$('#select_month').trigger('change');
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$(document).on('change','#select_searchvalue',function(){
		_searchvalue = "";
		_searchvalue = $(this).find(':selected').val();

	})

	$(document).on('change','#emp_status',function(){
		_emp_status = "";
		_emp_status = $(this).find(':selected').val();

	})
	$(document).on('change','#emp_type',function(){
		_emp_type = "";
		_emp_type = $(this).find(':selected').val();

	})
	$(document).on('change','#searchby',function(){
		_searchby = "";
		_searchby = $(this).find(':selected').val();

	})


var months ={
	1:'January',
	2:'February',
	3:'March',
	4:'April',
	5:'May',
	6:'June',
	7:'July',
	8:'August',
	9:'September',
	10:'October',
	11:'November',
	12:'December',
}

$(document).on('click','#print',function(){
	$('#reports').printThis();
});

// View posted report
$(document).on('click','.view',function(){
	_Year 	= $(this).data('year');
	_Month 	= $(this).data('month');
	_empid 	= $(this).data('employee_id');
	$('#preview').trigger('click');
});

$(document).on('click','#preview',function(){

	year = (_Year) ? _Year : '';
	empid = (_empid) ? _empid : '';
	month = (_Month) ? _Month : '';
	emp_type = (_emp_type) ? _emp_type : '';
	emp_status = (_emp_status) ? _emp_status : '';
	month = (_Month) ? _Month : '';
	category = (_searchvalue) ? _searchvalue : '';
	searchby = (_searchby) ? _searchby : '';

	if(!year || !month){

		swal({
			  title: "Select year, month, and employee first!",
			  type: "warning",
			  showCancelButton: false,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Yes",
			  closeOnConfirm: false

		});

	}else{
		$.ajax({
			url:base_url+module_prefix+module+'/show',
			data:{
				'id':empid,
				'year':year,
				'month':month,
				'division_id':$('#division_id').find(':selected').val()
			},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				if(data.transaction.length != 0){

					arr = [];
					ctr = 0;

					$.each(data.transaction,function(k,v){

						arr += '<div class="row mb-6">';
						arr += '<div class="col-md-12">';
						arr +=	'<img src="{{ url("images/mirdc_logo.gif") }}" style="height: 40px;display: inline-block;position: relative;">';
						arr += '<span><b>METAL INDUSTRY AND RESEARCH AND DEVELOPMENT</b></span>';
						arr += '</div>';
						arr += '</div>';

						firstname = (v.employees.firstname !== null) ? v.employees.firstname : '';
						lastname = (v.employees.lastname !== null) ? v.employees.lastname : '';
						middlename = (v.employees.middlename !== null) ? v.employees.middlename : '';
						employee_number = (v.employees.employee_number !== null) ? v.employees.employee_number : '';
						position = (v.positions !== null) ? v.positions.Name : '';
						division = (v.divisions !== null) ? v.divisions.Name : '';

						fullname = lastname+' '+firstname+' '+middlename;

						arr += '<div class="row mb-2">';
						arr += '<div class="col-md-2" style="font-weight: bold;">';
						arr += 'Employee Name <span style="padding-left: 15px">:</span>';
						arr += '</div>';
						arr += '<div class="col-md-4 text-left">'+fullname+'</div>';
						arr += '<div class="col-md-2"  style="font-weight: bold;">';
						arr += 'Employee No<span style="padding-left: 15px">:</span>';
						arr += '</div>';
						arr += '<div class="col-md-4">'+employee_number+'</div>';
						arr += '</div>';

						arr += '<div class="row mb-2">';
						arr += '<div class="col-md-2" style="font-weight: bold;">'
						arr += 'Position <span style="margin-left: 15px">:</span>';
						arr += '</div>';
						arr += '<div class="col-md-4">'+position+'</div>';
						arr += '<div class="col-md-2" style="font-weight: bold;">';
						arr += 'Division <span style="margin-left: 15px">:</span>';
						arr += '</div>';
						arr += '<div class="col-md-4">'+division+'</div>';
						arr += '</div>';


						arr +=  '<div class="row" style="border: 2px solid #000;">';
						arr +=	'<div class="col-md-12 text-center" style="font-weight: bold;">EMPLOYEE PAYSLIP</div>';
						arr +=  '</div>';

						arr +=  '<div class="row" style="padding-top: 10px;">';
						arr +=	'<div class="col-md-3 text-center" style="font-weight: bold;border: 1px solid #000;">BASIC SALARY</div>';
						// arr +=	'<div class="col-md-1">&nbsp;</div>';
						arr +=	'<div class="col-md-3 text-center" style="font-weight: bold;border: 1px solid #000;">BENEFITS</div>';
						// arr +=	'<div class="col-md-1">&nbsp;</div>';
						arr +=	'<div class="col-md-6 text-center" style="font-weight: bold;border: 1px solid #000;">'
						arr +=  'DEDUCTIONS';
						arr +=	'</div>';
						arr +=	'</div>';

						basic_amount = (v.total_basicpay_amount) ? v.total_basicpay_amount : 0;

						gsis_amount 		= (v.gsis_ee_share !== null) ? v.gsis_ee_share : 0;
						pagibig_amount 		= (v.pagibig_share !== null) ? v.pagibig_share : 0;
						philhealth_amount 	= (v.phic_share !== null) ? v.phic_share : 0;
						tax_amount 			= (v.tax_amount !== null) ? v.tax_amount : 0;
						total_loan_amount 	= (v.total_loan !== null) ? v.total_loan : 0;
						totalOtherDeduct 	= (v.total_otherdeduct !== null) ? v.total_otherdeduct : 0;

						// ===============
						// BENEFIT INFO
						// ===============
						transportation_amount = 0;
						representation_amount = 0;
						hazard_amount 		  = 0;
						sa_amount 		  	  = 0;
						la_amount 		  	  = 0;
						rata_amount 		  = 0;
						salaAbsentAmount 	  = 0;
						saUndertimeAmount	  = 0;
						laUndertimeAmount	  = 0;
						longevityAmount 	  = 0;
						pera_amount 		  = 0;
						pdiemAmount 		  = 0;
						usedAmount 			  = 0;
						coveredDate 		  = "";
						if(v.benefit_transaction.length !== 0){
							$.each(v.benefit_transaction,function(k1,v1){
								switch(v1.benefits.code){
									case 'RATA1':
										transportation_amount = (v1.amount !== null) ? v1.amount : 0;
										break;
									case 'RATA2':
										representation_amount = (v1.amount !== null) ? v1.amount : 0;
										usedAmount = (v1.used_amount !== null) ? v1.used_amount : 0;
										coveredDate = (v1.ta_covered_date !== null) ? v1.ta_covered_date : "";
										break;
									case 'HP':
										hazard_amount = (v1.amount !== null) ? v1.amount : 0;
										break;
									case 'SA':
										sa_amount = (v1.amount !== null) ? v1.amount : 0;
										salaAbsentAmount = (v1.sala_absent_amount) ? v1.sala_absent_amount : 0;
										saUndertimeAmount = (v1.sala_undertime_amount) ? v1.sala_undertime_amount : 0;
										break;
									case 'LA':
										la_amount = (v1.amount !== null) ? v1.amount : 0;
										laUndertimeAmount = (v1.sala_undertime_amount) ? v1.sala_undertime_amount : 0;
										break;
									case 'PERA':
										pera_amount = (v1.amount !== null) ? v1.amount : 0;
										break;
									case 'LP':
										longevityAmount = (v1.amount !== null) ? v1.amount : 0;
										break;
									case 'PDIEM':
										pdiemAmount = (v1.amount !== null) ? v1.amount : 0;
										break;
								}
							})
						}


						salaAmount = parseFloat(sa_amount) + parseFloat(la_amount);

						rata_amount = (parseFloat(transportation_amount) + parseFloat(representation_amount));

						gross_income_amount = (parseFloat(basic_amount) + parseFloat(pera_amount) + parseFloat(rata_amount) + parseFloat(hazard_amount) + parseFloat(salaAmount) + parseFloat(longevityAmount) + parseFloat(pdiemAmount));

						total_deduction_amount = (parseFloat(gsis_amount) + parseFloat(pagibig_amount) + parseFloat(philhealth_amount) + parseFloat(tax_amount) + parseFloat(total_loan_amount) + parseFloat(salaAbsentAmount) + parseFloat(saUndertimeAmount) + Number(laUndertimeAmount) + Number(usedAmount) + Number(totalOtherDeduct));

						salaUndertimeAmount = Number(saUndertimeAmount) + Number(laUndertimeAmount);

						net_income = (parseFloat(gross_income_amount) - parseFloat(total_deduction_amount));
						net_half = (parseFloat(net_income)/2);


						basic_amount = (basic_amount !== 0 )? commaSeparateNumber(parseFloat(basic_amount).toFixed(2)) : '';
						pera_amount = (pera_amount !== 0 )? commaSeparateNumber(parseFloat(pera_amount).toFixed(2)) : '';
						rata_amount = (rata_amount !== 0 ) ? commaSeparateNumber(parseFloat(rata_amount).toFixed(2)) : '';
						hazard_amount = (hazard_amount !== 0 ) ? commaSeparateNumber(parseFloat(hazard_amount).toFixed(2)) : '';
						sala_amount = (salaAmount !== 0 ) ? commaSeparateNumber(parseFloat(salaAmount).toFixed(2)) : '';
						sala_absent_amount = (salaAbsentAmount !== 0 ) ? commaSeparateNumber(parseFloat(salaAbsentAmount).toFixed(2)) : '';
						sala_undertime_amount = (salaUndertimeAmount !== 0 ) ? commaSeparateNumber(parseFloat(salaUndertimeAmount).toFixed(2)) : '';
						gsis_amount 	= (gsis_amount !== 0) ? commaSeparateNumber(parseFloat(gsis_amount).toFixed(2)) : '';
						pagibig_amount 	= (pagibig_amount !== 0) ? commaSeparateNumber(parseFloat(pagibig_amount).toFixed(2)) : '';
						philhealth_amount 	= (philhealth_amount !== 0) ? commaSeparateNumber(parseFloat(philhealth_amount).toFixed(2)) : '';
						tax_amount 	= (tax_amount !== 0) ? commaSeparateNumber(parseFloat(tax_amount).toFixed(2)) : '';
						gross_income_amount 	= (gross_income_amount !== 0) ? commaSeparateNumber(parseFloat(gross_income_amount).toFixed(2)) : '';
						net_income 	= (net_income !== 0) ? commaSeparateNumber(parseFloat(net_income).toFixed(2)) : '';
						net_half 	= (net_half !== 0) ? commaSeparateNumber(parseFloat(net_half).toFixed(2)) : '';
						total_deduction_amount 	= (total_deduction_amount !== 0) ? commaSeparateNumber(parseFloat(total_deduction_amount).toFixed(2)) : '';
						longevity_amount 	= (longevityAmount !== 0) ? commaSeparateNumber(parseFloat(longevityAmount).toFixed(2)) : '';

						pdiem_amount 	= (pdiemAmount !== 0) ? commaSeparateNumber(parseFloat(pdiemAmount).toFixed(2)) : '';

						arr += '<div class="row" style="margin-top:10px;">';
						arr +=		'<div class="col-md-3">';
						arr +=			'<div class="col-md-4">BASIC</div>';
						arr +=			'<div class="col-md-4 text-center">:</div>';
						arr +=			'<div class="col-md-4 text-right">'+basic_amount+'</div>';
						arr +=		'</div>';
						arr +=		'<div class="col-md-3">';
						arr +=			'<div class="col-md-6">SALA</div>';
						arr +=			'<div class="col-md-3 text-center">:</div>';
						arr +=			'<div class="col-md-3 text-right">'+sala_amount+'</div>';
						arr +=		'</div>';
						arr +=		'<div class="col-md-3">';
						arr +=			'<div class="col-md-6">GSIS</div>';
						arr +=			'<div class="col-md-2 text-center">:</div>';
						arr +=			'<div class="col-md-4 text-right">'+gsis_amount+'</div>';
						arr +=		'</div>';
						arr +=		'<div class="col-md-3">';
						arr +=			'<div class="col-md-6">Withholding Tax</div>';
						arr +=			'<div class="col-md-2 text-center">:</div>';
						arr +=			'<div class="col-md-4 text-right">'+tax_amount+'</div>';
						arr +=		'</div>';
						arr += '</div>';

						arr += '<div class="row">';
						arr +=		'<div class="col-md-3">';
						arr +=			'<div class="col-md-4">PERA</div>';
						arr +=			'<div class="col-md-4 text-center">:</div>';
						arr +=			'<div class="col-md-4 text-right">'+pera_amount+'</div>';
						arr +=		'</div>';
						arr +=		'<div class="col-md-3">';
						arr +=			'<div class="col-md-6">HAZARD PAY</div>';
						arr +=			'<div class="col-md-3 text-center">:</div>';
						arr +=			'<div class="col-md-3 text-right">'+hazard_amount+'</div>';
						arr +=		'</div>';
						arr +=		'<div class="col-md-3">';
						arr +=			'<div class="col-md-6">Pagibig</div>';
						arr +=			'<div class="col-md-2 text-center">:</div>';
						arr +=			'<div class="col-md-4 text-right">'+pagibig_amount+'</div>';
						arr +=		'</div>';
						arr +=		'<div class="col-md-3">';
						arr +=			'<div class="col-md-6">Philhealth</div>';
						arr +=			'<div class="col-md-2 text-center">:</div>';
						arr +=			'<div class="col-md-4 text-right">'+philhealth_amount+'</div>';
						arr +=		'</div>';
						arr += '</div>';

						arr += '<div class="row">';
						arr +=		'<div class="col-md-3">';
						arr +=			'<div class="col-md-4">RATA</div>';
						arr +=			'<div class="col-md-4 text-center">:</div>';
						arr +=			'<div class="col-md-4 text-right">'+rata_amount+'</div>';
						arr +=		'</div>';
						arr +=		'<div class="col-md-3">';
						arr +=			'<div class="col-md-6">Longetivity Pay</div>';
						arr +=			'<div class="col-md-3 text-center">:</div>';
						arr +=			'<div class="col-md-3 text-right">'+longevity_amount+'</div>';
						arr +=		'</div>';
						arr +=		'<div class="col-md-3">';
						arr +=			'<div class="col-md-6">Absent SALA</div>';
						arr +=			'<div class="col-md-2 text-center">:</div>';
						arr +=			'<div class="col-md-4 text-right">'+sala_absent_amount+'</div>';
						arr +=		'</div>';
						if(pdiemAmount){
						arr +=		'<div class="col-md-3">';
						arr +=			'<div class="col-md-6">PDIEM</div>';
						arr +=			'<div class="col-md-2 text-center">:</div>';
						arr +=			'<div class="col-md-4 text-right">'+pdiem_amount+'</div>';
						arr +=		'</div>';
						}
						arr += '</div>';

						arr += '<div class="row">';
						arr +=		'<div class="col-md-3">';
						arr +=		'</div>';
						arr +=		'<div class="col-md-3">';
						arr +=		'</div>';
						arr +=		'<div class="col-md-3">';
						arr +=			'<div class="col-md-6">Undertime SALA</div>';
						arr +=			'<div class="col-md-2 text-center">:</div>';
						arr +=			'<div class="col-md-4 text-right">'+sala_undertime_amount+'</div>';
						arr +=		'</div>';
						arr += '</div>';

						if(usedAmount !== 0){
							arr += '<div class="row">';
							arr +=		'<div class="col-md-3">';
							arr +=		'</div>';
							arr +=		'<div class="col-md-3">';
							arr +=		'</div>';
							arr +=		'<div class="col-md-3">';
							arr +=		'</div>';
							arr +=		'<div class="col-md-3">';
							arr +=			'<div class="col-md-6">TA ('+coveredDate+')</div>';
							arr +=			'<div class="col-md-2 text-center">:</div>';
							arr +=			'<div class="col-md-4 text-right">'+commaSeparateNumber(parseFloat(usedAmount).toFixed(2))+'</div>';
							arr +=		'</div>';
							arr += '</div>';
						}



						if(v.loan_transaction.length !== 0){
							$.each(v.loan_transaction,function(k1,v1){
								arr += '<div class="row">';
								arr +=		'<div class="col-md-3">';
								arr +=		'</div>';
								arr +=		'<div class="col-md-3">';
								arr +=		'</div>';
								arr +=		'<div class="col-md-3">';
								arr +=		'</div>';
								arr +=		'<div class="col-md-3">';
								arr +=			'<div class="col-md-6">'+v1.loans.name+'</div>';
								arr +=			'<div class="col-md-2 text-center">:</div>';
								arr +=			'<div class="col-md-4 text-right">'+commaSeparateNumber(parseFloat(v1.amount).toFixed(2))+'</div>';
								arr +=		'</div>';
								arr += '</div>';
							})
						}

						if(v.deduction_transaction.length !== 0){
							$.each(v.deduction_transaction,function(k1,v1){
								arr += '<div class="row">';
								arr +=		'<div class="col-md-3">';
								arr +=		'</div>';
								arr +=		'<div class="col-md-3">';
								arr +=		'</div>';
								arr +=		'<div class="col-md-3">';
								arr +=		'</div>';
								arr +=		'<div class="col-md-3">';
								arr +=			'<div class="col-md-6">'+v1.deductions.name+'</div>';
								arr +=			'<div class="col-md-2 text-center">:</div>';
								arr +=			'<div class="col-md-4 text-right">'+commaSeparateNumber(parseFloat(v1.amount).toFixed(2))+'</div>';
								arr +=		'</div>';
								arr += '</div>';
							})
						}


						arr += '<div class="row font-weight-bold mb-3 mt-3">';
						arr +=		'<div class="col-md-3">';
						arr +=		'</div>';
						arr +=		'<div class="col-md-3">';
						arr +=			'<div class="col-md-6">GROSS INCOME</div>';
						arr +=			'<div class="col-md-2 text-center">:</div>';
						arr +=			'<div class="col-md-4 text-right">'+gross_income_amount+'</div>';
						arr +=		'</div>';
						arr +=		'<div class="col-md-3">';
						arr +=		'</div>';
						arr +=		'<div class="col-md-3">';
						arr +=			'<div class="col-md-6">TOTAL DEDUCTIONS</div>';
						arr +=			'<div class="col-md-2 text-center">:</div>';
						arr +=			'<div class="col-md-4 text-right">'+total_deduction_amount+'</div>';
						arr +=		'</div>';
						arr += '</div>';

						arr += '<div class="row font-weight-bold mb-2">';
						arr +=		'<div class="col-md-4">';
						arr +=			'<div class="col-md-6">NET INCOME</div>';
						arr +=			'<div class="col-md-1 text-center">:</div>';
						arr +=			'<div class="col-md-4 text-right">'+months[_Month]+' '+_Year+'</div>';
						arr +=		'</div>';
						arr +=		'<div class="col-md-2">';
						arr +=			'<div class="col-md-6 text-right"></div>';
						arr +=			'<div class="col-md-1 text-center"></div>';
						arr +=			'<div class="col-md-5 text-right">'+net_income+'</div>';
						arr +=		'</div>';
						arr += '</div>';
						arr += '<div class="row mb-3 font-weight-bold">';
						arr +=		'<div class="col-md-4">';
						arr +=			'<div class="col-md-6">NET HALF</div>';
						arr +=			'<div class="col-md-1 text-center">:</div>';
						arr +=			'<div class="col-md-4 text-right"></div>';
						arr +=		'</div>';
						arr +=		'<div class="col-md-2">';
						arr +=			'<div class="col-md-6 text-right"></div>';
						arr +=			'<div class="col-md-1 text-center"></div>';
						arr +=			'<div class="col-md-5 text-right">'+net_half+'</div>';
						arr +=		'</div>';
						arr += '</div>';

						arr += '<div class="row mb-4" style="margin-left: 10px;">';
	   					arr += '<div class="col-md-6">';
	   					arr += '<div class="footer-left-message">';
	       				arr += '<i>';
	       				arr += 'This is a computer generated document and does';
						arr += 'not require any signature if without alterations';
		       			arr += '</i>';
		   				arr += '</div>';
		   				arr += '</div>';
		   				arr += '</div><hr>';

		   				ctr++;

		   				if(ctr == 2){
		   					arr += '<div style="page-break-after:always;"></div>';
		   					arr += '<br>';
		   					ctr = 0;
		   				}


					})

					$('.panel-body-report').html(arr);
					$('#btnModal').trigger('click');

				}else{
					swal({
						title: "No Records Found",
						type: "warning",
						showCancelButton: false,
						confirmButtonClass: "btn-danger",
						confirmButtonText: "Yes",
						closeOnConfirm: false
					});
				}
			}
		})
	}



});

})
</script>
@endsection