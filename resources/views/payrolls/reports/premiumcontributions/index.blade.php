@extends('app-reports')


@section('reports-content')


<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Covered Date</b></span>
					</div>

				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select class="form-control select2" name="month" id="select_month">
								<option value=""></option>
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control select2" name="year" id="select_year">
								<option value=""></option>
							</select>
						</div>

					</div>
				</div>

			</td>

		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="width: 900px;">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid ">
	       		<div class="row">
	       			<div class="col-md-12 text-center" style="font-weight: bold">
	       				<img src="{{ url('images/mwssreportheader.png') }}" style="height: 80px;"> <br>
	       				Metropolitan Waterworks and Sewerage System		<br>
						MWSS Complex, Katipunan Road, Balara, Quezon City	<br>
						REGULATORY OFFICE		<br>
						For the month of <span id="pay_period"></span>		<br>	<br>

						Remittance List of Premium Contributions
	       			</div>
					<table class="table" style="border: 2px solid #5f5f5f">
					<thead>
						<tr class="text-center" style="border-top:2px solid #5f5f5f;font-weight: bold">
							<td rowspan="2">No</td>
							<td rowspan="2">Name</td>
							<td rowspan="2">BP No.</td>
							<td rowspan="2">Basic Salary</td>
							<td rowspan="2">Position</td>
							<td colspan="2">
								Compulsory Contributions <br>
								for Life & Retirement
							</td>
							<td rowspan="2">
									Employees <br>
									Compensation
							</td>
							<td rowspan="2">Opt. Life Ins. Prem.</td>
							<td rowspan="2">Total</td>
							<td rowspan="2" colspan="2">Remarks</td>
						</tr>
						<tr class="text-center" style="font-weight: bold">
							<td>Personal Share</td>
							<td>Gov't Share</td>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td style="border:none"></td>
							<td colspan="2" class="text-center" style="border:none">Prepared  by:</td>
							<td colspan="4" class="text-center" style="border:none"> Checked & Verified:</td>
							<td colspan="4" class="text-center" style="border:none">Approved:</td>
						</tr>
						<tr>
							<td style="border:none"></td>
							<td colspan="2" class="text-center" style="border:none">
								<b>Theresa V. Makiling </b> <br>
								 Finance Office
							</td>
							<td colspan="4" class="text-center" style="border:none">
								<b>Joriel M. Dagsa</b> <br>
								 Chief Corporate Accountant
							</td>
							<td colspan="4" class="text-center" style="border:none">
								<b> Virginia V. Octa</b> <br>
								 Manager, Administration Dept.
							</td>
						</tr>
					</tfoot>
					 <tbody id="tbl_body">
					 </tbody>
					</table>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
        year += '<option value='+y+'>'+y+'</option>';
	}
    $('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
    for ( m =  0; m <= month.length - 1; m++) {
    	mArr += '<option '+month[m]+'>'+month[m]+'</option>';
    }
    $('#select_month').html(mArr);


// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$('#select_year').select2({
		allowClear:true,
	    placeholder: "Year",
	});

	$('#select_month').select2({
		allowClear:true,
	    placeholder: "Month",
	});

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$(document).on('change','#select_searchvalue',function(){
		_searchvalue = "";
		_searchvalue = $(this).find(':selected').val();

	})

	$(document).on('change','#emp_status',function(){
		_emp_status = "";
		_emp_status = $(this).find(':selected').val();

	})
	$(document).on('change','#emp_type',function(){
		_emp_type = "";
		_emp_type = $(this).find(':selected').val();

	})
	$(document).on('change','#searchby',function(){
		_searchby = "";
		_searchby = $(this).find(':selected').val();

	})


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});

$(document).on('click','#preview',function(){

	if(!_Year && !_Month){
		swal({
			  title: "Select year and month first",
			  type: "warning",
			  showCancelButton: false,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Yes",
			  closeOnConfirm: false

		});
	}else{
		$.ajax({
			url:base_url+module_prefix+module+'/show',
			data:{'month':_Month,'year':_Year},
			type:'GET',
			dataType:'JSON',
			success:function(data){
				console.log(data);
				if(data.length !== 0){

					arr = [];

					var total_premium_amount = 0;
					var net_premium_amount = 0;
					var net_employer_share_amount = 0;
					var net_personal_share_amount = 0;
					$.each(data,function(k,v){

						fullname = v.employees.lastname+' '+v.employees.firstname+' '+v.employees.middlename;
						bp_number = (v.employeeinfo.bp_no !== null) ? v.employeeinfo.bp_no : '';
						// position  = (v.positionitems !== null) ? v.positionitems.positions.name : '';
						basic_salary_amount  = (v.salaryinfo !== null) ? v.salaryinfo.salary_new_rate : 0;

						personal_share_amount = (v.employeeinfo !== null) ? v.employeeinfo.gsis_contribution : 0;
						employer_share_amount = (v.employeeinfo !== null) ? v.employeeinfo.er_gsis_share : 0;
						total_premium_amount = (parseFloat(personal_share_amount) + parseFloat(employer_share_amount));

						net_personal_share_amount += parseFloat(personal_share_amount);
						net_employer_share_amount += parseFloat(employer_share_amount);
						net_premium_amount += parseFloat(total_premium_amount);




						basic_salary_amount = (basic_salary_amount !== 0) ? commaSeparateNumber(parseFloat(basic_salary_amount).toFixed(2)) : '';
						personal_share_amount = (personal_share_amount !== 0) ? commaSeparateNumber(parseFloat(personal_share_amount).toFixed(2)) : '';
						employer_share_amount = (employer_share_amount !== 0) ? commaSeparateNumber(parseFloat(employer_share_amount).toFixed(2)) : '';
						total_premium_amount = (total_premium_amount !== 0) ? commaSeparateNumber(parseFloat(total_premium_amount).toFixed(2)) : '';
						arr += '<tr>';
						arr += '<td>'+(k+1)+'</td>';
						arr += '<td>'+fullname+'</td>';
						arr += '<td>'+bp_number+'</td>';
						arr += '<td class="text-right">'+basic_salary_amount+'</td>';
						arr += '<td></td>';
						arr += '<td class="text-right">'+personal_share_amount+'</td>';
						arr += '<td class="text-right">'+employer_share_amount+'</td>';
						arr += '<td></td>';
						arr += '<td></td>';
						arr += '<td class="text-right">'+total_premium_amount+'</td>';
						arr += '<td colspan="2"></td>';

						arr += '</tr>';

					});

					net_premium_amount = (net_premium_amount !== 0) ? commaSeparateNumber(parseFloat(net_premium_amount).toFixed(2)) : '';
					net_employer_share_amount = (net_employer_share_amount !== 0) ? commaSeparateNumber(parseFloat(net_employer_share_amount).toFixed(2)) : '';
					net_personal_share_amount = (net_personal_share_amount !== 0) ? commaSeparateNumber(parseFloat(net_personal_share_amount).toFixed(2)) : '';

					arr += '<tr>';
					arr += '<td></td>'
					arr += '<td colspan="4"><b>Total</b></td>'
					arr += '<td class="text-right"><b>'+net_personal_share_amount+'</b></td>'
					arr += '<td class="text-right"><b>'+net_employer_share_amount+'</b></td>'
					arr += '<td class="text-right"></td>';
					arr += '<td class="text-right"></td>';
					arr += '<td class="text-right" colspan="2"><b>'+net_premium_amount+'</b></td>'
					arr += '<td></td>'

					arr += '</tr>';


					$('#tbl_body').html(arr);

					$('#pay_period').text(_Month+' '+_Year)

					$('#btnModal').trigger('click');
				}else{
					swal({
						  title: "No Records Found",
						  type: "warning",
						  showCancelButton: false,
						  confirmButtonClass: "btn-danger",
						  confirmButtonText: "Yes",
						  closeOnConfirm: false

					});
				}



			}
		})
	}


});


})
</script>
@endsection