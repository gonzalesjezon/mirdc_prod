@extends('app-remittances')


@section('remittances-content')
<link rel="stylesheet" type="text/css" media="print" href="{{ asset('css/printportrait.css') }}">

<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Covered Date</b></span>
					</div>

				</div>
				<div class="row" style="margin-right: -5px;margin-left: -5px;">
					@include('payrolls.includes._months-year')
				</div>
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="panel-body">
		       <div class="row">
		       		<div class="col-md-12">
		       			<div class="reports" style="font-size: 12px;width: 960px;margin: auto;" id="reports" >
		       				<div class="report-header">
		       					<div class="row">
			       					<div class="col-md-12 text-center">
			       						Republic of the Philippines <br>
			       						<b>PHILIPPINE HEALTH INSURANCE CORPORATION</b> <br>
										8/F Philippine Heart Center Bldg., East Avenue, Diliman, Quezon City <br>
										Tel. No. 928-0349; 9281301 loc 3810 Fax No. 927-1272
			       					</div>
		       					</div>
		       					<div class="row" style="margin-top: 70px;padding-left: 20px;">
			       					<div class="col-md-9 text-left" >
			       						<b>REGISTERED EMPLOYER NAME:</b>  METALS INDUSTRY RESEARCH AND DEVELOPMENT CENTER  <br>
			       						<b>ADDRESS:</b>  MIRDC Compound General Santos Ave, Bicutan, Taguig, Metro Manila
			       					</div>
			       					<div class="col-md-3 text-left">
			       						EMPLOYER ID NO.: 	<br>
			       						EMPLOYER TIN   : 00-284-708  <br>
			       						FOR THE MONTH OF : <span id="for_month"></span>  <br>

			       					</div>
		       					</div>
		       				</div>
		       				<!-- <div style="border-bottom: 1px solid #e3e3e3; clear: both;"></div> -->
		       				<div class="reports-content ">
		       					<div class="report-title ">

		       					</div>

		       					<div class="report-name " style="padding-top: 20px;">
		       						<div class="row text-center" style="border-top: 2px solid #5d5858;border-bottom: 2px solid #5d5858;padding: 5px;">
			       						<div class="col-md-3">
			       							<span>NAME OF EMPLOYEE</span>
			       						</div>
			       						<div class="col-md-2">
			       							<span>POSITION TITLE</span>
			       						</div>
			       						<div class="col-md-2">
			       							<span>MONTHLY <br> COMPENSATION</span>
			       						</div>
			       						<div class="col-md-2">
			       							<span>PERSONAL <br> SHARE</span>
			       						</div>
			       						<div class="col-md-2">
			       							<span>EMPLOYER <br> SHARE</span>
			       						</div>
			       						<div class="col-md-1">
			       							<span>TOTAL</span>
			       						</div>
		       						</div>
		       					</div>

			       				<div class="reports-body" style="margin: auto;width: 95%;">

			       				</div>
		       				</div>
		       				<br>
		       				<div class="reports-footer">
		       					<div class="col-md-6">
		       						<div class="footer-left-message">
			       						<i>
			       							This is a computer generated document and does
											not require any signature if without alterations
			       						</i>

		       						</div>
		       					</div>
		       				</div>
		       			</div>
		       		</div>
		       </div>
	       </div>

	    </div>
	 </div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');

	var months ={
			1:'January',
			2:'February',
			3:'March',
			4:'April',
			5:'May',
			6:'June',
			7:'July',
			8:'August',
			9:'September',
			10:'October',
			11:'November',
			12:'December',
		}
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$(document).on('change','#select_searchvalue',function(){
		_searchvalue = "";
		_searchvalue = $(this).find(':selected').val();

	})

	$(document).on('change','#emp_status',function(){
		_emp_status = "";
		_emp_status = $(this).find(':selected').val();

	})
	$(document).on('change','#emp_type',function(){
		_emp_type = "";
		_emp_type = $(this).find(':selected').val();

	})
	$(document).on('change','#searchby',function(){
		_searchby = "";
		_searchby = $(this).find(':selected').val();

	})


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});

	$(document).on('click','#print',function(){
		$('#reports').printThis();
	});

	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){
				console.log(data);

					if(data.length !== 0){
						arr = [];

						sub_total_basic_amount = 0;
						sub_total_personal_amount = 0;
						sub_total_employer_amount = 0;
						sub_total_total_share_amount = 0;
						$.each(data,function(k,v){

							firstname = (v.employees.firstname !== null) ? v.employees.firstname : '';
							lastname = (v.employees.lastname !== null) ? v.employees.lastname : '';
							middlename = (v.employees.middlename !== null) ? v.employees.middlename : '';
							position = (v.positions !== null) ? v.positions.Name : '';

							fullname = lastname+' '+firstname+' '+middlename

							basic_amount = (v.total_basicpay_amount) ? v.total_basicpay_amount : 0;

							personal_amount = (v.phic_share !== null) ? v.phic_share : 0;
							employer_amount = (v.employeeinfo.er_philhealth_share !== null) ? v.employeeinfo.er_philhealth_share : 0;

							total_share_amount  = (parseFloat(personal_amount) + parseFloat(employer_amount));

							sub_total_basic_amount += parseFloat(basic_amount);
							sub_total_personal_amount += parseFloat(personal_amount);
							sub_total_employer_amount += parseFloat(employer_amount);
							sub_total_total_share_amount += parseFloat(total_share_amount);

							basic_amount = (basic_amount !== 0) ? commaSeparateNumber(parseFloat(basic_amount).toFixed(2)) : '';
							personal_amount = (personal_amount !== 0) ? commaSeparateNumber(parseFloat(personal_amount).toFixed(2)) : '' ;
							employer_amount = (employer_amount !== 0) ? commaSeparateNumber(parseFloat(employer_amount).toFixed(2)) : '' ;
							total_share_amount = (total_share_amount !== 0) ? commaSeparateNumber(parseFloat(total_share_amount).toFixed(2)) : '' ;

							arr += '<div class="row text-center">';
	       					arr += '	<div class="col-md-3 text-left">'+fullname+'</div>';
	       					arr += '	<div class="col-md-2">'+position+'</div>';
	       					arr += '	<div class="col-md-2" >'+basic_amount+'</div>';
	       					arr += '	<div class="col-md-2">'+personal_amount+'</div>';
	       					arr += '	<div class="col-md-2">'+employer_amount+'</div>';
	       					arr += '	<div class="col-md-1">'+total_share_amount+'</div>';
	   						arr += '</div>';

						})

						sub_total_basic_amount = (sub_total_basic_amount !== 0) ? commaSeparateNumber(parseFloat(sub_total_basic_amount).toFixed(2)) : '';
						sub_total_personal_amount = (sub_total_personal_amount !== 0) ? commaSeparateNumber(parseFloat(sub_total_personal_amount).toFixed(2)) : '';
						sub_total_employer_amount = (sub_total_employer_amount !== 0) ? commaSeparateNumber(parseFloat(sub_total_employer_amount).toFixed(2)) : '';
						sub_total_total_share_amount = (sub_total_total_share_amount !== 0) ? commaSeparateNumber(parseFloat(sub_total_total_share_amount).toFixed(2)) : '';

						arr += '<div class="row text-center" style="font-weight:bold;border-top: 2px solid #5d5858;border-bottom: 2px solid #5d5858;">';
       					arr += '	<div class="col-md-3 text-left">Record Count = '+data.length+'</div>';
       					arr += '	<div class="col-md-2">Grand Total</div>';
       					arr += '	<div class="col-md-2" >'+sub_total_basic_amount+'</div>';
       					arr += '	<div class="col-md-2">'+sub_total_personal_amount+'</div>';
       					arr += '	<div class="col-md-2">'+sub_total_employer_amount+'</div>';
       					arr += '	<div class="col-md-1">'+sub_total_total_share_amount+'</div>';
   						arr += '</div>';

   						arr += '<div class="row" style="padding-top:30px;">';
       					arr += '	<div class="col-md-6">PREPARED BY</div>';
       					arr += '	<div class="col-md-6">CERTIFIED CORRECT</div>';
   						arr += '</div>';

   						arr += '<div class="row" style="padding-top:30px;">';
       					arr += '	<div class="col-md-6"><b>AGNES F. PEDRAZA</b> <br> ADMIN OFFICER V</div>';
       					arr += '	<div class="col-md-6"><b>JOHNY G QUINCO</b> <br> ACCOUNTANT IV</div>';
   						arr += '</div>';



						$('.reports-body').html(arr);

						$('#for_month').text(months[_Month]+' '+_Year);

						$('#btnModal').trigger('click');

					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			})
		}


	});


})
</script>
@endsection