@extends('app-reports')


@section('reports-content')

<link rel="stylesheet" type="text/css" media="print" href="{{ asset('css/printlandscapetwo.css') }}">
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Covered Date</b></span>
					</div>
				</div>
				<div class="row" style="margin-right: -5px;margin-left: -5px;">
					@include('payrolls.includes._months-year')
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Pay Period</b></span>
					</div>

				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select id="pay_period" class="form-control font-style2 select2" name="pay_period">
								<option value=""></option>
								<option value="semimonthly">Semi Monthly</option>
								<option value="monthly">Monthly</option>
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control font-style2 hidden" id="semi_pay_period" name="semi_pay_period">
								<option value=""></option>
								<option value="firsthalf">First Half</option>
								<option value="secondhalf">Second Half</option>
							</select>
						</div>

					</div>
				</div>
			</td>
		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:100%;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports">
	       		<div class="row">
	       		<!-- 	<div class="col-md-5 text-right">
   						<img src="{{ url('images/mirdc_logo.gif') }}" style="height: 80px;">
   						<i></i>
   					</div> -->
   					<div class="col-md-12 text-center" style="font-weight: bold;margin-left: 20px;padding-top: 15px;">
   						Metals Industry Research and Development Center <br>
   						<span style="padding-left: 25px;">General Santos Ave., Bicutan, Taguig City</span>
   					</div>
	       		</div>
	       		<br>
	       		<div class="row">
	       			<div class="col-md-12 text-center" style="font-weight: bold">
	       				<span>
	       					SUMMARY OF DEDUCTIONS FOR MIRDC JOB ORDERS <br>
	       					<span class="covered_date"></span>
	       				</span>
	       			</div>
	       		</div>
   				<table class="table" style="border: 2px solid #5a5a5a">
   					<thead style="border: 2px solid #5a5a5a">
   						<tr class="text-center" style="font-weight: bold;">
   							<td rowspan="2">#</td>
   							<td rowspan="2">Name</td>
   							<td rowspan="2">Position</td>
   							<td rowspan="2">Division</td>
   							<td rowspan="2">Salary / Day</td>
   							<td colspan="2"><span class="covered_date"></span></td>
   							<td colspan="2">Absence</td>
   							<td colspan="2">Tardy/Undertime</td>
   							<td rowspan="2">Total Deductions</td>
   							<td rowspan="2">Gross Salary</td>
   							<td rowspan="2">Tax 3 %</td>
   							<td rowspan="2">Tax 5 %</td>
   							<td rowspan="2">Total</td>
   							<td rowspan="2">Overtime</td>
   							<td rowspan="2">Net Pay</td>
   							<td rowspan="2" colspan="5">Remarks</td>
   						</tr>
   						<tr class="text-center" style="font-weight: bold;">
   							<td colspan="2"><span id="no_of_day"></span></td>
   							<td>No of Days</td>
   							<td>Amount</td>
   							<td>Minutes</td>
   							<td>Amount</td>
   						</tr>
   					</thead>
   					<tfoot style="border: 2px solid #5a5a5a">
   						<tr>
   							<td class="text-left" colspan="3" style="border: none">
   								<br>
   								<b>LAILA R. PORLUCAS</b>	<br>
								Administrative Officer IV	 <br>
								Finance and Administrative Division
								 <br>
							</td>
							<td class="text-left" colspan="20" style="border: none">
								<br> <br>
								<b>JELLY N. ORTIZ, DPA	</b> <br>
								Chief, FAD-AGSS
								 <br>
							</td>
   						</tr>
   					</tfoot>
   					<tbody id="tbl_content"></tbody>
   				</table>
	       </div>
	 	</div>
	</div>
</div>
<!-- 0.328571 -->
@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');

	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})
	var _payPeriod;
	var _semiPayPeriod;
	$(document).on('change','#pay_period',function(){
		_payPeriod = $(this).find(':selected').val();
		switch(_payPeriod){
			case 'semimonthly':
				$('#semi_pay_period').removeClass('hidden');
			break;
			default:
				$('#semi_pay_period').addClass('hidden');
			break;
		}
	});

	$(document).on('change','#semi_pay_period',function(){
		_semiPayPeriod = $(this).find(':selected').val();
	})

$(document).on('click','#print',function(){
	$('#reports').printThis();
});

var months ={
	1:'January',
	2:'February',
	3:'March',
	4:'April',
	5:'May',
	6:'June',
	7:'July',
	8:'August',
	9:'September',
	10:'October',
	11:'November',
	12:'December',
}

$(document).on('click','#preview',function(){

	year = (_Year) ? _Year : '';
	month = (_Month) ? _Month : '';
	emp_type = (_emp_type) ? _emp_type : '';
	emp_status = (_emp_status) ? _emp_status : '';
	month = (_Month) ? _Month : '';
	category = (_searchvalue) ? _searchvalue : '';
	searchby = (_searchby) ? _searchby : '';

	if(!year || !month || !_payPeriod){
		swal({
			  title: "Select Year, Month, Pay Period and Employee First!",
			  type: "warning",
			  showCancelButton: false,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Yes",
			  closeOnConfirm: false

		});
	}else{
		$.ajax({
			url:base_url+module_prefix+module+'/show',
			data:{
				'id':_empid,
				'year':year,
				'month':month,
				'emp_type':emp_type,
				'emp_status':emp_status,
				'category':category,
				'searchby':searchby,
			},
			type:'GET',
			dataType:'JSON',
			success:function(data){
				console.log(data)
				if(data.transaction.length !== 0){
					arr = [];
					sub_total_daily_amount = 0;
					sub_total_actual_basicpay = 0;
					sub_total_actual_absence = 0;
					sub_total_actual_tardiness = 0;
					sub_total_actual_undertime = 0;
					sub_total_absence_amount = 0;
					sub_total_tardiness_amount = 0;
					sub_total_undertime_amount = 0;
					sub_total_total_ut_ta = 0;
					sub_total_total_ut_ta_amount = 0;
					sub_total_total_deduction = 0;
					sub_total_gross_salary = 0;
					sub_total_tax_amount_one = 0;
					sub_total_tax_amount_two = 0;
					sub_total_amount = 0;
					sub_total_net_pay = 0;
					$.each(data.transaction,function(k,v){
						firstname = (v.employees.firstname !== null) ? v.employees.firstname : '';
						lastname = (v.employees.lastname !== null) ? v.employees.lastname : '';
						middlename = (v.employees.middlename !== null) ? v.employees.middlename : '';

						position = (v.positions !== null) ? v.positions.Name : '';
						division = (v.divisions !== null) ? v.divisions.Name : '';

						fullname = lastname+' '+firstname+' '+middlename;

						daily_amount = (v.nonplantilla !== null) ? v.nonplantilla.daily_rate_amount : 0;
						actual_basicpay = (v.actual_basicpay_amount !== null) ? v.actual_basicpay_amount : 0;

						actual_absence = (v.actual_absences !== null) ? v.actual_absences : 0;
						actual_tardiness = (v.actual_tardiness !== null) ? v.actual_tardiness : 0;
						actual_undertime = (v.actual_undertime !== null) ? v.actual_undertime : 0;

						absence_amount = (v.total_absences_amount !== null) ? v.total_absences_amount : 0;
						tardiness_amount = (v.total_tardiness_amount !== null) ? v.total_tardiness_amount : 0;
						undertime_amount = (v.total_undertime_amount !== null) ? v.total_undertime_amount : 0;

						total_ut_ta = (parseFloat(actual_undertime) + parseFloat(actual_tardiness));
						total_ut_ta_amount = (parseFloat(tardiness_amount) + parseFloat(undertime_amount));
						total_deduction = (parseFloat(absence_amount) + parseFloat(total_ut_ta_amount));

						gross_salary = (parseFloat(actual_basicpay) - parseFloat(total_deduction));

						tax_amount_one = (v.tax_rate_amount_one !== null) ? v.tax_rate_amount_one : 0;
						tax_amount_two = (v.tax_rate_amount_two !== null) ? v.tax_rate_amount_two : 0;

						total_amount = (parseFloat(gross_salary) - (parseFloat(tax_amount_one) + parseFloat(tax_amount_two)));

						net_pay = total_amount;

						sub_total_daily_amount += parseFloat(daily_amount);
						sub_total_actual_basicpay += parseFloat(actual_basicpay);
						sub_total_actual_absence += parseFloat(actual_absence);
						sub_total_actual_tardiness += parseFloat(actual_tardiness);
						sub_total_actual_undertime += parseFloat(actual_undertime);
						sub_total_absence_amount += parseFloat(absence_amount);
						sub_total_tardiness_amount += parseFloat(tardiness_amount);
						sub_total_undertime_amount += parseFloat(undertime_amount);
						sub_total_total_ut_ta += parseFloat(total_ut_ta);
						sub_total_total_ut_ta_amount += parseFloat(total_ut_ta_amount);
						sub_total_total_deduction += parseFloat(total_deduction);
						sub_total_gross_salary += parseFloat(gross_salary);
						sub_total_tax_amount_one += parseFloat(tax_amount_one);
						sub_total_tax_amount_two += parseFloat(tax_amount_two);
						sub_total_amount += parseFloat(total_amount);
						sub_total_net_pay += parseFloat(net_pay);


						daily_amount = (daily_amount !== 0) ? commaSeparateNumber(parseFloat(daily_amount).toFixed(2)) : '';
						absence_amount = (absence_amount !== 0) ? commaSeparateNumber(parseFloat(absence_amount).toFixed(2)) : '';
						total_ut_ta = (total_ut_ta !== 0) ? total_ut_ta : '';
						total_ut_ta_amount = (total_ut_ta_amount !== 0) ? commaSeparateNumber(parseFloat(total_ut_ta_amount).toFixed(2)) : '';
						total_deduction = (total_deduction !== 0) ? commaSeparateNumber(parseFloat(total_deduction).toFixed(2)) : '';
						gross_salary = (gross_salary !== 0) ? commaSeparateNumber(parseFloat(gross_salary).toFixed(2)) : '';
						tax_amount_one = (tax_amount_one !== 0) ? commaSeparateNumber(parseFloat(tax_amount_one).toFixed(2)) : '';
						tax_amount_two = (tax_amount_two !== 0) ? commaSeparateNumber(parseFloat(tax_amount_two).toFixed(2)) : '';
						actual_basicpay = (actual_basicpay !== 0) ? commaSeparateNumber(parseFloat(actual_basicpay).toFixed(2)) : '';
						total_amount = (total_amount !== 0) ? commaSeparateNumber(parseFloat(total_amount).toFixed(2)) : '';
						net_pay = (net_pay !== 0) ? commaSeparateNumber(parseFloat(net_pay).toFixed(2)) : '';

						arr += '<tr class="text-right">';
						arr += '<td>'+(parseInt(k)+1)+'</td>';
						arr += '<td class="text-left">'+fullname+'</td>';
						arr += '<td class="text-center">'+position+'</td>';
						arr += '<td class="text-center">'+division+'</td>';
						arr += '<td>'+daily_amount+'</td>';
						arr += '<td colspan="2">'+actual_basicpay+'</td>';
						arr += '<td class="text-center">'+actual_absence+'</td>';
						arr += '<td class="text-center">'+absence_amount+'</td>';
						arr += '<td class="text-center">'+total_ut_ta+'</td>';
						arr += '<td>'+total_ut_ta_amount+'</td>';
						arr += '<td>'+total_deduction+'</td>';
						arr += '<td>'+gross_salary+'</td>';
						arr += '<td>'+tax_amount_one+'</td>';
						arr += '<td>'+tax_amount_two+'</td>';
						arr += '<td>'+total_amount+'</td>';
						arr += '<td></td>';
						arr += '<td>'+net_pay+'</td>';
						arr += '<td colspan="5"></td>';
						arr += '</tr>';
					});

					sub_total_daily_amount = (sub_total_daily_amount !== 0) ? commaSeparateNumber(parseFloat(sub_total_daily_amount).toFixed(2)) : '';
					sub_total_actual_basicpay = (sub_total_actual_basicpay !== 0) ? commaSeparateNumber(parseFloat(sub_total_actual_basicpay).toFixed(2)) : '';
					sub_total_actual_absence = (sub_total_actual_absence !== 0) ? commaSeparateNumber(parseFloat(sub_total_actual_absence).toFixed(2)) : '';
					sub_total_actual_tardiness = (sub_total_actual_tardiness !== 0) ? commaSeparateNumber(parseFloat(sub_total_actual_tardiness).toFixed(2)) : '';
					sub_total_actual_undertime = (sub_total_actual_undertime !== 0) ? commaSeparateNumber(parseFloat(sub_total_actual_undertime).toFixed(2)) : '';
					sub_total_absence_amount = (sub_total_absence_amount !== 0) ? commaSeparateNumber(parseFloat(sub_total_absence_amount).toFixed(2)) : '';
					sub_total_tardiness_amount = (sub_total_tardiness_amount !== 0) ? commaSeparateNumber(parseFloat(sub_total_tardiness_amount).toFixed(2)) : '';
					sub_total_undertime_amount = (sub_total_undertime_amount !== 0) ? commaSeparateNumber(parseFloat(sub_total_undertime_amount).toFixed(2)) : '';
					sub_total_total_ut_ta = (sub_total_total_ut_ta !== 0) ? commaSeparateNumber(parseFloat(sub_total_total_ut_ta).toFixed(2)) : '';
					sub_total_total_ut_ta_amount = (sub_total_total_ut_ta_amount !== 0) ? commaSeparateNumber(parseFloat(sub_total_total_ut_ta_amount).toFixed(2)) : '';
					sub_total_total_deduction = (sub_total_total_deduction !== 0) ? commaSeparateNumber(parseFloat(sub_total_total_deduction).toFixed(2)) : '';
					sub_total_gross_salary = (sub_total_gross_salary !== 0) ? commaSeparateNumber(parseFloat(sub_total_gross_salary).toFixed(2)) : '';
					sub_total_tax_amount_one = (sub_total_tax_amount_one !== 0) ? commaSeparateNumber(parseFloat(sub_total_tax_amount_one).toFixed(2)) : '';
					sub_total_tax_amount_two = (sub_total_tax_amount_two !== 0) ? commaSeparateNumber(parseFloat(sub_total_tax_amount_two).toFixed(2)) : '';
					sub_total_amount = (sub_total_amount !== 0) ? commaSeparateNumber(parseFloat(sub_total_amount).toFixed(2)) : '';
					sub_total_net_pay = (sub_total_net_pay !== 0) ? commaSeparateNumber(parseFloat(sub_total_net_pay).toFixed(2)) : '';

					arr += '<tr class="text-right" style="font-weight:bold;border-top:2px solid #5a5a5a">';
					arr += '<td></td>';
					arr += '<td class="text-left">Grand Total</td>';
					arr += '<td class="text-center"></td>';
					arr += '<td class="text-center"></td>';
					arr += '<td>'+sub_total_daily_amount+'</td>';
					arr += '<td colspan="2">'+sub_total_actual_basicpay+'</td>';
					arr += '<td class="text-center">'+sub_total_actual_absence+'</td>';
					arr += '<td class="text-center">'+sub_total_absence_amount+'</td>';
					arr += '<td class="text-center">'+sub_total_total_ut_ta+'</td>';
					arr += '<td>'+sub_total_total_ut_ta_amount+'</td>';
					arr += '<td>'+sub_total_total_deduction+'</td>';
					arr += '<td>'+sub_total_gross_salary+'</td>';
					arr += '<td>'+sub_total_tax_amount_one+'</td>';
					arr += '<td>'+sub_total_tax_amount_two+'</td>';
					arr += '<td>'+sub_total_amount+'</td>';
					arr += '<td></td>';
					arr += '<td>'+sub_total_net_pay+'</td>';
					arr += '<td colspan="5"></td>';
					arr += '</tr>';

					days = daysInMonth(_Month,_Year)

					if(_payPeriod == 'monthly'){
						_coveredPeriod =  months[_Month]+' 1-'+days+', '+_Year;
					}else{
						switch(_semiPayPeriod){
							case 'firsthalf':
								_coveredPeriod =  months[_Month]+' 1-15, '+_Year;
							break;
							default:
								_coveredPeriod = months[_Month]+' 16-'+days+', '+_Year;
							break;
						}
					}

					$('.covered_date').text(_coveredPeriod);

					$('#tbl_content').html(arr);

					$('#btnModal').trigger('click');

				}else{
					swal({
						title: "No Records Found",
						type: "warning",
						showCancelButton: false,
						confirmButtonClass: "btn-danger",
						confirmButtonText: "Yes",
						closeOnConfirm: false
					});
				}
			}
		})
	}
function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}


});
})
</script>
@endsection