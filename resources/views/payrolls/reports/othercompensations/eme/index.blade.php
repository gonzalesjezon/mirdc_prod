@extends('app-othercompensations')


@section('othercompensations-content')

<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Covered Date</b></span>
					</div>

				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select class="form-control select2" name="month" id="select_month">
								<option value=""></option>
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control select2" name="year" id="select_year">
								<option value=""></option>
							</select>
						</div>

					</div>
				</div>

			</td>

		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="width: 960px;">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid ">
	       		<div class="row">
	       			<div class="col-md-6">
	       				<img src="{{ url('images/new-logo.jpg') }}" style="height: 80px;">
	       				<h5><b>{{ $title }}</b></h5>
	       				<h6><b>Period Covered:</b> <span id="month_year"></span></h6>
	       			</div>
					<table class="table">
						<thead class="text-center">
							<tr >
								<td ><b >#</b></td>
								<td ><b >EMPLOYEE NAME</b></td>
								<td ><b >SG</b></td>
								<td ><b >POSITION/DESIGNATION</b></td>
								<td ><b>OFFICE</b></td>
								<td ><b>EXTRAORDINARY EXPENSES</b></td>
								<td ><b>MISCELLANEOUS EXPENSE</b></td>
								<td ><b>TOTAL</b></td>
								<td ><b >SIGNATURE</b></td>
								<td ><b>REMARKS</b></td>
							</tr>
						</thead>
						<tfoot >
						 	<tr style="border-top:1px solid #c0c0c0;border-bottom:1px solid #c0c0c0;">
						 		<td style="border:none;" colspan="4"></td>
						 		<td style="border:none;" class="text-right"><b>Grand Total</b></td>
						 		<td style="border:none;font-weight: bold" class="text-right"><span id="net_ee_amount"></span></td>
						 		<td style="border:none;font-weight: bold" class="text-right"><span id="net_me_amount"></span></td>
						 		<td style="border:none;font-weight: bold" class="text-right"><span id="net_total"></span></td>
						 		<td style="border:none;" colspan="3"></td>
						 		<!-- <td style="border:none;font-weight: bold"><span id="net_er_pagibig_share"></span></td> -->
						 <!-- 		<td style="border:none;font-weight: bold;border-right:1px solid #c0c0c0;"><span id="net_pagibig"></span></td> -->
						 	</tr>
						 	<tr>
						 		<td style="border:none;" colspan="6" >
						 			<br>
						 			<b><i>CERTIFICATION: This is to certify the above personal are entitled to receive monthly communications allotment pursuant to Commission Resolution No. 80 series of 2016, as amended.
						 			</i></b>
						 		</td>
						 		<td style="border:none;" colspan="6" ></td>
						 	</tr>
						 	<tr class="text-left" >
						 		<td style="border:none;" colspan="4">
							 		Certified Correct
							 		<br><br>
							 		<b>WYNONA R. ALMIN</b>
							 		<br>
							 		<span>HRMO II, HRDD</span>
							 		<br><br>
							 		<span><b>Certified:</b> Supporting documents complete <br>and </span>
							 		<span>proper, and cash available in the amount of</span><br>
							 		<span>Php _________________________.</span><br><br><br>
							 		<span><b>CAROLYN V. AQUINO</b></span><br>
							 		<span>Accountant III, FPMO</span>
						 		</td>
						 		<td style="border:none;text-align: left;" colspan="2">
						 			<br><br>
							 		<b>ANTONIO LYNNEL L. BAUTISTA</b>
							 		<br>
							 		<span>Chief Admin Officer, HRDD</span>
						 		</td>
						 		<td  style="border:none;border-right: 1px solid #c0c0c0;" colspan="4">
							 		Approved for Payment
							 		<br>
							 		<br>
							 		<b>KENNETH V. TANATE</b>
							 		<br>
							 		<span>Executive Director</span>
							 		<br><br>
							 		<span><b>Certified:</b> Each employee whose name appears on the payroll has </span>
							 		<span>been paid the amount as indicated opposite his/her name.</span><br><br><br><br>
							 		<span><b>MAC VINCENT E. JAVIER</b></span><br>
							 		<span>Cashier II, ALO</span>
						 		</td>
						 	</tr>
					 	</tfoot>
					 <tbody id="tbl_body">
					 </tbody>
					</table>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
        year += '<option value='+y+'>'+y+'</option>';
	}
    $('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
    for ( m =  0; m <= month.length - 1; m++) {
    	mArr += '<option '+month[m]+'>'+month[m]+'</option>';
    }
    $('#select_month').html(mArr);


// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$('#select_year').select2({
		allowClear:true,
	    placeholder: "Year",
	});

	$('#select_month').select2({
		allowClear:true,
	    placeholder: "Month",
	});

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$(document).on('change','#select_searchvalue',function(){
		_searchvalue = "";
		_searchvalue = $(this).find(':selected').val();

	})

	$(document).on('change','#emp_status',function(){
		_emp_status = "";
		_emp_status = $(this).find(':selected').val();

	})
	$(document).on('change','#emp_type',function(){
		_emp_type = "";
		_emp_type = $(this).find(':selected').val();

	})
	$(document).on('change','#searchby',function(){
		_searchby = "";
		_searchby = $(this).find(':selected').val();

	})


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});

	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/getEmployeeinfo',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){
				console.log(data);

					if(data.length !== 0){
						arr = [];
						ctr = 0;
						net_ee_amount = 0;
						net_me_amount = 0;
						net_total 	  = 0;

						$.each(data,function(key,val){
							sub_total_ee_amount = 0;
							sub_total_me_amount = 0;
							sub_total 			= 0;

							arr += '<tr style="border:1px solid #c0c0c0;">';
							arr += '<td style="border-right:none;"></td>';
							arr += '<td  colspan="9" style="border-left:none;"><b>'+key+'</b></td>';
							arr += '</tr>';

							$.each(val,function(k,v){
								ee_amount 		= 0;
								me_amount 		= 0;
								total 			= 0;
								arr += '<tr style="border:1px solid #c0c0c0;" class="text-center">';
									arr += '<td >'+(ctr+1)+'</td>';
									arr += '<td >&nbsp;'+v.employees.lastname+' '+v.employees.firstname+' '+v.employees.middlename+'</td>';
									arr += '<td >&nbsp;'+v.salaryinfo.jobgrade.job_grade+'</td>';
									if(v.positionitems.position_id !== null){
										arr += '<td >&nbsp;'+v.positionitems.positions.name+'</td>';
									}else{
										arr += '<td ></td>';
									}

									arr += '<td >&nbsp;'+v.offices.name+'</td>';

									$.each(v.employeeinfo.benefitinfo,function(k1,v1){
										switch(v1.benefits.code){
											case 'EE':
												ee_amount = (v1.benefit_amount) ? v1.benefit_amount : 0;
											break;
											case 'ME':
												me_amount = (v1.benefit_amount) ? v1.benefit_amount : 0;
											break;
										}
									})

									sub_total_ee_amount += parseFloat(ee_amount);
									sub_total_me_amount += parseFloat(me_amount);

									total = (parseFloat(ee_amount) + parseFloat(me_amount));
									sub_total += parseFloat(total);

									ee_amount = (ee_amount == 0) ? '-' : parseFloat(ee_amount).toFixed(2);
									me_amount = (me_amount == 0) ? '-' : parseFloat(me_amount).toFixed(2);

									arr += '<td class="text-right">'+commaSeparateNumber(ee_amount) +'</td>';
									arr += '<td class="text-right">'+commaSeparateNumber(me_amount) +'</td>';

									total = (total == 0) ? '-' : parseFloat(total).toFixed(2);
									arr += '<td class="text-right">'+commaSeparateNumber(total) +'</td>';
									arr += '<td ></td>';
									arr += '<td ></td>';
								arr += '</tr>';
								ctr++;
							});
							ctr = 0;
							arr += '<tr style="border:1px solid #c0c0c0;font-weight:bold;" class="text-center">';
							arr += '<td style="border:none" colspan="4"></td>';
							arr += '<td style="border:none" class="text-right"><b>Sub Total</b></td>';

							net_ee_amount += parseFloat(sub_total_ee_amount);
							net_me_amount += parseFloat(sub_total_me_amount);
							net_total 	  += parseFloat(sub_total);

							sub_total_ee_amount = (sub_total_ee_amount) ? parseFloat(sub_total_ee_amount).toFixed(2) : '-';
							sub_total_me_amount = (sub_total_me_amount) ? parseFloat(sub_total_me_amount).toFixed(2) : '-';
							sub_total = (sub_total) ? parseFloat(sub_total).toFixed(2) : '-';
							arr += '<td style="border:none" class="text-right">'+commaSeparateNumber(sub_total_ee_amount) +'</td>';
							arr += '<td style="border:none" class="text-right">'+commaSeparateNumber(sub_total_me_amount) +'</td>';
							arr += '<td style="border:none" class="text-right">'+commaSeparateNumber(sub_total) +'</td>';
							arr += '<td style="border:none" class="text-right" colspan="1"></td>';
							arr += '</tr>';
						});

						net_ee_amount = (net_ee_amount == 0) ? '-' : parseFloat(net_ee_amount).toFixed(2);
						net_me_amount = (net_me_amount == 0) ? '-' : parseFloat(net_me_amount).toFixed(2);
						net_total 	  = (net_total == 0) ? '-' : parseFloat(net_total).toFixed(2);

						$('#net_ee_amount').text(commaSeparateNumber(net_ee_amount) );
						$('#net_me_amount').text(commaSeparateNumber(net_me_amount) );
						$('#net_total').text(commaSeparateNumber(net_total) );

						$('#tbl_body').html(arr);

						$('#month_year').text(_Month+' '+_Year);

						$('#btnModal').trigger('click');

					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			})
		}
	});


})
</script>
@endsection