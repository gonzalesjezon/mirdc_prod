@extends('app-reports')


@section('reports-content')

<link rel="stylesheet" type="text/css" media="print" href="{{ asset('css/printlandscapetwo.css') }}">
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Covered Date</b></span>
					</div>
				</div>
				<div class="row" style="margin-right: -5px;margin-left: -5px;">
					@include('payrolls.includes._months-year')
				</div>
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog" >
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:95%;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports">
	       		<div class="row">
	       		<!-- 	<div class="col-md-5 text-right">
   						<img src="{{ url('images/mirdc_logo.gif') }}" style="height: 80px;">
   						<i></i>
   					</div> -->
   					<div class="col-md-12 text-left" style="margin-left:15px;padding-top: 15px;">
   						<h5><span style="font-weight: bold;">Remitting Agency</span>	METALS IND RESEARCH AND DEVT CTR</h5>
   						<h5><span style="font-weight: bold;">Office Code</span> 	1000015374</h5>
   						<h5><span style="font-weight: bold;">Due Month</span>	<span class="covered_date"></span></h5>
   					</div>
	       		</div>
   				<table class="table" style="border: 2px solid #5a5a5a;width: 3000px;">
   					<thead style="border: 2px solid #5a5a5a">
   						<tr class="text-center" style="font-weight: bold;">
   							<td>BPNO</td>
   							<td>LastName</td>
   							<td>FirstName</td>
   							<td>MI</td>
   							<td>PREFIX</td>
   							<td>APPELLATION</td>
   							<td>BirthDate</td>
   							<td>CRN</td>
   							<td>Basic Monthly Salary</td>
   							<td>Effectivity Date</td>
   							<td>PS</td>
   							<td>GS</td>
   							<td>EC</td>
   							<td>CONSOLOAN</td>
   							<td>ECARDPLUS</td>
   							<td>SALARY_LOAN</td>
   							<td>CASH_ADV</td>
   							<td>EMRGYLN</td>
   							<td>EDUC_ASST</td>
   							<td>ELA</td>
   							<td>SOS</td>
   							<td>PLREG</td>
   							<td>PLOPT</td>
   							<td>REL</td>
   							<td>LCH_DCS</td>
   							<td>STOCK_PURCHASE</td>
   							<td>OPT_LIFE</td>
   							<td>CEAP</td>
   							<td>EDU_CHILD</td>
   							<td>GENESIS</td>
   							<td>GENPLUS</td>
   							<td>GENFLEXI</td>
   							<td>GENSPCL</td>
   							<td>HELP</td>
   							<td>GFAL</td>
   						</tr>
   					</thead>
   					<tbody id="tbl_content"></tbody>
   				</table>
	       </div>
	 	</div>
	</div>
</div>
<!-- 0.328571 -->
@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})

	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');

	var months ={
			1:'January',
			2:'February',
			3:'March',
			4:'April',
			5:'May',
			6:'June',
			7:'July',
			8:'August',
			9:'September',
			10:'October',
			11:'November',
			12:'December',
		}
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})
	var _payPeriod;
	var _semiPayPeriod;
	$(document).on('change','#pay_period',function(){
		_payPeriod = $(this).find(':selected').val();
		switch(_payPeriod){
			case 'semimonthly':
				$('#semi_pay_period').removeClass('hidden');
			break;
			default:
				$('#semi_pay_period').addClass('hidden');
			break;
		}
	});

	$(document).on('change','#semi_pay_period',function(){
		_semiPayPeriod = $(this).find(':selected').val();
	})

$(document).on('click','#print',function(){
	$('#reports').printThis();
});

$(document).on('click','#preview',function(){
	year = (_Year) ? _Year : '';
	month = (_Month) ? _Month : '';
	emp_type = (_emp_type) ? _emp_type : '';
	emp_status = (_emp_status) ? _emp_status : '';
	month = (_Month) ? _Month : '';
	category = (_searchvalue) ? _searchvalue : '';
	searchby = (_searchby) ? _searchby : '';

	if(!year || !month){
		swal({
			  title: "Select Year and Month First!",
			  type: "warning",
			  showCancelButton: false,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Yes",
			  closeOnConfirm: false

		});
	}else{
		$.ajax({
			url:base_url+module_prefix+module+'/show',
			data:{
				'id':_empid,
				'year':year,
				'month':month,
				'emp_type':emp_type,
				'emp_status':emp_status,
				'category':category,
				'searchby':searchby,
			},
			type:'GET',
			dataType:'JSON',
			success:function(data){
				console.log(data)
				if(data !== null){
					arr = [];
					ctr = 0;

					netconsoLoanAmount  = 0;
					netpolicyLoanAmount = 0;
					netemergLoan 		= 0;
					neteducLoan 		= 0;
					nethdmfLoan 		= 0;
					netmplLoan 			= 0;
					netlbMobile 		= 0;
					netmembaiLoan 		= 0;
					netprovidentLoan 	= 0;
					netTotalLoans 		= 0;
					$.each(data,function(k,v){

						consoLoanAmount  = 0;
						policyLoanAmount = 0;
						emergLoan 		 = 0;
						educLoan 		 = 0;
						hdmfLoan 		 = 0;
						mplLoan 		 = 0;
						lbMobile 		 = 0;
						membaiLoan 		 = 0;
						providentLoan 	 = 0;
						ec_amount 		 = 100;

						bp_no = (v.bp_no !== null) ? v.bp_no : '';
						crn = (v.crn !== null) ? v.crn : '';
						firstname = (v.employees.firstname !== null) ? v.employees.firstname : '';
						lastname = (v.employees.lastname !== null) ? v.employees.lastname : '';
						middlename = (v.employees.middlename !== null) ? v.employees.middlename : '';

						basic_one = (v.salaryinfo) ? v.salaryinfo.basic_amount_one : 0;
						basic_two = (v.salaryinfo) ? v.salaryinfo.basic_amount_two : 0;
						effectivity_date = (v.salaryinfo) ? v.salaryinfo.salary_effectivity_date : 0;

						psShareAmount = (v.gsis_contribution) ? v.gsis_contribution : 0;
						erShareAmount = (v.er_gsis_share) ? v.er_gsis_share : 0;

						basic_two = (v.salaryinfo) ? v.salaryinfo.basic_amount_two : 0;

						basicAmount = parseFloat(basic_one) + parseFloat(basic_two);

						// LOANS


						if(v.loanstransaction.length !== 0){
							$.each(v.loanstransaction,function(key,val){
								if(val.loans){
									switch(val.loans.code){
										case 'CS':
											consoLoanAmount = (val.amount) ? val.amount : 0;
										break;
										case 'PL':
											policyLoanAmount = (val.amount) ? val.amount : 0;
										break;
										case 'EA':
											educLoan = (val.amount) ? val.amount : 0;
										break;
										case 'EL':
											emergLoan = (val.amount) ? val.amount : 0;
										break;
									}
								}

							});
						}


						basic_amount = (basicAmount !== 0) ? commaSeparateNumber(parseFloat(basicAmount).toFixed(2)) : 0;
						ps_share_amount = (psShareAmount !== 0) ? commaSeparateNumber(parseFloat(psShareAmount).toFixed(2)) : 0;
						er_share_amount = (erShareAmount !== 0) ? commaSeparateNumber(parseFloat(erShareAmount).toFixed(2)) : 0;

						conso_loan_amount = (consoLoanAmount !== 0) ? commaSeparateNumber(parseFloat(consoLoanAmount).toFixed(2)) : 0;
						policy_loan_amount = (policyLoanAmount !== 0) ? commaSeparateNumber(parseFloat(policyLoanAmount).toFixed(2)) : 0;
						educ_loan_amount = (educLoan !== 0) ? commaSeparateNumber(parseFloat(educLoan).toFixed(2)) : 0;
						emerg_loan_amount = (emergLoan !== 0) ? commaSeparateNumber(parseFloat(emergLoan).toFixed(2)) : 0;

						arr += '<tr class="text-right">';
						arr += '<td>'+bp_no+'</td>';
						arr += '<td class="text-left">'+lastname+'</td>';
						arr += '<td class="text-left">'+firstname+'</td>';
						arr += '<td class="text-left">'+middlename+'</td>';
						arr += '<td class="text-center"></td>'; // PREFIX
						arr += '<td class="text-center"></td>'; // APPELATION
						arr += '<td class="text-center"></td>'; // BIRTHDATE
						arr += '<td class="text-center">'+crn+'</td>'; // CRN
						arr += '<td class="text-right">'+basic_amount+'</td>';
						arr += '<td class="text-right">'+effectivity_date+'</td>';
						arr += '<td class="text-right">'+ps_share_amount+'</td>';
						arr += '<td class="text-right">'+er_share_amount+'</td>';
						arr += '<td class="text-right">'+ec_amount+'</td>';
						arr += '<td class="text-right">'+conso_loan_amount+'</td>'; // CONSO LOAN
						arr += '<td class="text-right"></td>'; // ECARD PLUS
						arr += '<td class="text-right"></td>'; // SALARY LOAN
						arr += '<td class="text-right"></td>'; // CASH ADVANCE
						arr += '<td class="text-right">'+emerg_loan_amount+'</td>'; // EMERGENCY LOAN
						arr += '<td class="text-right">'+educ_loan_amount+'</td>'; // EDUC ASSISTS LOAN
						arr += '<td class="text-right"></td>'; // ELA
						arr += '<td class="text-right"></td>'; // SOS
						arr += '<td class="text-right"></td>'; // PL REG
						arr += '<td class="text-right"></td>'; // PLOPT
						arr += '<td class="text-right"></td>'; // REL
						arr += '<td class="text-right"></td>'; // LCH_DCS
						arr += '<td class="text-right"></td>'; // STOCK PURCHASE
						arr += '<td class="text-right"></td>'; // OPT LIFE
						arr += '<td class="text-right"></td>'; // CEAP
						arr += '<td class="text-right"></td>'; // EDUC CHILD
						arr += '<td class="text-right"></td>'; // GENESIS
						arr += '<td class="text-right"></td>'; // GENSPCL
						arr += '<td class="text-right"></td>'; // HELP
						arr += '<td class="text-right"></td>'; // GFAL
						arr += '<td class="text-right"></td>'; // GFAL
						arr += '<td class="text-right"></td>'; // GFAL

						arr += '</tr>';
						ctr++;

					});
					ctr = 0;

					days = daysInMonth(_Month,_Year)

					if(_payPeriod == 'monthly'){
						_coveredPeriod = months[_Month]+' 1-'+days+', '+_Year;
					}else{
						switch(_semiPayPeriod){
							case 'firsthalf':
								_coveredPeriod = months[_Month]+' 1-15, '+_Year;
							break;
							default:
								_coveredPeriod = months[_Month]+' 16-'+days+', '+_Year;
							break;
						}
					}

					$('.covered_date').text(months[_Month]+' '+_Year);

					$('#tbl_content').html(arr);

					$('#btnModal').trigger('click');

				}else{
					swal({
						title: "No Records Found",
						type: "warning",
						showCancelButton: false,
						confirmButtonClass: "btn-danger",
						confirmButtonText: "Yes",
						closeOnConfirm: false
					});
				}
			}
		})
	}


});

function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}
})
</script>
@endsection