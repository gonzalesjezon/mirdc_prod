<div class="reports-bot">
	<div class="col-md-6">
		<button class="btn btn-success btn-xs btn-editbg submit" id="btn_posted">
			Post
		</button>
	</div>
	<div class="col-md-6 text-right">
		<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
		<a class="btn btn-danger btn-xs" id="preview">Preview</a>
	</div>
</div>
