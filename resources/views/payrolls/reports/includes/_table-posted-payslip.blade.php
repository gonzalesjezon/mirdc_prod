<style type="text/css">
.table thead>tr>td{
	font-size: 12px !important;
}

.table tbody>tr>td{
	font-size: 12px !important;
}

.tr td{
	border-bottom: none;
}
.noborder{
    border:none !important;
}
.noborder-top{
    border-top:none !important;
}
</style>
<div class="row pl-4 pr-4">
	<div class="col-md-12">
		<table class="table noborder border datatable" id="tblPosted">
			<thead>
				<tr>
					<th>Date Posted</th>
					@if($module == 'payslips')
					<th>Division</th>
					@endif

					<th>Transaction Date</th>

					@if($module == 'payslips')
					<th>Employee Name</th>
					@endif

					@if($module == 'cosgeneralpayrollreports')
					<th>Pay Period</th>
					@endif

					@if($module == 'specialpayrollreports')
					<th>Report Type</th>
					@endif

					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($posted as $key => $value)
				<tr class="text-center">
					<td nowrap>{{$value->created_at}}</td>

					@if($module == 'payslips')
					<td nowrap>{!! $value->division->Code !!}</td>
					@endif
					<td>{{date("F", strtotime($value->year."-".$value->month."-01"))}} {{$value->year}}</td>

					@if($module == 'payslips')
					<td nowrap>{!! $value->employee->firstname !!} {!! $value->employee->middlename !!} {!! $value->employee->lastname !!}</td>
					@endif

					@if($module == 'cosgeneralpayrollreports')
					<td>{!! $value->pay_period !!}</td>
					@endif

					@if($module == 'specialpayrollreports')
					<td>{!! $value->report_type!!} </td>
					@endif

					<td>
						<a class="btn btn-success btn-xs view" data-employee_id="{{$value->employee_id}}" data-year="{{$value->year}}" data-month="{{$value->month}}" data-pay_period="{{ $value->pay_period }}" data-report_type="{{$value->report_type}}" >View</a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		var table = $('#tblPosted').DataTable({
			'paging':true,
			"oLanguage": {
				"oPaginate": {
				"sFirst": "<<", // This is the link to the first page
				"sPrevious": "<", // This is the link to the previous page
				"sNext": ">", // This is the link to the next page
				"sLast": ">>" // This is the link to the last page
				}
			}
		});
	})
</script>