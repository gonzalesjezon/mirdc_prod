@extends('app-reports')


@section('reports-content')

<link rel="stylesheet" type="text/css" media="print" href="{{ asset('css/printportrait.css') }}">
<style type="text/css">
	p{
		color: #101010;
	}
	.borderstyle2{
		border-left: none !important;
		border-right: none !important;
		border-bottom: none !important;
		border-top: 2px solid #5a5a5a !important;
	}
</style>
<div class="loan-reports">
	<label>{{ $title }}</label>
	<div class="panel panel-default">
		<div class="panel-body">
			@include('payrolls.reports.includes._table-posted-payslip')
			<form action="{{ url($module_prefix.'/'.$module) }}" method="post" id="form">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="row pl-1">
					<div class="col-md-12">
						<span>Covered Date</span>
					</div>
				</div>
				<div class="row p-2">
					@include('payrolls.includes._months-year')
				</div>
				<div class="row p-2">
					<div class="col-md-6">
						<span>Pay Period</span>
						<select class="form-control font-style2" id="semi_pay_period" name="pay_period">
							<option value=""></option>
							<option value="firsthalf">First Half</option>
							<option value="secondhalf">Second Half</option>
						</select>
					</div>
				</div>
				<div class="row p-2">
					<div class="col-md-6">
						<span>Responsibility</span>
						<select class="form-control font-style2" id="rcenter" name="responsibility_id">
							<option value=""></option>
							@foreach($rcenter as $value)
							<option value="{{ $value->id }}">{{ $value->name }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<br>
				@include('payrolls.reports.includes._post-button')
			</form>
		</div>
	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:100%;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports">
   				<table class="table" style="margin-top: 20px;border:none;">
   					<thead>
   						<tr>
   							<td colspan="10" class="text-center" style="border: none;">
			       				<h5 style="font-weight: bold;">METALS INDUSTRY RESEARCH AND DEVELOPMENT</h5>
			       				<h5 style="font-weight: bold;">GENERAL PAYROLL</h5>
   								<span>
			       					We hereby acknowledge to have received from MIRDC the sum herein specified our respective names representing our salaries for the perio <span class="covered_year"></span>
			       				</span>
   							</td>
   						</tr>
   						<tr>
   							<td class="text-center" colspan="10" style="border: none;"><span id="header_title"></span></td>
   						</tr>
   						<tr class="text-center borderless" style="border-top: 2px solid #5a5a5a;font-weight: bold;margin-top: 5em;">
   							<td rowspan="2" style="border-bottom: 2px solid #5a5a5a;vertical-align: middle;">ID #</td>
   							<td rowspan="2" style="border-bottom: 2px solid #5a5a5a;vertical-align: middle;">Name</td>
   							<td rowspan="2" style="border-bottom: 2px solid #5a5a5a;vertical-align: middle;">Basic</td>
   							<td rowspan="2" style="border-bottom: 2px solid #5a5a5a;vertical-align: middle;">Gross Pay</td>
   							<td rowspan="2" style="border-bottom: 2px solid #5a5a5a;vertical-align: middle;">OP/UP</td>
   							<td rowspan="2" style="border-bottom: 2px solid #5a5a5a;vertical-align: middle;">LWOP</td>
   							<td rowspan="2" style="border-bottom: 2px solid #5a5a5a;vertical-align: middle;">OTHER DEDUCTION</td>
   							<td colspan="2">W TAX</td>
   							<td rowspan="2" style="border-bottom: 2px solid #5a5a5a;">NET PAY</td>
   						</tr>
   						<tr class="text-center" style="border-bottom: 2px solid #5a5a5a;font-weight: bold;">
   							<td style="border: none;">10%|3%</td>
   							<td style="border: none;">3%|10%</td>
   						</tr>
   					</thead>
   					<tbody id="tbl_content"></tbody>
   				</table>
	       </div>
	 	</div>
	</div>
</div>
<!-- 0.328571 -->
@endsection

@section('js-logic2')
@include('payrolls.reports.includes._post-button-script')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');

	var months ={
		1:'January',
		2:'February',
		3:'March',
		4:'April',
		5:'May',
		6:'June',
		7:'July',
		8:'August',
		9:'September',
		10:'October',
		11:'November',
		12:'December',
	}

	var payrollType;
	var payrollText;
	$('#select_payroll').on('change',function(){
		payrollText = $(this).find(':selected').text();
		payrollType = $(this).find(':selected').val();

		$('#payroll_type').text(payrollText);
	});

	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})
	var _semiPayPeriod;
	$(document).on('change','#semi_pay_period',function(){
		_semiPayPeriod = $(this).find(':selected').val();
	})

$(document).on('click','#print',function(){
	$('#reports').printThis();
});

// View posted report
$(document).on('click','.view',function(){
	_Year 			= $(this).data('year');
	_Month 			= $(this).data('month');
	_semiPayPeriod 	= $(this).data('pay_period');
	$('#preview').trigger('click');
});

$(document).on('click','#preview',function(){

	year = (_Year) ? _Year : '';
	month = (_Month) ? _Month : '';
	emp_type = (_emp_type) ? _emp_type : '';
	emp_status = (_emp_status) ? _emp_status : '';
	month = (_Month) ? _Month : '';
	category = (_searchvalue) ? _searchvalue : '';
	searchby = (_searchby) ? _searchby : '';

	if(!year || !month){
		swal({
			  title: "Select Year and Month!",
			  type: "warning",
			  showCancelButton: false,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Yes",
			  closeOnConfirm: false

		});
	}else{
		$.ajax({
			url:base_url+module_prefix+module+'/show',
			data:{
				'year':year,
				'month':month,
				'sub_period':_semiPayPeriod,
				'rcenter':$('#rcenter').val()
			},
			type:'GET',
			dataType:'JSON',
			success:function(data){
				if(data.length !== 0){
					arr = [];
					netBasicAmount 			= 0;
					netgrossAmount 			= 0;
					netLWOPAmount 			= 0;
					netTaxAmountOne 		= 0;
					netTaxAmountTwo 		= 0;
					netNetAmount 			= 0;
					netOTUTAmount 			= 0;
					netOtherDeductionAmount = 0;
					$.each(data,function(k,v){
						$('#header_title').text(k);
						$.each(v,function(k1,v1){

						subBasicAmount 			= 0;
						subgrossAmount 			= 0;
						subLWOPAmount 			= 0;
						subTaxAmountOne 		= 0;
						subTaxAmountTwo 		= 0;
						subNetAmount 			= 0;
						subOTUTAmount 			= 0;
						subOtherDeductionAmount = 0;

						arr += '<br><tr class="text-left" style="border-bottom: 2px solid #5a5a5a;">';
						arr += '<td style="font-weight:bold;border-left:none;border-right:none;" colspan="4">'+k1+'</td>';
						arr += '</tr>';

						$.each(v1,function(key,val){

							employee_number = (val.employees.employee_number) ? val.employees.employee_number : '';
							firstname = val.employees.firstname;
							lastname = val.employees.lastname;
							middlename = (val.employees.middlename) ? val.employees.middlename : '';
							// basicAmount = (val.employees.middlename) ? val.employees.middlename : '';

							fullname = lastname+' '+firstname+' '+middlename;

							basicAmount 	= val.monthly_rate_amount;
							grossAmount 	= val.gross_pay;
							lwopAmount 		= (val.total_absences_amount) ? val.total_absences_amount : 0;
							taxAmountOne 	= val.tax_rate_amount_one;
							taxAmountTwo 	= val.tax_rate_amount_two;
							overPayment 	= (val.overpayment_amount) ? val.overpayment_amount : 0;
							underPayment 	= (val.underpayment_amount) ? val.underpayment_amount : 0;
							otherDeduction 	= (val.other_deduction_amount) ? val.other_deduction_amount : 0;

							OTUTAmount = Number(overPayment) - Number(underPayment);

							deductionAmount = parseFloat(lwopAmount) + parseFloat(taxAmountOne) + parseFloat(taxAmountTwo) + Number(OTUTAmount) + Number(otherDeduction);

							netAmount = parseFloat(basicAmount) - parseFloat(deductionAmount);

							subBasicAmount += parseFloat(basicAmount);
							subgrossAmount += parseFloat(basicAmount);
							subLWOPAmount += parseFloat(lwopAmount);
							subTaxAmountOne += parseFloat(taxAmountOne);
							subTaxAmountTwo += parseFloat(taxAmountTwo);
							subNetAmount	+= parseFloat(netAmount);
							subOTUTAmount	+= parseFloat(OTUTAmount);
							subOtherDeductionAmount	+= parseFloat(otherDeduction);

							basic_amount = (basicAmount) ? commaSeparateNumber(parseFloat(basicAmount).toFixed(2)) : '0.00';
							gross_amount = (grossAmount) ? commaSeparateNumber(parseFloat(grossAmount).toFixed(2)) : '0.00';
							lwop_amount = (lwopAmount) ? commaSeparateNumber(parseFloat(lwopAmount).toFixed(2)) : '0.00';
							tax_amount_one = (taxAmountOne) ? commaSeparateNumber(parseFloat(taxAmountOne).toFixed(2)) : '0.00';
							tax_amount_two = (taxAmountTwo) ? commaSeparateNumber(parseFloat(taxAmountTwo).toFixed(2)) : '0.00';
							net_amount = (netAmount) ? commaSeparateNumber(parseFloat(netAmount).toFixed(2)) : '0.00';
							otut_amount = (OTUTAmount) ? commaSeparateNumber(parseFloat(Math.abs(OTUTAmount)).toFixed(2)) : '0.00';
							other_deduction_amount = (otherDeduction) ? commaSeparateNumber(parseFloat(otherDeduction).toFixed(2)) : '0.00';

							arr += '<tr>';
							arr += '<td class="text-center" style="border:none;">'+employee_number+'</td>';
							arr += '<td class="text-left" style="border:none;">'+fullname+'</td>';
							arr += '<td class="text-right" style="border:none;">'+basic_amount+'</td>';
							arr += '<td class="text-right" style="border:none;">'+basic_amount+'</td>';
							arr += '<td class="text-right" style="border:none;">'+otut_amount+'</td>'; // ot/ut
							arr += '<td class="text-right" style="border:none;">'+lwop_amount+'</td>';
							arr += '<td class="text-right" style="border:none;">'+other_deduction_amount+'</td>'; // other deduction
							arr += '<td class="text-right" style="border:none;">'+tax_amount_two+'</td>';
							arr += '<td class="text-right" style="border:none;">'+tax_amount_one+'</td>';
							arr += '<td class="text-right" style="border:none;">'+net_amount+'</td>';
							arr += '</tr>';
						});

						netBasicAmount += parseFloat(subBasicAmount);
						netgrossAmount += parseFloat(subgrossAmount);
						netLWOPAmount += parseFloat(subLWOPAmount);
						netTaxAmountOne += parseFloat(subTaxAmountOne);
						netTaxAmountTwo += parseFloat(subTaxAmountTwo);
						netNetAmount += parseFloat(subNetAmount);
						netOtherDeductionAmount += Number(subOtherDeductionAmount);
						netOTUTAmount += Number(subOTUTAmount);

						sub_basic_amount = (subBasicAmount) ? commaSeparateNumber(parseFloat(subBasicAmount).toFixed(2)) : '0.00';
						sub_gross_amount = (subgrossAmount) ? commaSeparateNumber(parseFloat(subgrossAmount).toFixed(2)) : '0.00';
						sub_lwop_amount = (subLWOPAmount) ? commaSeparateNumber(parseFloat(subLWOPAmount).toFixed(2)) : '0.00';
						sub_tax_amount_one = (subTaxAmountOne) ? commaSeparateNumber(parseFloat(subTaxAmountOne).toFixed(2)) : '0.00';
						sub_tax_amount_two = (subTaxAmountTwo) ? commaSeparateNumber(parseFloat(subTaxAmountTwo).toFixed(2)) : '0.00';
						sub_net_amount = (subNetAmount) ? commaSeparateNumber(parseFloat(subNetAmount).toFixed(2)) : '0.00';
						sub_other_deduction_amount = (subOtherDeductionAmount) ? commaSeparateNumber(parseFloat(subOtherDeductionAmount).toFixed(2)) : '0.00';
						sub_otut_amount = (subOTUTAmount) ? commaSeparateNumber(parseFloat(Math.abs(subOTUTAmount)).toFixed(2)) : '0.00';

						arr += '<tr style="font-weight:bold;">';
						arr += '<td class="text-right " style="border:none;"></td>';
						arr += '<td class="text-right " style="border:none;">TOTAL</td>';
						arr += '<td class="text-right borderstyle2">'+sub_basic_amount+'</td>';
						arr += '<td class="text-right borderstyle2">'+sub_gross_amount+'</td>';
						arr += '<td class="text-right borderstyle2">'+sub_otut_amount+'</td>';
						arr += '<td class="text-right borderstyle2">'+sub_lwop_amount+'</td>';
						arr += '<td class="text-right borderstyle2">'+sub_other_deduction_amount+'</td>';
						arr += '<td class="text-right borderstyle2">'+sub_tax_amount_two+'</td>';
						arr += '<td class="text-right borderstyle2">'+sub_tax_amount_one+'</td>';
						arr += '<td class="text-right borderstyle2">'+sub_net_amount+'</td>';
						arr += '</tr>';

						});

					});

					net_basic_amount = (netBasicAmount) ? commaSeparateNumber(parseFloat(netBasicAmount).toFixed(2)) : '0.00';
					net_gross_amount = (netgrossAmount) ? commaSeparateNumber(parseFloat(netgrossAmount).toFixed(2)) : '0.00';
					net_lwop_amount = (netLWOPAmount) ? commaSeparateNumber(parseFloat(netLWOPAmount).toFixed(2)) : '0.00';
					net_tax_amount_one = (netTaxAmountOne) ? commaSeparateNumber(parseFloat(netTaxAmountOne).toFixed(2)) : '0.00';
					net_tax_amount_two = (netTaxAmountTwo) ? commaSeparateNumber(parseFloat(netTaxAmountTwo).toFixed(2)) : '0.00';
					net_net_amount = (netNetAmount) ? commaSeparateNumber(parseFloat(netNetAmount).toFixed(2)) : '0.00';
					net_otut_amount = (netOTUTAmount) ? commaSeparateNumber(parseFloat(Math.abs(netOTUTAmount)).toFixed(2)) : '0.00';
					net_other_deduction_amount = (netOtherDeductionAmount) ? commaSeparateNumber(parseFloat(netOtherDeductionAmount).toFixed(2)) : '0.00';

					arr += '<tr style="font-weight:bold;">';
					arr += '<td class="text-center" style="border:none;"></td>';
					arr += '<td class="text-right" style="border:none;">GRAND TOTAL</td>';
					arr += '<td class="text-right borderstyle2">'+net_basic_amount+'</td>';
					arr += '<td class="text-right borderstyle2">'+net_gross_amount+'</td>';
					arr += '<td class="text-right borderstyle2">'+net_otut_amount+'</td>';
					arr += '<td class="text-right borderstyle2">'+net_lwop_amount+'</td>';
					arr += '<td class="text-right borderstyle2">'+net_other_deduction_amount+'</td>';
					arr += '<td class="text-right borderstyle2">'+net_tax_amount_two+'</td>';
					arr += '<td class="text-right borderstyle2">'+net_tax_amount_one+'</td>';
					arr += '<td class="text-right borderstyle2">'+net_net_amount+'</td>';
					arr += '</tr>';
					arr += '<div style="height:50px;"></div>'
					arr += '<tr >';
					arr += '<td style="border:none;" colspan="5" >C E R T I F I E D : Services duly rendered as stated</td>';
					arr += '<td style="border:none;" colspan="7"> APPROVED FOR PAYMENT _______________________________</td>';
					arr += '</tr>';

					arr += '<tr class="text-center">';
					arr += '<td style="border:none;padding-top: 30px;"colspan="4"> <p style="font-weight:bold;">JELLY N. ORTIZ</p> <p style="line-height:0.5em;">Supvng. Administrative Officer </p> </td>';
					arr += '<td style="border:none;padding-top: 30px;"colspan="5"> <p style="font-weight:bold;">AGUSTIN M. FUDOLIG </p> <p style="line-height:0.5em;">  Supvng. Deputy Executive Director </p></td>';
					arr += '</tr>';

					arr += '<tr >';
					arr += '<td style="border:none;padding-top: 30px;" colspan="5" >C E R T I F I E D :  Supporting documents complete & proper <br>  and Cash available in the amount of PHP'+net_net_amount+'</td>';
					arr += '</tr>';

					arr += '<tr class="text-center" >';
					arr += '<td style="border:none;padding-top: 30px;" colspan="5"> <p style="font-weight:bold;">JOHNNY G. QUINGCO</p> <p style="line-height:0.5em;">Accountant IV</p> </td>';
					arr += '</tr>';

					days = daysInMonth(_Month,_Year)

					switch(_semiPayPeriod){
						case 'firsthalf':
							_coveredPeriod =  months[_Month]+' 1-15, '+_Year;
						break;
						default:
							_coveredPeriod = months[_Month]+' 16-'+days+', '+_Year;
						break;
					}

					$('.covered_year').text(_coveredPeriod);

					$('#tbl_content').html(arr);

					$('#btnModal').trigger('click');

				}else{
					swal({
						title: "No Records Found",
						type: "warning",
						showCancelButton: false,
						confirmButtonClass: "btn-danger",
						confirmButtonText: "Yes",
						closeOnConfirm: false
					});
				}
			}
		})
	}

});

function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}

})
</script>
@endsection