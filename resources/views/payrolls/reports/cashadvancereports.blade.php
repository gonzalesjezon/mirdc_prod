@extends('app-reports')

@section('reports-content')
<style type="text/css">
	@media print{
		html,body{ font-size: 10pt;  }
		@page{
			size: legal landscape;
			max-width: 100%;
		}
	}
</style>
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Covered Date</b></span>
					</div>

				</div>
				<div class="row" style="margin-right: -5px;margin-left: -5px;">
					@include('payrolls.includes._months-year')
				</div>
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports">
	       		<div class="row" style="margin-left: -5px;">
	       			<div class="col-md-12 text-center">
	       				<h3><b>Cash Advance Summary</b></h3>
	       				<h6>Pay Period  <b><span id="month_year"></span></b></h6>

	       			</div>
	       		</div>
				<table class="table" style="border: 2px solid #585555;">
				 <thead class="text-center" style="font-weight: bold;">
					 <tr >

					 	<td rowspan="2" style="border: 2px solid #585555;">DIV</td>
					 	<td colspan="8" style="border: 2px solid #585555;">Salary Earned</td>
					 	<td colspan="5" style="border: 2px solid #585555;">Deductions</td>
					 	<td colspan="2" style="border: 2px solid #585555;">Net Pay</td>
					 </tr>
					 <tr>
					 	<td style="border: 2px solid #585555;">Basic1</td>
					 	<td style="border: 2px solid #585555;">Basic2</td>
					 	<td style="border: 2px solid #585555;">Rata1</td>
					 	<td style="border: 2px solid #585555;">Rata2</td>
					 	<td style="border: 2px solid #585555;">Pera</td>
					 	<td style="border: 2px solid #585555;">Sala</td>
					 	<td style="border: 2px solid #585555;">HP</td>
					 	<td style="border: 2px solid #585555;">LP</td>
					 	<td style="border: 2px solid #585555;">Philhealth</td>
					 	<td style="border: 2px solid #585555;">GSIS</td>
					 	<td style="border: 2px solid #585555;">Pagibig</td>
					 	<td style="border: 2px solid #585555;">Wtax</td>
					 	<td style="border: 2px solid #585555;">Others</td>
					 	<td style="border: 2px solid #585555;">Month</td>
					 	<td style="border: 2px solid #585555;"><span id="date_covered">1-15 / 16-31</span></td>
					 </tr>
				 </thead>
				 <tbody id="tbl_body">
				 </tbody>
				</table>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	var _payPeriod;
	$('.select2').select2();
	$('#select_pay_period').select2({
		allowClear:true,
	    placeholder: "Pay Period",
	});

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');

	var months ={
			1:'January',
			2:'February',
			3:'March',
			4:'April',
			5:'May',
			6:'June',
			7:'July',
			8:'August',
			9:'September',
			10:'October',
			11:'November',
			12:'December',
		}
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$(document).on('change','#select_searchvalue',function(){
		_searchvalue = "";
		_searchvalue = $(this).find(':selected').val();

	})

	$(document).on('change','#emp_status',function(){
		_emp_status = "";
		_emp_status = $(this).find(':selected').val();

	})
	$(document).on('change','#emp_type',function(){
		_emp_type = "";
		_emp_type = $(this).find(':selected').val();

	})
	$(document).on('change','#searchby',function(){
		_searchby = "";
		_searchby = $(this).find(':selected').val();

	})

	$(document).on('change','#select_pay_period',function(){
		_payPeriod = $(this).find(':selected').val()
		$('#pay_period').text(_payPeriod);
	});


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});

	$(document).on('click','#print',function(){
		$('#reports').printThis();
	});

	$(document).on('click','#preview',function(){
		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});

		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){
					console.log(data);
					if(data.transactions.length !== 0){
						body = [];

						netEarnedAmount = 0;
						grandDeductionAmount = 0;

						subBasicOneAmount 			= 0;
						subBasicTwoAmount 			= 0;
						subRataOneAmount 			= 0;
						subRataTwoAmount 			= 0;
						subPeraAmount 				= 0;
						subHpAmount 				= 0;
						subSalaAmount 				= 0;
						subLongevityAmount 			= 0;
						subPagibigAmount 			= 0;
						subPhilhealthAmount 		= 0;
						subGsisAmount 				= 0;
						subTaxAmount 				= 0;
						subTotalLoanAmount 			= 0;
						subTotalDeductionAmount 	= 0;
						subSalaryEarnedAmount 		= 0;
						subDeductionAmount 			= 0;
						subNetPay 			   		= 0;
						subDividedNetPay 	   		= 0;
						$.each(data.transactions,function(key,val){

							body += '<tr>';
							body += '<td class="text-center">'+key+'</td>';

							basicOneAmount 	= 0;
							basicTwoAmount 	= 0;
							rataOneAmount 	= 0;
							rataTwoAmount 	= 0;
							peraAmount 		= 0;
							hpAmount 		= 0;
							salaAmount 		= 0;
							longevityAmount = 0;
							pagibigAmount 	= 0;
							philhealthAmount = 0;
							gsisAmount 		 = 0;
							taxAmount 		 = 0;
							totalLoanAmount 	= 0;
							totalDeductionAmount = 0;
							salaryEarnedAmount 	= 0;
							deductionAmount    	= 0;
							netPay 			   	= 0;
							dividedNetPay 	   	= 0;
							salaAbsentAmount 	= 0;
							salaUndertimeAmount = 0;
							usedAmount 			= 0;

							$.each(val,function(k,v){
								basicOne = (v.salaryinfo.basic_amount_one) ? v.salaryinfo.basic_amount_one : 0;
								basicTwo = (v.salaryinfo.basic_amount_two) ? v.salaryinfo.basic_amount_two : 0;
								basicOneAmount += parseFloat(basicOne);
								basicTwoAmount += parseFloat(basicTwo);

								rataOne 			= 0;
								rataTwo 			= 0;
								pera 				= 0;
								hp 					= 0;
								saAmount 			= 0;
								laAmount 			= 0;
								salaAbsent 			= 0;
								salaUndertime 		= 0;
								longevity 			= 0;
								saAbsentAmount 		= 0;
								saUndertimeAmount 	= 0;
								laAbsentAmount 		= 0;
								laUndertimeAmount 	= 0;
								pdiemAmount 		= 0;
								coveredDate 		= 0;

								$.each(v.benefit_transactions,function(k1,v1){

									switch(v1.benefits.code){
										case 'PERA':
											pera = (v1.amount) ? v1.amount : 0;
											break;
										case 'RATA1':
											rataOne = (v1.amount) ? v1.amount : 0;
											break;
										case 'RATA2':
											rataTwo = (v1.amount) ? v1.amount : 0;
											usedAmount = (v1.used_amount !== null) ? v1.used_amount : 0;
											coveredDate = (v1.ta_covered_date !== null) ? v1.ta_covered_date : "";
											break;
										case 'HP':
											hp = (v1.amount) ? v1.amount : 0;
											break;
										case 'SA':
											saAmount = (v1.amount) ? v1.amount : 0;
											saAbsentAmount = (v1.sala_absent_amount) ? v1.sala_absent_amount : 0;
											saUndertimeAmount = (v1.sala_undertime_amount) ? v1.sala_undertime_amount : 0;
											break;
										case 'LA':
											laAmount = (v1.amount) ? v1.amount : 0;
											laAbsentAmount = (v1.sala_absent_amount) ? v1.sala_absent_amount : 0;
											laUndertimeAmount = (v1.sala_undertime_amount) ? v1.sala_undertime_amount : 0;
											break;
										case 'LP':
											longevity = (v1.amount) ? v1.amount : 0;
											break;
										case 'PDIEM':
											pdiemAmount = (v1.amount) ? v.amount : 0;
											break;
									}
								});

								rataOneAmount += parseFloat(rataOne);
								rataTwoAmount += parseFloat(rataTwo);
								peraAmount += parseFloat(pera);

								salaAbsent = parseFloat(saAbsentAmount) + parseFloat(laAbsentAmount);
								salaUndertime = parseFloat(saUndertimeAmount) + parseFloat(laUndertimeAmount);
								sala = parseFloat(saAmount) + parseFloat(laAmount);

								hpAmount += parseFloat(hp);
								longevityAmount += parseFloat(longevity);
								salaAmount += parseFloat(sala);

								philhealth = (v.phic_share) ? v.phic_share : 0;
								pagibig = (v.pagibig_share) ? v.pagibig_share : 0;
								gsis = (v.gsis_ee_share) ? v.gsis_ee_share : 0;
								tax = (v.tax_amount) ? v.tax_amount : 0;
								totalLoan = (v.total_loan) ? v.total_loan : 0;
								totalDeduction = (v.total_otherdeduct) ? v.total_otherdeduct : 0;

								salaAbsent = (salaAbsent !== null) ? salaAbsent : 0;
								salaUndertime = (salaUndertime !== null) ? salaUndertime : 0;

								pagibigAmount += parseFloat(pagibig);
								philhealthAmount += parseFloat(philhealth);
								gsisAmount += parseFloat(gsis);
								taxAmount += parseFloat(tax);
								salaAbsentAmount += parseFloat(salaAbsent);
								salaUndertimeAmount += parseFloat(salaUndertime);
								totalLoanAmount += parseFloat(totalLoan)
								totalDeductionAmount += parseFloat(totalDeduction);

							});


							salaryEarnedAmount = parseFloat(basicOneAmount) + parseFloat(basicTwoAmount) + parseFloat(rataOneAmount) + parseFloat(rataTwoAmount) + parseFloat(peraAmount) + parseFloat(hpAmount) + parseFloat(salaAmount) + parseFloat(longevityAmount) + parseFloat(pdiemAmount);

							deductionAmount = parseFloat(pagibigAmount) + parseFloat(philhealthAmount) + parseFloat(gsisAmount) + parseFloat(taxAmount) + parseFloat(totalLoanAmount) + parseFloat(totalDeductionAmount) + parseFloat(salaAbsentAmount) + parseFloat(salaUndertimeAmount) + Number(usedAmount);

							netPay = parseFloat(salaryEarnedAmount) - parseFloat(deductionAmount);

							dividedNetPay = parseFloat(netPay) / 2;

							subBasicOneAmount += parseFloat(basicOneAmount)
							subBasicTwoAmount += parseFloat(basicTwoAmount)
							subRataOneAmount += parseFloat(rataOneAmount)
							subRataTwoAmount += parseFloat(rataTwoAmount)
							subPeraAmount += parseFloat(peraAmount)
							subHpAmount += parseFloat(hpAmount)
							subSalaAmount += parseFloat(salaAmount)
							subLongevityAmount += parseFloat(longevityAmount)
							subPagibigAmount += parseFloat(pagibigAmount)
							subPhilhealthAmount += parseFloat(philhealthAmount)
							subGsisAmount += parseFloat(gsisAmount)
							subTaxAmount += parseFloat(taxAmount)
							subTotalLoanAmount += parseFloat(totalLoanAmount)
							subTotalDeductionAmount += parseFloat(deductionAmount)
							subSalaryEarnedAmount += parseFloat(salaryEarnedAmount)
							subNetPay 		+= parseFloat(netPay)
							subDividedNetPay += parseFloat(dividedNetPay)

							basic_one_amount = (basicOneAmount) ? commaSeparateNumber(parseFloat(basicOneAmount).toFixed(2)) : '';
							basic_two_amount = (basicTwoAmount) ? commaSeparateNumber(parseFloat(basicTwoAmount).toFixed(2)) : '';

							rata_one_amount = (rataOneAmount) ? commaSeparateNumber(parseFloat(rataOneAmount).toFixed(2)) : '';
							rata_two_amount = (rataTwoAmount) ? commaSeparateNumber(parseFloat(rataTwoAmount).toFixed(2)) : '';

							pera_amount = (peraAmount) ? commaSeparateNumber(parseFloat(peraAmount).toFixed(2)) : '';

							hp_amount = (hpAmount) ? commaSeparateNumber(parseFloat(hpAmount).toFixed(2)) : '';
							sala_amount = (salaAmount) ? commaSeparateNumber(parseFloat(salaAmount).toFixed(2)) : '';
							longevity_amount = (longevityAmount) ? commaSeparateNumber(parseFloat(longevityAmount).toFixed(2)) : '';

							pagibig_amount = (pagibigAmount) ? commaSeparateNumber(parseFloat(pagibigAmount).toFixed(2)) : '';
							philhealth_amount = (philhealthAmount) ? commaSeparateNumber(parseFloat(philhealthAmount).toFixed(2)) : '';
							gsis_amount = (gsisAmount) ? commaSeparateNumber(parseFloat(gsisAmount).toFixed(2)) : '';
							tax_amount = (taxAmount) ? commaSeparateNumber(parseFloat(taxAmount).toFixed(2)) : '';
							total_deduction_amount = (deductionAmount) ? commaSeparateNumber(parseFloat(deductionAmount).toFixed(2)) : '';
							net_pay_amount = (netPay) ? commaSeparateNumber(parseFloat(netPay).toFixed(2)) : '';
							divided_net_pay_amount = (dividedNetPay) ? commaSeparateNumber(parseFloat(dividedNetPay).toFixed(2)) : '';
							sala_absent_amount = (salaAbsentAmount) ? commaSeparateNumber(parseFloat(salaAbsentAmount).toFixed(2)) : '';
							sala_undertime_amount = (salaUndertimeAmount) ? commaSeparateNumber(parseFloat(salaUndertimeAmount).toFixed(2)) : '';
							used_amount = (usedAmount) ? commaSeparateNumber(parseFloat(usedAmount).toFixed(2)) : '';
							pdiem_amount = (pdiemAmount !== 0) ? commaSeparateNumber(parseFloat(pdiemAmount).toFixed(2)) : '';

							body += '<td class="text-right">'+basic_one_amount+'</td>';
							body += '<td class="text-right">'+basic_two_amount+'</td>';

							body += '<td class="text-right">'+rata_one_amount+'</td>';
							body += '<td class="text-right">'+rata_two_amount+'</td>';

							body += '<td class="text-right">'+pera_amount+'</td>';

							body += '<td class="text-right">'+sala_amount+'</td>';
							body += '<td class="text-right">'+hp_amount+'</td>';
							body += '<td class="text-right">'+longevity_amount+'</td>';

							body += '<td class="text-right">'+philhealth_amount+'</td>';
							body += '<td class="text-right">'+gsis_amount+'</td>';
							body += '<td class="text-right">'+pagibig_amount+'</td>';
							body += '<td class="text-right">'+tax_amount+'</td>';

							body += '<td class="text-right">'+total_deduction_amount+'</td>';
							body += '<td class="text-right">'+net_pay_amount+'</td>';
							body += '<td class="text-right">'+divided_net_pay_amount+'</td>';
							body += '</tr>';

							if(salaAbsentAmount){
								body += '<tr>';
								body += '<td colspan="13" style="border-right:none;"></td>';
								body += '<td class="text-right">'+sala_absent_amount+'</td>';
								body += '<td>Absent (SALA)</td>';
								body += '<td></td>';
								body += '</tr>';
							}

							if(salaUndertimeAmount){
								body += '<tr>';
								body += '<td colspan="13" style="border-right:none;"></td>';
								body += '<td class="text-right">'+sala_undertime_amount+'</td>';
								body += '<td>Undertime (SALA)</td>';
								body += '<td></td>';
								body += '</tr>';
							}

							// Rata 2
							if(usedAmount){
								body += '<tr>';
								body += '<td colspan="13" style="border-right:none;"></td>';
								body += '<td class="text-right">'+used_amount+'</td>';
								body += '<td>TA ('+coveredDate+')</td>';
								body += '<td></td>';
								body += '</tr>';
							}

							if(pdiemAmount){
								body += '<tr>';
								body += '<td colspan="13" style="border-right:none;"></td>';
								body += '<td class="text-right">'+pdiem_amount+'</td>';
								body += '<td>TA</td>';
								body += '<td></td>';
								body += '</tr>';
							}

							$.each(data.loanlists[key],function(k1,v1){
								var loanName
								var loansAmount = 0;
								loanName = k1;
								$.each(v1,function(k2,v2){
									amount = (v2.amount) ? v2.amount : 0;
									loansAmount += parseFloat(amount);
								});


								loans_amount = (loansAmount) ? commaSeparateNumber(parseFloat(loansAmount).toFixed(2)) : '';

								body += '<tr>';
								body += '<td colspan="13" style="border-right:none;"></td>';
								body += '<td class="text-right">'+loans_amount+'</td>';
								body += '<td>'+loanName+'</td>';
								body += '<td></td>';
								body += '</tr>';
							})

							$.each(data.deductionlists[key],function(k3,v3){
								var deductionName;
								var deductionsAmount = 0;
								deductionName = k3;
								$.each(v3,function(k4,v4){
									amount = (v4.amount) ? v4.amount : 0;
									deductionsAmount += parseFloat(v4.amount)
								});

								deduction_amount = (deductionsAmount) ? commaSeparateNumber(parseFloat(deductionsAmount).toFixed(2)) : '';

								body += '<tr>';
								body += '<td colspan="13" style="border-right:none;"></td>';
								body += '<td class="text-right">'+deduction_amount+'</td>';
								body += '<td>'+deductionName+'</td>';
								body += '<td></td>';
								body += '</tr>';
							})

						});


					totalEarnedAmount = parseFloat(subBasicOneAmount) + parseFloat(subBasicTwoAmount) + parseFloat(subRataOneAmount) + parseFloat(subRataTwoAmount) + parseFloat(subPeraAmount) + parseFloat(subHpAmount) + parseFloat(subSalaAmount) + parseFloat(subLongevityAmount);

					netDeductionAmount = parseFloat(subTotalDeductionAmount) + parseFloat(subTotalLoanAmount);

					netEarnedAmount += parseFloat(totalEarnedAmount);
					grandDeductionAmount += parseFloat(netDeductionAmount);

					sub_basicone_amount = (subBasicOneAmount) ? commaSeparateNumber(parseFloat(subBasicOneAmount).toFixed(2)) : '';
					sub_basictwo_amount = (subBasicTwoAmount) ? commaSeparateNumber(parseFloat(subBasicTwoAmount).toFixed(2)) : '';

					sub_rataone_amount = (subRataOneAmount) ? commaSeparateNumber(parseFloat(subRataOneAmount).toFixed(2)) : '';
					sub_ratatwo_amount = (subRataTwoAmount) ? commaSeparateNumber(parseFloat(subRataTwoAmount).toFixed(2)) : '';

					sub_pera_amount = (subPeraAmount) ? commaSeparateNumber(parseFloat(subPeraAmount).toFixed(2)) : '';

					sub_hp_amount = (subHpAmount) ? commaSeparateNumber(parseFloat(subHpAmount).toFixed(2)) : '';
					sub_sala_amount = (subSalaAmount) ? commaSeparateNumber(parseFloat(subSalaAmount).toFixed(2)) : '';
					sub_longevity_amount = (subLongevityAmount) ? commaSeparateNumber(parseFloat(subLongevityAmount).toFixed(2)) : '';

					sub_pagibig_amount = (subPagibigAmount) ? commaSeparateNumber(parseFloat(subPagibigAmount).toFixed(2)) : '';
					sub_philhealth_amount = (subPhilhealthAmount) ? commaSeparateNumber(parseFloat(subPhilhealthAmount).toFixed(2)) : '';
					sub_gsis_amount = (subGsisAmount) ? commaSeparateNumber(parseFloat(subGsisAmount).toFixed(2)) : '';
					sub_tax_amount = (subTaxAmount) ? commaSeparateNumber(parseFloat(subTaxAmount).toFixed(2)) : '';
					sub_deduction_amount = (subTotalDeductionAmount) ? commaSeparateNumber(parseFloat(subTotalDeductionAmount).toFixed(2)) : '';
					sub_netpay_amount = (subNetPay) ? commaSeparateNumber(parseFloat(subNetPay).toFixed(2)) : '';
					sub_divided_net_pay_amount = (subDividedNetPay) ? commaSeparateNumber(parseFloat(subDividedNetPay).toFixed(2)) : '';
					body += '<tr style="font-weight:bold;">';
					body += '<td class="text-right">TOTAL</td>';
					body += '<td class="text-right">'+sub_basicone_amount+'</td>';
					body += '<td class="text-right">'+sub_basictwo_amount+'</td>';

					body += '<td class="text-right">'+sub_rataone_amount+'</td>';
					body += '<td class="text-right">'+sub_ratatwo_amount+'</td>';

					body += '<td class="text-right">'+sub_pera_amount+'</td>';

					body += '<td class="text-right">'+sub_sala_amount+'</td>';
					body += '<td class="text-right">'+sub_hp_amount+'</td>';
					body += '<td class="text-right">'+sub_longevity_amount+'</td>';

					body += '<td class="text-right">'+sub_philhealth_amount+'</td>';
					body += '<td class="text-right">'+sub_gsis_amount+'</td>';
					body += '<td class="text-right">'+sub_pagibig_amount+'</td>';
					body += '<td class="text-right">'+sub_tax_amount+'</td>';

					body += '<td class="text-right">'+sub_deduction_amount+'</td>';
					body += '<td class="text-right">'+sub_netpay_amount+'</td>';
					body += '<td class="text-right">'+sub_divided_net_pay_amount+'</td>';
					body += '</tr>';

					net_earned_amount = (netEarnedAmount) ? commaSeparateNumber(parseFloat(netEarnedAmount).toFixed(2)) : '';
					grand_deduction_amount = (grandDeductionAmount) ? commaSeparateNumber(parseFloat(grandDeductionAmount).toFixed(2)) : '';

					body += '<tr>';
					body += '<td style="padding-top:30px;font-weight:bold;border:none;" colspan="8" class="text-center">'+net_earned_amount+'</td>';
					body += '<td style="padding-top:30px;font-weight:bold;border:none;" colspan="5" class="text-center">'+grand_deduction_amount+'</td>';
					body += '<td style="padding-top:30px;font-weight:bold;border:none;" colspan="3" class="text-center"></td>';
					body += '</tr>';

					body += '<tr>';
					body += '<td style="padding-top:30px;font-weight:bold;border:none;" colspan="14" class="text-center">DEDUCTION TOTALS</td>';
					body += '<td style="padding-top:30px;font-weight:bold;border:none;" colspan="2" class="text-center"></td>';
					body += '</tr>';

					$.each(data.grouploans,function(k1,v1){
						var loanName
						var loansAmount = 0;
						loanName = k1;
						$.each(v1,function(k2,v2){
							amount = (v2.amount) ? v2.amount : 0;
							loansAmount += parseFloat(amount);
						});

						loans_amount = (loansAmount) ? commaSeparateNumber(parseFloat(loansAmount).toFixed(2)) : '';

						body += '<tr>';
						body += '<td colspan="4" style="border:none;"></td>';
						body += '<td colspan="3" class="text-right" style="border:none;">'+loans_amount+'</td>';
						body += '<td colspan="3" style="border:none;">'+loanName+'</td>';
						body += '<td colspan="6" style="border:none;"></td>';
						body += '</tr>';
					})

					$.each(data.groupdeductions,function(k3,v3){
						var deductionName;
						var deductionsAmount = 0;
						deductionName = k3;
						$.each(v3,function(k4,v4){
							amount = (v4.amount) ? v4.amount : 0;
							deductionsAmount += parseFloat(v4.amount)
						});

						deduction_amount = (deductionsAmount) ? commaSeparateNumber(parseFloat(deductionsAmount).toFixed(2)) : '';

						body += '<tr>';
						body += '<td colspan="4" style="border:none;"></td>';
						body += '<td colspan="3" class="text-right" style="border:none;">'+deduction_amount+'</td>';
						body += '<td colspan="3" style="border:none;">'+deductionName+'</td>';
						body += '<td colspan="6" style="border:none;"></td>';
						body += '</tr>';
					})


						$('#tbl_body').html(body);
						$('#month_year').text(months[_Month]+' '+_Year);
						$('#btnModal').trigger('click');

					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			});
		}
	});
})
</script>
@endsection