@extends('app-reports')


@section('reports-content')
<link rel="stylesheet" type="text/css" media="print" href="{{ asset('css/printportrait.css') }}">
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Covered Date</b></span>
					</div>
				</div>
				<div class="row" style="margin-right: -5px;margin-left: -5px;">
					@include('payrolls.includes._months-year')
				</div>
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:100%;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports" style="width: 960px;">
	       		<div class="row">
	       		<!-- 	<div class="col-md-5 text-right">
   						<img src="{{ url('images/mirdc_logo.gif') }}" style="height: 80px;">
   						<i></i>
   					</div> -->
   					<div class="col-md-12 text-center" style="font-weight: bold;margin: auto;padding-top: 15px;">
   						Department of Science and Technology <br>
   						Metals Industry Research and Development Center <br>
   					</div>
	       		</div>
	       		<br>
	       		<div class="row">
	       			<div class="col-md-12 text-center" style="font-weight: bold">
	       				<span>
							List of Employees Entitled to Uniform/ Clothing Allowance
	       					<span class="covered_date"></span>
	       				</span>
	       			</div>
	       		</div>
   				<table class="table" style="border: 2px solid #5a5a5a">
   					<thead style="border: 2px solid #5a5a5a">
   						<tr class="text-center" style="font-weight: bold;">
   							<td  style="width: 10px;">#</td>
   							<td >Name</td>
   							<td >Uniform Allowance</td>
   							<td >Cost of Uniform</td>
   							<td>Net Uniform Allowance</td>
   						</tr>
   					</thead>
   					</tfoot>
   					<tbody id="tbl_content"></tbody>
   				</table>
   				<div class="row" style="padding: 10px;">
   					<div class="col-md-6">
   						Prepared by: <br><br><br>
   						<p style="margin-left: 30px;color:#333;">
							<b>LAILA R. PORLUCAS</b> <br>
							Administrative Officer IV
   						</p>
   					</div>
   					<div class="col-md-6">
   						Certified Correct: <br><br><br>
   						<p style="margin-left: 30px;color:#333;">
							<b>JELLY N. ORTIZ, DPA</b> <br>
							Chief, FAD - AGSS
   						</p>
   					</div>
   				</div>
	       </div>
	 	</div>
	</div>
</div>
<!-- 0.328571 -->
@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})

	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');

	var months ={
			1:'January',
			2:'February',
			3:'March',
			4:'April',
			5:'May',
			6:'June',
			7:'July',
			8:'August',
			9:'September',
			10:'October',
			11:'November',
			12:'December',
		}
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})
	var _payPeriod;
	var _semiPayPeriod;
	$(document).on('change','#pay_period',function(){
		_payPeriod = $(this).find(':selected').val();
		switch(_payPeriod){
			case 'semimonthly':
				$('#semi_pay_period').removeClass('hidden');
			break;
			default:
				$('#semi_pay_period').addClass('hidden');
			break;
		}
	});

	$(document).on('change','#semi_pay_period',function(){
		_semiPayPeriod = $(this).find(':selected').val();
	})

$(document).on('click','#print',function(){
	$('#reports').printThis();
});

$(document).on('click','#preview',function(){

	year = (_Year) ? _Year : '';
	month = (_Month) ? _Month : '';
	emp_type = (_emp_type) ? _emp_type : '';
	emp_status = (_emp_status) ? _emp_status : '';
	month = (_Month) ? _Month : '';
	category = (_searchvalue) ? _searchvalue : '';
	searchby = (_searchby) ? _searchby : '';

	if(!year || !month){
		swal({
			  title: "Select Year and Month!",
			  type: "warning",
			  showCancelButton: false,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Yes",
			  closeOnConfirm: false

		});
	}else{
		$.ajax({
			url:base_url+module_prefix+module+'/show',
			data:{
				'id':_empid,
				'year':year,
				'month':month,
				'emp_type':emp_type,
				'emp_status':emp_status,
				'category':category,
				'searchby':searchby,
			},
			type:'GET',
			dataType:'JSON',
			success:function(data){
				console.log(data)
				if(data.length !== 0){
					arr = [];

					ctr = 1;
					grand_total = 0;
					net_unfiform = 0;
					net_cost  = 0;
					net_total =  0;
					$.each(data,function(key,value){
						sub_total = 0;
						sub_total_unform_allowance = 0
						sub_total_cost_allowance = 0;
						arr += '<tr>';
						arr += '<td></td>';
						arr += '<td style="font-weight:bold;" colspan="4">'+key+'</td>';

						$.each(value,function(k,v){

							firstname = (v.employees.firstname) ? v.employees.firstname : '';
							lastname = (v.employees.lastname) ? v.employees.lastname : '';
							middlename = (v.employees.middlename) ? v.employees.middlename : '';
							net_allowance = (v.amount) ? v.amount : 0;
							cost_uniform_amount = (v.cost_uniform_amount) ? v.cost_uniform_amount : 0;
							percentage = (v.percentage) ? v.percentage : 0;
							uniform_allowance = (v.amount) ? v.amount : 0;

							fullname = lastname+' '+firstname+' '+middlename;

							net_allowance = Number(uniform_allowance) - Number(cost_uniform_amount);

							sub_total += parseFloat(net_allowance);
							sub_total_unform_allowance += parseFloat(uniform_allowance);
							sub_total_cost_allowance += parseFloat(cost_uniform_amount);

							net_allowance = (net_allowance !== 0) ? commaSeparateNumber(parseFloat(net_allowance).toFixed(2)) : '';
							cost_uniform_amount = (cost_uniform_amount !== 0) ? commaSeparateNumber(parseFloat(cost_uniform_amount).toFixed(2)) : '';
							uniform_allowance = (uniform_allowance !== 0) ? commaSeparateNumber(parseFloat(uniform_allowance).toFixed(2)) : '';
							percentage = (percentage !== 0) ? parseInt(percentage) +'%' : '';


							arr += '<tr>';
							arr += '<td>'+ctr+'</td>';
							arr += '<td>'+fullname+'</td>';
							arr += '<td class="text-right">'+uniform_allowance+'</td>';
							arr += '<td class="text-right">'+cost_uniform_amount+'</td>';
							arr += '<td class="text-right">'+net_allowance+'</td>';
							arr += '</tr>';

							ctr++;

						});

						arr += '</tr>';

						net_unfiform += parseFloat(sub_total);
						net_cost += parseFloat(sub_total_cost_allowance);
						net_total += parseFloat(sub_total_unform_allowance);

						sub_total = (sub_total !== 0) ? commaSeparateNumber(parseFloat(sub_total).toFixed(2)) : '';
						sub_total_cost_allowance = (sub_total_cost_allowance !== 0) ? commaSeparateNumber(parseFloat(sub_total_cost_allowance).toFixed(2)) : '';
						sub_total_unform_allowance = (sub_total_unform_allowance !== 0) ? commaSeparateNumber(parseFloat(sub_total_unform_allowance).toFixed(2)) : '';

						arr += '<tr>';
						arr += '<td></td>';
						arr += '<td style="font-weight:bold;">SUB TOTAL</td>';
						arr += '<td style="font-weight:bold;" class="text-right">'+sub_total_unform_allowance+'</td>';
						arr += '<td style="font-weight:bold;" class="text-right">'+sub_total_cost_allowance+'</td>';
						arr += '<td style="font-weight:bold;" class="text-right">'+sub_total+'</td>';
						arr += '</tr>';
					});

						net_total = (net_total !== 0) ? commaSeparateNumber(parseFloat(net_total).toFixed(2)) : '';
						net_cost = (net_cost !== 0) ? commaSeparateNumber(parseFloat(net_cost).toFixed(2)) : '';
						net_unfiform = (net_unfiform !== 0) ? commaSeparateNumber(parseFloat(net_unfiform).toFixed(2)) : '';
						arr += '<tr>';
						arr += '<td></td>';
						arr += '<td style="font-weight:bold;">TOTAL</td>';
						arr += '<td style="font-weight:bold;" class="text-right">'+net_total+'</td>';
						arr += '<td style="font-weight:bold;" class="text-right">'+net_cost+'</td>';
						arr += '<td style="font-weight:bold;" class="text-right">'+net_unfiform+'</td>';
						arr += '</tr>';


					$('#tbl_content').html(arr);


					// days = daysInMonth(_monthNumber,_Year)

					// if(_payPeriod == 'monthly'){

					// }else{
					// 	switch(_semiPayPeriod){
					// 		case 'firsthalf':
					// 			_coveredPeriod = _Month+' 1-15, '+_Year;
					// 		break;
					// 		default:
					// 			_coveredPeriod =_Month+' 16-'+days+', '+_Year;
					// 		break;
					// 	}
					// }

					_coveredPeriod = _Year;

					$('.covered_date').text(_coveredPeriod);



					$('#btnModal').trigger('click');

				}else{
					swal({
						title: "No Records Found",
						type: "warning",
						showCancelButton: false,
						confirmButtonClass: "btn-danger",
						confirmButtonText: "Yes",
						closeOnConfirm: false
					});
				}
			}
		})
	}



});
function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}
})
</script>
@endsection