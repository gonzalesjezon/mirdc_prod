@extends('app-front')

@section('content')

<div class="row" style="height: 470px;">
	<div class="col-md-12" >
		<hr>
		<div class="col-md-4">
			<table class="table borderless" style="border: none;width: 90%; margin-left: 12px;position: relative;bottom: -20px;">
				<tr>
					<td><label>Select Other Payroll</label></td>
					<td>
						<select class="form-control font-style2" id="select_otherpayroll">
							<option></option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label>Transaction Date</label></td>
					<td>
						<input type="text" name="input_date" class="form-control font-style2" id="input_date" />
					</td>
				</tr>

			</table>

			<div class="border-style4" >
				<table class="table">
					<thead>
						<tr>
							<th>Sel</th>
							<th>Transaction Date</th>
							<th>Type of Payroll</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<div class="col-md-8">
			<div class="col-md-12">
				<div style="padding-top: 15px;position: relative;" class="text-right" >
					<div class="col-md-2">
						<label >Search by</label>
					</div>
					<div class="col-md-2">
						<select class="form-control font-style2" id="select_searchby">
							<option></option>
						</select>
					</div>
					<div class="col-md-2">
						<label class="text-right">Search Value</label></td>
					</div>
					<div class="col-md-2">
						<select class="form-control font-style2" id="select_value">
							<option></option>
						</select>
					</div>
					<div class="col-md-1">
						<label>Search</label>
					</div>
					<div class="col-md-2">
						<input type="text" name="input_search" id="input_search" class="form-control font-style2">
					</div>
				</div>
			</div>
			<div style="height: 45px"></div>
			<div class="border-style4">
				<table class="table">
					<thead>
						<tr>
							<th>Sel</th>
							<th>ID No</th>
							<th>Name</th>
							<th>Net Pay</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
			

	</div>
</div>

@endsection

@section('js-login1')
@endsection