<div class="col-md-12" id="benefitForm" >
	<div class="box-1 button-style-wrapper" >
			<a class="btn btn-xs btn-info btn-savebg btn_new" id="newBenefit" data-btnnew="newBenefit" data-btncancel="cancelBenefit" data-btnedit="editBenefit" data-btnsave="saveBenefit"><i class="fa fa-save"></i> New</a>

			<a class="btn btn-xs btn-success btn-editbg btn_edit hidden" data-btnnew="newBenefit" data-btnedit="editBenefit" data-btnsave="saveBenefit" data-btncancel="cancelBenefit" data-btndelete="deleteBenefit" id="editBenefit"><i class="fa fa-edit"></i> Edit</a>

			<a class="btn btn-xs btn-info btn-savebg btn_save submitme hidden" data-btnnew="newBenefit" data-btnedit="editBenefit" data-btnsave="saveBenefit" data-btncancel="cancelBenefit" data-btndelete="deleteBenefit" data-form="form3" id="saveBenefit"><i class="fa fa-save"></i> Save</a>
			<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newBenefit" data-btnedit="editBenefit" data-btnsave="saveBenefit" data-btncancel="cancelBenefit" data-btndelete="deleteBenefit" data-form="myform3" id="cancelBenefit"> Cancel</a>
	</div>
	<form method="POST" action="{{ url($module_prefix.'/'.$module.'/storeBenefitinfo')}}" onsubmit="return false" id="form3" class="myform3" >
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<div class="panel newAttendance" style="padding: 10px;">

			<!-- Rata Adjustment -->
			<div class="row form-group hidden rataAdjust">
				<label class="col-md-2">Used</label>
				<div class="col-md-3">
					<input type="text" name="used_count" id="used_count" class="form-control font-style2 newBenefit">
				</div>

				<label class="col-md-1">Amount</label>
				<div class="col-md-3">
					<input type="text" name="used_amount" id="used_amount" class="form-control font-style2" readonly placeholder="0.00"/>
				</div>
			</div>

			<div class="row form-group hidden rataAdjust">
				<label class="col-md-2">Date</label>
				<div class="col-md-3">
					<input type="text" name="ta_covered_date" id="ta_covered_date" class="form-control font-style2 newBenefit">
				</div>
			</div>

			<div class="row form-group hidden daysPresent">
				<label class="col-md-2">Days of Present</label>
				<div class="col-md-3">
					<input type="text" name="days_present_sala" id="days_present_sala" class="form-control font-style2  newBenefit" maxlength="2">
				</div>

				<label class="col-md-1">Amount</label>
				<div class="col-md-3">
					<input type="text" name="benefit_amount_sala" id="benefits_amount_sala" class="form-control font-style2 onlyNumber newBenefit" readonly>
				</div>
			</div>

			<!-- SA & LA Absent adjustment -->
			<div class="row form-group hidden daysPresent">
				<label class="col-md-2">Absent</label>
				<div class="col-md-3">
					<input type="text" name="sala_absent" id="sala_absent" class="form-control font-style2  newBenefit" maxlength="2">
				</div>

				<label class="col-md-1">Amount</label>
				<div class="col-md-3">
					<input type="text" name="sala_absent_amount" id="sala_absent_amount" class="form-control font-style2 onlyNumber newBenefit" readonly>
				</div>
			</div>

			<!-- SA & LA Undertime adjustment -->
			<div class="row form-group hidden daysPresent" id="notInclude">
				<label class="col-md-2">Undertime</label>
				<div class="col-md-3">
					<input type="text" name="sala_undertime" id="sala_undertime" class="form-control font-style2  newBenefit" maxlength="4">
				</div>

				<label class="col-md-1">Amount</label>
				<div class="col-md-3">
					<input type="text" name="sala_undertime_amount" id="sala_undertime_amount" class="form-control font-style2 onlyNumber newBenefit" readonly>
				</div>
			</div>

			<!-- HP Adjustment -->
			<div class="row form-group hidden hpRate">
				<label class="col-md-2">Days of Present</label>
				<div class="col-md-3">
					<input type="text" name="days_present_hp" id="days_present_hp" class="form-control font-style2  newBenefit">
				</div>

				<label class="col-md-1">Amount</label>
				<div class="col-md-3">
					<input type="text" name="benefit_amount_hp" id="benefits_amount_hp" class="form-control font-style2 onlyNumber newBenefit" readonly>
				</div>
			</div>

			<div class="row form-group hidden hpRate">
				<label class="col-md-2">Select Rate</label>
				<div class="col-md-3">
					<select name="benefit_rate" id="benefit_rate" class="form-control font-style2 newBenefit">
						<option value=""></option>
						@foreach($hp_rate as $key => $value)
						<option value="{{ $value->rate }}">{{ $value->rate*100 }} %</option>
						@endforeach
					</select>
				</div>
			</div>

			<div class="row form-group benefitSetup">
				<label class="col-md-2">Select Benefit</label>
				<div class="col-md-3">
					<select name="benefit_id" id="benefit_id" class="form-control font-style2 newBenefit">
						<option value=""></option>
						@foreach($benefit as $key => $value)
						<option value="{{ $value->id }}">{{ $value->name }}</option>
						@endforeach
					</select>
				</div>

				<label class="col-md-1">Amount</label>
				<div class="col-md-3">
					<input type="text" name="amount" id="amount" class="form-control font-style2 onlyNumber newBenefit">
				</div>
			</div>
		</div>
		<br>
		<input type="hidden" name="id" id="benefit_info_id">
		<input type="hidden" name="employee_id" class="employee_id">
		<input type="hidden" name="year" id="year">
		<input type="hidden" name="month" id="month">
		<input type="hidden" name="benefit_status" id="benefit_status">
	</form>
</div>