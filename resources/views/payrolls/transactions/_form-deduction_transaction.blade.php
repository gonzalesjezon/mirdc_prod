<div class="col-md-6">
	<div class="button-wrapper" style="position: relative;top: -10px;" >
		<a class="btn btn-xs btn-info btn-savebg btn_new" id="newDeduction" data-btnnew="newDeduction" data-btncancel="cancelDeduction" data-btnedit="editDeduction" data-btnsave="saveDeduction"><i class="fa fa-save"></i> New</a>

		<a class="btn btn-xs btn-info btn-savebg btn_save submitme hidden" data-form="formDeduction" data-btnnew="newDeduction" data-btncancel="cancelDeduction" data-btnedit="editDeduction" data-btnsave="saveDeduction" id="saveDeduction"><i class="fa fa-save"></i> Save</a>
		<a class="btn btn-xs btn-danger btn_cancel2 hidden" data-btnnew="newDeduction" data-btncancel="cancelDeduction" data-form="myform" data-btnedit="editDeduction" data-btnsave="saveDeduction"id="cancelDeduction"> Cancel</a>
	</div>
	<div class="panel" style="padding: 10px;">
		<form method="post" action="{{ url($module_prefix.'/'.$module.'/storeNewDeduction') }}" onsubmit="return false" id="formDeduction" class="formDeduction">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="employee_id" class="employee_id">
			<input type="hidden" name="id" id="deduction_info_id">
			<input type="hidden" name="year" class="year">
			<input type="hidden" name="month" class="month">
			<table class="table borderless noborder">
				<tbody class="noborder-top">
					<tr>
						<td>
							Select Deduction
						</td>
						<td class="newDeduction">
							<select class="form-control font-style2" id="select_deduction" name="select_deduction">
								<option></option>
								@foreach($deductions as $key => $value)
									<option value="{{ $value->id }}">{{ $value->name }}</option>
								@endforeach
							</select>
						</td>
					</tr>
					<tr>
						<td>
							Amount
						</td>
						<td class="newDeduction">
							<input type="text" name="deduction_amount" id="deduction_amount" class="form-control font-style2 onlyNumber" placeholder="0.00">
						</td>
					</tr>
					<tr>
						<td>
							Date Start
						</td>
						<td class="newDeduction">
							<input type="text" name="date_start" id="date_start" class="form-control font-style2 datepicker">
						</td>
					</tr>
				</tbody>
			</table>
		</form>
	</div>
</div>