@extends('app-front')

@section('content')
<style type="text/css">
.noborder{
border:none !important;
}
.noborder-top{
border-top:none !important;
}
</style>
<div class="row">
	<div class="col-md-12">
		<hr>
		<div class="col-md-3" style="padding: 0px 15px 0px 15px;">
			<div class="row">
				<div class="col-md-12">
					<label>Filter By</label>
					<div class="form-group">
						<select class="form-control font-style2 select2" id="filter_by_division">
							<option value="">Select division</option>
							@foreach($divisions as $division)
							<option value="{{ $division->RefId }}">{{ $division->Name }}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<label style="padding-top: 5px;">Transaction Period</label>
				</div>
			</div>
			<div class="row">
				@include('payrolls.includes._months-year')
			</div>
			<br>
			<div class="row">
				<div class="col-md-4">
					<a class="btn btn-xs btn-info btnfilter hidden"style="background-color: #164c8a;" style="float: left;line-height: 16px;" ><i class="fa fa-filter"></i>Filter</a>
				</div>
				<div class="col-md-8 text-right">
					<button class="btn btn-xs btn-info" id="process_payroll" style="background-color: #164c8a;"><i class="far fa-save"></i>&nbsp;Process</button>
					<a class="btn btn-xs btn-danger" id="delete_payroll" ><i class="fas fa-minus-circle"></i>&nbsp;Delete</a>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 ">
					<div class="col-md-2">
						<span>Search</span>
					</div>
					<div class="col-md-10 text-right">
						<a class="radiobut-style radio-inline">
							<input type="radio" name="chk_wpayroll" id="wpayroll" value="wpayroll">
							W/Payroll
						</a>
						<a class="radiobut-style radio-inline ">
							<input type="radio" name="chk_wpayroll" id="woutpayroll" value="wopayroll">
							W/ Out Payroll
						</a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<input type="text" name="filter_search" class="form-control _searchname">
				</div>
			</div>
			<div class="row" style="padding-top: 30px;">
				<div class="col-md-12">
					<div class="namelist" style="position: relative;top: -25px;">
						{!! $controller->show() !!}
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-9">
			<div class="col-md-12">
				<div class="col-md-6">
					<div class="progress hidden">
						  <div class="progress-bar" role="progressbar" id="progressBar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
						  <span class="sr-only">0% Complete</span>
					</div>
					<div class="newSummary-name">
						<label id="d-name" style="text-transform: uppercase;"></label>
					</div>
				</div>
				<div class="col-md-6">

				</div>
			</div>
			<br>
			<br>
			<div class="button-wrapper hidden" style="position: relative;bottom:5px;left: 5px;" id="editForm" >
				<a class="btn btn-xs btn-info btn-editbg btn_edit " id="editAttendance" data-btnnew="newAttendance" data-btncancel="cancelAttendance" data-btnedit="editAttendance" data-btnsave="saveAttendance"><i class="fa fa-save"></i> Edit</a>
				<a class="btn btn-xs btn-info btn-savebg btn_save update_payroll hidden" data-form="form" data-btnnew="newAttendance" data-btncancel="cancelAttendance" data-btnedit="editAttendance" data-btnsave="saveAttendance" id="saveAttendance"><i class="fa fa-save"></i> Save</a>
				<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newAttendance" data-btncancel="cancelAttendance" data-form="myform" data-btnedit="editAttendance" data-btnsave="saveAttendance"id="cancelAttendance"> Cancel</a>
			</div>
			<div class="tab-container">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#payrollsummary">Payroll Summary</a></li>
					<li><a href="#attendance">Attendance</a></li>
					<li><a href="#benefitsinfo">Benefits Info</a></li>
					<li><a href="#contribution">Contributions</a></li>
					<li><a href="#loansinfo">Loans Info</a></li>
					<li><a href="#deducinfo">Deduction Info</a></li>
				</ul>
			</div>

			<div class="tab-content myForm">
				<!-- PAYROLL SUMMARY -->
				<div id="payrollsummary" class="tab-pane fade in active">
					<input type="hidden" name="transaction_id" id="transaction_id">
					<input type="hidden" name="salaryinfo_id" id="salaryinfo_id">
					<div class="col-md-12">
						<div class="col-md-9">
							<div class="border-style2">
								<table class="table borderless">
									<tr class="text-center">
										<td></td>
										<td>Actual</td>
										<td>Adjustment</td>
										<td>Total</td>
									</tr>
									<tr>
										<td>Basic Pay</td>
										<td>
											<input type="text" name="actual_basicpay" id="input_basicpayactual" class="form-control font-style2 onlyNumber computebasicpay" readonly>
										</td>
										<td>
											<input type="text" name="adjust_basicpay" id="input_basicpayadjust" class="form-control font-style2 onlyNumber computebasicpay" readonly>
										</td>
										<td>
											<input type="text" name="total_basicpay" id="input_basicpayatotal" class="form-control font-style2 onlyNumber" readonly>
										</td>
									</tr>
									<tr>
										<td>LWOP</td>
										<td>
											<input type="text" name="actual_absences" id="input_absencesactual" class="form-control font-style2 onlyNumber computeabsences" readonly>
										</td>
										<td>
											<input type="text" name="adjust_absences" id="input_absencesadjust" class="form-control font-style2 onlyNumber computeabsences" readonly>
										</td>
										<td>
											<input type="text" name="total_absences" id="input_absencestotal" class="form-control font-style2 onlyNumber" readonly>
										</td>
									</tr>
									<tr>
										<td>Tardines</td>
										<td>
											<input type="text" name="actual_tardines" id="input_tardinesactual" class="form-control font-style2 onlyNumber computetardines" readonly>
										</td>
										<td>
											<input type="text" name="adjust_tardines" id="input_tardinesadjust" class="form-control font-style2 onlyNumber computetardines" readonly>
										</td>
										<td>
											<input type="text" name="total_tardines" id="input_tardinestotal" class="form-control font-style2 onlyNumber" readonly>
										</td>
									</tr>
									<tr>
										<td>Undertime</td>
										<td>
											<input type="text" name="actual_undertime" id="input_actualundertime" class="form-control font-style2 onlyNumber computeundertime" readonly>
										</td>
										<td>
											<input type="text" name="adjust_undertime" id="input_adjustundertime" class="form-control font-style2 onlyNumber computeundertime" readonly>
										</td>
										<td>
											<input type="text" name="total_undertime" id="input_undertimetotal" class="form-control font-style2 onlyNumber" readonly>
										</td>
									</tr>
									<tr>
										<td></td>
										<td></td>
										<td class="text-right">Basic Net Pay</td>
										<td><input type="text" name="basic_net_pay" id="basic_net_pay" class="form-control font-style2 onlyNumber" readonly></td>
									</tr>

								</table>
							</div>

							<div class="border-style2" style="margin-top: -25px;">
								<table class="table borderless">
									<tr class="text-center">
										<td></td>
										<td>Actual</td>
										<td>Adjustment</td>
										<td>Total</td>
									</tr>
									<tr>
										<td>Total Contributions</td>
										<td>
											<input type="text" name="actual_contribution" id="input_actualcontribution" class="form-control font-style2 onlyNumber computecontribution" readonly>
										</td>
										<td>
											<input type="text" name="adjust_contribution" id="input_adjustcontribution" class="form-control font-style2 onlyNumber computecontribution" readonly>
										</td>
										<td>
											<input type="text" name="total_contribution" id="input_totalcontribution" class="form-control font-style2 onlyNumber" readonly>
										</td>
									</tr>
									<tr>
										<td>Total Loans</td>
										<td>
											<input type="text" name="actual_loan" id="input_actualloan" class="form-control font-style2 onlyNumber computeloan" readonly>
										</td>
										<td>
											<input type="text" name="adjust_loan" id="input_adjustloan" class="form-control font-style2 onlyNumber computeloan" readonly>
										</td>
										<td>
											<input type="text" name="total_loan" id="input_totalloans" class="form-control font-style2 onlyNumber" readonly>
										</td>
									</tr>
									<tr>
										<td>Total Other Deductions</td>
										<td>
											<input type="text" name="actual_otherdeduct" id="input_actualotherdeduct" class="form-control font-style2 onlyNumber computeotherdeduction" readonly>
										</td>
										<td>
											<input type="text" name="adjust_otherdeduct" id="input_adjustotherdeduct" class="form-control font-style2 onlyNumber computeotherdeduction" readonly>
										</td>
										<td>
											<input type="text" name="total_otherdeduct" id="input_totalotherdeduct" class="form-control font-style2 onlyNumber" readonly>
										</td>
									</tr>
									<tr>
										<td></td>
										<td></td>
										<td class="text-right">Total Deductions</td>
										<td><input type="text" name="net_deduction" id="net_deduction" class="form-control font-style2 onlyNumber" readonly></td>
									</tr>
								</table>
							</div>
							<br>
						</div>

						<div class="col-md-3">
							<div class="box-grosspay" style="padding-top: 22px;">
								<label style="display: flex;">Gross Pay</label>
								<span id="grosspay">0.00</span>
							</div>

							<div class="box-grosspay" style="padding-top: 22px;">
								<label style="display: flex;">Gross Taxable Pay</label>
								<span id="grosstaxablepay">0.00</span>
							</div>

							<div class="box-grosspay" style="padding-top: 22px;">
								<label style="display: flex;">Net Pay</label>
								<span id="netpay">0.00</span>
							</div>

							<div class="box-grosspay" style="padding-top: 22px;">
								<label>Hold</label><br>
								<input type="checkbox" name="chk_holdpay" id="chk_holdpay" >

							</div>
						</div>

					</div>
				</div>
				<!-- PAYROLL SUMMARY -->
				<!-- ATTENDANCE -->
				<div id="attendance" class="tab-pane fade in">
					<div class="col-md-12">
						<div class="border-style2">
							<input type="hidden" name="attendance_id" id="attendance_id">
							<input type="hidden" name="attendance_id" class="employee_id">
							<table class="table borderless">
								<tr>
									<td><label>Salary e</label></td>
									<td class="text-left"><label id="_salaryrate"></label></td>
									<td></td>
									<td></td>

								</tr>
								<tr>
									<td></td>
									<td class="text-center"><span>Actual</span></td>
									<td class="text-center"><span>Adjustment</span></td>
									<td class="text-center"><span>Total</span></td>
								</tr>
								<tr>
									<td>Work Days</td>
									<td>
										<input type="text" name="actual_workdays" id="input_actualworkdays" class="form-control font-style2 input attendance workdays" maxlength="2" readonly>
									</td>
									<td class="newAttendance">
										<input type="text" name="adjust_workdays" id="input_adjustworkdays" class="form-control font-style2 input attendance  workdays isNumber" maxlength="2">
									</td>
								<!-- 	<td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_adjworkdays"></a>
											<a href="#" class="fa fa-minus" id="diff_adjworkdays"></a>
										</div>
									</td> -->
									<td>
										<input type="text" name="total_workdays" id="input_totalworkdays" class="form-control font-style2 input attendance" placeholder="0.00" readonly>
									</td>
								</tr>
								<tr>
									<td>LWOP</td>
									<td class="newAttendance">
										<input type="text" name="actual_absences" id="input_actualabsence" class="form-control font-style2 input attendance absences isNumber" maxlength="5">
									</td>
									<td class="newAttendance">
										<input type="text" name="adjust_absences" id="input_adjustabsence" class="form-control font-style2 input attendance absences isNumber" maxlength="2">
									</td>
								<!-- 	<td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_adjabsence"></a>
											<a href="#" class="fa fa-minus" id="diff_adjabsence"></a>
										</div>
									</td> -->
									<td>
										<input type="text" name="total_absences" id="input_totalabsence" class="form-control font-style2 input attendance" placeholder="0.00" readonly>
									</td>
								</tr>
								<tr>
									<td>Tardiness</td>
									<td class="newAttendance">
										<input type="text" name="actual_tardines" id="input_actualwtardiness" class="form-control font-style2 attendance input computetardines isNumber">
									</td>
									<td class="newAttendance">
										<input type="text" name="adjust_tardines" id="input_adjusttardiness" class="form-control font-style2 attendance input computetardines isNumber">
									</td>
								<!-- 	<td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_adjtardiness"></a>
											<a href="#" class="fa fa-minus" id="diff_adjtardiness"></a>
										</div>
									</td> -->
									<td>
										<input type="text" name="total_tardines" id="input_totaltardiness" class="form-control font-style2 input attendance " readonly placeholder="0.00">
									</td>
								</tr>
								<tr>
									<td>Undertime</td>
									<td class="newAttendance">
										<input type="text" name="actual_attendance_undertime" id="actual_attendance_undertime" class="form-control attendance font-style2 input  computeundertime isNumber">
									</td>
									<td class="newAttendance">
										<input type="text" name="adjust_attendance_undertime" id="adjust_attendance_undertime" class="form-control attendance font-style2 input computeundertime isNumber">
									</td>
									<!-- <td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_adjundertime"></a>
											<a href="#" class="fa fa-minus" id="diff_adjundertime"></a>
										</div>
									</td> -->
									<td>
										<input type="text" name="total_undertime" id="input_totalundertime" class="form-control font-style2 input attendance " readonly placeholder="0.00">
									</td>
								</tr>
								<tr>
									<td colspan="3" class="text-right">
										<label>Basic Net Pay</label>
									</td>
									<td><label id="totalamount">0.00</label></td>

								</tr>
							</table>
						</div>
						<br>
					</div>
				</div>
				<!-- ATTENDANCE -->
				<!-- BENF /  ALLOW INFO -->
				<div id="benefitsinfo" class="tab-pane fade in">
					<div class="sub-panel" style="margin-top: 45px;">
						{!! $controller->showBenefitinfo() !!}
					</div>
					@include('payrolls.transactions._form-benefit_transaction')
					<br>
				</div>
				<!-- BENF /  ALLOW INFO -->
				<!-- CONTRIBUTIONS -->
				<div id="contribution" class="tab-pane fade in">
					<div class="col-md-12">
						<div class="border-style2">
							<table class="table borderless">
								<tr class="text-center">
									<td colspan="2"><label style="margin-left: 80px;">Employee Share</label></td>
									<td colspan="1"><label style="margin-right: 32px;">Employer Share</label></td>
									<td colspan="3"><label style="margin-right: 24px;">ECC</label></td>
								<!-- 	<td></td>
									<td></td> -->
								</tr>
								<tr>
									<td>GSIS</td>
									<td>
										<input type="text" name="gsis_ee_share" id="gsis_ee_share" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<!-- <td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_contribEmpShareGsis"></a>
											<a href="#" class="fa fa-minus" id="diff_contribEmpShareGsis"></a>
										</div>
									</td> -->
									<td>
										<input type="text" name="gsis_er_share" id="gsis_er_share" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<!-- <td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_contribEmprShareGsis"></a>
											<a href="#" class="fa fa-minus" id="diff_contribEmprShareGsis"></a>
										</div>
									</td> -->
									<td>
										<input type="text" name="ecc" id="ecc" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<!-- <td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_contribEccGsis"></a>
											<a href="#" class="fa fa-minus" id="diff_contribEccGsis"></a>
										</div>
									</td> -->
									<td colspan="2"></td>
								</tr>
								<tr>
									<td>Philhealth</td>
									<td>
										<input type="text" name="philhealth_ee_share" id="philhealth_ee_share" class="form-control font-style2 onlyNumber" readonly>
									</td>
								<!-- 	<td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_contribEmpSharePhhealth"></a>
											<a href="#" class="fa fa-minus" id="diff_contribEmpSharePhhealth"></a>
										</div>
									</td> -->
									<td>
										<input type="text" name="philhealth_er_share" id="philhealth_er_share" class="form-control font-style2 onlyNumber" readonly>
									</td>
								<!-- 	<td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_contribEmprSharePhhealth"></a>
											<a href="#" class="fa fa-minus" id="diff_contribEmprSharePhhealth"></a>
										</div>
									</td> -->
									<td colspan="3"></td>
								</tr>
								<tr>
									<tr>
									<td>Pag-ibig</td>
									<td>
										<input type="text" name="pagibig_ee_share" id="pagibig_ee_share" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<!-- <td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_contribEmpSharePagibig"></a>
											<a href="#" class="fa fa-minus" id="diff_contribEmpSharePagibig"></a>
										</div>
									</td> -->
									<td>
										<input type="text" name="pagibig_er_share" id="pagibig_er_share" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<!-- <td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_contribEmprSharePagibig"></a>
											<a href="#" class="fa fa-minus" id="diff_contribEmprSharePagibig"></a>
										</div>
									</td> -->
									<td colspan="3"></td>
								</tr>
								<tr>
									<td>Withholding Tax</td>
									<td>
										<input type="text" name="witholding_tax" id="witholding_tax" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<!-- <td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_contribEmpShareWtax"></a>
											<a href="#" class="fa fa-minus" id="diff_contribEmpShareWtax"></a>
										</div>
									</td> -->
									<td colspan="4"></td>
								</tr>
								<tr>
									<td>Provident Fund</td>
									<td>
										<input type="text" name="input_contribEmpSharePfund" id="input_contribEmpSharePfund" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<!-- <td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_contribEmpSharePfund"></a>
											<a href="#" class="fa fa-minus" id="diff_contribEmpSharePfund"></a>
										</div>
									</td> -->
									<td>
										<input type="text" name="input_contribEmprSharePfund" id="input_contribEmprSharePfund" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<!-- <td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_contribEmprSharePfund"></a>
											<a href="#" class="fa fa-minus" id="diff_contribEmprSharePfund"></a>
										</div>
									</td> -->
									<td colspan="3"></td>
								</tr>
								<tr>
									<td>Association Dues</td>
									<td>
										<input type="text" name="input_contribEmpShareAdues" id="input_contribEmpShareAdues" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<!-- <td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_contribEmpShareAdues"></a>
											<a href="#" class="fa fa-minus" id="diff_contribEmpShareAdues"></a>
										</div>
									</td> -->
									<td colspan="4"></td>
								</tr>
								<tr>
									<td>Pag-ibig Fund II</td>
									<td>
										<input type="text" name="pagibig2" id="pagibig2" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<!-- <td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_contribEmpSharePfund2"></a>
											<a href="#" class="fa fa-minus" id="diff_contribEmpSharePfund2"></a>
										</div>
									</td> -->
									<td colspan="4"></td>
								</tr>
								<tr>
									<td>GSIS UOLI Premium I</td>
									<td>
										<input type="text" name="input_contribEmpShareGsisUoliPremI" id="input_contribEmpShareGsisUoliPremI" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<!-- <td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_contribEmpShareGsisUoliPremI"></a>
											<a href="#" class="fa fa-minus" id="diff_contribEmpShareGsisUoliPremI"></a>
										</div>
									</td> -->
									<td colspan="4"></td>
								</tr>
								<tr>
									<td>GSIS UOLI Premium II</td>
									<td>
										<input type="text" name="input_contribEmpShareGsisUoliPremII" id="input_contribEmpShareGsisUoliPremII" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<!-- <td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_contribEmpShareGsisUoliPremII"></a>
											<a href="#" class="fa fa-minus" id="diff_contribEmpShareGsisUoliPremII"></a>
										</div>
									</td> -->
									<td colspan="4"></td>
								</tr>
								<tr>
									<td>GSIS UOLI Premium II</td>
									<td>
										<input type="text" name="input_contribEmpShareGsisUoliPremIII" id="input_contribEmpShareGsisUoliPremIII" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<!-- <td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_contribEmpShareGsisUoliPremIII"></a>
											<a href="#" class="fa fa-minus" id="diff_contribEmpShareGsisUoliPremIII"></a>
										</div>
									</td> -->
									<td colspan="4"></td>
								</tr>
								<tr>
									<td>GSIS UOLI Premium IV</td>
									<td>
										<input type="text" name="input_contribEmpShareGsisUoliPremIV" id="input_contribEmpShareGsisUoliPremII" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<!-- <td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_contribEmpShareGsisUoliPremIV"></a>
											<a href="#" class="fa fa-minus" id="diff_contribEmpShareGsisUoliPremIV"></a>
										</div>
									</td> -->
									<td colspan="4"></td>
								</tr>
								<tr>
									<td>GSIS UOLI Premium V</td>
									<td>
										<input type="text" name="input_contribEmpShareGsisUoliPremV" id="input_contribEmpShareGsisUoliPremV" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<!-- <td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_contribEmpShareGsisUoliPremV"></a>
											<a href="#" class="fa fa-minus" id="diff_contribEmpShareGsisUoliPremV"></a>
										</div>
									</td> -->
									<td colspan="4"></td>
								</tr>
							</table>
						</div>
						<br>
					</div>
				</div>
				<!-- CONTRIBUTIONS -->
				<!-- LOANS INFO -->
				<div id="loansinfo" class="tab-pane fade in">
					<div class="sub-panel" style="margin-top: 50px;z-index: 1;">

						{!! $controller->showLoaninfo() !!}

					</div>
				</div>
				<!-- LOANS INFO  -->
				<!-- DEDUCT INFO -->
				<div id="deducinfo" class="tab-pane fade in">
					<div class="sub-panel" style="margin-top: 50px;z-index: 1;">
					{!! $controller->showDeductioninfo() !!}
					</div>
					@include('payrolls.transactions._form-deduction_transaction')
				</div>
				<!-- DEDUCT INFO -->
			</div>
		</div>
	</div>
</div>
@endsection

@section('js-logic1')
<script type="text/javascript">
$(document).ready(function(){

var _grossPay;
var _grossTaxable;
var _netPay;
var _taxContribution;
var _allowances;

var _totalHoursDay;
var _totalWorkingDays;
var _totalPayPerday;
var _totalPayHour;

var _basicNetPay;
var _basicPayTotal;
var	_basicPayActual;
var	_basscPayAdjust;

var _totalContribution;
var _actualContribution;
var	_adjustContribution;

var _totalLoans;
var _actualLoan;
var	_adjustLoan;

var _totalOtherDeductions;
var _totalDeductions;
var _actualDeduction;
var	_adjustDeduction;

var _actualWorkingDays;
var _adjustWorkingDays;
var _actualAbsence;
var _adjustAbsence;
var _totalAbsence ;

var _actualTardines;
var _adjustTardines;
var _totalTardines;

var _actualUndertime;
var _adjustUndertime;
var _totalUndertime;

var _adjustTardinesPerHour;
var	_adjustTardinesPerMinute;
var	_actualUndertimePerHour;
var	_actualUndertimePerMinute;
var _salaryRate;
var _regularDayRate = 1.25;
var _specialHolidayRate = 1.50;
var _regularHolidayRate = 1.50;

var _salaAmount;
var _hpAmount;
var tBenefit = $('#tbl_benefitTransact').DataTable({
	'dom':'<lf<t>pi>',
 	"paging": false,
});
var tDeduct = $('#tbl_deductTransact').DataTable();
var tLoan = $('#tbl_loanTransact').DataTable();

// ************************************************
var _bool = false;
var _Year;
var _Month;
$(document).on('change','#select_year',function(){
	_Year = "";
	_Year = $(this).find(':selected').val();
	$('#year').val(_Year);
	$('.year').val(_Year);
	if(_bool == true){
		$('.btnfilter').trigger('click');
	}
})
$(document).on('change','#select_month',function(){
	_Month = "";
	_Month = $(this).find(':selected').val();
	$('#month').val(_Month);
	$('.month').val(_Month);
	if(_bool == true){
		$('.btnfilter').trigger('click');
	}
})

$('.datepicker').datepicker({
	dateFormat:'yy-mm-dd'
})

$('.select2').select2();

$('#select_year').trigger('change');
$('#select_month').trigger('change');

$('.newAttendance :input').attr('disabled',true);
$('.newAttendance').attr('disabled',true);
$('.newDeduction :input').attr('disabled',true);
$('.newDeduction').attr('disabled',true);
$('.rataAdjust :input').attr('disabled',true);
$('.rataAdjust').attr('disabled',true);

$('#woutpayroll').prop('checked',false);

$('.btn_new').on('click',function(){
	// $('#transaction_id').val('');
	btnnew = $(this).data('btnnew');
	btnsave = $(this).data('btnsave');
	btncancel = $(this).data('btncancel');
	$('.'+btnnew+' :input').attr("disabled",false);
	$('.'+btnnew).attr('disabled',false);
	$('#'+btnnew).addClass('hidden');
	$('#'+btnsave).removeClass('hidden');
	$('#'+btncancel).removeClass('hidden');
});

$('.btn_edit').on('click',function(){
	btnnew = $(this).data('btnnew');
	btnsave = $(this).data('btnsave');
	btncancel = $(this).data('btncancel');
	btnedit = $(this).data('btnedit');
	$('.rataAdjust :input').attr('disabled',false);
	$('.'+btnnew+' :input').attr("disabled",false);
	$('.'+btnnew).attr('disabled',false);
	$('#'+btnnew).addClass('hidden');
	$('#'+btnedit).addClass('hidden');
	$('#'+btnsave).removeClass('hidden');
	$('#'+btncancel).removeClass('hidden');

	editClicked = true;
});

$('.btn_cancel').on('click',function(){
	$('#transaction_id').val('');
	btnnew = $(this).data('btnnew');
	btnsave = $(this).data('btnsave');
	btncancel = $(this).data('btncancel');
	btnedit = $(this).data('btnedit');

	$('.'+btnnew+' :input').attr("disabled",true);
	$('.'+btnnew).attr('disabled',true);
	$('#'+btnnew).removeClass('hidden');
	$('#'+btnedit).removeClass('hidden');
	$('#'+btnsave).addClass('hidden');
	$('#'+btncancel).addClass('hidden');
	// $('#benefitForm').addClass('hidden');
	$('.hpRate').addClass('hidden');
	$('.daysPresent').addClass('hidden');
	$('.rataAdjust').addClass('hidden');
	$('.benefitSetup').removeClass('hidden');
	clear_form_elements('nonplantilla');
	$('.error-msg').remove();

	editClicked = false;

});

$('.btn_cancel2').on('click',function(){
	btnnew = $(this).data('btnnew');
	btnsave = $(this).data('btnsave');
	btncancel = $(this).data('btncancel');
	btnedit = $(this).data('btnedit');

	$('.'+btnnew+' :input').attr("disabled",true);
	$('.'+btnnew).attr('disabled',true);
	$('#'+btnnew).removeClass('hidden');
	$('#'+btnedit).addClass('hidden');
	$('#'+btnsave).addClass('hidden');
	$('#'+btncancel).addClass('hidden');
	$('#benefitForm').addClass('hidden');
	clear_form_elements('formDeduction');
	$('.error-msg').remove();

});



$(document).on('change','#select_period',function(){
	period = $(this).val();
	arr = [];
	switch(period){
		case 'semimonthly':
			arr += '<option value="First Half">First Half</option>';
			arr += '<option value="Second Half">Second Half</option>';
		break;
		case 'monthly':
		break;
		default:
		$('#select_subperiod').html();
	}
	$('#select_subperiod').html(arr);
});


$(document).on('change','#searchby',function(){
	var val = $(this).val();
	$.ajax({
		url:base_url+module_prefix+module+'/getSearchby',
		data:{'q':val},
		type:'GET',
		dataType:'JSON',
		success:function(data){
			arr = [];
			$.each(data,function(k,v){
				arr += '<option value='+v.id+'>'+v.name+'</option>';
			})
			$('#select_searchvalue').html(arr);
		}
	})
});

var editClicked = false;
$('.nav-tabs a').click(function(){
	tab = $(this).text();

	switch(tab){
		case 'Attendance':
				if(editClicked == false){
					swal({
						title: "Click Edit First",
						type: "warning",
						showCancelButton: false,
						confirmButtonClass: "btn-danger",
						confirmButtonText: "Yes",
						closeOnConfirm: false
					});

				}else{
					$(this).tab('show');
				}
		break;
		case 'Benefits Info':
			if(editClicked == false){
				swal({
					title: "Click Edit First",
					type: "warning",
					showCancelButton: false,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Yes",
					closeOnConfirm: false
					});
			}else{
				$(this).tab('show');
			}
		break;
		case 'Deduction Info':
			if(editClicked == false){
				swal({
					title: "Click Edit First",
					type: "warning",
					showCancelButton: false,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Yes",
					closeOnConfirm: false
					});
			}else{
				$(this).tab('show');
			}
		break;
		default:
			$(this).tab('show');
		break;
	}
});

$('.isNumber').keypress(function (event) {
    return isNumber(event, this)
});

$('.onlyNumber').keypress(function (event) {
    return isNumber(event, this)
});

$('.onlyNumber').prop('placeholder','0.00');

$(".onlyNumber").keyup(function(){
	amount  = $(this).val();
	if(amount == 0){
		$(this).val('');
	}else{
		plainAmount = amount.replace(/\,/g,'')
		$(this).val(commaSeparateNumber(plainAmount));
	}
});

$('.attendance').keypress(function (event) {
	return isNumber(event, this)
});

$(document).on('keyup','#input_actualloan',function(){

	$(this).val(commaSeparateNumber(parseFloat(_totalLoans).toFixed(2)))
	$('#input_totalloans').val($(this).val());

	sum = compute_netdeduction();
	$('#net_deduction').val(commaSeparateNumber(sum.toFixed(2)));

});

$(document).on('keyup','#input_actualcontribution',function(){
	$(this).val(commaSeparateNumber(parseFloat(_totalContribution).toFixed(2)));

	$('#input_totalcontribution').val(commaSeparateNumber(parseFloat(_totalContribution).toFixed(2)));

	sum = compute_netdeduction();
	$('#net_deduction').val(commaSeparateNumber(sum.toFixed(2)));

});

$(document).on('keyup','#input_actualotherdeduct',function(){

	total_other_deduction = (_totalOtherDeductions) ? commaSeparateNumber(parseFloat(_totalOtherDeductions).toFixed(2)) : '';
	$('#input_totalotherdeduct').val(total_other_deduction);

	sum = compute_netdeduction();
	$('#net_deduction').val(commaSeparateNumber(sum.toFixed(2)));
})


var _actualWorkDays;
var _adjustWorkDays;
var _totalWorkDays;
$(document).on('keyup','#input_actualworkdays',function(){
	_actualWorkingDays = $(this).val();

	_actualWorkDays = compute_workingdays(_totalPayPerday,_actualWorkingDays);
	_totalWorkDays = _actualWorkDays;

	actual_workdays = (_actualWorkDays) ? commaSeparateNumber(_actualWorkDays.toFixed(2)) : 0;

	$('#totalamount').text(actual_workdays)
	$('#input_totalworkdays').val(actual_workdays);
	$('#input_basicpayactual').val(actual_workdays)
	$('#input_basicpayatotal').val(actual_workdays);
	$('#basic_net_pay').val(actual_workdays);

	_basicNetPay = _actualWorkDays;
	_grossPay = compute_grosspay(_basicNetPay,_allowances,_salaAmount,_hpAmount);

	gross_pay = (_grossPay) ? commaSeparateNumber(parseFloat(_grossPay).toFixed(2)) : 0;
	$('#totalGrossPay').text(actual_workdays)

	_grossTaxable = compute_grosstaxable(_actualWorkDays,_totalContribution);
	grossTaxable = (_grossTaxable) ? commaSeparateNumber(parseFloat(grossTaxable).toFixed(2)) : 0;
	$('#grosstaxablepay').text(grossTaxable)

	_netPay = compute_netpay(_taxContribution,_totalLoans,_totalOtherDeductions,_grossTaxable,_allowances);
	net_pay = (_netPay) ? commaSeparateNumber(parseFloat(_netPay).toFixed(2)) : 0;
	$('#netpay').text(net_pay);
	$('#grosspay').text(gross_pay);

});

$(document).on('keyup','#input_adjustworkdays',function(){
	_actualWorkingDays = $(this).val();

	_adjustWorkDays = compute_workingdays(_totalPayPerday,_actualWorkingDays);

	//SUMMARY TAB
	$('#input_basicpayadjust').val(commaSeparateNumber(parseFloat(_adjustWorkDays).toFixed(2)))

	_totalWorkDays = compute_totalworkdays(_actualWorkDays,_adjustWorkDays);

	if(_totalWorkDays){
		_adjustWorkDays = _totalWorkDays;
	}

	_basicNetPay = compute_total(_totalWorkDays,_totalAbsence,_totalUndertime,_totalTardines);

	if(!_basicNetPay){
		_basicNetPay = _totalWorkDays;
	}

	$('#totalamount').text(commaSeparateNumber(parseFloat(_basicNetPay).toFixed(2)))
	$('#input_totalworkdays').val(commaSeparateNumber(parseFloat(_adjustWorkDays).toFixed(2)));

	//SUMMARY TAB
	$('#input_basicpayatotal').val(commaSeparateNumber(parseFloat(_totalWorkDays).toFixed(2)))
	$('#basic_net_pay').val(commaSeparateNumber(parseFloat(_basicNetPay).toFixed(2)));

	_grossPay = compute_grosspay(_adjustWorkDays,_allowances,_salaAmount,_hpAmount);
	$('#totalGrossPay').text(commaSeparateNumber(parseFloat(_grossPay).toFixed(2)))

	_grossTaxable = compute_grosstaxable(_basicNetPay,_totalContribution);
	grossTaxable = (_grossTaxable) ? _grossTaxable : 0;
	$('#grosstaxablepay').text(commaSeparateNumber(parseFloat(grossTaxable).toFixed(2)))

	_netPay = compute_netpay(_taxContribution,_totalLoans,_totalOtherDeductions,_grossTaxable,_allowances);
	$('#netpay').text(commaSeparateNumber(parseFloat(_netPay).toFixed(2)));

	$('#grosspay').text(commaSeparateNumber(parseFloat(_grossPay).toFixed(2)))

});

$(document).on('keyup','#input_actualabsence',function(){
	_actualWorkingDays = $(this).val();

	_actualAbsence = compute_workingdays(_totalPayPerday,_actualWorkingDays);

	//SUMMARY TAB
	$('#input_absencesactual').val(commaSeparateNumber(parseFloat(_actualAbsence).toFixed(2)))

	_totalAbsence = compute_totalabsences(_actualAbsence, _adjustAbsence);

	if(!_totalAbsence){
		_totalAbsence = _actualAbsence
	}

	_basicNetPay = compute_total(_totalWorkDays,_totalAbsence,_totalUndertime,_totalTardines);

	if(!_basicNetPay){
		_basicNetPay = _totalAbsence;
	}
	$('#totalamount').text(commaSeparateNumber(parseFloat(_basicNetPay).toFixed(2)))

	$('#input_totalabsence').val(commaSeparateNumber(parseFloat(_totalAbsence).toFixed(2)));

	//SUMMARY TAB
	$('#input_absencestotal').val(commaSeparateNumber(parseFloat(_totalAbsence).toFixed(2)))
	$('#basic_net_pay').val(commaSeparateNumber(parseFloat(_basicNetPay).toFixed(2)));

	_grossPay = compute_grosspay(_basicNetPay,_allowances,_salaAmount,_hpAmount);
	$('#totalGrossPay').text(commaSeparateNumber(parseFloat(_grossPay).toFixed(2)))

	_grossTaxable = compute_grosstaxable(_basicNetPay,_totalContribution);
	grossTaxable = (_grossTaxable) ? _grossTaxable : 0;
	$('#grosstaxablepay').text(commaSeparateNumber(parseFloat(grossTaxable).toFixed(2)))

	_netPay = compute_netpay(_taxContribution,_totalLoans,_totalOtherDeductions,_grossTaxable,_allowances);
	$('#netpay').text(commaSeparateNumber(parseFloat(_netPay).toFixed(2)));

	$('#grosspay').text(commaSeparateNumber(parseFloat(_grossPay).toFixed(2)))

});

$(document).on('keyup','#input_adjustabsence',function(){
	_actualWorkingDays = $(this).val();

	_adjustAbsence = compute_workingdays(_totalPayPerday,_actualWorkingDays);

	//SUMMARY TAB
	$('#input_absencesadjust').val(commaSeparateNumber(parseFloat(_adjustAbsence).toFixed(2)))

	_totalAbsence = compute_totalabsences(_actualAbsence, _adjustAbsence);

	if(!_totalAbsence){
		_totalAbsence = _actualAbsence
	}

	_basicNetPay = compute_total(_totalWorkDays,_totalAbsence,_totalUndertime,_totalTardines);

	if(!_basicNetPay){
		_basicNetPay = _totalAbsence;
	}
	$('#totalamount').text(commaSeparateNumber(parseFloat(_basicNetPay).toFixed(2)))

	$('#input_totalabsence').val(commaSeparateNumber(parseFloat(_totalAbsence).toFixed(2)));

	//SUMMARY TAB
	$('#input_absencestotal').val(commaSeparateNumber(parseFloat(_totalAbsence).toFixed(2)))
	$('#basic_net_pay').val(commaSeparateNumber(parseFloat(_basicNetPay).toFixed(2)));

	_grossPay = compute_grosspay(_basicNetPay,_allowances,_salaAmount,_hpAmount);
	$('#totalGrossPay').text(commaSeparateNumber(parseFloat(_grossPay).toFixed(2)))

	_grossTaxable = compute_grosstaxable(_basicNetPay,_totalContribution);
	grossTaxable = (_grossTaxable) ? _grossTaxable : 0;
	$('#grosstaxablepay').text(commaSeparateNumber(parseFloat(grossTaxable).toFixed(2)))

	_netPay = compute_netpay(_taxContribution,_totalLoans,_totalOtherDeductions,_grossTaxable,_allowances);
	$('#netpay').text(commaSeparateNumber(parseFloat(_netPay).toFixed(2)));

	$('#grosspay').text(commaSeparateNumber(parseFloat(_grossPay).toFixed(2)))

});

$(document).on('keyup','#input_actualwtardiness',function(){
	var payPerMinute = 0;
	_ratePerMinute 	 = $(this).val();

	_actualTardines  = rate_perminute(_totalPayHour, _ratePerMinute);

	//SUMMARY TAB
	$('#input_tardinesactual').val(commaSeparateNumber(parseFloat(_actualTardines).toFixed(2)));

	_totalTardines = compute_totaltardines(_actualTardines,_adjustTardines);

	if(!_totalTardines){
		_totalTardines = _actualTardines;
	}

	_basicNetPay = compute_total(_totalWorkDays,_totalAbsence,_totalUndertime,_totalTardines);

	if(!_basicNetPay){
		_basicNetPay = _totalTardines;
	}
	$('#totalamount').text(commaSeparateNumber(parseFloat(_basicNetPay).toFixed(2)))

	$('#input_totaltardiness').val(commaSeparateNumber(parseFloat(_totalTardines).toFixed(2)));

	//SUMMARY TAB
	$('#input_tardinestotal').val(commaSeparateNumber(parseFloat(_totalTardines).toFixed(2)))
	$('#basic_net_pay').val(commaSeparateNumber(parseFloat(_basicNetPay).toFixed(2)));

	_grossPay = compute_grosspay(_basicNetPay,_allowances,_salaAmount,_hpAmount);
	$('#totalGrossPay').text(commaSeparateNumber(parseFloat(_grossPay).toFixed(2)))

	_grossTaxable = compute_grosstaxable(_basicNetPay,_totalContribution);
	grossTaxable = (_grossTaxable) ? _grossTaxable : 0;
	$('#grosstaxablepay').text(commaSeparateNumber(parseFloat(grossTaxable).toFixed(2)))

	_netPay = compute_netpay(_taxContribution,_totalLoans,_totalOtherDeductions,_grossTaxable,);
	$('#netpay').text(commaSeparateNumber(parseFloat(_netPay).toFixed(2)));

	$('#grosspay').text(commaSeparateNumber(parseFloat(_grossPay).toFixed(2)))

});

$(document).on('keyup','#input_adjusttardiness',function(){

	_ratePerMinute 	 = $(this).val();

	_adjustTardines = rate_perminute(_totalPayHour, _ratePerMinute);

	//SUMMARY TAB
	$('#input_tardinesadjust').val(commaSeparateNumber(parseFloat(_adjustTardines).toFixed(2)));

	_totalTardines = compute_totaltardines(_actualTardines,_adjustTardines);

	if(!_totalTardines){
		_totalTardines = _adjustTardines;
	}

	_basicNetPay = compute_total(_totalWorkDays,_totalAbsence,_totalUndertime,_totalTardines);

	if(!_basicNetPay){
		_basicNetPay = _totalTardines;
	}
	$('#totalamount').text(commaSeparateNumber(parseFloat(_basicNetPay).toFixed(2)))

	$('#input_totaltardiness').val(commaSeparateNumber(parseFloat(_totalTardines).toFixed(2)));

	//SUMMARY TAB
	$('#input_tardinestotal').val(commaSeparateNumber(parseFloat(_totalTardines).toFixed(2)))
	$('#basic_net_pay').val(commaSeparateNumber(parseFloat(_basicNetPay).toFixed(2)));

	_grossPay = compute_grosspay(_basicNetPay,_allowances,_salaAmount,_hpAmount);
	grossTaxable = (_grossTaxable) ? _grossTaxable : 0;
	$('#grosstaxablepay').text(commaSeparateNumber(parseFloat(grossTaxable).toFixed(2)))

	_grossTaxable = compute_grosstaxable(_basicNetPay,_totalContribution);
	$('#grosstaxablepay').text(commaSeparateNumber(parseFloat(_grossTaxable).toFixed(2)));

	_netPay = compute_netpay(_taxContribution,_totalLoans,_totalOtherDeductions,_grossTaxable);
	$('#netpay').text(commaSeparateNumber(parseFloat(_netPay).toFixed(2)));

	$('#grosspay').text(commaSeparateNumber(parseFloat(_grossPay).toFixed(2)))

});


$(document).on('keyup','#actual_attendance_undertime',function(){

	_ratePerMinute 	 = $(this).val();

	_actualUndertime = rate_perminute(_totalPayHour, _ratePerMinute);

	//SUMMARY TAB
	$('#input_actualundertime').val(commaSeparateNumber(parseFloat(_actualUndertime).toFixed(2)));

	_totalUndertime = compute_totalundertime(_actualUndertime,_adjustUndertime);

	if(!_totalUndertime){
		_totalUndertime = _actualUndertime;
	}

	_basicNetPay = compute_total(_totalWorkDays,_totalAbsence,_totalUndertime,_totalTardines);

	if(!_basicNetPay){
		_basicNetPay = _totalUndertime;
	}
	$('#totalamount').text(commaSeparateNumber(parseFloat(_basicNetPay).toFixed(2)))

	$('#input_totalundertime').val(commaSeparateNumber(parseFloat(_totalUndertime).toFixed(2)));

	//SUMMARY TAB
	$('#input_undertimetotal').val(commaSeparateNumber(parseFloat(_totalUndertime).toFixed(2)))
	$('#basic_net_pay').val(commaSeparateNumber(parseFloat(_basicNetPay).toFixed(2)));

	_grossPay = compute_grosspay(_basicNetPay,_allowances,_salaAmount,_hpAmount);
	$('#totalGrossPay').text(commaSeparateNumber(parseFloat(_grossPay).toFixed(2)))

	_grossTaxable = compute_grosstaxable(_basicNetPay,_totalContribution);
	grossTaxable = (_grossTaxable) ? _grossTaxable : 0;
	$('#grosstaxablepay').text(commaSeparateNumber(parseFloat(grossTaxable).toFixed(2)))

	_netPay = compute_netpay(_taxContribution,_totalLoans,_totalOtherDeductions,_grossTaxable,_allowances);
	$('#netpay').text(commaSeparateNumber(parseFloat(_netPay).toFixed(2)));

	$('#grosspay').text(commaSeparateNumber(parseFloat(_grossPay).toFixed(2)))

});

$(document).on('keyup','#adjust_attendance_undertime',function(){

	_ratePerMinute 	 = $(this).val();

	_adjustUndertime = rate_perminute(_totalPayHour, _ratePerMinute);

	//SUMMARY TAB
	$('#input_adjustundertime').val(commaSeparateNumber(parseFloat(_adjustUndertime).toFixed(2)));
	_totalUndertime = compute_totalundertime(_actualUndertime,_adjustUndertime);

	if(!_totalUndertime){
		_totalUndertime = _adjustUndertime;
	}

	_basicNetPay = compute_total(_totalWorkDays,_totalAbsence,_totalUndertime,_totalTardines);

	if(!_basicNetPay){
		_basicNetPay = _totalUndertime;
	}
	$('#totalamount').text(commaSeparateNumber(parseFloat(_basicNetPay).toFixed(2)))
	$('#input_totalundertime').val(commaSeparateNumber(parseFloat(_totalUndertime).toFixed(2)));

	//SUMMARY TAB
	$('#input_undertimetotal').val(commaSeparateNumber(parseFloat(_totalUndertime).toFixed(2)))
	$('#basic_net_pay').val(commaSeparateNumber(parseFloat(total).toFixed(2)));

	_grossPay = compute_grosspay(total,_allowances,_salaAmount,_hpAmount);
	$('#totalGrossPay').text(commaSeparateNumber(parseFloat(_grossPay).toFixed(2)))

	_grossTaxable = compute_grosstaxable(total,_totalContribution);
	grossTaxable = (_grossTaxable) ? _grossTaxable : 0;
	$('#grosstaxablepay').text(commaSeparateNumber(parseFloat(grossTaxable).toFixed(2)))

	_netPay = compute_netpay(_taxContribution,_totalLoans,_totalOtherDeductions,_grossTaxable,_allowances);
	$('#netpay').text(commaSeparateNumber(parseFloat(_netPay).toFixed(2)));

	$('#grosspay').text(commaSeparateNumber(parseFloat(_grossPay).toFixed(2)))

});



var _listId = [];
$(document).on('click','#check_all',function(){
	if(!_Year && !_Month){
		swal({
			title: "Select year and month first",
			type: "warning",
			showCancelButton: false,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Yes",
			closeOnConfirm: false
		});
		$('#check_all').prop('checked',false);
	}else{
		if ($(this).is(':checked')) {
	        $('.emp_select').prop('checked', 'checked');
	        $('.emp_select:checked').each(function(){
	        	_listId.push($(this).val())
	        });
	    } else {
	        $('.emp_select').prop('checked', false)
	        _listId = [];
	    }
	}
});

$(document).on('click','.emp_select',function(){
	empid = $(this).val();
	index = $(this).data('key');
	if(!_Year && !_Month){
		swal({
			title: "Select year and month first",
			type: "warning",
			showCancelButton: false,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Yes",
			closeOnConfirm: false
		});
		$('.emp_select').prop('checked',false);
	}else{
		if($(this).is(':checked')){
			_listId[index] =  empid;

		}else{
			delete _listId[index];
		}
	}
});


$('#process_payroll').on('click',function(){
	if(_listId.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Process Payroll?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.processPayroll();
			}else{
				return false;
			}
		});
	}
});

$.processPayroll = function(){
	$.ajax({
        type:'POST',
        data:{
        	'empid':_listId,
        	'_token':"{{ csrf_token() }}"
        	,'year':_Year,
        	'month':_Month,
        	'transaction_id':$('#transaction_id').val(),
        	'attendance_id':$('#attendance_id').val(),
        	'salaryinfo_id':$('#salaryinfo_id').val(),
        	'employee_id':$('#employee_id').val(),
        	'summary':{
        		'actual_basicpay':$('#input_basicpayactual').val(),
        		'adjust_basicpay':$('#input_basicpayadjust').val(),
        		'total_basicpay':$('#input_basicpayatotal').val(),
        		'actual_absences':$('#input_absencesactual').val(),
        		'adjust_absences':$('#input_absencesadjust').val(),
        		'total_absences':$('#input_absencestotal').val(),
        		'actual_tardines':$('#input_tardinesactual').val(),
        		'adjust_tardines':$('#input_tardinesadjust').val(),
        		'total_tardines':$('#input_tardinestotal').val(),
        		'actual_undertime':$('#input_actualundertime').val(),
        		'adjust_undertime':$('#input_adjustundertime').val(),
        		'total_undertime':$('#input_undertimetotal').val(),
        		'basic_net_pay':$('#basic_net_pay').val(),
        		'actual_contribution':$('#input_actualcontribution').val(),
        		'adjust_contribution':$('#input_adjustcontribution').val(),
        		'total_contribution':$('#input_totalcontribution').val(),
        		'actual_loan':$('#input_actualloan').val(),
        		'adjust_loan':$('#input_adjustloan').val(),
        		'total_loan':$('#input_totalloans').val(),
        		'actual_otherdeduct':$('#input_actualotherdeduct').val(),
        		'adjust_otherdeduct':$('#input_adjustotherdeduct').val(),
        		'total_otherdeduct':$('#input_totalotherdeduct').val(),
        		'net_deduction':$('#net_deduction').val(),
        		'net_pay':_netPay,
        		'gross_pay':_grossPay,
        		'gross_taxable_pay':_grossTaxable,
        		'hold':$('#chk_holdpay').val()
        	},
        	'attendance':{
        		'actual_workdays':$('#input_actualworkdays').val(),
        		'adjust_workdays':$('#input_adjustworkdays').val(),
        		'total_workdays':$('#input_totalworkdays').val(),
        		'actual_absences':$('#input_actualabsence').val(),
        		'adjust_absences':$('#input_adjustabsence').val(),
        		'total_absences':$('#input_totalabsence').val(),
        		'actual_tardines':$('#input_actualwtardiness').val(),
        		'adjust_tardines':$('#input_adjusttardiness').val(),
        		'total_tardines':$('#input_totaltardiness').val(),
        		'actual_undertime':$('#actual_attendance_undertime').val(),
        		'adjust_undertime':$('#adjust_attendance_undertime').val(),
        		'total_undertime':$('#input_totalundertime').val(),
        		'year':_Year,
        		'month':_Month
        	}
        },
        url: base_url+module_prefix+module+'/processPayroll',
        beforeSend:function(){
        	$('#process_payroll').html('<i class="fa fa-spinner fa-spin"></i> Processing').prop('disabled',true);
        },
        success:function(data) {
        	par = JSON.parse(data);
        	if(par.status){
           		swal({
					  title: par.response,
					  type: "success",
					  showCancelButton: false,
					  confirmButtonClass: "btn-success",
					  confirmButtonText: "OK",
					  closeOnConfirm: false
				}).then(function(){
					$('#process_payroll').html('<i class="fa fa-save"></i> Process').prop('disabled',false);
					$('.btn_save').addClass('hidden');
					$('.btn_cancel').addClass('hidden');
					$('.btn_edit').removeClass('hidden');
					$('#transaction_id').val('');
					$('.newAttendance :input').attr('disabled',true);
					$('.newAttendance').attr('disabled',true);
					clear_form_elements('attendance');
					_checkpayroll = 'wopayroll';
					$('.btnfilter').trigger('click');
					editClicked = false;
					_listId = [];
				});
        	}else{
        		swal({
            	   title: par.response,
				   type: "warning",
				   showCancelButton: false,
				   confirmButtonClass: "btn-warning",
				   confirmButtonText: "OK",
				   closeOnConfirm: false
        		});
        		$('#process_payroll').html('<i class="fa fa-save"></i> Process').prop('disabled',false);
        	}
        },
        complete:function(){
        }
	})
}

var days_present;
var sa_per_day;
var sa_per_hour;
var benefitCode;
var saAmount = 0;
var la_amount = 500;
var sa_amount = 150;
$(document).on('keyup','#days_present_sala',function(){

	days_present = $(this).val();

	sa_per_day 	= 0;
	sa_per_hour = 0;
	saAmount 	= 0;
	switch(benefitCode){
		case 'SA':
			saAmount = parseFloat(sa_amount) * days_present;
			sa_per_day = parseFloat(saAmount) / days_present;
			sa_per_hour = parseFloat(saAmount) / 8 / 60;
			break;
		case 'LA':
			saAmount = (parseFloat(la_amount) / days_present) * days_present;
			sa_per_day = parseFloat(saAmount) / days_present;
			sa_per_hour = parseFloat(saAmount) / 8 / 60;
			break;
	}

	_grossPay = compute_grosspay(_basicNetPay,_allowances,saAmount,_hpAmount);
	gross_pay = (_grossPay) ? commaSeparateNumber(parseFloat(_grossPay).toFixed(2)) : 0;
	$('#grosspay').text(gross_pay);
	$('#benefits_amount_sala').val(saAmount.toFixed(2));


});
var days_absent = 0;
$(document).on('keyup','#sala_absent',function(){
	days_absent = $(this).val();
	amount 		= 0;
	switch(benefitCode){
		case 'SA':
			amount = (parseFloat(sa_per_day) * days_absent);
			break;
		case 'LA':
			amount = (parseFloat(sa_per_day) * days_absent);
			break;
		default:
			amount = 0;
			break;
	}

	amount = (parseFloat(sa_per_day) * days_absent);


	grossPay = compute_grosspay(_basicNetPay,_allowances,amount,_hpAmount);
	gross_pay = (_grossPay) ? commaSeparateNumber(parseFloat(_grossPay).toFixed(2)) : 0;

	$('#grosspay').text(gross_pay);
	$('#sala_absent_amount').val(amount.toFixed(2));

});
$(document).on('keyup','#sala_undertime',function(){
	sala_undertime = $(this).val();

	convertToHour = parseInt(sala_undertime) / 60;
	sala = 8 - parseInt(convertToHour);
	sala = Math.abs(parseInt(sala) * 60);
	amount = 0
	if(sala >= 360 && sala < 480){
		amount = 25;
	}else if(sala >= 300 && sala < 360){
		amount = 50;
	}else if(sala >= 240 && sala < 300){
		amount = 75;
	}else if(sala < 240){
		amount = 0;
	}


	grossPay = compute_grosspay(_basicNetPay,_allowances,amount,_hpAmount);
	gross_pay = (_grossPay) ? commaSeparateNumber(parseFloat(_grossPay).toFixed(2)) : 0;
	$('#grosspay').text(gross_pay);
	$('#sala_undertime_amount').val(amount.toFixed(2));

});


var benefit_rate;
$(document).on('keyup','#days_present_hp',function(){
	days_present = $(this).val();
	rate = (benefit_rate/_totalWorkingDays);
	basic_pay = _salaryRate;
	amount = (basic_pay*rate)*days_present;

	_grossPay = compute_grosspay(_basicNetPay,_allowances,_salaAmount,amount);

	gross_pay = (_grossPay) ? commaSeparateNumber(parseFloat(_grossPay).toFixed(2)) : 0;
	$('#grosspay').text(gross_pay);
	$('#benefits_amount_hp').val(amount.toFixed(2));

});
var taAmount = 0;
$(document).on('keyup','#used_count',function(){
	used_count = $(this).val();
	taDailyRate = parseFloat(taAmount) / 22;

	amount = parseFloat(taDailyRate) * used_count;

	_grossPay = compute_grosspay(_basicNetPay,_allowances,_salaAmount,amount);

	gross_pay = (_grossPay) ? commaSeparateNumber(parseFloat(_grossPay).toFixed(2)) : 0;
	$('#grosspay').text(gross_pay);
	$('#used_amount').val(amount.toFixed(2));

});



$(document).on('click','#namelist tr',function(){
	_id = $(this).data('empid');
	$('#d-name').text($(this).data('employee'));
	$.ajax({
		url:base_url+module_prefix+module+'/getEmployeesinfo',
		data:{
			'id':_id,
			'year':_Year,
			'month':_Month
		},
		type:'get',
		dataType:'JSON',
		success:function(data){
			clear_form_elements('myForm');
			$('#lbl_empname').text('');
			$('#grosspay').text(0.00);
			$('#netpay').text(0.00);
			$('#grosstaxablepay').text(0.00);
			$('#transaction_id').val('');
			$('#attendance_id').val('');
    		$('#employee_id').val('');

			if(data.salaryinfo !== null){
				$('#salaryinfo_id').val(data.salaryinfo.id);
			}
			if(data.employeeinfo !== null){

				_totalHoursDay 	  = 8;
				_totalWorkingDays = 22;
			}


			// IF THE TRANSACTION IS NOT EMPTY
			if(data.transaction !== null){
				fullname = data.transaction.employees.lastname+' '+data.transaction.employees.firstname+' '+ data.transaction.employees.middlename;
				allowance = 0;
				$.each(data.benefitinfo,function(k,v){
					amount = (v.amount) ? v.amount : 0;
					allowance += parseFloat(amount);
				});
				totalLoans = 0;
				$.each(data.loaninfo,function(k,v){
					amount = (v.amount) ? v.amount : 0;
					totalLoans += parseFloat(amount);
				});
				totalDeductions = 0;
				$.each(data.deductioninfo,function(k,v){
					amount = (v.amount) ? v.amount : 0;
					totalDeductions += parseFloat(amount);
				});

				_allowances 	= allowance;
				_salaryRate 	= data.transaction.actual_basicpay_amount;
    			_totalPayPerday = (parseFloat(_salaryRate))/_totalWorkingDays;
    			_totalPayHour 	= (parseFloat(_totalPayPerday)) / _totalHoursDay;
				_totalContribution 	  = (data.transaction.total_contribution) ? data.transaction.total_contribution : 0;
				_totalLoans 		  = totalLoans;
				_totalOtherDeductions = totalDeductions;
				_taxContribution  = (data.transaction.tax_amount) ? data.transaction.tax_amount : 0;

				$('#lbl_empname').text(fullname);
				$('#transaction_id').val(data.transaction.id);
				$('#employee_id').val(data.transaction.employee_id);

				$('#_salaryrate').text(commaSeparateNumber(parseFloat(_salaryRate).toFixed(2)));

				$('#input_actualworkdays').val(_totalWorkingDays).trigger('keyup');
    			$('#input_actualotherdeduct').val(commaSeparateNumber(parseFloat(_totalOtherDeductions).toFixed(2)) ).trigger('keyup');

    			if(_totalContribution){
	    			$('#input_actualcontribution').val(commaSeparateNumber(parseFloat(_totalContribution).toFixed(2))).trigger('keyup');
    			}else{
    				$('#input_actualcontribution').val(0).trigger('keyup');
    			}
	    		if(_totalLoans){
	    			$('#input_actualloan').val(commaSeparateNumber(parseFloat(_totalLoans).toFixed(2))).trigger('keyup');
	    		}else{
	    			$('#input_actualloan').val(0).trigger('keyup');
	    		}

	    		gsis_ee_share = (data.transaction.gsis_ee_share) ? data.transaction.gsis_ee_share : 0;
				$('#gsis_ee_share').val(commaSeparateNumber(parseFloat(gsis_ee_share).toFixed(2)));
				er_gsis_share = (data.transaction.gsis_er_share) ? data.transaction.gsis_er_share : 0;
				$('#gsis_er_share').val(commaSeparateNumber(parseFloat(er_gsis_share).toFixed(2)));

				philhealth_ee_share = (data.transaction.phic_share) ? data.transaction.phic_share : 0;
				$('#philhealth_ee_share').val(commaSeparateNumber(parseFloat(philhealth_ee_share).toFixed(2)));

				philhealth_er_share = (data.employeeinfo.er_philhealth_share) ? data.employeeinfo.er_philhealth_share : 0;
				$('#philhealth_er_share').val(commaSeparateNumber(parseFloat(philhealth_er_share).toFixed(2)));
				if(data.transaction.mp1_amount !== null){
					er_pagibig_share = (data.transaction.pagibig_share) ? data.transaction.pagibig_share : 0;
					er_pagibig_personal = (data.transaction.mp1_amount) ? data.transaction.mp1_amount : 0;
					er_pagibig = (parseFloat(er_pagibig_share) + parseFloat(er_pagibig_personal));
				}else{
					er_pagibig = (data.transaction.pagibig_share) ? data.transaction.pagibig_share : 0;
				}

				$('#pagibig_ee_share').val(commaSeparateNumber(parseFloat(er_pagibig).toFixed(2)));
				er_pagibig_share = (data.transaction.pagibig_share) ? data.transaction.pagibig_share : 0;
				$('#pagibig_er_share').val(commaSeparateNumber(parseFloat(er_pagibig_share).toFixed(2)));
				pagibig2 = (data.transaction.mp2_amount) ? data.transaction.mp2_amount : 0;
				$('#pagibig2').val(commaSeparateNumber(parseFloat(pagibig2).toFixed(2)));

				ecc_amount = (data.transaction.ecc_amount) ? data.transaction.ecc_amount : 0;
				$('#ecc_amount').val(commaSeparateNumber(parseFloat(ecc_amount).toFixed(2)));

	    		$('#witholding_tax').val(commaSeparateNumber(parseFloat(_taxContribution).toFixed(2)));
	    		$('#input_adjustworkdays').val(data.transaction.adjust_workdays).trigger('keyup');
				$('#input_actualabsence').val(data.transaction.actual_absences).trigger('keyup');
				$('#input_adjustabsence').val(data.transaction.adjust_absences).trigger('keyup');
				$('#input_actualwtardiness').val(data.transaction.actual_tardiness).trigger('keyup');
				$('#input_adjusttardiness').val(data.transaction.adjust_tardiness).trigger('keyup');
				$('#actual_attendance_undertime').val(data.transaction.actual_undertime).trigger('keyup');
				$('#adjust_attendance_undertime').val(data.transaction.adjust_undertime).trigger('keyup');
			}else{
				_salaryRate = 0;
				_grossPay = 0;
				_grossTaxable = 0;
				_netPay = 0;
			}

			// GENERATE TR FOR DEDUCTION INFO TAB
			DeductionInfo(data.deductioninfo)

			// GENERATE TR FOR LOAN INFO TAB
			LoanInfo(data.loaninfo);

			// GENERA BENEFITINFO DATATABLE
			BenefitInfo(data.benefitinfo);

		}
	})
});

_checkpayroll = ""
$('input[type=radio][name=chk_wpayroll]').change(function() {
 	if(!_Year){
 		swal({
			  title: 'Select year and month first',
			  type: "warning",
			  showCancelButton: false,
			  confirmButtonClass: "btn-warning",
			  confirmButtonText: "OK",
			  closeOnConfirm: false
		})
		$(this).prop('checked',false);
 	}else{
		if (this.value == 'wpayroll') {
			$('#process_payroll').prop('disabled',true);
			$('#editForm').removeClass('hidden');
			_bool = true;
        	_checkpayroll = 'wpayroll';
        	_listId = [];
        }
        else if (this.value == 'wopayroll') {
        	$('#process_payroll').prop('disabled',false);
        	$('#editForm').addClass('hidden');
            _checkpayroll = 'wopayroll';
            _listId = [];
        }
        $('.btnfilter').trigger('click');
 	}
});

$(document).on('change', '#filter_by_division', function() {
	$('.btnfilter').trigger('click');
});


var timer;
$(document).on('click','.btnfilter',function(){
	// $('input.search').addClass('searchSpinner');
	tools  	  = $('#tools-form').serialize()
	year 	  = $('#select_year :selected').val();
	month 	  = $('#select_month :selected').val();
	category  = $('#select_searchvalue :selected').val();
	empstatus = $('#emp_status :selected').val();
	searchby  = $('#searchby :selected').val();
	subperiod = $('#select_subperiod :selected').val();
	period	  = $('#select_period :selected').val();
	division  = $('#filter_by_division :selected').val();

	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
			   type: "GET",
			   url: base_url+module_prefix+module+'/show',
			   data: {
			   	'category':category,
			   	'empstatus':empstatus,
			   	'searchby':searchby,
			   	'year':year,
			   	'month':month,
			   	'subperiod':subperiod,
			   	'period':period,
			   	'checkpayroll':_checkpayroll,
			   	'division':division,
			   },
			   beforeSend:function(){
			   		$('#loading').removeClass('hidden');
			   },
			   complete:function(){
			   		$('#loading').addClass('hidden');
			   },
			   success: function(res){
			   	// console.log(res);
			      $(".namelist").html(res);
			   }
			});
		},500);
});


$(document).on('keyup','._searchname',function(){

	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
			   type: "GET",
			   url: base_url+module_prefix+module+'/show',
			   data: {
			   	"q":$('._searchname').val(),
			   	'check_payroll':_checkpayroll,
			   	'_year':_Year,
			   	'_month':_Month,
			   	'division':$('#filter_by_division :selected').val(),
			},
			   success: function(res){
			      $(".namelist").html(res);
			   }
			});
		},500);
});


$('.update_payroll').on('click',function(){
	if(_listId.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Update Payroll?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.updatePayroll();
			}else{
				return false;
			}
		});
	}
});

$.updatePayroll = function(){
	$.ajax({
        type:'POST',
        data:{
        	'empid':_listId,
        	'_token':"{{ csrf_token() }}",
        	'year':_Year,
        	'month':_Month,
        	'pera_id':peraId,
        	'transaction_id':$('#transaction_id').val(),
        	'attendance_id':$('#attendance_id').val(),
        	'salaryinfo_id':$('#salaryinfo_id').val(),
        	'employee_id':$('.employee_id').val(),
        	'gsis_ee_share':$('#gsis_ee_share').val(),
        	'gsis_er_share':$('#gsis_er_share').val(),
        	'summary':{
        		'actual_basicpay':$('#input_basicpayactual').val(),
        		'adjust_basicpay':$('#input_basicpayadjust').val(),
        		'total_basicpay':$('#input_basicpayatotal').val(),
        		'actual_absences':$('#input_absencesactual').val(),
        		'adjust_absences':$('#input_absencesadjust').val(),
        		'total_absences':$('#input_absencestotal').val(),
        		'actual_tardines':$('#input_tardinesactual').val(),
        		'adjust_tardines':$('#input_tardinesadjust').val(),
        		'total_tardines':$('#input_tardinestotal').val(),
        		'actual_undertime':$('#input_actualundertime').val(),
        		'adjust_undertime':$('#input_adjustundertime').val(),
        		'total_undertime':$('#input_undertimetotal').val(),
        		'basic_net_pay':$('#basic_net_pay').val(),
        		'actual_contribution':$('#input_actualcontribution').val(),
        		'adjust_contribution':$('#input_adjustcontribution').val(),
        		'total_contribution':$('#input_totalcontribution').val(),
        		'actual_loan':$('#input_actualloan').val(),
        		'adjust_loan':$('#input_adjustloan').val(),
        		'total_loan':$('#input_totalloans').val(),
        		'actual_otherdeduct':$('#input_actualotherdeduct').val(),
        		'adjust_otherdeduct':$('#input_adjustotherdeduct').val(),
        		'total_otherdeduct':$('#input_totalotherdeduct').val(),
        		'net_deduction':$('#net_deduction').val(),
        		'net_pay':_netPay,
        		'gross_pay':_grossPay,
        		'gross_taxable_pay':_grossTaxable,
        		'hold':$('#chk_holdpay').val()
        	},
        	'attendance':{
        		'actual_workdays':$('#input_actualworkdays').val(),
        		'adjust_workdays':$('#input_adjustworkdays').val(),
        		'total_workdays':$('#input_totalworkdays').val(),
        		'actual_absences':$('#input_actualabsence').val(),
        		'adjust_absences':$('#input_adjustabsence').val(),
        		'total_absences':$('#input_totalabsence').val(),
        		'actual_tardines':$('#input_actualwtardiness').val(),
        		'adjust_tardines':$('#input_adjusttardiness').val(),
        		'total_tardines':$('#input_totaltardiness').val(),
        		'actual_undertime':$('#actual_attendance_undertime').val(),
        		'adjust_undertime':$('#adjust_attendance_undertime').val(),
        		'total_undertime':$('#input_totalundertime').val(),
        		'year':_Year,
        		'month':_Month
        	}
        },
        url: base_url+module_prefix+module+'/updatePayroll',
        success:function(data) {
        	par = JSON.parse(data);
        	if(par.status){
           		swal({
					  title: par.response,
					  type: "success",
					  showCancelButton: false,
					  confirmButtonClass: "btn-success",
					  confirmButtonText: "OK",
					  closeOnConfirm: false
				}).then(function(){
					$('#transaction_id').val('');
					_listId = [];
					$('.btn_save').addClass('hidden');
					$('.btn_cancel').addClass('hidden');
					$('#editAttendance').removeClass('hidden');
					$('.newAttendance :input').attr('disabled',true);
					$('.newAttendance').attr('disabled',true);
					// clear_form_elements('attendance');
					// _checkpayroll = 'wopayroll';
					// $('.btnfilter').trigger('click');
				});
        	}else{
        		swal({
            	   title: par.response,
				   type: "warning",
				   showCancelButton: false,
				   confirmButtonClass: "btn-warning",
				   confirmButtonText: "OK",
				   closeOnConfirm: false
        		});
        	}
        },
        complete:function(){
        }
	})
}


function DeductionInfo(deductioninfo){

	tDeduct.clear().draw();
	deductionAmount = 0;
	$.each(deductioninfo,function(k,v){

		start_date = (v.start_date !== null) ? v.deductioninfo.start_date : '';
		end_date = (v.end_date !== null) ? v.deductioninfo.end_date : '';
		date_terminated = (v.deductioninfo.date_terminated !== null) ? v.deductioninfo.date_terminated : '';
		deduction_name = (v.deductions !== null) ? v.deductions.name : '';
		amount = (v.amount !== 0) ? commaSeparateNumber(parseFloat(v.amount).toFixed(2)) : '';

		tDeduct.row.add( [
        	deduction_name,
        	amount,
			start_date,
			end_date,
			'<a class="btn btn-xs btn-danger delete_item" data-function_name="deleteDeduction" data-loan_id="'+v.id+'" data-year="'+v.year+'" data-month="'+v.month+'" data-employee_id="'+v.employee_id+'"><i class="fa fa-trash"></i> Delete</a>'
        ]).draw( false );
        tDeduct.rows(k).nodes().to$().attr("data-id", v.id);
        tDeduct.rows(k).nodes().to$().attr("data-employeeid", v.employee_id);
        tDeduct.rows(k).nodes().to$().attr("data-deduction_id", v.deduction_id);
        tDeduct.rows(k).nodes().to$().attr("data-amount", v.amount);
        tDeduct.rows(k).nodes().to$().attr("data-start_date", start_date);
        tDeduct.rows(k).nodes().to$().attr("data-end_date", end_date);
        tDeduct.rows(k).nodes().to$().attr("data-date_terminated", date_terminated);
        tDeduct.rows(k).nodes().to$().attr("data-btnnew", "newDeduct");
        tDeduct.rows(k).nodes().to$().attr("data-btnsave", "saveDeduct");
        tDeduct.rows(k).nodes().to$().attr("data-btnedit", "editDeduct");
        tDeduct.rows(k).nodes().to$().attr("data-btncancel", "cancelDeduct");
		deductionAmount += parseFloat(v.amount);
	});

	_totalOtherDeductions = deductionAmount;
	$('#input_actualotherdeduct').val(_totalOtherDeductions).trigger('keyup');
}

function LoanInfo(loaninfo){

	tLoan.clear().draw();
	loan_amount = 0;
	$.each(loaninfo,function(k,v){
		amount = (v.amount !== 0) ? commaSeparateNumber(parseFloat(v.amount).toFixed(2)) : '';
		tLoan.row.add( [
        	v.loans.name,
        	v.loaninfo.loan_total_amount,
			v.loaninfo.loan_total_balance,
			amount,
			v.loaninfo.start_date,
			v.loaninfo.end_date,
			'<a class="btn btn-xs btn-danger delete_item" data-function_name="deleteLoan" data-loan_id="'+v.id+'" data-year="'+v.year+'" data-month="'+v.month+'" data-employee_id="'+v.employee_id+'"><i class="fa fa-trash"></i> Delete</a>'
        ]).draw( false );
        tLoan.rows(k).nodes().to$().attr("data-id", v.id);
        tLoan.rows(k).nodes().to$().attr("data-loan_id", v.loan_id);
        tLoan.rows(k).nodes().to$().attr("data-employeeid", v.employee_id);
        tLoan.rows(k).nodes().to$().attr("data-loan_total_amount", v.loaninfo.loan_total_amount);
        tLoan.rows(k).nodes().to$().attr("data-loan_total_balance", v.loaninfo.loan_total_balance);
        tLoan.rows(k).nodes().to$().attr("data-amount", v.amount);
        tLoan.rows(k).nodes().to$().attr("data-start_date", v.loaninfo.start_date);
        tLoan.rows(k).nodes().to$().attr("data-end_date", v.loaninfo.end_date);
        tLoan.rows(k).nodes().to$().attr("data-loan_date_granted", v.loaninfo.loan_date_granted);
        tLoan.rows(k).nodes().to$().attr("data-date_terminated", v.loaninfo.date_terminated);
        tLoan.rows(k).nodes().to$().attr("data-btnnew", "newLoan");
        tLoan.rows(k).nodes().to$().attr("data-btnsave", "saveLoan");
        tLoan.rows(k).nodes().to$().attr("data-btnedit", "editLoan");
        tLoan.rows(k).nodes().to$().attr("data-btncancel", "cancelLoan");
		loan_amount += parseFloat(v.amount);
	});

	_totalLoans = loan_amount;
	$('#input_actualloan').val(_totalLoans).trigger('keyup');

}

var peraId;
function BenefitInfo(benefitinfo){

	salaCode = "";
	benefitAmount = 0;
	tBenefit.clear().draw();

	$.each(benefitinfo,function(k,v){

		if(v.benefits.name == 'PERA'){
			peraId = v.id;
		}

		benefit_name 	 = (v.benefits.name) ? v.benefits.name : '';
		salaCode 	     = (v.benefits.code) ? v.benefits.code : '';
		benefit_amount 	 = (v.amount) ? v.amount : 0;
		start_date 		 = (v.benefitinfo) ? v.benefitinfo.start_date : '';
		end_date 		 = (v.benefitinfo) ? v.benefitinfo.end_date : '';
		amount = (benefit_amount !== 0) ? commaSeparateNumber(parseFloat(benefit_amount).toFixed(2)) : 0;

		tBenefit.row.add( [
        	start_date,
        	benefit_name,
			amount,
			'<a class="btn btn-xs btn-danger delete_item" data-function_name="deleteBenefit" data-loan_id="'+v.id+'" data-year="'+v.year+'" data-month="'+v.month+'" data-employee_id="'+v.employee_id+'"><i class="fa fa-trash"></i> Delete</a>'
		]).draw( false );
		benefit_rate = v.benefit_rate;
		$('.employee_id').val(v.employee_id);
		// $('#benefit_info_id').val(v.id);
		// $('#benefit_status').val(benefit_name);
        tBenefit.rows(k).nodes().to$().attr("data-id", v.id);
        tBenefit.rows(k).nodes().to$().attr("data-employee_id", v.employee_id);
        tBenefit.rows(k).nodes().to$().attr("data-name", benefit_name);
        tBenefit.rows(k).nodes().to$().attr("data-code", salaCode);
        tBenefit.rows(k).nodes().to$().attr("data-benefit_amount", v.amount);
        tBenefit.rows(k).nodes().to$().attr("data-benefit_id", v.benefit_id);
        tBenefit.rows(k).nodes().to$().attr("data-start_date", start_date);
        tBenefit.rows(k).nodes().to$().attr("data-end_date", end_date);
        tBenefit.rows(k).nodes().to$().attr("data-dayspresent", v.days_present);
        tBenefit.rows(k).nodes().to$().attr("data-sala_absent", v.sala_absent);
        tBenefit.rows(k).nodes().to$().attr("data-sala_undertime", v.sala_undertime);
        tBenefit.rows(k).nodes().to$().attr("data-sala_absent_amount", v.sala_absent_amount);
        tBenefit.rows(k).nodes().to$().attr("data-sala_undertime_amount", v.sala_undertime_amount);
        tBenefit.rows(k).nodes().to$().attr("data-benefit_rate", v.benefit_rate);
        tBenefit.rows(k).nodes().to$().attr("data-btnnew", "newBenefit");
        tBenefit.rows(k).nodes().to$().attr("data-btnsave", "saveBenefit");
        tBenefit.rows(k).nodes().to$().attr("data-btnedit", "editBenefit");
        tBenefit.rows(k).nodes().to$().attr("data-btndelete", "deleteBenefit");
        tBenefit.rows(k).nodes().to$().attr("data-btncancel", "cancelBenefit");

		benefitAmount += parseFloat(v.amount);
     });

	_allowances = benefitAmount;

	$('.newBenefit').attr('disabled',true);
	$('#newBenefit').removeClass('hidden');
	$('#saveBenefit').addClass('hidden');
	$('#editBenefit').addClass('hidden');
	$('#deleteBenefit').addClass('hidden');
	$('#cancelBenefit').addClass('hidden');
	clear_form_elements("myform3");
}

// ======================================================= //
// ============ DELETE  FUNCTION ==================== //
// ===================================================== //

$(document).on('click','.delete_item',function(){
	loan_id 	= $(this).data('loan_id');
	year 		= $(this).data('year');
	month 		= $(this).data('month');
	employee_id = $(this).data('employee_id');
	function_name = $(this).data('function_name')

	if(loan_id){
		swal({
			title: "Delete?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.ajax({
					url:base_url+module_prefix+module+'/'+function_name,
					data:{
						'id':loan_id,
						'year':year,
						'month':month,
						'employee_id':employee_id,
						'_token':"{{ csrf_token() }}"
					},
					type:'post',
					dataType:'JSON',
					success:function(res){
						swal({
							  title: 'Deleted Successfully!',
							  type: "warning",
							  showCancelButton: false,
							  confirmButtonClass: "btn-warning",
							  confirmButtonText: "OK",
							  closeOnConfirm: false
						})
						switch(res.status){
							case 'loans':
								LoanInfo(res.data.loaninfo)
								$('#input_actualloan').val(_totalLoans).trigger('keyup');
							break;
							case 'deductions':
								DeductionInfo(res.data.deductioninfo)
								$('#input_actualotherdeduct').val(_totalOtherDeductions).trigger('keyup');
							break;
							case 'benefits':
								BenefitInfo(res.data.benefitinfo)
								$("#input_actualworkdays").trigger('keyup');
							break;
						}
					}

				})
			}else{
				return false;
			}
		});
	}
})

function compute_grosspay(basicpay,allowance,sala,hp){
		basicpay = (basicpay) ? basicpay : 0;
		allowance = (allowance) ? allowance : 0;
		sala = (sala) ? sala : 0;
		hp = (hp) ? hp : 0;
		_grossPay = (parseFloat(basicpay) + parseFloat(allowance) + parseFloat(sala) + parseFloat(hp));
		return _grossPay;
}
function compute_totalundertime(actual,adjust){
	totalundertime = (actual + adjust);
	return totalundertime;
}

function compute_totaltardines(actual,adjust){
	total_tardines = (actual + adjust);
	return total_tardines;
}

function rate_perminute(payPerHour, ratePerMinute){
	payPerMinute = ((payPerHour / 60) * (ratePerMinute));
	return payPerMinute;
}

function compute_total(tWorkingDays,tAbsence,tUndertime,tTardiness){
	tWorkingDays = (tWorkingDays) ? tWorkingDays : 0;
	tAbsence 	 = (tAbsence) ? tAbsence : 0;
	tUndertime 	 = (tUndertime) ? tUndertime : 0;
	tTardiness 	 = (tTardiness) ? tTardiness : 0;
	total = (parseFloat(tWorkingDays) - (parseFloat(tAbsence) + parseFloat(tUndertime) + parseFloat(tTardiness)));
	return total;
}

function compute_netpay(taxAmount,tLoan,tOtherDeductions,gTaxable,allowances){
	taxAmount 			  = (taxAmount) ? taxAmount : 0;
	tLoan 	  			  = (tLoan) ? tLoan : 0;
	tOtherDeductions 	  = (tOtherDeductions) ? tOtherDeductions : 0;
	gTaxable 	  		  = (gTaxable) ? gTaxable : 0;
	allowances 			  = (allowances) ? allowances : 0;
	if(gTaxable){
		netPay = ((parseFloat(gTaxable) + parseFloat(allowances)) - (parseFloat(taxAmount) + parseFloat(tLoan) + parseFloat(tOtherDeductions)));
	}else{
		netPay = 0;
	}
	return netPay;
}

function compute_totalworkdays(actualworkdays,adjustworkdays){
	totalworkdays = (actualworkdays + adjustworkdays);
	return totalworkdays;
}

function compute_workingdays(totalPay, actualDays){
	payPerDay = (totalPay * actualDays);
	return payPerDay;
}

function compute_totalabsences(actual,adjust){
	totalAbsence = (actual + adjust);
	return totalAbsence;
}

function compute_netdeduction(){

	contributions 		= (_totalContribution) ? parseFloat(_totalContribution) : 0;
	loans 				= (_totalLoans) ? parseFloat(_totalLoans) : 0;
	otherdeductions 	= (_totalOtherDeductions) ? parseFloat(_totalOtherDeductions) : 0;
	sum = (contributions + loans + otherdeductions)
	return sum;
}

function compute_grosstaxable(basicnet,totalContribution){
	grossTaxable = (parseFloat(basicnet) - parseFloat(totalContribution));
	return grossTaxable;
}

//SUBMIT FORM
$(document).off('click',".submitme").on('click',".submitme",function(){
	btn = $(this);
	form = $(this).data('form');
		$('#'+form).ajaxForm({
			beforeSend:function(){

			},
			success:function(data){
				par  =  JSON.parse(data);

				if(par.status){

					swal({  title: par.response,
							text: '',
							type: "success",
							icon: 'success',

						}).then(function(){

							if(par.employeeload){
								// window.location.href = base_url+module_prefix+module;
								$('.btn_cancel').trigger('click');
								$('.btn_cancel2').trigger('click');
								$('.btnfilter').trigger('click');
							}

							if(par.benefitinfo){

								// GENERATE TR FOR BENEFITS TAB

								BenefitInfo(par.benefitinfo);

							}// end of BenefitInfo

							if(par.deductioninfo){

								// GENERATE TR FOR DeductionInfo TAB

								DeductionInfo(par.deductioninfo);
								$('.btn_cancel2').trigger('click');

							}// end of Deductioninfo


						});// end swal

				}else{
					swal({  title: par.response,
							text: '',
							type: "error",
							icon: 'error',

						});

				}// end of main IF STATUS

				btn.button('reset');
			},
			error:function(data){
				$error = data.responseJSON;
				/*reset popover*/
				$('input[type="text"], select').popover('destroy');

				/*add popover*/
				block = 0;
				$(".error-msg").remove();
				$.each($error.errors,function(k,v){
					var messages = v.join(', ');
					msg = '<div class="error-msg err-'+k+'" style="color:red;"><i class="fa fa-exclamation-triangle" style="color:rgb(255, 184, 0);"></i> '+messages+'</div>';
					$('input[name="'+k+'"], textarea[name="'+k+'"], select[name="'+k+'"]').after(msg).attr('data-content',messages);
					if(block == 0){
						$('html, body').animate({
					        scrollTop: $('.err-'+k).offset().top - 250
					    }, 500);
					    block++;
					}
				})
				$('.saving').replaceWith(btn);
			},
			always:function(){
				setTimeout(function(){
						$('.saving').replaceWith(btn);
					},300)
			}
		}).submit();
});


$(document).on('click','#delete_payroll',function(){
	if(_listId.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Delete Payroll?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.deletePayroll();
			}else{
				return false;
			}
		});
	}
});

$.deletePayroll = function(){
	$.ajax({
		url:base_url+module_prefix+module+'/deletePayroll',
		data:{'empid':_listId,'year':_Year,'month':_Month,'_token':"{{ csrf_token() }}"},
		type:'post',
		beforeSend:function(){
			$('#delete_payroll').html('<i class="fa fa-spinner fa-spin"></i> Deleting').prop('disabled',true);
		},
		success:function(response){
			par = JSON.parse(response);
			if(par.status){
				swal({
				  title: par.response,
				  type: "success",
				  showCancelButton: false,
				  confirmButtonClass: "btn-success",
				  confirmButtonText: "OK",
				  closeOnConfirm: false
				});
			_listId = [];
			$('#delete_payroll').html('<i class="fas fa-minus-circle"></i> Delete').prop('disabled',false);
			_checkpayroll = 'wpayroll';
			$('.btnfilter').trigger('click');
			clear_form_elements('myForm');
			tDeduct.clear().draw();
			tLoan.clear().draw();
			tBenefit.clear().draw();
			$("#_salaryrate").text(0.00)
			$("#totalamount").text(0.00)
			$("#totalOT").text(0.00)
			$("#totalGrossPay").text(0.00)
			$("#grosspay").text(0.00)
			$("#grosstaxablepay").text(0.00)
			$("#netpay").text(0.00)
			$('#transaction_id').val('');
			$('attendance_id').val('');
			}else{
				swal({
				  title: par.response,
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-warning",
				  confirmButtonText: "OK",
				  closeOnConfirm: false
				})
				_listId = [];
				$('.btnfilter').trigger('click');
				$('#delete_payroll').html('<i class="fas fa-minus-circle"></i> Delete').prop('disabled',false);
			}
		}
	});
}



$('#tbl_benefitTransact tbody').on( 'click', 'tr', function () {
    if ( $(this).hasClass('selected') ) {

        $(this).removeClass('selected');

        $('#daysPresent').addClass('hidden');


        id 						= $(this).data('id');
		employee_id 			= $(this).data('employee_id');
		 benefitCode			= $(this).data('code');
		 amount					= $(this).data('benefit_amount');
		_daysPresent 			= $(this).data('dayspresent');
		benefit_rate 	 		= $(this).data('benefit_rate');
		sala_absent 	 		= $(this).data('sala_absent');
		sala_absent_amount 	 	= $(this).data('sala_absent_amount');
		sala_undertime 	 		= $(this).data('sala_undertime');
		sala_undertime_amount 	= $(this).data('sala_undertime_amount');

		$('#start_date').val(start_date);
		$('#benefit_info_id').val(id);
		$('.employee_id').val(employee_id);

		$('#benefit_rate').val(benefit_rate);

		if(benefitCode === 'SA'){

			$('.daysPresent').removeClass('hidden');
			$('.hpRate').addClass('hidden');
			$('.rataAdjust').addClass('hidden');
			$('#benefitForm').removeClass('hidden');
			$('.benefitSetup').addClass('hidden');
			$('#days_present_sala').val(_daysPresent)
			$('#benefits_amount_sala').val(amount);
			$('#benefit_status').val(benefitCode);
			$('#sala_absent').val(sala_absent);
			$('#sala_absent_amount').val(sala_absent_amount);
			$('#sala_undertime').val(sala_undertime);
			$('#sala_undertime_amount').val(sala_undertime_amount);

		}else if(benefitCode === 'LA'){

			$('.daysPresent').removeClass('hidden');
			$('.hpRate').addClass('hidden');
			$('.rataAdjust').addClass('hidden');
			$('#benefitForm').removeClass('hidden');
			$('.benefitSetup').addClass('hidden');
			$('#notInclude').addClass('hidden');

			$('#days_present_sala').val(_daysPresent)
			$('#benefits_amount_sala').val(amount);
			$('#benefit_status').val(benefitCode);
			$('#sala_absent').val(sala_absent);
			$('#sala_absent_amount').val(sala_absent_amount);
			$('#sala_undertime').val(sala_undertime);
			$('#sala_undertime_amount').val(sala_undertime_amount);

		}else if(benefitCode === 'HP'){

			$('.hpRate').removeClass('hidden');
			$('.rataAdjust').addClass('hidden');
			$('.daysPresent').addClass('hidden');
			$('.benefitSetup').addClass('hidden');
			$('#benefitForm').removeClass('hidden');
			$('#days_present_hp').val(_daysPresent);
			$('#benefits_amount_hp').val(amount);
			$('#benefit_status').val(benefitCode);

		}else if(benefitCode === 'RATA2'){
			$('.hpRate').addClass('hidden');
			$('.daysPresent').addClass('hidden');
			$('.rataAdjust').removeClass('hidden');
			$('.benefitSetup').addClass('hidden');
			$('#benefitForm').removeClass('hidden');
			$('#benefit_status').val(benefitCode);
			taAmount = amount;

		}else{
			// $('#benefitForm').addClass('hidden');
			$('.hpRate').addClass('hidden');
			$('.daysPresent').addClass('hidden');
			$('.rataAdjust').addClass('hidden');
			$('.benefitSetup').removeClass('hidden');
		}


		btnnew = $(this).data('btnnew');
		btnsave = $(this).data('btnsave');
		btnedit = $(this).data('btnedit');
		btncancel = $(this).data('btncancel');
		btndelete = $(this).data('btndelete');

		if(!$('#'+btnsave).is(':visible')){
			$('#'+btnedit).removeClass('hidden');
			$('#'+btncancel).removeClass('hidden');
			$('#'+btndelete).removeClass('hidden');
			$('#'+btnnew).addClass('hidden');
		}

		$('#days_present_sala').trigger('keyup');

    }
    else {
        tBenefit.$('tr.selected').removeClass('selected');
        $(this).addClass('selected');
    }
});


$('#button').click( function () {
    tBenefit.row('.selected').remove().draw( false );
});


});




</script>
@endsection