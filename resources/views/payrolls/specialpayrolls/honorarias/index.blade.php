@extends('app-front')

@section('content')
<div style="margin-top: 50px;">
<div class="col-md-3">
	<h5 ><b>Filter Employee By</b></h5>
	<table class="table borderless" style="border:none;font-weight: bold">
		<tr class="text-left">
			<td colspan="2"><span>Transaction Period</span></td>
		</tr>
		<tr>
			<td>
				<div class="row" style="margin-right: -5px;margin-left: -5px;">
					@include('payrolls.includes._months-year')
				</div>
			</td>

		</tr>
	</table>
	<div class="col-md-12 text-right">
		<button class="btn btn-xs btn-info" id="btn_process_pbb" style="background-color: #164c8a;"><i class="far fa-save"></i>&nbsp;Process</button>
		<a class="btn btn-xs btn-danger" id="delete_pbb" ><i class="fas fa-minus-circle"></i>&nbsp;Delete</a>
	</div>
	<a class="btn btn-xs btn-info btnfilter hidden"style="background-color: #164c8a;" style="float: left;line-height: 16px;" ><i class="fa fa-filter"></i>Filter</a>
	<div class="search-btn">
		<div class="col-md-4">
			<span>Search</span>
		</div>
		<div class="col-md-8">
			<label class="radiobut-style radio-inline ">
				<input type="radio" name="chk_pbb" id="wpbb" value="wpbb">
				With
			</label>
			<label class="radiobut-style radio-inline ">
				<input type="radio" name="chk_pbb" id="wopbb" value="wopbb">
				W/Out
			</label>
		</div>
	</div>
	<div >
		<input type="text" name="filter_search" class="form-control _searchname">
	</div>
	<div style="height: 5px;"></div>
	<div class="sub-panelnamelist ">
		{!! $controller->show() !!}
	</div>

</div>


<div class="col-md-9" id="pbb">
	<div class="row" style="margin:0 15px 0 15px;">
		<label style="font-weight: 600;font-size: 15px;padding-left: 5px;" id="d-name"></label>
		<div class="sub-panel">
			{!! $controller->showHonoraria() !!}
		</div>
	</div>
	<div class="row" id="formAddPBB" style="margin-left: 20px;">
		<div class="col-md-4">
			<div class="panel benefits-content">
				<div class="panel-body">
						<div class="col-md-12">
							<div class=" button-style-wrapper2">
								<a class="btn btn-xs btn-info btn-savebg btn_new" id="btn_saveadjust"><i class="fa fa-save"></i> Add</a>
								<a class="btn btn-xs btn-danger btn_cancel"> Cancel</a>
							</div>
							<hr>
							<div class="form-group">
								<label>Honoraria Amount</label>
								<input type="text" name="honoraria_amount" id="honoraria_amount" class="form-control onlyNumber">
							</div>
							<div class="form-group">
								<label>Deduction Amount</label>
								<input type="text" name="deduction_amount" id="deduction_amount" class="form-control onlyNumber">
							</div>
							<div class="form-group">
								<label>Tax Amount</label>
								<input type="text" name="tax_amount" id="tax_amount" class="form-control onlyNumber">
							</div>
						</div>
				</div>
			</div>
		</div>
	</div>
</div>

</div>
	@endsection

@section('js-logic1')
<script type="text/javascript">
$(document).ready(function(){
	var tblPbb = $('#tbl_pbb').DataTable();
	number_of_actual_work = 0;

    $('#inclusive_leave_date').datepicker({
    	dateFormat:'yy-mm-dd'
    });

    var _Year;
    var _Month;
    $(document).on('change','#select_year',function(){
    	_Year = "";
    	_Year = $(this).find(':selected').val();

    });
    $(document).on('change','#select_month',function(){
    	_Month = "";
    	_Month = $(this).find(':selected').val();
    });

    $('.select2').select2();
    $('#select_year').trigger('change');
    $('#select_month').trigger('change');

    $('#wopbb').prop('checked',false);

    $('.benefits-content :input').attr("disabled",true);
   $('.btn_new').on('click',function(){
    	$('.benefits-content :input').attr("disabled",false);
    	$('.btn_new').addClass('hidden');
    	$('.btn_edit').addClass('hidden');
    	$('.btn_save').removeClass('hidden');
    });
    $('.btn_edit').on('click',function(){
    	$('.leave-field').attr("disabled",false);
    	$('.btn_edit').addClass('hidden');
    	$('.btn_new').addClass('hidden');
    	$('.btn_save').removeClass('hidden');
    });
    $('.btn_cancel').on('click',function(){
    	$('.benefits-content :input').attr("disabled",true);
    	$('.btn_new').removeClass('hidden');
    	$('.btn_save').addClass('hidden');
    	$('.btn_edit').removeClass('hidden');

    	myform = "myform";
    	clear_form_elements(myform);
    	clear_form_elements('benefits-content');
    	$('#tbl_leave').html('');
    	$('.error-msg').remove();
    	$('#for_update').val('');
    });

    var honoraria_amount = 0;
    $(document).on('keyup','#honoraria_amount',function(){
    	honoraria_amount = $(this).val().replace(',','');
    });

    var deduction_amount = 0;
    $(document).on('keyup','#deduction_amount',function(){
    	deduction_amount = $(this).val().replace(',','');
    });

    var tax_amount = 0;
    $(document).on('keyup','#tax_amount',function(){
    	tax_amount = $(this).val().replace(',','');
    });

    $('.onlyNumber').keypress(function (event) {
    	return isNumber(event, this)
    });


    var _listId = [];
    $(document).on('click','#check_all',function(){

    	if(!_Year && !_Month){
    		swal({
    			title: "Select year and month first",
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-danger",
    			confirmButtonText: "Yes",
    			closeOnConfirm: false

    		});

    		$('#check_all').prop('checked',false);

    	}else{
    		if ($(this).is(':checked')) {
    			$('.emp_select').prop('checked', 'checked');

    			$('.emp_select:checked').each(function(){
    				_listId.push($(this).val())
    			});

    		} else {
    			$('.emp_select').prop('checked', false)
    			_listId = [];
    		}

    	}

    });

    _checkpbb = ""
    $('input[type=radio][name=chk_pbb]').change(function() {

    	if(!_Year){
    		swal({
    			title: 'Select year and month first',
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-warning",
    			confirmButtonText: "OK",
    			closeOnConfirm: false
    		})

    		$(this).prop('checked',false);

    	}else{

    		if (this.value == 'wpbb') {
    			$('#btn_process_pbb').prop('disabled',true);
    			_checkpbb = 'wpbb';

    		}
    		else if (this.value == 'wopbb') {
    			$('#btn_process_pbb').prop('disabled',false);
    			_checkpbb = 'wopbb';

    		}
    		$('.btnfilter').trigger('click');

    	}

    });


    $(document).on('click','.emp_select',function(){
    	empid = $(this).val();
    	index = $(this).data('key');
    	if(!_Year && !_Month){

    		swal({
    			title: "Select year and month first",
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-danger",
    			confirmButtonText: "Yes",
    			closeOnConfirm: false

    		});

    		$('.emp_select').prop('checked',false);

    	}else{

    		if($(this).is(':checked')){
    			_listId[index] =  empid;

    		}else{
    			delete _listId[index];
    		}

    	}
    });

    var pbb_id;
    var employee_id;
    $(document).on('click','#namelist tr',function(){
    	employee_id = $(this).data('empid');
    	$('#d-name').text($(this).data('employee'));

    	if(_Year && _Month){

    		$.ajax({
    			url:base_url+module_prefix+module+'/getHonoraria',
    			data:{
    				'employee_id':employee_id,
    				'year':_Year,
    				'month':_Month,
    			},
    			type:'GET',
    			dataType:'JSON',
    			success:function(data){
    				console.log(data);
    				clear_form_elements('benefits-content');
    				pbb_id = '';

    				if(data !== null){
	    				var	benefits_amount = 0;
	    				var pbb_total = 0;
	    				var office = '';
	    				var position = '';

						tblPbb.clear().draw();
	    				office = (data.offices !== null) ? data.offices.name : '';
						position = (data.positions !== null) ? data.positions.name : '';
						honoraria_amount = (data.amount) ? data.amount : 0;
						deduction_amount = (data.deduction_amount) ? data.deduction_amount : 0;
						tax_amount = (data.tax_amount) ? data.tax_amount : 0;

						totalAmount = parseFloat(honoraria_amount) - parseFloat(deduction_amount) - parseFloat(tax_amount);


	    				honoraria_amount = (honoraria_amount) ? commaSeparateNumber(parseFloat(honoraria_amount).toFixed(2)) : '';
	    				deduction_amount = (deduction_amount) ? commaSeparateNumber(parseFloat(deduction_amount).toFixed(2)) : '';
	    				tax_amount = (tax_amount) ? commaSeparateNumber(parseFloat(tax_amount).toFixed(2)) : '';
	    				total_amount = (totalAmount) ? commaSeparateNumber(parseFloat(totalAmount).toFixed(2)) : '';

						tblPbb.row.add( [
							honoraria_amount,
							deduction_amount,
							tax_amount,
							total_amount

						]).draw( false );
    				}

				}
			});
    	}else{
    		swal({
    			title: 'Select year and month first',
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-warning",
    			confirmButtonText: "Yes",
    			closeOnConfirm: false

    		});
    	}

    })

$(document).on('keyup','._searchname',function(){
	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
				type: "GET",
				url: base_url+module_prefix+module+'/show',
				data: {
					'q':$('._searchname').val(),
					'check_pbb':_checkpbb
				},
				beforeSend:function(){
				   		// $('.ajax-loader').css("visibility", "visible");

				   	},
				   	success: function(res){
				   		$(".sub-panelnamelist").html(res);

				   	},
				   	complete:function(){
				   		// $('.ajax-loader').css("visibility", "hidden");
				   	}
				 });
		},500);
});

$(document).on('change','#searchby',function(){
	var val = $(this).val();
	console.log(base_url+module_prefix+module)
	$.ajax({
		url:base_url+module_prefix+module+'/getSearchby',
		data:{'q':val},
		type:'GET',
		dataType:'JSON',
		success:function(data){

			arr = [];
			$.each(data,function(k,v){
				arr += '<option value='+v.id+'>'+v.name+'</option>';
			})

			$('#select_searchvalue').html(arr);
		}
	})

});

$(document).on('click','#btn_process_pbb',function(){
	console.log(_listId);
	if(_listId.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Process Honoraria?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.processPbb();
			}else{
				return false;
			}
		});
	}

});

$.processPbb = function(){
	$.ajax({
		url:base_url+module_prefix+module+'/processHonoraria',
		data:{
			'_token':'{{ csrf_token() }}',
			'list_id':_listId,
			'year':_Year,
			'month':_Month,
			'pbb_id':pbb_id,
			'honoraria_amount':honoraria_amount,
			'deduction_amount':deduction_amount,
			'tax_amount':tax_amount,
			'employee_id':employee_id
		},
		type:'POST',
		beforeSend:function(){
			$('#btn_process_pbb').html('<i class="fa fa-spinner fa-spin"></i> Processing').prop('disabled',true);
		},
		success:function(data){
			par = JSON.parse(data);
			if(par.status){
				swal({
					title: par.response,
					type: "success",
					showCancelButton: false,
					confirmButtonClass: "btn-success",
					confirmButtonText: "Yes",
					closeOnConfirm: false
				});
				// window.location.href = base_url+module_prefix+module;
				_listId = [];
				$('.btn_cancel').trigger('click');
				$('.btnfilter').trigger('click');
				$('#btn_process_pbb').html('<i class="fa fa-save"></i> Process').prop('disabled',false);
			}else{

				swal({
					title: par.response,
					type: "warning",
					showCancelButton: false,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Yes",
					closeOnConfirm: false
				});

				$('#btn_process_pbb').html('<i class="fa fa-save"></i> Process').prop('disabled',false);

			}
		}
	});
}

$(document).on('click','#btn_save_pbb',function(){

	if(no_of_actual_work == null){
		swal("No of field is empty!", "", "warning");
	}else{
		$.savePbb();
	}

});

$(document).on('click','#delete_pbb',function(){
	if(_listId.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Delete Honoraria?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.deletePbb();
			}else{
				return false;
			}
		});
	}
})

$.deletePbb = function(){
	$.ajax({
		url:base_url+module_prefix+module+'/deleteHonoraria',
		data:{'empid':_listId,'year':_Year,'month':_Month,'_token':"{{ csrf_token() }}"},
		type:'post',
		beforeSend:function(){
			$('#delete_pbb').html('<i class="fa fa-spinner fa-spin"></i> Deleting').prop('disabled',true);
		},
		success:function(response){
			par = JSON.parse(response)

			if(par.status){
				swal({
					title: par.response,
					type: "success",
					showCancelButton: false,
					confirmButtonClass: "btn-success",
					confirmButtonText: "OK",
					closeOnConfirm: false
				})

				_listId = [];
				$('#delete_pbb').html('<i class="fas fa-minus-circle"></i> Delete').prop('disabled',false);
				$('.btnfilter').trigger('click');
			}
		}
	});
}


var timer;
$(document).on('click','.btnfilter',function(){
	year 	  = $('#select_year :selected').val();
	month 	  = $('#select_month :selected').val();

	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
			   type: "GET",
			   url: base_url+module_prefix+module+'/show',
			   data: {'year':_Year,'month':_Month,'checkpbb':_checkpbb },
			   beforeSend:function(){
			   		$('#loading').removeClass('hidden');
			   },
			   complete:function(){
			   		$('#loading').addClass('hidden');
			   },
			   success: function(res){
			   	// console.log(res);
			      $(".sub-panelnamelist").html(res);
			   }
			});
		},500);
});



});


</script>
@endsection