@extends('app-front')

@section('content')
<div class="row">
	<div class="col-md-12">
		<hr>
		<div class="col-md-3">
			<div>
				<h5 ><b>Filter Employee By</b></h5>
				<table class="table borderless" style="border:none;font-weight: bold">
					<tr class="text-left">
						<td colspan="2"><span>Transaction Period</span></td>
					</tr>
					<tr>
						<td>
							<div class="col-md-6" style="padding-left: 0px;">
								<select class="employee-type form-control font-style2 select2" id="select_month" name="select_month" placeholder="Month">
									<option value=""></option>
								</select>
							</div>
							<div class="col-md-6" style="padding-right: 0px;">
								<select class="employee-type form-control font-style2 select2" id="select_year" name="select_year" placeholder="Year" >
									<option value=""></option>
								</select>
							</div>
						</td>

					</tr>
				</table>
				<div class="col-md-12 text-right">
					<button class="btn btn-xs btn-info" id="btn_process_cea" style="background-color: #164c8a;"><i class="far fa-save"></i>&nbsp;Process</button>
					<a class="btn btn-xs btn-danger" id="delete_cea" ><i class="fas fa-minus-circle"></i>&nbsp;Delete</a>
				</div>
				<a class="btn btn-xs btn-info btnfilter hidden"style="background-color: #164c8a;" style="float: left;line-height: 16px;" ><i class="fa fa-filter"></i>Filter</a>
				<div class="search-btn">
					<div class="col-md-4">
						<span>Search</span>
					</div>
					<div class="col-md-8">
						<label class="radiobut-style radio-inline ">
							<input type="radio" name="chk_cea" id="wcea" value="wcea">
							With
						</label>
						<label class="radiobut-style radio-inline ">
							<input type="radio" name="chk_cea" id="wocea" value="wocea">
							W/Out
						</label>
					</div>
				</div>
				<div >
					<input type="text" name="filter_search" class="form-control _searchname">
				</div>
				<div style="height: 5px;"></div>
				<div class="sub-panelnamelist ">
					{!! $controller->show() !!}
				</div>

			</div>
		</div>

		<div class="col-md-9" id="cea">
			<label style="font-weight: 600;font-size: 15px;">&nbsp;&nbsp;{{ $title }}</label>
			<div class="sub-panel">
				{!! $controller->showCeaDatatable() !!}
			</div>
			<div class="benefits-content style-box2">
				<div class="col-md-6">
				</div>
				<div class="col-md-6">
				</div>
			</div>
		</div>


		</div>
	</div>
	@endsection

@section('js-logic1')
<script type="text/javascript">
$(document).ready(function(){
	var tblCea = $('#tbl_cea').DataTable();
	number_of_actual_work = 0;
	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
		year += '<option value='+y+'>'+y+'</option>';
	}
	$('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
    for ( m =  0; m <= month.length - 1; m++) {
    	mArr += '<option '+month[m]+'>'+month[m]+'</option>';
    }
    $('#select_month').html(mArr);

    $('#inclusive_leave_date').datepicker({
    	dateFormat:'yy-mm-dd'
    });

    var _Year;
    var _Month;
    $(document).on('change','#select_year',function(){
    	_Year = "";
    	_Year = $(this).find(':selected').val();

    });
    $(document).on('change','#select_month',function(){
    	_Month = "";
    	_Month = $(this).find(':selected').val();
    });

    $('.select2').select2();

    $('#wocea').prop('checked',false);

    $('.benefits-content :input').attr("disabled",true);
    $('.btn_new').on('click',function(){
    	$('.benefits-content :input').attr("disabled",false);
    	$('.btn_new').addClass('hidden');
    	$('.btn_edit').addClass('hidden');
    	$('.btn_save').removeClass('hidden');
    });
    $('.btn_edit').on('click',function(){
    	$('.leave-field').attr("disabled",false);
    	$('.btn_edit').addClass('hidden');
    	$('.btn_new').addClass('hidden');
    	$('.btn_save').removeClass('hidden');
    });
    $('.btn_cancel').on('click',function(){
    	$('.benefits-content :input').attr("disabled",true);
    	// $('.btn_new').removeClass('hidden');
    	$('.btn_save').addClass('hidden');
    	$('.btn_edit').removeClass('hidden');

    	myform = "myform";
    	clear_form_elements(myform);
    	clear_form_elements('benefits-content');
    	$('#tbl_leave').html('');
    	$('.error-msg').remove();
    	$('#for_update').val('');
    });

    ;

    $('.onlyNumber').keypress(function (event) {
    	return isNumber(event, this)
    });


    var _listId = [];
    $(document).on('click','#check_all',function(){

    	if(!_Year && !_Month){
    		swal({
    			title: "Select year and month first",
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-danger",
    			confirmButtonText: "Yes",
    			closeOnConfirm: false

    		});

    		$('#check_all').prop('checked',false);

    	}else{
    		if ($(this).is(':checked')) {
    			$('.emp_select').prop('checked', 'checked');

    			$('.emp_select:checked').each(function(){
    				_listId.push($(this).val())
    			});

    		} else {
    			$('.emp_select').prop('checked', false)
    			_listId = [];
    		}

    	}

    });

    _checkcea = ""
    $('input[type=radio][name=chk_cea]').change(function() {

    	if(!_Year){
    		swal({
    			title: 'Select year and month first',
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-warning",
    			confirmButtonText: "OK",
    			closeOnConfirm: false
    		})

    		$(this).prop('checked',false);

    	}else{

    		if (this.value == 'wcea') {
    			$('#btn_process_cea').prop('disabled',true);
    			_checkcea = 'wcea';

    		}
    		else if (this.value == 'wocea') {
    			$('#btn_process_cea').prop('disabled',false);
    			_checkcea = 'wocea';

    		}
    		$('.btnfilter').trigger('click');

    	}

    });


    $(document).on('click','.emp_select',function(){
    	empid = $(this).val();
    	index = $(this).data('key');
    	if(!_Year && !_Month){

    		swal({
    			title: "Select year and month first",
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-danger",
    			confirmButtonText: "Yes",
    			closeOnConfirm: false

    		});

    		$('.emp_select').prop('checked',false);

    	}else{

    		if($(this).is(':checked')){
    			_listId[index] =  empid;

    		}else{
    			delete _listId[index];
    		}

    	}
    	console.log(_listId);
    });

    var cea_id;
    var employee_id;
    $(document).on('click','#namelist tr',function(){
    	employee_id = $(this).data('empid');

    	if(_Year && _Month){

    		$.ajax({
    			url:base_url+module_prefix+module+'/getCeaInfo',
    			data:{
    				'employee_id':employee_id,
    				'year':_Year,
    				'month':_Month,
    			},
    			type:'GET',
    			dataType:'JSON',
    			success:function(data){

    				clear_form_elements('benefits-content');
    				cea_id = '';

    				var	cea_amount = 0;
    				var cea_total = 0;
    				var office = '';
    				var position = '';
					tblCea.clear().draw();
    				$.each(data,function(k,v){
    					office = v.offices.name;
    					position = (v.positionitems !== null) ? v.positionitems.positions.name : '';
    					cea_amount = (v.benefitinfo.benefit_amount) ? v.benefitinfo.benefit_amount : 0;

    				});

    				cea_total = (cea_amount) ? commaSeparateNumber(parseFloat(cea_amount).toFixed(2)) : '';
					tblCea.row.add( [
						position,
						office,
						cea_total

					]).draw( false );
				}
			});
    	}else{
    		swal({
    			title: 'Select year and month first',
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-warning",
    			confirmButtonText: "Yes",
    			closeOnConfirm: false

    		});
    	}

    })

$(document).on('keyup','._searchname',function(){
	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
				type: "GET",
				url: base_url+module_prefix+module+'/show',
				data: {
					'q':$('._searchname').val(),
					'check_cea':_checkcea
				},
				beforeSend:function(){
				   		// $('.ajax-loader').css("visibility", "visible");

				   	},
				   	success: function(res){
				   		$(".sub-panelnamelist").html(res);

				   	},
				   	complete:function(){
				   		// $('.ajax-loader').css("visibility", "hidden");
				   	}
				 });
		},500);
});

$(document).on('change','#searchby',function(){
	var val = $(this).val();
	console.log(base_url+module_prefix+module)
	$.ajax({
		url:base_url+module_prefix+module+'/getSearchby',
		data:{'q':val},
		type:'GET',
		dataType:'JSON',
		success:function(data){

			arr = [];
			$.each(data,function(k,v){
				arr += '<option value='+v.id+'>'+v.name+'</option>';
			})

			$('#select_searchvalue').html(arr);
		}
	})

});

$(document).on('click','#btn_process_cea',function(){
	console.log(_listId);
	if(_listId.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Process Communication Expense?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.processCea();
			}else{
				return false;
			}
		});
	}

});

$.processCea = function(){
	$.ajax({
		url:base_url+module_prefix+module+'/processCea',
		data:{
			'_token':'{{ csrf_token() }}',
			'list_id':_listId,
			'year':_Year,
			'month':_Month,
			'cea_id':cea_id,
			'employee_id':employee_id
		},
		type:'POST',
		beforeSend:function(){
        	$('#btn_process_cea').html('<i class="fa fa-spinner fa-spin"></i> Processing').prop('disabled',true);
        },
		success:function(data){
			par = JSON.parse(data);
			if(par.status){
				swal({
					title: par.response,
					type: "success",
					showCancelButton: false,
					confirmButtonClass: "btn-success",
					confirmButtonText: "Yes",
					closeOnConfirm: false
				});
					// window.location.href = base_url+module_prefix+module;
					$('#btn_process_cea').html('<i class="fa fa-save"></i> Process').prop('disabled',false);
					_listId = [];
					$('.btnfilter').trigger('click');
				}else{

				}
			}
	});
}

$(document).on('click','#btn_save_cea',function(){

	if(no_of_actual_work == null){
		swal("No of field is empty!", "", "warning");
	}else{
		$.saveCea();
	}

});

$(document).on('click','#delete_cea',function(){
	if(_listId.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Delete Communication Expense?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.deleteCea();
			}else{
				return false;
			}
		});
	}
})

$.deleteCea = function(){
	$.ajax({
		url:base_url+module_prefix+module+'/deleteCea',
		data:{'empid':_listId,'year':_Year,'month':_Month,'_token':"{{ csrf_token() }}"},
		type:'post',
		beforeSend:function(){
			$('#delete_cea').html('<i class="fa fa-spinner fa-spin"></i> Deleting').prop('disabled',true);
		},
		success:function(response){
			par = JSON.parse(response)

			if(par.status){
				swal({
					title: par.response,
					type: "success",
					showCancelButton: false,
					confirmButtonClass: "btn-success",
					confirmButtonText: "OK",
					closeOnConfirm: false
				})

				_listId = [];
				$('.btnfilter').trigger('click');
				$('#delete_cea').html('<i class="fas fa-minus-circle"></i> Delete').prop('disabled',false);
			}
		}
	});
}


var timer;
$(document).on('click','.btnfilter',function(){
	year 	  = $('#select_year :selected').val();
	month 	  = $('#select_month :selected').val();

	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
			   type: "GET",
			   url: base_url+module_prefix+module+'/show',
			   data: {'year':_Year,'month':_Month,'checkcea':_checkcea },
			   beforeSend:function(){
			   		$('#loading').removeClass('hidden');
			   },
			   complete:function(){
			   		$('#loading').addClass('hidden');
			   },
			   success: function(res){
			   	// console.log(res);
			      $(".sub-panelnamelist").html(res);
			   }
			});
		},500);
});



});


</script>
@endsection