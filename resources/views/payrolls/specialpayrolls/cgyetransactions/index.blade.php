@extends('app-front')

@section('content')
<div class="row">
	<div class="col-md-12">
		<hr>
		<div class="col-md-3">
			<div>
				<h5 ><b>Filter Employee By</b></h5>
				<table class="table borderless" style="border:none;font-weight: bold">
					<tr class="text-left">
						<td colspan="2"><span>Transaction Period</span></td>
					</tr>
					<tr>
						<td>
							<div class="row" style="margin-right: -5px;margin-left: -5px;">
								@include('payrolls.includes._months-year')
							</div>
						</td>
					</tr>
				</table>
				<div class="col-md-12 text-right">
					<button class="btn btn-xs btn-info" id="btn_process_cgye" style="background-color: #164c8a;"><i class="far fa-save"></i>&nbsp;Process</button>
					<a class="btn btn-xs btn-danger" id="delete_cgye" ><i class="fas fa-minus-circle"></i>&nbsp;Delete</a>
				</div>
				<a class="btn btn-xs btn-info btnfilter hidden"style="background-color: #164c8a;" style="float: left;line-height: 16px;" ><i class="fa fa-filter"></i>Filter</a>
				<div class="search-btn">
					<div class="col-md-4">
						<span>Search</span>
					</div>
					<div class="col-md-8">
						<label class="radiobut-style radio-inline ">
							<input type="radio" name="chk_cgye" id="wcgye" value="wcgye">
							With
						</label>
						<label class="radiobut-style radio-inline ">
							<input type="radio" name="chk_cgye" id="wocgye" value="wocgye">
							W/Out
						</label>
					</div>
				</div>
				<div >
					<input type="text" name="filter_search" class="form-control _searchname">
				</div>
				<div style="height: 5px;"></div>
				<div class="sub-panelnamelist ">
					{!! $controller->show() !!}
				</div>

			</div>
		</div>

		<div class="col-md-9" id="cgye">
			<div class="row" style="margin:0 15px 0 15px;">
		<label style="font-weight: 600;font-size: 15px;padding-left: 5px;" id="d-name"></label>
		<div class="sub-panel">
			{!! $controller->showCgyeDatatable() !!}
		</div>
	</div>
	<div class="row" id="formAddPBB" style="margin-left: 20px;">
		<div class="col-md-4">
			<div class="panel benefits-content">
				<div class="panel-body">
					<div class="col-md-12">
						<div class=" button-style-wrapper2">
							<a class="btn btn-xs btn-info btn-savebg btn_new" id="btn_saveadjust"><i class="fa fa-save"></i> Add</a>
							<a class="btn btn-xs btn-danger btn_cancel"> Cancel</a>
						</div>
						<hr>
						<div class="form-group">
							<label>Deduction Amount</label>
							<input type="text" name="deduction_amount" id="deduction_amount" class="form-control onlyNumber">
						</div>
						<div class="form-group">
							<label>Tax Amount</label>
							<input type="text" name="tax_amount" id="tax_amount" class="form-control onlyNumber">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
		</div>


		</div>
	</div>
	@endsection

@section('js-logic1')
<script type="text/javascript">
$(document).ready(function(){
	var tblCgye = $('#tbl_cgye').DataTable();
	number_of_actual_work = 0;

    $('#inclusive_leave_date').datepicker({
    	dateFormat:'yy-mm-dd'
    });

    var _Year;
    var _Month;
    $(document).on('change','#select_year',function(){
    	_Year = "";
    	_Year = $(this).find(':selected').val();

    });
    $(document).on('change','#select_month',function(){
    	_Month = "";
    	_Month = $(this).find(':selected').val();
    });

    $('.select2').select2();
    $('#select_month').trigger('change');
    $('#select_year').trigger('change');

    $('#wocgye').prop('checked',false);

    $('.benefits-content :input').attr("disabled",true);
    $('.btn_new').on('click',function(){
    	$('.benefits-content :input').attr("disabled",false);
    	$('.btn_new').addClass('hidden');
    	$('.btn_edit').addClass('hidden');
    	$('.btn_save').removeClass('hidden');
    });
    $('.btn_edit').on('click',function(){
    	$('.leave-field').attr("disabled",false);
    	$('.btn_edit').addClass('hidden');
    	$('.btn_new').addClass('hidden');
    	$('.btn_save').removeClass('hidden');
    });
    $('.btn_cancel').on('click',function(){
    	$('.benefits-content :input').attr("disabled",true);
    	$('.btn_new').removeClass('hidden');
    	$('.btn_save').addClass('hidden');
    	$('.btn_edit').removeClass('hidden');

    	myform = "myform";
    	clear_form_elements(myform);
    	clear_form_elements('benefits-content');
    	$('#tbl_leave').html('');
    	$('.error-msg').remove();
    	$('#for_update').val('');
    });

    var deduction_amount = 0;
    $(document).on('keyup','#deduction_amount',function(){
    	deduction_amount = $(this).val().replace(',','');
    });

    var tax_amount = 0;
    $(document).on('keyup','#tax_amount',function(){
    	tax_amount = $(this).val().replace(',','');
    });

    $('.onlyNumber').keypress(function (event) {
    	return isNumber(event, this)
    });


    var _listId = [];
    $(document).on('click','#check_all',function(){

    	if(!_Year && !_Month){
    		swal({
    			title: "Select year and month first",
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-danger",
    			confirmButtonText: "Yes",
    			closeOnConfirm: false

    		});

    		$('#check_all').prop('checked',false);

    	}else{
    		if ($(this).is(':checked')) {
    			$('.emp_select').prop('checked', 'checked');

    			$('.emp_select:checked').each(function(){
    				_listId.push($(this).val())
    			});

    		} else {
    			$('.emp_select').prop('checked', false)
    			_listId = [];
    		}

    	}

    });

    _checkcgye = ""
    $('input[type=radio][name=chk_cgye]').change(function() {

    	if(!_Year){
    		swal({
    			title: 'Select year and month first',
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-warning",
    			confirmButtonText: "OK",
    			closeOnConfirm: false
    		})

    		$(this).prop('checked',false);

    	}else{

    		if (this.value == 'wcgye') {
    			$('#btn_process_cgye').prop('disabled',true);
    			_checkcgye = 'wcgye';

    		}
    		else if (this.value == 'wocgye') {
    			$('#btn_process_cgye').prop('disabled',false);
    			_checkcgye = 'wocgye';

    		}
    		$('.btnfilter').trigger('click');

    	}

    });


    $(document).on('click','.emp_select',function(){
    	empid = $(this).val();
    	index = $(this).data('key');
    	if(!_Year && !_Month){

    		swal({
    			title: "Select year and month first",
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-danger",
    			confirmButtonText: "Yes",
    			closeOnConfirm: false

    		});

    		$('.emp_select').prop('checked',false);

    	}else{

    		if($(this).is(':checked')){
    			_listId[index] =  empid;

    		}else{
    			delete _listId[index];
    		}

    	}
    	console.log(_listId);
    });

    var cgye_id;
    var employee_id;
    $(document).on('click','#namelist tr',function(){
    	employee_id = $(this).data('empid');
    	$('#d-name').text($(this).data('employee'));

    	if(_Year && _Month){

    		$.ajax({
    			url:base_url+module_prefix+module+'/getCgyeInfo',
    			data:{
    				'employee_id':employee_id,
    				'year':_Year,
    				'month':_Month,
    			},
    			type:'GET',
    			dataType:'JSON',
    			success:function(data){
    				if(data !== null){
	    				tblCgye.clear().draw();
	    				if(data.length !== 0){
		    				clear_form_elements('benefits-content');
		    				year_end_amount = 0;
		    				cash_gift_amount = 0;
		    				percentage = 0;
	    					year_end_amount = (data.amount) ? data.amount : 0;
	    					cash_gift_amount = (data.cash_gift_amount) ? data.cash_gift_amount : 0;
	    					tax_amount = (data.tax_amount) ? data.tax_amount : 0;
	    					deduction_amount = (data.deduction_amount) ? data.deduction_amount : 0;
	    					percentage = data.percentage;


		    				total = (parseFloat(year_end_amount) + parseFloat(cash_gift_amount));
		    				total = (total) ? commaSeparateNumber(parseFloat(total).toFixed(2)) : '';
		    				year_end_amount = (year_end_amount !== 0) ? commaSeparateNumber(parseFloat(year_end_amount).toFixed(2)) : '';
		    				cash_gift_amount = (cash_gift_amount !== 0) ? commaSeparateNumber(parseFloat(cash_gift_amount).toFixed(2)) : '';
		    				percentage = (percentage !== 0) ? percentage +'%': '';
							tblCgye.row.add( [
								cash_gift_amount,
								year_end_amount,
								percentage,
								'',
								total

							]).draw( false );

    					}
    				}
				}
			});
    	}else{
    		swal({
    			title: 'Select year and month first',
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-warning",
    			confirmButtonText: "Yes",
    			closeOnConfirm: false

    		});
    	}

    })

$(document).on('keyup','._searchname',function(){
	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
				type: "GET",
				url: base_url+module_prefix+module+'/show',
				data: {
					'q':$('._searchname').val(),
					'check_cgye':_checkcgye,
					'_year':_Year,
					'_month':_Month
				},
				beforeSend:function(){
				   		// $('.ajax-loader').css("visibility", "visible");

				   	},
				   	success: function(res){
				   		$(".sub-panelnamelist").html(res);

				   	},
				   	complete:function(){
				   		// $('.ajax-loader').css("visibility", "hidden");
				   	}
				 });
		},500);
});

$(document).on('change','#searchby',function(){
	var val = $(this).val();
	console.log(base_url+module_prefix+module)
	$.ajax({
		url:base_url+module_prefix+module+'/getSearchby',
		data:{'q':val},
		type:'GET',
		dataType:'JSON',
		success:function(data){

			arr = [];
			$.each(data,function(k,v){
				arr += '<option value='+v.id+'>'+v.name+'</option>';
			})

			$('#select_searchvalue').html(arr);
		}
	})

});

$(document).on('click','#btn_process_cgye',function(){
	console.log(_listId);
	if(_listId.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Process Cash Gift and Year End Bonus?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.processCgye();
			}else{
				return false;
			}
		});
	}

});

$.processCgye = function(){
	$.ajax({
		url:base_url+module_prefix+module+'/processCgye',
		data:{
			'_token':'{{ csrf_token() }}',
			'list_id':_listId,
			'year':_Year,
			'month':_Month,
			'cgye_id':cgye_id,
			'employee_id':employee_id,
			'tax_amount':tax_amount,
			'deduction_amount':deduction_amount
		},
		type:'POST',
		beforeSend:function(){
        	$('#btn_process_cgye').html('<i class="fa fa-spinner fa-spin"></i> Processing').prop('disabled',true);
        },
		success:function(data){
			par = JSON.parse(data);
			if(par.status){
				swal({
					title: par.response,
					type: "success",
					showCancelButton: false,
					confirmButtonClass: "btn-success",
					confirmButtonText: "Yes",
					closeOnConfirm: false
				});
				// window.location.href = base_url+module_prefix+module;
				_listId = [];
				$('#btn_process_cgye').html('<i class="fa fa-save"></i> Process').prop('disabled',false);
				$('.btnfilter').trigger('click');
			}else{

				swal({
					title: par.response,
					type: "warning",
					showCancelButton: false,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Yes",
					closeOnConfirm: false
				});
				$('#btn_process_cgye').html('<i class="fa fa-save"></i> Process').prop('disabled',false);

			}
		}
	});
}

$(document).on('click','#btn_save_cgye',function(){

	if(no_of_actual_work == null){
		swal("No of field is empty!", "", "warning");
	}else{
		$.saveCgye();
	}

});

$(document).on('click','#delete_cgye',function(){
	if(_listId.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Delete Cash Gift and Year End Bonus?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.deleteCgye();
			}else{
				return false;
			}
		});
	}
})

$.deleteCgye = function(){
	$.ajax({
		url:base_url+module_prefix+module+'/deleteCgye',
		data:{'empid':_listId,'year':_Year,'month':_Month,'_token':"{{ csrf_token() }}"},
		type:'post',
		beforeSend:function(){
			$('#delete_cgye').html('<i class="fa fa-spinner fa-spin"></i> Deleting').prop('disabled',true);
		},
		success:function(response){
			par = JSON.parse(response)

			if(par.status){
				swal({
					title: par.response,
					type: "success",
					showCancelButton: false,
					confirmButtonClass: "btn-success",
					confirmButtonText: "OK",
					closeOnConfirm: false
				})

				_listId = [];
				$('#delete_cgye').html('<i class="fas fa-minus-circle"></i> Delete').prop('disabled',false);
				$('.btnfilter').trigger('click');
			}
		}
	});
}


var timer;
$(document).on('click','.btnfilter',function(){
	year 	  = $('#select_year :selected').val();
	month 	  = $('#select_month :selected').val();

	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
			   type: "GET",
			   url: base_url+module_prefix+module+'/show',
			   data: {'year':_Year,'month':_Month,'checkcgye':_checkcgye },
			   beforeSend:function(){
			   		$('#loading').removeClass('hidden');
			   },
			   complete:function(){
			   		$('#loading').addClass('hidden');
			   },
			   success: function(res){
			   	// console.log(res);
			      $(".sub-panelnamelist").html(res);
			   }
			});
		},500);
});



});


</script>
@endsection