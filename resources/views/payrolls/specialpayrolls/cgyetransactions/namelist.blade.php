<table class="table scroll datatable" id="namelist">
	<thead>
		<tr>
			<th><input type="checkbox" name="check_all" id="check_all"></th>
			<th>Name</th>
		</tr>
	</thead>
	<tbody>
		@foreach($data as $key => $value)
			<tr data-empid="{{ $value->id }}" data-employee="{{ $value->getFullName()}}" >
				<td nowrap>
					<input type="checkbox" name="checked_emp_id[]" value="{{ $value->id  }}" class="emp_select" data-key="{{ $key }}" >
					<span style="padding-left: 2px;position: relative;">[{!! $key+1 !!}] {{ $value->lastname }} {{ $value->firstname }} {{ $value->middlename }}</span>
				</td>
			</tr>
		@endforeach

	</tbody>
</table>
