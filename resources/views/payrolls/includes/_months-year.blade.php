<div class="col-md-6" style="padding-left: 0px;">
	<select class="employee-type form-control font-style2 select2" id="select_month" name="month" placeholder="Month">
		@foreach($months as $key => $month)
		<option value="{{$key}}" {{ ($key == $current_month) ? 'selected' : '' }}>{{ $month }}</option>
		@endforeach
	</select>
</div>
<div class="col-md-6" style="padding-right: 0px;">
	<select class="employee-type form-control font-style2 select2" id="select_year" name="year">
		@foreach( range($latest_year,$earliest_year) as $i)
		<option value="{{$i}}">{{$i}}</option>
		@endforeach
	</select>
</div>