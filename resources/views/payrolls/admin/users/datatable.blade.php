<table id="tbl_benefits" class="table table-responsive datatable">
	<thead>
		<tr>
			<th>Name</th>
			<th>Username</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
			@foreach($data as $value)
			<tr data-id="{{ $value->id }}" data-username="{{ $value->username }}" data-access_type_id="{{ $value->access_type_id }}" data-password="{{ $value->password }}" data-name="{{ $value->name }}" data-employee_id="{{ $value->employee_id }}" class="text-center" >
				<td>{{ $value->name }}</td>
				<td>{{ $value->username }}</td>
                <td>
                	@if($value->name !== 'Admin')
					<a class="btn btn-xs btn-danger delete_item" data-function_name="deleteUser" data-id="{{ $value->id }}"><i class="fa fa-trash"></i> Delete</a>'
					@endif
				</td>
			</tr>
			@endforeach

	</tbody>
</table>
<script type="text/javascript">
$(document).ready(function(){

	 var table = $('#tbl_benefits').DataTable({
	 	'dom':'<lf<t>pi>',
	 	"paging": false,
	 	// "scrollY":"250px",
   //      "scrollCollapse": true,
	 });

	$('#tbl_benefits tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');

	    }else {

	        table.$('tr.selected').removeClass('selected');

	        $(this).addClass('selected');
	    }
	});

})
</script>
