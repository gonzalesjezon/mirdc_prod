@extends('app-front')

@section('content')
<div class="row">
	<div class="col-md-12">
		<hr>
		<div class="col-md-3" style="margin-top: 30px;">
			<div class="search-btn">
				<span>Search</span>
				<!-- <a class="btn btn-xs btn-danger btnfilter" style="float: right;line-height: 16px;margin-bottom: 3px;" ><i class="fa fa-filter"></i>Filter</a> -->
			</div>
			<div >
				<input type="text" name="filter_search" class="form-control search1">
			</div>
			<br>
			<div class="sub-panelnamelist ">
				{!! $controller->show() !!}
			</div>
		</div>
		<div class="col-md-9">
			<div class="row" style="padding: 20px;">
				<div class="col-md-12">
					{!! $controller->showLongevityPay() !!}
				</div>
			</div>
			<div class="row" style="margin:25px 0px 0px 5px;">
				<div class="col-md-12">
					<label id="employee_name"></label>
				</div>
			</div>
			<div class="row" style="margin-left: 2px;">
				<div class="col-md-12">
					<div class="button-wrapper" style="position: relative;top: 10px;left: 5px;" >
						<a class="btn btn-xs btn-info btn-savebg btn_new" id="newSummary" data-btnnew="newSummary" data-btncancel="cancelSummary" data-btnedit="editSummary" data-btnsave="saveSummary"><i class="fa fa-save"></i> New</a>

						<a class="btn btn-xs btn-info btn-editbg btn_edit hidden" id="editSummary" data-btnnew="newSummary" data-btncancel="cancelSummary" data-btnedit="editSummary" data-btnsave="saveSummary"><i class="fa fa-save"></i> Edit</a>

						<a class="btn btn-xs btn-info btn-savebg btn_save submit hidden" data-form="form" data-btnnew="newSummary" data-btncancel="cancelSummary" data-btnedit="editSummary" data-btnsave="saveSummary" id="saveSummary"><i class="fa fa-save"></i> Save</a>
						<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newSummary" data-btncancel="cancelSummary" data-form="myform" data-btnedit="editSummary" data-btnsave="saveSummary"id="cancelSummary"> Cancel</a>
					</div>
				</div>
			</div>
			<div class="row" style="padding: 20px;">
				<form method="POST" action="{{ url($module_prefix.'/'.$module)}}" onsubmit="return false" id="form" class="myform">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="employee_id" id="employee_id">
				<input type="hidden" name="longevity_id" id="longevity_id">
				<input type="hidden" name="employee_number" id="employee_number">
					<div class="col-md-3">
						<div class="form-group newSummary">
							<label>Date</label>
							<input type="text" name="longevity_date" id="longevity_date" class="form-control datepicker">
						</div>
						<div class="form-group newSummary">
							<label>Basic Salary</label>
							<input type="text" name="basic_salary_amount" id="basic_salary_amount" class="form-control onlyNumber">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group newSummary">
							<label>LP</label>
							<input type="text" name="lp" id="lp" class="form-control onlyNumber">
						</div>
						<div class="form-group newSummary">
							<label>Longevity Amount</label>
							<input type="text" name="longevity_amount" id="longevity_amount" class="form-control onlyNumber">
						</div>
					</div>
				</form>
			</div>
		</div>

	</div>


</div>
<!-- <div class="ajax-loader">
  <img src="{{ asset('images/ajax-loader1.gif') }}" class="img-responsive" />
</div> -->
<br>
@endsection

@section('js-logic1')
<script type="text/javascript">
$(document).ready(function(){
	// GENERATE YEAR
var year = [];
year += '<option ></option>';
for(y = 2018; y <= 2100; y++) {
    year += '<option value='+y+'>'+y+'</option>';
}
$('#select_year').html(year);

// GENERATE MONTH
month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
mArr = [];

mArr += '<option ></option>';
for ( m =  0; m <= month.length - 1; m++) {
	mArr += '<option '+month[m]+'>'+month[m]+'</option>';
}
$('#select_month').html(mArr);


// ************************************************
var _Year;
var _Month;
$(document).on('change','#select_year',function(){
	_Year = "";
	_Year = $(this).find(':selected').val();
	$('#year').val(_Year);

})
$(document).on('change','#select_month',function(){
	_Month = "";
	_Month = $(this).find(':selected').val();
	$('#month').val(_Month);
})

$('.select2').select2();

$('#select_month').select2({
    allowClear:true,
    placeholder: "Month",
});

$('#select_year').select2({
    allowClear:true,
    placeholder: "Year"
});

	var _monthlyRate;
	var taxAmountBR;
	var _taxDue;
	$('.newNonPlantilla :input').attr('disabled',true);
	$('.newNonPlantilla').attr('disabled',true);
	$('.newSummary :input').attr('disabled',true);
	$('.newSummary').attr('disabled',true);
	$('.newSalary :input').attr('disabled',true);
	$('.newSalary').attr('disabled',true);
	$('.newBenefit :input').attr('disabled',true);
	$('.newBenefit').attr('disabled',true);
	$('.newLoan :input').attr('disabled',true);
	$('.newLoan').attr('disabled',true);
	$('.newDeduct :input').attr('disabled',true);
	$('.newDeduct').attr('disabled',true);

	$('.btn_new').on('click',function(){
		$('#benefitinfo_id').val('');
		$('#deductinfo_id').val('');
		$('#loaninfo_id').val('');

		btnnew = $(this).data('btnnew');
		btnsave = $(this).data('btnsave');
		btncancel = $(this).data('btncancel');
		$('.'+btnnew+' :input').attr("disabled",false);
		$('.'+btnnew).attr('disabled',false);
		$('#select_taxspolicy').attr('disabled',true);
		$('#'+btnnew).addClass('hidden');
		$('#'+btnsave).removeClass('hidden');
		$('#'+btncancel).removeClass('hidden');
		_taxDue = 0;
	});

	$('.btn_edit').on('click',function(){
		btnnew = $(this).data('btnnew');
		btnsave = $(this).data('btnsave');
		btncancel = $(this).data('btncancel');
		btnedit = $(this).data('btnedit');
		btndelete = $(this).data('btndelete');
		$('.'+btnnew+' :input').attr("disabled",false);
		$('.'+btnnew).attr('disabled',false);
		$('#select_taxspolicy').attr('disabled',true);
		$('#'+btnnew).addClass('hidden');
		$('#'+btnedit).addClass('hidden');
		$('#'+btnsave).removeClass('hidden');
		$('#'+btndelete).removeClass('hidden');
		$('#'+btncancel).removeClass('hidden');
	});

	$('.btn_cancel').on('click',function(){
		$('#benefitinfo_id').val('');
		$('#deductinfo_id').val('');
		$('#loaninfo_id').val('');

		btnnew = $(this).data('btnnew');
		btnsave = $(this).data('btnsave');
		btncancel = $(this).data('btncancel');
		btnedit = $(this).data('btnedit');
		btndelete = $(this).data('btndelete');

		$('.'+btnnew+' :input').attr("disabled",true);
		$('.'+btnnew).attr('disabled',true);
		$('#'+btnnew).removeClass('hidden');
		$('#'+btnedit).addClass('hidden');
		$('#'+btnsave).addClass('hidden');
		$('#'+btncancel).addClass('hidden');
		$('#'+btndelete).addClass('hidden');
		$('.weekly').addClass('hidden');
		$('.semi-monthly').addClass('hidden');

		$('#employee_name').text('');
		$('#otherpayroll_id').val('');
		form = $(this).data('form');
		clear_form_elements(form);
		clear_form_elements('nonplantilla');
		$('.error-msg').remove();

	});

	$('.select2').select2();

	$('.onlyNumber').keypress(function (event) {
		return isNumber(event, this)
	});

	$(".onlyNumber").keyup(function(){
		amount  = $(this).val();
		if(amount == 0){
			$(this).val('');
		}else{
			plainAmount = amount.replace(/\,/g,'')
			$(this).val(commaSeparateNumber(plainAmount));
		}
	});


	// DATE PICKER
	$('.datepicker').datepicker({
		dateFormat:'yy-mm-dd'
	});


$(document).on('click','#namelist tr',function(){

	employee_id = $(this).data('empid');
	$('#employee_id').val(employee_id);
	console.log(employee_id);
	fullname = $(this).data('fullname');
	$('#employee_name').text(fullname);

	$.ajax({
		url:base_url+module_prefix+module+'/getLongevityPay',
		data:{'id':employee_id},
		type:'GET',
		dataType:'JSON',
		success:function(data){
			console.log(data);

			if(data.longevity !== null){
				basic_amount = (data.longevity.basic_amount) ? commaSeparateNumber(parseFloat(data.longevity.basic_amount).toFixed(2)) : '';
				longevity_amount = (data.longevity.longevity_amount) ? commaSeparateNumber(parseFloat(data.longevity.longevity_amount).toFixed(2)) : '';
				longevity_date = (data.longevity.longevity_date) ? data.longevity.longevity_date : '';
				employee_number = (data.longevity.employee_number) ? data.longevity.employee_number : '';
				lp = (data.longevity.lp) ? data.longevity.lp : '';

				$('#basic_salary_amount').val(basic_amount);
				$('#longevity_amount').val(longevity_amount);
				$('#longevity_date').val(longevity_date);
				$('#employee_number').val(employee_number);
				$('#lp').val(lp);
			}

			if(data.res !== null){

				// GENERATE TR FOR SALARY INFO TAB
				var tLp = $('#tbl_initial_salary').DataTable();

				tLp.clear().draw();

				$.each(data.res,function(k,v){

					tLp.row.add( [
			        	v.longevity_date,
			        	v.basic_pay_amount,
			        	v.lp,
			        	v.longevity_amount
			        ]).draw( false );

			        tLp.rows(k).nodes().to$().attr("data-id", v.id);
			        tLp.rows(k).nodes().to$().attr("data-employee_id", v.employee_id);
			        tLp.rows(k).nodes().to$().attr("data-basic_pay_amount", v.basic_pay_amount);
			        tLp.rows(k).nodes().to$().attr("data-longevity_date", v.longevity_date);
			        tLp.rows(k).nodes().to$().attr("data-lp", v.lp);
			        tLp.rows(k).nodes().to$().attr("data-longevity_amount", v.longevity_amount);
			        tLp.rows(k).nodes().to$().attr("data-btnnew", "newSalary");
			        tLp.rows(k).nodes().to$().attr("data-btnsave", "saveSalary");
			        tLp.rows(k).nodes().to$().attr("data-btnedit", "editSalary");
			        tLp.rows(k).nodes().to$().attr("data-btncancel", "cancelSalary");
				});
			}


		}
	});
});



var timer;
$(document).on('click','.btnfilter',function(){
	// $('input.search').addClass('searchSpinner');
	tools  = $('#tools-form').serialize()

	category  	= $('#select_searchvalue :selected').val();
	empstatus   = $('#emp_status :selected').val();
	emp_type    = $('#emp_type :selected').val();
	searchby    = $('#searchby :selected').val();

	clearTimeout(timer);
	timer = setTimeout(
				function(){
					$.ajax({
					   type: "GET",
					   url: base_url+module_prefix+module+'/show',
					   data: {'category':category,'empstatus':empstatus,'emp_type':emp_type,'searchby':searchby },
					   beforeSend:function(){
					   		// $('#loading').removeClass('hidden');
					   },
					   complete:function(){
					   		// $('#loading').addClass('hidden');
					   },
					   success: function(res){
					      $(".sub-panelnamelist").html(res);
					      // $('input.search').removeClass('searchSpinner');
					   }
					});
				},500);
})


$(document).on('keyup','.search1',function(){
	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
			   type: "GET",
			   url: base_url+module_prefix+module+'/show',
			   data: {"q":$('.search1').val(),'limit':$(".limit").val()},
			   beforeSend:function(){

			   },
			   success: function(res){
			      $(".sub-panelnamelist").html(res);

			   },
			   complete:function(){

			   }
			});
		},500);
});


function generateBenefitInfoTable(benefitinfo){
	var tBenefit = $('#tbl_benefitinfo').DataTable();

	tBenefit.clear().draw();

	$.each(benefitinfo,function(k,v){

		tBenefit.row.add( [
        	benefitinfo[k].benefit_effectivity_date,
        	benefitinfo[k].benefits.name,
			benefitinfo[k].benefit_description,
			benefitinfo[k].benefit_amount,
			benefitinfo[k].benefit_pay_period,
        ]).draw( false );

        tBenefit.rows(k).nodes().to$().attr("data-id", v.id);
        tBenefit.rows(k).nodes().to$().attr("data-employeeid", v.employee_id);
        tBenefit.rows(k).nodes().to$().attr("data-date", v.benefit_effectivity_date);
        tBenefit.rows(k).nodes().to$().attr("data-name", v.benefits.name);
        tBenefit.rows(k).nodes().to$().attr("data-description", v.benefit_description);
        tBenefit.rows(k).nodes().to$().attr("data-payperiod", v.benefit_pay_period);
        tBenefit.rows(k).nodes().to$().attr("data-paysub", v.benefit_pay_sub);
        tBenefit.rows(k).nodes().to$().attr("data-amount", v.benefit_amount);
        tBenefit.rows(k).nodes().to$().attr("data-benefitid", v.benefit_id);
        tBenefit.rows(k).nodes().to$().attr("data-datefrom", v.date_from);
        tBenefit.rows(k).nodes().to$().attr("data-dateto", v.date_to);
        tBenefit.rows(k).nodes().to$().attr("data-btnnew", "newBenefit");
        tBenefit.rows(k).nodes().to$().attr("data-btnsave", "saveBenefit");
        tBenefit.rows(k).nodes().to$().attr("data-btnedit", "editBenefit");
        tBenefit.rows(k).nodes().to$().attr("data-btndelete", "deleteBenefit");
        tBenefit.rows(k).nodes().to$().attr("data-btncancel", "cancelBenefit");


	});

	$('.newBenefit').attr('disabled',true);
	$('#newBenefit').removeClass('hidden');
	$('#saveBenefit').addClass('hidden');
	$('#editBenefit').addClass('hidden');
	$('#deleteBenefit').addClass('hidden');
	$('#cancelBenefit').addClass('hidden');
	$('.weekly').removeClass('show');
	$('.weekly').addClass('hidden');
	$('.semi-monthly').removeClass('show');
	$('.semi-monthly').addClass('hidden');
	clear_form_elements("myform3");
}

// # loan Info Table
function generateLoanInfoTable(loaninfo){
	var tLoan = $('#tbl_loaninfo').DataTable();

	tLoan.clear().draw();

	$.each(loaninfo,function(k,v){

		tLoan.row.add( [
        	loaninfo[k].loans.name,
        	loaninfo[k].loan_totalamount,
			loaninfo[k].loan_totalbalance,
			loaninfo[k].loan_amortization,
			loaninfo[k].loan_date_started,
			loaninfo[k].loan_date_end,
        ]).draw( false );

        tLoan.rows(k).nodes().to$().attr("data-id", v.id);
        tLoan.rows(k).nodes().to$().attr("data-employeeid", v.employee_id);
        tLoan.rows(k).nodes().to$().attr("data-loanamount", v.loan_totalamount);
        tLoan.rows(k).nodes().to$().attr("data-loanbalance", v.loan_totalbalance);
        tLoan.rows(k).nodes().to$().attr("data-amortization", v.loan_amortization);
        tLoan.rows(k).nodes().to$().attr("data-datestart", v.loan_date_started);
        tLoan.rows(k).nodes().to$().attr("data-dateend", v.loan_date_end);
        tLoan.rows(k).nodes().to$().attr("data-loanid", v.loan_id);
        tLoan.rows(k).nodes().to$().attr("data-payperiod", v.loan_pay_period);
        tLoan.rows(k).nodes().to$().attr("data-dategranted", v.loan_date_granted);
        tLoan.rows(k).nodes().to$().attr("data-dateterminated", v.loan_date_terminated);
        tLoan.rows(k).nodes().to$().attr("data-btnnew", "newLoan");
        tLoan.rows(k).nodes().to$().attr("data-btnsave", "saveLoan");
        tLoan.rows(k).nodes().to$().attr("data-btnedit", "editLoan");
        tLoan.rows(k).nodes().to$().attr("data-btncancel", "cancelLoan");
        tLoan.rows(k).nodes().to$().attr("data-btndelete", "deleteLoan");
	});

	$('.newLoan').attr('disabled',true);
	$('#newLoan').removeClass('hidden');
	$('#editLoan').addClass('hidden');
	$('#deleteLoan').addClass('hidden');
	$('#saveLoan').addClass('hidden');
	$('#cancelLoan').addClass('hidden');
	clear_form_elements("myform4");
}

function generateDeductionInfoTable(deductioninfo){
	var tDeduct = $('#tbl_deductioninfo').DataTable();

	tDeduct.clear().draw();

	$.each(deductioninfo,function(k,v){

		tDeduct.row.add( [
        	deductioninfo[k].deductions.name,
        	deductioninfo[k].deduct_amount,
			deductioninfo[k].deduct_date_start,
			deductioninfo[k].deduct_date_end,
			deductioninfo[k].deduct_pay_period,
        ]).draw( false );

        tDeduct.rows(k).nodes().to$().attr("data-id", v.id);
        tDeduct.rows(k).nodes().to$().attr("data-employeeid", v.employee_id);
        tDeduct.rows(k).nodes().to$().attr("data-deductionid", v.deduction_id);
        tDeduct.rows(k).nodes().to$().attr("data-amount", v.deduct_amount);
        tDeduct.rows(k).nodes().to$().attr("data-datestart", v.deduct_date_start);
        tDeduct.rows(k).nodes().to$().attr("data-dateend", v.deduct_date_end);
        tDeduct.rows(k).nodes().to$().attr("data-payperiod", v.deduct_pay_period);
        tDeduct.rows(k).nodes().to$().attr("data-dateterminated", v.deduct_date_terminated);
        tDeduct.rows(k).nodes().to$().attr("data-btnnew", "newDeduct");
        tDeduct.rows(k).nodes().to$().attr("data-btnsave", "saveDeduct");
        tDeduct.rows(k).nodes().to$().attr("data-btnedit", "editDeduct");
        tDeduct.rows(k).nodes().to$().attr("data-btncancel", "cancelDeduct");
        tDeduct.rows(k).nodes().to$().attr("data-btndelete", "deleteDeduct");
	});

	$('.newDeduct').attr('disabled',true);
	$('#newDeduct').removeClass('hidden');
	$('#saveDeduct').addClass('hidden');
	$('#editDeduct').addClass('hidden');
	$('#deleteDeduct').addClass('hidden');
	$('#cancelDeduct').addClass('hidden');
	clear_form_elements("myform5");
}



})
</script>
@endsection
