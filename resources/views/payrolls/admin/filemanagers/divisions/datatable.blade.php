<br><br>
<div class="col-md-12">
	<table class="table table-responsive datatable" id="tbl_divisions">
		<thead>
			<tr>
				<th>Code</th>
				<th>Name</th>
			</tr>
		</thead>
		<tbody class="text-center">
			@foreach($data as $value)
			<tr data-id="{{ $value->id }}" data-name="{{ $value->name }}" data-code="{{ $value->code }}" data-btnnew="newDivisions" data-btnedit="editDivisions" data-btnsave="saveDivisions" data-btncancel="cancelDivisions">
				<td>{{ $value->code }}</td>
				<td>{{ $value->name }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
<script type="text/javascript">
$(document).ready(function(){

	 var table = $('#tbl_divisions').DataTable({
	 	'dom':'<lf<t>pi>',
	 	"paging": false,
	 	"scrollY":"250px",
        "scrollCollapse": true,
	 });

	$('#tbl_divisions tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');

	       	office_id 	= $(this).data('id');
			code 			= $(this).data('code');
			name 			= $(this).data('name');

			$('#office_id').val(office_id);
			$('#code').val(code);
			$('#name').val(name);

			btnnew = $(this).data('btnnew');
			btnsave = $(this).data('btnsave');
			btnedit = $(this).data('btnedit');
			btncancel = $(this).data('btncancel');

			if(!$('#'+btnsave).is(':visible')){
				$('#'+btnedit).removeClass('hidden');
				$('#'+btncancel).removeClass('hidden');
				$('#'+btnnew).addClass('hidden');
			}


	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	} );

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	});


})
</script>
