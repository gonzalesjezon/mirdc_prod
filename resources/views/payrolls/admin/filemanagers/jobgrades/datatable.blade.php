<div class="col-md-12">
	<table class="table datatable" id="salarytable">
		<thead>
			<tr>
				<th>Job Grade</th>
				<th>Step 1</th>
				<th>Step 2</th>
				<th>Step 3</th>
				<th>Step 4</th>
				<th>Step 5</th>
				<th>Step 6</th>
				<th>Step 7</th>
				<th>Step 8</th>
			</tr>
		</thead>
		<tbody>
				@foreach($data as $value)
				<tr>
					<td>{{ $value->job_grade }}</td>
					<td>{{ $value->step1 }}</td>
					<td>{{ $value->step2 }}</td>
					<td>{{ $value->step3 }}</td>
					<td>{{ $value->step4 }}</td>
					<td>{{ $value->step5 }}</td>
					<td>{{ $value->step6 }}</td>
					<td>{{ $value->step7 }}</td>
					<td>{{ $value->step8 }}</td>
				</tr>
				@endforeach
			
		</tbody>
	</table>
</div>
<script type="text/javascript">
$(document).ready(function(){

	var table = $('#salarytable').DataTable({
        'dom':'<lf<t>pi>',
		"paging": false,
	 	"scrollY":"250px",
        "scrollCollapse": true,
	});

	$('#salarytable tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');

	        rows = table.rows(0).data();

	        _code = rows[0][0];


	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	});

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	});

})
</script>
