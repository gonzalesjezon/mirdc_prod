@extends('app-filemanager')

@section('filemanager-content')
<div id="deductions" class="benefits-wrapper">
	<div class="col-md-12" style="padding-bottom: 10px;">
		<div class="col-md-6">
			<label>DEDUCTIONS</label>
		</div>

	</div>

	<div class="sub-panel">
		{!! $controller->show() !!}
	</div>

	<div class="formcontent">
		<form method="POST" action="{{ url($module_prefix.'/'.$module)}}" onsubmit="return false" id="form" class="myform">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="id" id="deduction_id">
			<div class="col-md-12">
				<div class="box-1 button-style-wrapper">
					<a class="btn btn-xs btn-info btn-savebg btn_new" id="btn_savededuct"><i class="fa fa-save"></i> New</a>
					<a class="btn btn-xs btn-info btn-savebg btn_save submit hidden" id="btn_savebenefits"><i class="fa fa-save"></i> Save</a>
					<a class="btn btn-xs btn-success btn-editbg btn_edit"><i class="fa fa-edit"></i> Edit</a>
					<a class="btn btn-xs btn-danger btn_cancel"> Cancel</a>
				</div>
			</div>
			<div class="benefits-content style-box2">
				<div class="col-md-12">
					<div class="col-md-7">
						<table class="table borderless" style="border: none;">
							<tr>
								<td><label>Code</label></td>
								<td>
									@if ($errors->has('code'))
									    <span class="text-danger">{{ $errors->first('code') }}</span>
									@endif
									<input type="text" name="code" id="code" class="form-control font-style2">
								</td>
							</tr>
							<tr>
								<td><label>Name</label></td>
								<td>
									@if ($errors->has('name'))
									    <span class="text-danger">{{ $errors->first('name') }}</span>
									@endif
									<input type="text" name="name" id="name" class="form-control font-style2">
								</td>
							</tr>
							<tr>
								<td><label>Type</label></td>
								<td>
									@if ($errors->has('tax_type'))
									    <span class="text-danger">{{ $errors->first('tax_type') }}</span>
									@endif
									<select name="tax_type" id="tax_type" class="form-control font-style2">
										<option value=""></option>
										<option value="Non Taxable">Non Taxable</option>
										<option value="Taxable">Taxable</option>
									</select>
								</td>
							</tr>
							<tr>
								<td><label>Amount</label></td>
								<td>
									@if ($errors->has('amount'))
									    <span class="text-danger">{{ $errors->first('amount') }}</span>
									@endif
									<input type="text" name="amount" id="amount" class="form-control font-style2 onlyNumber">
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection

@section('js-logic2')
<script type="text/javascript">
	$(document).ready(function(){

		$('.benefits-content :input').attr("disabled",true);
		$('.btn_new').on('click',function(){
			$('.benefits-content :input').attr("disabled",false);
			$('.btn_new').addClass('hidden');
			$('.btn_edit').addClass('hidden');
			$('.btn_save').removeClass('hidden');
		});
		$('.btn_edit').on('click',function(){
			$('.benefits-content :input').attr("disabled",false);
			$('.btn_edit').addClass('hidden');
			$('.btn_new').addClass('hidden');
			$('.btn_save').removeClass('hidden');
		});
		$('.btn_cancel').on('click',function(){
			$('.benefits-content :input').attr("disabled",true);
			$('.btn_new').removeClass('hidden');
			$('.btn_save').addClass('hidden');
			$('.btn_edit').removeClass('hidden');
		});


		$(".btn_cancel").click(function() {
			myform = "myform";
			clear_form_elements(myform);
			$('.error-msg').remove();
			$('#deduction_id').val();
		});

		$('.onlyNumber').keypress(function (event) {
	    	return isNumber(event, this)
		});

		$(".onlyNumber").keyup(function(){
			amount  = $(this).val();
			if(amount == 0){
				$(this).val('');
			}else{
				plainAmount = amount.replace(/\,/g,'')
				$(this).val(commaSeparateNumber(plainAmount));
			}
		})

	})
</script>
@endsection