<br><br>
<div class="col-md-12">
	<table class="table table-responsive datatable" id="tbl_positions">
		<thead>
			<tr>
				<th>Rate</th>
				<th>Status</th>
				<th>Created</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody class="text-center">
			@foreach($data as $value)
			<tr data-id="{{ $value->id }}" data-rate="{{ $value->rate }}" data-status="{{ $value->status }}"  data-btnnew="newPositions" data-btnedit="editPositions" data-btnsave="savePositions" data-btncancel="cancelPositions">
				<td>{{ $value->rate*100 }}</td>
				<td>{{ $value->status }}</td>
				<td>{{ $value->created_at }}</td>
				<td><a class="btn btn-xs btn-danger delete_item" data-function_name="delete" data-id="{{ $value->id }}"><i class="fa fa-trash"></i> Delete</a></td>
			</tr>
			@endforeach
		</tbody>

	</table>
</div>
<script type="text/javascript">
$(document).ready(function(){

	 var table = $('#tbl_positions').DataTable({
	 	'dom':'<lf<t>pi>',
	 	"paging": false,
	 	"scrollY":"250px",
        "scrollCollapse": true,
	 });

	$('#tbl_positions tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');

	       	rate_id 	= $(this).data('id');
			rate 		= $(this).data('rate');
			status 		= $(this).data('status');

			$('#rate_id').val(rate_id);
			$('#rate').val(rate*100);
			$('#status').val(status);

			btnnew = $(this).data('btnnew');
			btnsave = $(this).data('btnsave');
			btnedit = $(this).data('btnedit');
			btncancel = $(this).data('btncancel');

			if(!$('#'+btnsave).is(':visible')){
				$('#'+btnedit).removeClass('hidden');
				$('#'+btncancel).removeClass('hidden');
				$('#'+btnnew).addClass('hidden');
			}


	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	} );

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	});


$(document).on('click','.delete_item',function(){
	id 	= $(this).data('id');
	function_name = $(this).data('function_name')

	if(id){
		swal({
			title: "Delete?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.ajax({
					url:base_url+module_prefix+module+'/'+function_name,
					data:{
						'id':id,
						'_token':"{{ csrf_token() }}"
					},
					type:'post',
					dataType:'JSON',
					success:function(res){
						swal({
							  title: 'Deleted Successfully!',
							  type: "warning",
							  showCancelButton: false,
							  confirmButtonClass: "btn-warning",
							  confirmButtonText: "OK",
							  closeOnConfirm: false
						}).then(function(isConfirm){
							if(isConfirm.value == true){
								window.location.href = base_url+module_prefix+module;
							}else{
								return false;
							}
						})

					}

				})
			}else{
				return false;
			}
		});
	}
})

})
</script>
