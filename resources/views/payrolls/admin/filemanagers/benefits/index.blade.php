@extends('app-filemanager')

@section('filemanager-content')
<div id="benefits" class="benefits-wrapper">
	<div class="col-md-12" style="padding-bottom: 10px;">
		<div class="col-md-6">
			<label>BENEFITS</label>
		</div>

	</div>

	<div class="sub-panel" style="z-index: 1;">

		{!! $controller->show() !!}

	</div>
	<div class="formcontent">
		<form method="POST" action="{{ url($module_prefix.'/'.$module)}}" onsubmit="return false" id="form" class="myform">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="benefit_id" id="benefit_id">
			<div class="box-1 button-style-wrapper" style="z-index: 2">
				<div class="col-md-12">
					<a class="btn btn-xs btn-info btn-savebg btn_new" id="btn_savebenefits"><i class="fa fa-save"></i> New</a>
					<a class="btn btn-xs btn-info btn-savebg btn_save hidden submit"><i class="fa fa-save"></i> Save </a>
					<a class="btn btn-xs btn-success btn-editbg btn_edit"><i class="fa fa-edit"></i> Edit</a>
					<a class="btn btn-xs btn-danger btn_cancel"> Cancel</a>
				</div>
			</div>

			<div class="benefits-content style-box2">
				<div class="col-md-12">
					<div class="col-md-7">
						<table class="table borderless" style="border: none;">
							<tr>
								<td><label>Code</label></td>
								<td>
									@if ($errors->has('code'))
									    <span class="text-danger">{{ $errors->first('code') }}</span>
									@endif
									<input type="text" name="code" id="code" class="form-control font-style2"  >

								</td>
							</tr>
							<tr>
								<td><label>Name</label></td>
								<td>
									@if ($errors->has('name'))
									    <span class="text-danger">{{ $errors->first('name') }}</span>
									@endif
									<input type="text" name="name" id="name" class="form-control font-style2">
								</td>
							</tr>
							<tr>
								<td><label>Remarks</label></td>
								<td>
									@if ($errors->has('remarks'))
									    <span class="text-danger">{{ $errors->first('remarks') }}</span>
									@endif
									<select name="remarks" id="remarks" class="form-control font-style2">
										<option value=""></option>
										<option value="pdiem">For benefit transactions</option>
									</select>
								</td>
							</tr>
							<!-- <tr>
								<td><label>Type</label></td>
								<td>
									@if ($errors->has('tax_type'))
									    <span class="text-danger">{{ $errors->first('tax_type') }}</span>
									@endif
									<select name="tax_type" id="tax_type" class="form-control font-style2">
										<option value=""></option>
										<option value="Non Taxable">Non Taxable</option>
										<option value="Taxable">Taxable</option>
										<option value="Taxable in excess of threshold">Taxable in excess of threshold</option>
									</select>
								</td>
							</tr>
							<tr>
								<td><label>Computation Type</label></td>
								<td>
									@if ($errors->has('computation_type'))
									    <span class="text-danger">{{ $errors->first('computation_type') }}</span>
									@endif
									<select name="computation_type" id="computation_type" class="form-control font-style2">
										<option></option>
										<option value="Based on Attendance">Based on Attendance</option>
										<option value="Fixed Monthly">Fixed Monthly</option>
									</select>
								</td>
							</tr>
							<tr>
								<td><label>Amount</label></td>
								<td>
									<input type="text" name="amount" id="amount" class="form-control onlyNumber">
								</td>
							</tr> -->
							<!-- <tr> -->
						</table>
					</div>
				</div>

			</div>
		</form>

	</div>

</div>


@endsection
@section('js-logic2')
<!-- <script src="{{ asset('js/payroll-vue/benefits.js') }} "></script> -->
<script type="text/javascript">
	$(document).ready(function(){

		$('.benefits-content :input').attr("disabled",true);
		$('.btn_new').on('click',function(){
			$('.benefits-content :input').attr("disabled",false);
			$('.btn_new').addClass('hidden');
			$('.btn_edit').addClass('hidden');
			$('.btn_save').removeClass('hidden');
		});
		$('.btn_edit').on('click',function(){
			$('.benefits-content :input').attr("disabled",false);
			$('.btn_edit').addClass('hidden');
			$('.btn_new').addClass('hidden');
			$('.btn_save').removeClass('hidden');
		});
		$('.btn_cancel').on('click',function(){
			$('.benefits-content :input').attr("disabled",true);
			$('.btn_new').removeClass('hidden');
			$('.btn_save').addClass('hidden');
			$('.btn_edit').removeClass('hidden');
		});


		$(".btn_cancel").click(function() {
			myform = "myform";
			clear_form_elements(myform);
			$('#benefit_id').val('');
			$('.error-msg').remove();
		});

		$('#tbl_benefits tr').on('click',function(){
			var id = 0;
			id = $(this).data('id');
			$.ajax({
				url:base_url+module_prefix+module+'/getItem',
				data:{'id':id},
				type:'GET',
				dataType:'JSON',
				success:function(data){

					$('#code').val(data.code);
					$('#name').val(data.name);
					$('#tax_type').val(data.tax_type);
					$('#de_minimis_type').val(data.de_minimis_type);
					$('#computation_type').val(data.computation_type);
					$('#amount').val(data.amount);
					$('#payroll_group').val(data.payroll_group);
					$('#itr_classification').val(data.itr_classification);
					$('#remarks').val(data.remarks);
					$('#alphalist_classification').val(data.alphalist_classification);
					$('#benefit_id').val(data.id);
				}
			})
		});

	    $('.onlyNumber').keypress(function (event) {
	    	return isNumber(event, this)
		});

		$(".onlyNumber").keyup(function(){
			amount  = $(this).val();
			if(amount == 0){
				$(this).val('');
			}else{
				plainAmount = amount.replace(/\,/g,'')
				$(this).val(commaSeparateNumber(plainAmount));
			}
		})

	});

</script>
@endsection

