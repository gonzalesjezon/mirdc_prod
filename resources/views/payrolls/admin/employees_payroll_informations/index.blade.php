@extends('app-front')

@section('content')
<div class="row">
	<hr>
	<div class="col-md-3">
		<h5 >Filter Employee By</h5>
		<table class="table borderless" style="border:none;">
			<tr>
				<td><span>Search by:</span></td>
				<td>
					<select class="search-by form-control font-style2 select2 " id="searchby" name="searchby">
						<option value=""></option>
						<option value="company">Company</option>
						<option value="department">Department</option>
						<option value="office">Offices</option>
						<option value="division">Divisions</option>
						<option value="position">Positions</option>
					</select>
				</td>
			</tr>
			<tr>
				<td><span>Search value:</span></td>
				<td>
					<select class="search-value form-control font-style2 select2 " name="select_searchvalue" id="select_searchvalue">
						<option value=""></option>
					</select>
				</td>
			</tr>
			<tr>
				<td><span>Employment Status:</span></td>
				<td>
					<select class="form-control font-style2 select2" name="emp_status" id="emp_status">
						<option value=""></option>
						<option value="plantilla">Plantilla</option>
						<option value="nonplantilla">Non Plantilla</option>
					</select>
				</td>
			</tr>
			<tr>
				<td><span>Employee type:</span></td>
				<td>
					<select class="employee-type form-control font-style2 select2" name="emp_type" id="emp_type">
						<option value=""></option>
						<option value="active">Active</option>
						<option value="inactive">InActive</option>
					</select>
				</td>
			</tr>
		</table>
		<div class="search-btn">
			<span>Search</span>
			<a class="btn btn-xs btn-danger btnfilter" style="float: right;line-height: 16px;margin-bottom: 3px;" ><i class="fa fa-filter"></i>Filter</a>
		</div>
		<div >
			<input type="text" name="filter_search" class="form-control search1">
		</div>
		<br>
		<div class="sub-panelnamelist ">
			{!! $controller->show() !!}
		</div>
	</div>

	<div class="col-md-9">
		<div class="col-md-12">
				<div class="col-md-6">
					<div class="newSummary-name">
						<label id="lbl_empname" style="text-transform: uppercase;"></label>
					</div>
				</div>
				<div class="col-md-6 text-right" >
					<a class="btn btn-success" id="recompute"><i class="fa fa-cog"></i> Recompute</a>
				</div>
			</div>
		<br>
		<div class="tab-container" id="plantilla">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#empsummary">Summary</a></li>
				<!-- <li><a href="#taxinfo">Tax Info</a></li> -->
				<li><a href="#salaryinfo" >Salary Info</a></li>
				<li><a href="#benefitsinfo" class="checkEmployeeinfo">Benefits Info</a></li>
				<li><a href="#loansinfo" class="checkEmployeeinfo">Loans Info</a></li>
				<li><a href="#deducinfo" class="checkEmployeeinfo">Deduction Info</a></li>
				<!-- <li><a href="#bonusinfo">Bonus Info</a></li> -->
			</ul>

			<div class="tab-content">
				<!-- EMPLOYEE SUMMARY -->

				<div id="empsummary" class="tab-pane fade in active">
					<div class="col-md-12">
						<div class="button-wrapper" style="position: relative;top: 10px;left: 5px;" >
							<a class="btn btn-xs btn-info btn-savebg btn_new" id="newSummary" data-btnnew="newSummary" data-btncancel="cancelSummary" data-btnedit="editSummary" data-btnsave="saveSummary"><i class="fa fa-save"></i> New</a>

							<a class="btn btn-xs btn-info btn-editbg btn_edit hidden" id="editSummary" data-btnnew="newSummary" data-btncancel="cancelSummary" data-btnedit="editSummary" data-btnsave="saveSummary"><i class="fa fa-save"></i> Edit</a>

							<a class="btn btn-xs btn-info btn-savebg btn_save submitme hidden" data-form="form" data-btnnew="newSummary" data-btncancel="cancelSummary" data-btnedit="editSummary" data-btnsave="saveSummary" id="saveSummary"><i class="fa fa-save"></i> Save</a>
							<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newSummary" data-btncancel="cancelSummary" data-form="myform" data-btnedit="editSummary" data-btnsave="saveSummary"id="cancelSummary"> Cancel</a>
						</div>
						<form method="POST" action="{{ url($module_prefix.'/'.$module)}}" onsubmit="return false" id="form" class="myform">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="tax_bracket" id="tax_bracket">
							<input type="hidden" name="tax_bracket_amount" id="tax_bracket_amount">
							<input type="hidden" name="tax_inexcess" id="tax_inexcess">
							<input type="hidden" name="id" class="employee_info_id">
							<input type="hidden" name="employee_id" class="employee_id">
							<input type="hidden" name="c_range" id="c_range">
							<div class="col-md-4">
								<div class="border-style02">
									<table class="table borderless">
										<tr>
											<td>
												<span>Employee Status</span>
											</td>
											<td>
												<input type="text" name="empstatus" id="empstatus" class="form-control font-style2 "  readOnly>
											</td>
										</tr>
										<tr>
											<td>
												<span>Employee No.</span>
											</td>
											<td>
												<input type="text" name="emp_no" id="emp_no" class="form-control font-style2" readOnly>
											</td>
										</tr>
										<tr>
											<td>
												<span>BP No.</span>
											</td>
											<td>
												<input type="text" name="bp_no" id="bp_no" class="form-control font-style2 newSummary">
											</td>
										</tr>
									</table>
								</div>
								<div class="border-style02 margintop-25">
									<table class="table borderless">
										<tr>
											<td>
												<span>Position Item No.</span>
											</td>
											<td>
												<input type="text" name="position_itemno" id="position_itemno" class="form-control font-style2" readOnly>
											</td>
										</tr>
										<tr>
											<td>
												<span>Position</span>
											</td>
											<td>
												<input type="text" name="position" id="position" class="form-control font-style2" readOnly>
											</td>
										</tr>
										<tr>
											<td>
												<span>Salary Grade</span>
											</td>
											<td>
												<input type="text" name="salary_grade" id="salary_grade" class="form-control font-style2 _input" readOnly>
											</td>
										</tr>
										<tr>
											<td>
												<span>Step Increment</span>
											</td>
											<td>
												<input type="text" name="input_stepinc" id="input_stepinc" class="form-control font-style2 _input" readOnly>
											</td>
										</tr>
										<tr>
											<td>
												<span>Office</span>
											</td>
											<td>
												<input type="text" name="office" id="office" class="form-control font-style2 _input" readOnly>
											</td>
										</tr>
										<tr>
											<td>
												<span>Division</span>
											</td>
											<td>
												<input type="text" name="division" id="division" class="form-control font-style2 _input" readOnly>
											</td>
										</tr>

										<tr>
											<td>
												<span>Resp Center</span>
											</td>
											<td>
												<select class="form-control font-style2 newSummary customselect" id="select_respcenter _input">
													<option></option>
												</select>
											</td>
										</tr>
									</table>
								</div>

								<div class="col-md-12" style="clear: both;width: 206%;position: relative;top: -32px;left: -4px;">
									<div class="border-style2">
										<table class="table borderless">
											<tr class="text-center">
												<td>Government Policy / Contribution</td>
												<td>Policy</td>
												<td>Contribution</td>
											</tr>
											<tr>
												<td><span>GSIS Policy</span></td>
												<td>
													<select class="font-style2 form-control newSummary customselect" name="gsispolicy_id" id="select_gsispolicy">
														<option value=""></option>
														@foreach($gsis as $value)
														<option data-policytype="{{ $value->policy_type }}" data-percent="{{ $value->ee_percentage }}" data-erpercent="{{ $value->er_percentage }}" value="{{ $value->id }}" >{{ $value->policy_name }}</option>
														@endforeach
													</select>
												</td>
												<td>
													<input type="text" name="gsis_contribution" id="gsis_contribution" class="form-control font-style2 onlyNumber newSummary ">
													<input type="hidden" name="er_gsis_share" id="er_gsis_share">
												</td>
											</tr>
											<tr>
												<td><span>Philhealth Policy</span></td>
												<td>
													<select class="font-style2 form-control newSummary customselect" id="select_philhealthpolicy" name="philhealthpolicy_id">
														<option value=""></option>
														@foreach($philhealth as $value)
														<option value="{{ $value->id }}" data-policytype="{{ $value->policy_type }}" data-above="{{ $value->above }}" data-below="{{ $value->below }}">{{ $value->policy_name }}</option>
														@endforeach
													</select>
												</td>
												<td>

													<input type="text" name="philhealth_contribution" id="philhealth_contribution" class="form-control font-style2 onlyNumber newSummary ">
													<input type="hidden" name="er_philhealth_share" id="er_philhealth_share">
												</td>
											</tr>
											<tr>
												<td><span>Pagibig Policy</span></td>
												<td>
													<select class="font-style2 form-control newSummary customselect" id="select_pagibigbpolicy" name="pagibigpolicy_id">
														<option value=""></option>
														@foreach($pagibig as $value)
														<option value="{{ $value->id }}" data-policytype="{{ $value->policy_type }}">{{ $value->policy_name }}</option>
														@endforeach
													</select>
												</td>
												<td>
													<input type="text" name="pagibig_contribution" id="pagibig_contribution" class="form-control font-style2 onlyNumber newSummary">
													<input type="hidden" name="er_pagibig_share" id="er_pagibig_share">
												</td>
											</tr>
											<tr>
												<td  colspan="2"></td>
												<td>
													<input type="text" name="pagibig_personal" id="personal_share" class="form-control font-style2 onlyNumber newSummary" placeholder="Personal Share">
												</td>
											</tr>
												<td><span>Pagibig II</span></td>
												<td></td>
												<td >
													<input type="text" name="pagibig2" id="pagibig2" class="form-control font-style2 onlyNumber newSummary">
												</td>
											<tr>
												<td><span>Tax Policy</span></td>
												<td>
													<select class="font-style2 form-control newSummary customselect" id="select_taxspolicy" name="taxpolicy_id">
														<option value=""></option>
														@foreach($tax_policy as $value)
														<option data-policytype="{{ $value->policy_type }}" data-policyname="{{ $value->policy_name}}" value="{{ $value->id }}" >{{ $value->policy_name }}</option>
														@endforeach
													</select>
												</td>
												<td>
													<input type="text" name="tax_contribution" id="tax_contribution" class="form-control font-style2 onlyNumber" readOnly>
												</td>
											</tr>

										</table>
									</div>
								</div>
							</div>
							<div class="col-md-4">

								<div class="border-style2">
									<table class="table borderless">
										<tr>
											<td>
												<span>Monthly Rate</span>
											</td>
											<td>
												<input type="text" name="monthly_rate_amount" class="form-control font-style2 onlyNumber _input" id="monthly_rate" readOnly />
											</td>
										</tr>
										<tr>
											<td>
												<span>Daily Rate</span>
											</td>
											<td>
												<input type="text" name="daily_rate_amount" class="form-control font-style2 onlyNumber _input" id="daily_rate" readOnly />
											</td>
										</tr>
										<tr>
											<td>
												<span>Weekly Rate</span>
											</td>
											<td>
												<input type="text" name="weekly_rate" class="form-control font-style2 onlyNumber _input" id="weekly_rate" readOnly />
											</td>
										</tr>
										<tr>
											<td>
												<span>Annual Rate</span>
											</td>
											<td>
												<input type="text" name="annual_rate_amount" class="form-control font-style2 onlyNumber _input" id="annual_rate" readOnly />
											</td>
										</tr>

										<tr>
											<td>
												<span>Wages Status</span>
											</td>
											<td>
												<select class="form-control font-style2 newSummary customselect" name="wagestatus_id" id="wagestatus_id">
													<option value=""></option>
													@foreach($wagerate as $value)
													<option value="{{ $value->id }}">{{ $value->wage_region }}</option>
													@endforeach
												</select>
											</td>
										</tr>
										<tr>
											<td>
												<span>Union Dues</span>
											</td>
											<td>
												<select class="form-control font-style2 newSummary customselect" id="select_uniondues">
													<option></option>
												</select>

											</td>
										</tr>
										<tr>
											<td>
												<span>Provident Fund</span>
											</td>
											<td>
												<select class="form-control font-style2 newSummary customselect" name="providentfund_id" id="providentfund_id">
													<option value=""></option>
												</select>
											</td>
										</tr>
									</table>

								</div>
								<div class="border-style2 margintop-25">
									<table class="table borderless">
										<tr>
											<td class="text-center" colspan="2"><span>For Job Order/Contractual</span></td>
											<td></td>
										</tr>
										<tr>
											<td>
												<span>Assumption to Duty</span>
											</td>
											<td>
												<input type="text" class="form-control font-style2 _input" id="assumption_to_duty" name="assumption_to_duty" readOnly />
											</td>
											<td></td>
										</tr>
										<tr>
											<td>
												<span>End of Contract</span>
											</td>
											<td>
												<input type="text" class="form-control font-style2  _input" id="end_of_contract" name="end_of_contract" readOnly />
											</td>
											<td></td>
										</tr>
									</table>
								</div>
							</div>
							<div class="col-md-4">
								<div class="border-style2 ">
									<table class="table borderless newSummary">
										<tr>
											<td><span>TIN</span></td>
											<td>
												<input type="text" name="tax_id_number" id="tax_id_number" class="form-control font-style2">
											</td>
										</tr>
										<tr>
											<td><span>ATM No</span></td>
											<td>
												<input type="text" name="atm_no" id="atm_no" class="form-control font-style2">
											</td>
										</tr>
										<tr>
											<td>
												<span>Bank</span>
											</td>
											<td>
												<select class="form-control font-style2 customselect"  id="select_bank" name="bank_id">
													<option value=""></option>
													@foreach($bank as $value)
														<option data-bankbranch="{{ $value->branch_name }}" value="{{ $value->id }}"  >{{ $value->name }}</option>
													@endforeach
												</select>
											</td>
										</tr>
										<tr>
											<td>
												<span>Bank Branch</span>
											</td>
											<td>
												<input type="text" class="form-control font-style2 customselect" id="bank_branch" name="bank_branch" readonly />
											</td>
										</tr>
									</table>
								</div>
								<div class="border-style2 margintop-25 newSummary">
									<table class="table borderless">
										<tr>
											<td>
												<span>Overtime Balance</span>
											</td>
											<td>
												<input type="text" class="form-control font-style2 onlyNumber" id="overtime_balance_amount" name="overtime_balance_amount" />

											</td>
										</tr>
									</table>
								</div>
						</form>

						</div>
					</div>
				</div>

				<!-- EMPLOYEE SUMMARY -->

				<!-- SALARY INFO -->
				<div id="salaryinfo" class="tab-pane fade in ">
						<div class="sub-panel" style="margin-top: 50px;z-index: 1;">
							{!! $controller->showSalaryinfo() !!}
						</div>
						<!-- @include('payrolls.admin.employees_payroll_informations._form-salaryinfo') -->
				</div>
				<!-- SALARY INFO -->

				<!-- BENEFITS INFO -->
				<div id="benefitsinfo" class="tab-pane fade in ">

					<div class="sub-panel" style="margin-top: 50px;z-index: 1;">
						{!! $controller->showBenefitinfo() !!}
					</div>
					@include('payrolls.admin.employees_payroll_informations._form-benefit_info')
				</div>
				<!-- BENEFITS INFO -->

				<!-- LOANS INFO -->
				<div id="loansinfo" class="tab-pane fade in">

					<div class="sub-panel" style="margin-top: 50px;z-index: 1;">

							{!! $controller->showLoaninfo() !!}

					</div>
					@include('payrolls.admin.employees_payroll_informations._form-loan_info')
				</div>
				<!-- LOANS INFO -->

				<!-- DEDUCTION INFO -->
				<div id="deducinfo" class="tab-pane fade in">
					<div class="sub-panel" style="margin-top: 50px;z-index: 1;">
						{!! $controller->showDeductioninfo() !!}
					</div>
					@include('payrolls.admin.employees_payroll_informations._form-deduction_info')
				</div>
				<!-- DEDUCTION INFO -->

			</div>
		</div>
		<!-- NON PLANTILLA -->
		<div class="tab-container hidden" id="nonplantilla">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#">Summary</a></li>
			</ul>
			@include('payrolls.admin.employees_payroll_informations._form-nonplantilla')
		</div>
	</div>
</div>
<!-- <div class="ajax-loader">
  <img src="{{ asset('images/ajax-loader1.gif') }}" class="img-responsive" />
</div> -->
<br>
@endsection

@section('js-logic1')
<script type="text/javascript">
$(document).ready(function(){
	var _monthlyRate;
	var taxAmountBR;
	var _taxDue;

	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
	    year += '<option value='+y+'>'+y+'</option>';
	}
	$('#loan_year').html(year);

	// GENERATE MONTH
	month = [
		"January",
		"February",
		"March",
		"April",
		"May",
		"June",
		"July",
		"August",
		"September",
		"October",
		"November",
		"December"
	];
	mArr = [];

	mArr += '<option ></option>';
	for ( m =  0; m <= month.length - 1; m++) {
		mArr += '<option '+month[m]+' value='+month[m]+'>'+month[m]+'</option>';
	}

	$('#loan_month').html(mArr);

	$('.newNonPlantilla :input').attr('disabled',true);
	$('.newNonPlantilla').attr('disabled',true);
	$('.newSummary :input').attr('disabled',true);
	$('.newSummary').attr('disabled',true);
	$('.newSalary :input').attr('disabled',true);
	$('.newSalary').attr('disabled',true);
	$('.newBenefit :input').attr('disabled',true);
	$('.newBenefit').attr('disabled',true);
	$('.newLoan :input').attr('disabled',true);
	$('.newLoan').attr('disabled',true);
	$('.newDeduct :input').attr('disabled',true);
	$('.newDeduct').attr('disabled',true);

	$('.btn_new').on('click',function(){
		$('#benefitinfo_id').val('');
		$('#deductinfo_id').val('');
		$('#loaninfo_id').val('');

		btnnew = $(this).data('btnnew');
		btnsave = $(this).data('btnsave');
		btncancel = $(this).data('btncancel');
		$('.'+btnnew+' :input').attr("disabled",false);
		$('.'+btnnew).attr('disabled',false);
		$('#'+btnnew).addClass('hidden');
		$('#'+btnsave).removeClass('hidden');
		$('#'+btncancel).removeClass('hidden');
		_taxDue = 0;
	});

	$('.btn_edit').on('click',function(){
		btnnew = $(this).data('btnnew');
		btnsave = $(this).data('btnsave');
		btncancel = $(this).data('btncancel');
		btnedit = $(this).data('btnedit');
		btndelete = $(this).data('btndelete');
		$('.'+btnnew+' :input').attr("disabled",false);
		$('.'+btnnew).attr('disabled',false);
		$('#'+btnnew).addClass('hidden');
		$('#'+btnedit).addClass('hidden');
		$('#'+btnsave).removeClass('hidden');
		$('#'+btndelete).removeClass('hidden');
		$('#'+btncancel).removeClass('hidden');
	});

	$('.btn_cancel').on('click',function(){
		$('#benefit_info_id').val('');
		$('#deduct_info_id').val('');
		$('#loani_nfo_id').val('');

		btnnew = $(this).data('btnnew');
		btnsave = $(this).data('btnsave');
		btncancel = $(this).data('btncancel');
		btnedit = $(this).data('btnedit');
		btndelete = $(this).data('btndelete');

		$('.'+btnnew+' :input').attr("disabled",true);
		$('.'+btnnew).attr('disabled',true);
		$('#'+btnnew).removeClass('hidden');
		$('#'+btnedit).addClass('hidden');
		$('#'+btnsave).addClass('hidden');
		$('#'+btncancel).addClass('hidden');
		$('#'+btndelete).addClass('hidden');

		$('#daysPresent').addClass('hidden');
		$('#hpRate').addClass('hidden');
		form = $(this).data('form');
		clear_form_elements(form);
		clear_form_elements('nonplantilla');
		$('.error-msg').remove();
		$('#jo_tax_policy_id').trigger('change');

	});

	$('.select2').select2();

	$('.onlyNumber').keypress(function (event) {
		return isNumber(event, this)
	});

	$(".onlyNumber").keyup(function(){
		amount  = $(this).val();
		if(amount == 0){
			$(this).val('');
		}else{
			plainAmount = amount.replace(/\,/g,'')
			$(this).val(commaSeparateNumber(plainAmount));
		}
	});

	$('#jo_tax_policy_id').on('change',function(){
		value = $(this).find(':selected').val();

		$('.tax_rate').addClass('hidden');
		$('.inputted').addClass('hidden');
		$('.sworn').addClass('hidden');
		switch(value){
			case '1':
				$('.inputted').removeClass('hidden');
				break;
			case '2':
				$('.tax_rate').removeClass('hidden');
				break;
			case '3':
				$('.sworn').removeClass('hidden');
				break;
		}
	});

	var benefits = "";
	$(document).on('change','#benefit_id',function(){
		benefit_amount = 0;
		benefit_amount = $(this).find(':selected').data('amount');
		code = $(this).find(':selected').data('code');
		benefits = $(this).find(':selected').text();

		switch(code){
			case 'HP':
				$('#hpRate').removeClass('hidden');
				$('.lpRate').addClass('hidden');
				$('#daysPresent').addClass('hidden');
				break;
			case 'LP':
				$('.lpRate').removeClass('hidden');
				$('#hpRate').addClass('hidden');
				break;
			default:
				$('#hpRate').addClass('hidden');
				$('.lpRate').addClass('hidden');
				break;
		}

		benefit_amount = (benefit_amount) ? commaSeparateNumber(benefit_amount) : '';
		$('#benefits_amount').val(benefit_amount);
	});

	var hp_rate = 0;
	$(document).on('change','#hp_rate',function(){
		hp_rate = $(this).find(':selected').val();
		hp_amount = Number(_monthlyRate) * hp_rate;
		$('#benefit_amount').val(commaSeparateNumber(Number(hp_amount).toFixed(2)));

	});

	var lp_rate = 0;
	$(document).on('change','#lp_rate',function(){
		lp_rate = $(this).find(':selected').val();
	});

	$('#lp_basic_salary').on('keyup',function(){
		basic_amount = $(this).val().replace(/\,/g,'');
		amount = Number(basic_amount) * lp_rate;
		$('#benefit_amount').val(commaSeparateNumber(Number(amount).toFixed(2)));
	})

	$(document).on('keyup','#days_present',function(){
		days_present = $(this).val();

		switch(benefits){
			case 'SALA':
				amount = (days_present * 150) + 500;
			break;
			case 'HAZARD PAY':
				amount = (parseFloat(_monthlyRate)*hp_rate)*days_present;
			break;
		}
		$('#benefit_amount').val(commaSeparateNumber(parseFloat(amount).toFixed(2)));


	});


	// DATE PICKER
	$('.datepicker').datepicker({
		dateFormat:'yy-mm-dd'
	});


	$('.nav-tabs a').click(function(){
		tab = $(this).text();
		switch(tab){
			case 'Benefits Info':
				if($('.employee_info_id').val()){
					$(this).tab('show');
				}else{
					swal({  title: 'Please setup first the summary',
							text: '',
							type: "warning",
							icon: 'warning',

						});
				}
			break;
			case 'Summary':
				$(this).tab('show');
			break;
			case 'Salary Info':
				$(this).tab('show');
			break;
			case 'Loans Info':
				if($('.employee_info_id').val()){
					$(this).tab('show');
				}else{
					swal({  title: 'Please setup first the summary',
							text: '',
							type: "warning",
							icon: 'warning',

						});
				}
			break;
			case 'Deduction Info':
				if($('.employee_info_id').val()){
					$(this).tab('show');
				}else{
					swal({  title: 'Please setup first the summary',
							text: '',
							type: "warning",
							icon: 'warning',
						});
				}
			break;
		}

	});

	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	})

	// DEDUCTION TERMINATED
	$('#chk_deduction_terminated').on('click',function(){

		check = $(this).prop('checked');
		if(check){
			$('#deduction_date_terminated').removeAttr('disabled');
		}else{
			$('#deduction_date_terminated').attr('disabled',true);
		}

	});


	// LOAN TERMINATED
	$('#chk_loan_terminated').on('click',function(){

		check = $(this).prop('checked');
		if(check){
			$('#loan_info_date_terminated').removeAttr('disabled');
		}else{
			$('#loan_info_date_terminated').attr('disabled',true);
		}

	});

	// BENEFIT TERMINATED
	$('#chk_benefit_terminated').on('click',function(){

		check = $(this).prop('checked');
		if(check){
			$('#benefit_info_date_terminated').removeAttr('disabled');
		}else{
			$('#benefit_info_date_terminated').attr('disabled',true);
		}

	});

	var _newRate = 0;
	$('#salary_adjustment').on('keyup',function(){
		_adjustment = $(this).val().replace(/\,/g,'');

		_newRate =  ((_adjustment) ? parseFloat(_monthlyRate) + parseFloat(_adjustment) : _monthlyRate);

		$('#new_rate').val(commaSeparateNumber(_newRate));
	})

	var _pagibigcontribution = 100;
	var _employer_pagibig_share
	$(document).on('change','#select_pagibigbpolicy',function(){
		policytype = $(this).find(':selected').data('policytype');

		switch(policytype){
			case 'System Generated':
				$('#pagibig_contribution').attr('readOnly',true);
				$('#pagibig_contribution').val(_pagibigcontribution);
				$('#er_pagibig_share').val(_pagibigcontribution);
				_employer_pagibig_share = _pagibigcontribution;
			break;

			case 'Inputted':
				$('#pagibig_contribution').val('');
				$('#pagibig_contribution').attr('readOnly',false);
			break;

			default:

			$('#select_pagibigbpolicy').val('');
			$('#pagibig_contribution').val('');
			$('#pagibig_contribution').attr('readOnly',false);
		}

	});


	var _clRange;
	var _prescribeTax;
	var _prescribedPercentage
	var _totalContribution;
	var _gsiscontribution;
	var _employer_gsis_share;
	var _philhealthcontribution;
	var _employer_philhealth_share;

	var _first;
	var _second;
	var _newdata;
	var _arrItem;
	$(document).on('change','#select_taxspolicy',function(){
		policytype = $(this).find(':selected').data('policytype');
		policy_name = $(this).find(':selected').data('policyname');

		switch(policy_name){
			case 'STANDARD POLICY':
				switch(policytype){
					case 'System Generated':
						$('#tax_contribution').attr('readOnly',true);
						if(!_taxDue){
							_gross_salary = 0;
							// _totalContribution = (parseFloat(_gsiscontribution) + parseFloat(_philhealthcontribution) + parseFloat(_pagibigcontribution))
							// _gross_salary = (_monthlyRate) - Math.abs(_totalContribution);
							_gross_salary = _monthlyRate;

							$.each(_arrItem,function(k,v){
								if(_gross_salary > parseFloat(_newdata.first[k]) && _gross_salary < parseFloat(_newdata.second[k])){

									_prescribeTax 		  = _arrItem[k][0];
									_prescribedPercentage = _arrItem[k][1];
									_clRange			  = _newdata.first[k];
								}

							});

							if(_gross_salary <= 20833){
								_clRange 			  = 0;
								_prescribedPercentage = 0;
							}

							$('#tax_bracket').val(parseFloat(_clRange));
							$('#tax_bracket_amount').val(parseFloat(_prescribeTax));

							_grossTaxable = 0;
							_grossTaxable = (Math.abs(parseFloat(_gross_salary)) - Math.abs(parseFloat(_clRange)));

							$('#tax_inexcess').val(Math.abs(_grossTaxable));
							_taxDue = 0;
							_taxPercentage = (Math.abs(_grossTaxable)*_prescribedPercentage);
							_taxDue = (Math.abs(parseFloat(_taxPercentage)) + Math.abs(parseFloat(_prescribeTax)));
						}
						$('#tax_contribution').val(commaSeparateNumber(Math.abs(parseFloat(_taxDue).toFixed(2))));

						// console.log('_arrItem '+_arrItem);
						// console.log('_clRange '+_clRange);
						// console.log('_gsiscontribution '+_gsiscontribution);
						// console.log('_philhealthcontribution '+_philhealthcontribution);
						// console.log('_pagibigcontribution '+_pagibigcontribution);
						// console.log('_prescribeTax '+_prescribeTax);
						// console.log('_totalContribution '+_totalContribution);
						// console.log('_gross_salary '+_gross_salary);
						// console.log('_grossTaxable '+_grossTaxable);
						// console.log('_taxPercentage '+_taxPercentage);
						// console.log('_monthlyRate '+_monthlyRate);

					break;

					case 'Inputted':
						$('#tax_contribution').val('');
						$('#tax_contribution').attr('readOnly',false);
					break;

					default:

					$('#select_taxspolicy').val('');
					$('#tax_contribution').val('');
					$('#tax_contribution').attr('readOnly',false);
					break;
				}
			break;
			case 'ANNUAL POLICY' :
				$('#tax_contribution').val('');
				$('#tax_contribution').attr('readOnly',true);
			break;
		}


	});


	$(document).on('change','#select_gsispolicy',function(){
		ee_percent = $(this).find(':selected').data('percent');
		er_percent = $(this).find(':selected').data('erpercent');
		policytype = $(this).find(':selected').data('policytype');
		_gsiscontribution = 0;
		switch(policytype){
			case 'System Generated':
				$('#gsis_contribution').attr('readOnly',true);
				_gsiscontribution = (parseFloat(_monthlyRate)*ee_percent);
				_employer_gsis_share = (parseFloat(_monthlyRate)*er_percent)

				if(_gsiscontribution){
					$('#gsis_contribution').val(commaSeparateNumber(parseFloat(_gsiscontribution).toFixed(2)));
					$('#er_gsis_share').val(_employer_gsis_share);
				}
			break;

			case 'Inputted':
				$('#gsis_contribution').val('');
				$('#er_gsis_share').val('');
				$('#gsis_contribution').attr('readOnly',false);
			break;

			default:

			$('#select_gsispolicy').val('');
			$('#gsis_contribution').val('');
			$('#er_gsis_share').val('');
			$('#gsis_contribution').attr('readOnly',false);
		}

	});

	$(document).on('change','#select_philhealthpolicy',function(){
		below = 0;
		above = 0;
		_philhealthcontribution = 0;
		policytype = $(this).find(':selected').data('policytype');
		below = $(this).find(':selected').data('below');
		above = $(this).find(':selected').data('above');

		switch(policytype){
			case 'System Generated':
				$('#philhealth_contribution').attr('readOnly',true);

				if(_monthlyRate >= 40000){
					_philhealthcontribution = (above/2);
					_employer_philhealth_share = _philhealthcontribution;
				}else{
					salary = Number(_monthlyRate) * 0.0275;
					salary = salary.toFixed(2);
					emp_Share = Number(salary) / 2;
					emp_Share = emp_Share.toFixed(2);
					_philhealthcontribution = Number(salary) - Number(emp_Share);
					_employer_philhealth_share = emp_Share;
				}

				if(_philhealthcontribution){
					$('#philhealth_contribution').val(commaSeparateNumber(parseFloat(_philhealthcontribution).toFixed(2)) );
					$('#er_philhealth_share').val(_employer_philhealth_share);
				}

			break;

			case 'Inputted':
				$('#philhealth_contribution').val('');
				$('#philhealth_contribution').attr('readOnly',false);
			break;

			default:

			$('#select_pagibigbpolicy').val('');
			$('#philhealth_contribution').val('');
			$('#philhealth_contribution').attr('readOnly',false);
		}

	});

	$(document).on('change','#select_bank',function(){
		_branchName = $(this).find(':selected').data('bankbranch');
		$('#bank_branch').val(_branchName);
	})


	$(document).on('click','#namelist tr',function(){
		id = $(this).data('empid');
		$('.employee_id').val(id);
		$.ajax({
			url:base_url+module_prefix+module+'/getEmployeesinfo',
			data:{
				'id':id,
			},
			type:'GET',
			dataType:'JSON',
			success:function(data){
				_newdata = data;
				$('._input').val('');
				clear_form_elements('myform');
				clear_form_elements('myform2');
				clear_form_elements('myform3');
				clear_form_elements('myform4');
				clear_form_elements('myform5');

				lastname = (data.employeeinfo !== null) ? data.employeeinfo.employees.lastname : '';
				firstname = (data.employeeinfo !== null) ? data.employeeinfo.employees.firstname : '';
				middlename = (data.employeeinfo !== null) ? data.employeeinfo.employees.middlename : '';

				$('#lbl_empname').text(lastname+' '+firstname+' '+middlename)

				if(data.pmsemployeeinfo !== undefined && data.pmsemployeeinfo !== null){
					$('#editSummary').removeClass('hidden');
					$('#newSummary').addClass('hidden');
					$('#saveSummary').addClass('hidden');
					$('._taxperiod').addClass('hidden');
					$('.employee_info_id').val(data.employeeinfo.id);
					if(data.pmsemployeeinfo.wagestatus_id != null){
						$('#wagestatus_id').val(data.pmsemployeeinfo.wagestatus_id);
					}
					if(data.salaryinfo !== null){
						code = (data.salaryinfo.salarygrade) ? data.salaryinfo.salarygrade.Code : '';
						$('#salary_grade').val();
					}
					if(data.pmsemployeeinfo.providentfund_id != null){
						$('#providentfund_id').val(data.pmsemployeeinfo.providentfunds.policy_name);
					}
					if(data.pmsemployeeinfo.bank_id != null){
						$('#select_bank').val(data.pmsemployeeinfo.bank_id);
						$("#bank_branch").val(data.pmsemployeeinfo.banks.branch_name);
					}
					if(data.pmsemployeeinfo.gsispolicy_id != null){
						$('#select_gsispolicy').val(data.pmsemployeeinfo.gsispolicy_id);
					}

					employee_number = (data.pmsemployeeinfo !== null) ? data.pmsemployeeinfo.employees.employee_number : '';
					$('#emp_no').val(employee_number);
					$('#bp_no').val(data.pmsemployeeinfo.bp_no);
					$('#atm_no').val(data.pmsemployeeinfo.atm_no);
					$('#tax_id_number').val(data.pmsemployeeinfo.tax_id_number);

					_gsiscontribution = 0;
					if(data.pmsemployeeinfo.gsispolicy_id !== null && data.pmsemployeeinfo.gsispolicy_id !== 'undefined'){
						if(data.pmsemployeeinfo.gsispolicy.policy_type == "System Generated"){
							_gsiscontribution = data.pmsemployeeinfo.gsis_contribution;
							$('#gsis_contribution').val(commaSeparateNumber(data.pmsemployeeinfo.gsis_contribution)).prop('readOnly',true);
							$('#er_gsis_share').val(data.pmsemployeeinfo.er_gsis_share)
						}else{
							$('#gsis_contribution').val(commaSeparateNumber(data.pmsemployeeinfo.gsis_contribution));
							$('#er_gsis_share').val(data.pmsemployeeinfo.er_gsis_share)
						}
					}
					_taxDue = 0;
					if(data.pmsemployeeinfo.taxpolicy_id !== null && data.pmsemployeeinfo.taxpolicy_id !== 'undefined'){
						if(data.pmsemployeeinfo.taxpolicy.policy_type == "System Generated"){
							_taxDue = (data.pmsemployeeinfo.tax_contribution) ? data.pmsemployeeinfo.tax_contribution : '';

							$('#tax_contribution').val(commaSeparateNumber(_taxDue)).prop('readOnly',true);
						}else{

							$('#tax_contribution').val(commaSeparateNumber(_taxDue));
						}
					}
					// TEMPORARY
					if(data.pmsemployeeinfo.tax_contribution !== ""){
						_taxDue = (data.pmsemployeeinfo.tax_contribution) ? data.pmsemployeeinfo.tax_contribution : '';

						$('#tax_contribution').val(commaSeparateNumber(_taxDue));
					}

					_philhealthcontribution = 0;
					if(data.pmsemployeeinfo.philhealthpolicy_id !== null){
						$('#select_philhealthpolicy').val(data.pmsemployeeinfo.philhealthpolicy_id);
						if(data.pmsemployeeinfo.philhealthpolicy.policy_type == "System Generated"){
							_philhealthcontribution = data.pmsemployeeinfo.philhealth_contribution;
							$('#philhealth_contribution').val(commaSeparateNumber(data.pmsemployeeinfo.philhealth_contribution)).prop('readOnly',true);
							$('#er_philhealth_share').val(data.pmsemployeeinfo.er_philhealth_share)
						}else{
							$('#philhealth_contribution').val(commaSeparateNumber(data.pmsemployeeinfo.philhealth_contribution));
							$('#er_philhealth_share').val(data.pmsemployeeinfo.er_philhealth_share)
						}
					}

					if(data.pmsemployeeinfo.pagibigpolicy_id !== null){
						$('#select_pagibigbpolicy').val(data.pmsemployeeinfo.pagibigpolicy_id);
						if(data.pmsemployeeinfo.pagibigpolicy_id !==  null  && data.pmsemployeeinfo.pagibigpolicy_id !== 'undefined'){

							if(data.pmsemployeeinfo.pagibigpolicy.policy_type == "System Generated"){
								$('#pagibig_contribution').val(commaSeparateNumber(data.pmsemployeeinfo.pagibig_contribution)).prop('readOnly',true);
								$('#er_pagibig_share').val(data.pmsemployeeinfo.er_pagibig_share)
							}else{
								$('#pagibig_contribution').val(commaSeparateNumber(data.pmsemployeeinfo.pagibig_contribution));
								$('#er_pagibig_share').val(data.pmsemployeeinfo.er_pagibig_share)
							}
						}
					}

					$('#pagibig2').val(data.pmsemployeeinfo.pagibig2);
					$('#personal_share').val(data.pmsemployeeinfo.pagibig_personal);

					if(data.pmsemployeeinfo.taxpolicy_id !== null){
						$('#select_taxspolicy').val(data.pmsemployeeinfo.taxpolicy_id);
					}

				}else{
					$('.newSummary :input').attr('disabled',true);
					$('.newSummary').attr('disabled',true);
					$('._taxperiod').removeClass('hidden');
					$('#editSummary').addClass('hidden');
					$('#saveSummary').addClass('hidden');
					$('#cancelSummary').addClass('hidden');
					$('#newSummary').removeClass('hidden');
					assumption_date = (data.employeeinfo !== null) ? data.employeeinfo.assumption_date : '';
					$('#effective_salarydate').val();
					if(data.salaryinfo !== null){
						salary_grade = (data.salaryinfo.salarygrade_id) ? data.salaryinfo.salarygrade.salary_grade : '';
						$('#salary_grade').val(salary_grade);
					}

				}

				code = '';
				if(data.employeeinfo !== null){
					$('#empstatus').val(data.employeeinfo.employeestatus.Name);
					code = data.employeeinfo.employeestatus.Code;
					$('#position').val(data.employeeinfo.positions.Name);
					if(data.employeeinfo.office_id){
						$('#office').val(data.employeeinfo.offices.Name);
					}

					if(data.employeeinfo.assumption_date){
						$('#assumption_to_duty').val(data.employeeinfo.assumption_date);
					}

					if(data.employeeinfo.division_id){
						$('#division').val(data.employeeinfo.divisions.Name);
					}
					$('#positionitem_id').val(data.employeeinfo.position_item_id);
					$('#position_id').val(data.employeeinfo.position_id);
				}



				basic_amount_two = 0
				basic_amount_one = 0;
				basic_amount = 0;

				if(data.salaryinfo !== null){
					basic_amount_one = (data.salaryinfo.basic_amount_one !== null) ? data.salaryinfo.basic_amount_one : 0;
					basic_amount_two = (data.salaryinfo.basic_amount_two !== null) ? data.salaryinfo.basic_amount_two : 0;
					basic_amount = (parseFloat(basic_amount_one) + parseFloat(basic_amount_two));

					_monthlyRate = basic_amount;
					overtime_balance_amount = (_monthlyRate/2);
					$('#monthly_rate').val(commaSeparateNumber(parseFloat(_monthlyRate).toFixed(2)));
					$('#overtime_balance_amount').val(commaSeparateNumber(parseFloat(overtime_balance_amount).toFixed(2)));
					$('#new_rate').val(commaSeparateNumber(_monthlyRate));

				}

				item = "";
				_arrItem = [];
				$.each(data.cl.monthlyCL,function(k,v){
					item  = v.split('-');
					_arrItem.push(item);

				});

				_dailyRate = 0;
				if(data.salaryinfo !== null){
					if(code == 'COS'){
						_dailyRate = parseFloat(basic_amount) / data.cos_days;
					}else{
						_dailyRate = parseFloat(basic_amount)/22;
					}
					$('#daily_rate').val(commaSeparateNumber(_dailyRate.toFixed(2)));

				}
				_weeklyRate = 0;
				if(data.salaryinfo !== null){
					_weeklyRate = parseFloat(basic_amount)/4;
					$('#weekly_rate').val(commaSeparateNumber(_weeklyRate.toFixed(2)));
				}

				_annualRate = 0;
				if(data.salaryinfo !== null){
					_annualRate = parseFloat(basic_amount)*12;
					$('#annual_rate').val(commaSeparateNumber(_annualRate.toFixed(2)));
					$('#input_stepinc').val(data.salaryinfo.step_inc);
				}

				overtime_balance_amount = (_annualRate) ? _annualRate/2  : '';
				$('#overtime_balance_amount').val(commaSeparateNumber(parseFloat(overtime_balance_amount).toFixed(2)) );


				// GENERATE TR FOR BENEFITS TAB

				generateBenefitInfoTable(data.benefitinfo);

				// GENERATE TR FOR LOAN INFO TAB
				generateLoanInfoTable(data.loaninfo);


				// GENERATE TR FOR DEDUCTION INFO TAB
				generateDeductionInfoTable(data.deductioninfo);


				// GENERATE TR FOR SALARY INFO TAB
				var tSalary = $('#tbl_salaryinfo').DataTable();
				var sgjg = "";
				tSalary.clear().draw();

				$.each(data.salarylist,function(k,v){
					if(v.salarygrade_id){
						sg = (v.salarygrade) ? v.salarygrade.Code : "";
						sgjg = 'SG '+sg;
					}

					total = (parseFloat(data.salarylist[k].basic_amount_one) + parseFloat(data.salarylist[k].basic_amount_two))

					tSalary.row.add( [
			        	data.salarylist[k].salary_effectivity_date,
			        	data.salarylist[k].salary_description,
						sgjg,
						data.salarylist[k].basic_amount_one,
						data.salarylist[k].basic_amount_two,
						total
			        ]).draw( false );

			        tSalary.rows(k).nodes().to$().attr("data-id", v.id);
			        tSalary.rows(k).nodes().to$().attr("data-employeeid", v.employee_id);
			        tSalary.rows(k).nodes().to$().attr("data-salarygradeid", v.salarygrade_id);
			        // tSalary.rows(k).nodes().to$().attr("data-jobgradeid", v.jobgrade_id);
			        tSalary.rows(k).nodes().to$().attr("data-description", v.salary_description);
			        tSalary.rows(k).nodes().to$().attr("data-basic_amount_one", v.basic_amount_one);
			        tSalary.rows(k).nodes().to$().attr("data-basic_amount_two", v.basic_amount_two);
			        tSalary.rows(k).nodes().to$().attr("data-effectivitydate", v.salary_effectivity_date);
			        tSalary.rows(k).nodes().to$().attr("data-positionitem_id", v.positionitem_id);
			        tSalary.rows(k).nodes().to$().attr("data-position_id", v.position_id);
			        tSalary.rows(k).nodes().to$().attr("data-step_inc", v.step_inc);
			        tSalary.rows(k).nodes().to$().attr("data-btnnew", "newSalary");
			        tSalary.rows(k).nodes().to$().attr("data-btnsave", "saveSalary");
			        tSalary.rows(k).nodes().to$().attr("data-btnedit", "editSalary");
			        tSalary.rows(k).nodes().to$().attr("data-btncancel", "cancelSalary");
				});

				//  NON PLANTILLA

				clear_form_elements('nonplantilla');
				$('.newNonPlantilla').attr('disabled',true);

				if(data.employeeinfo !== null){
					$('#jo_employee_no').val(data.employeeinfo.employee_number);
					if(data.employeeinfo.employeestatus !== null){
						$('#jo_employee_status').val(data.employeeinfo.employeestatus.Name);
					}
					if(data.employeeinfo.positions !== null){
						$('#jo_position').val(data.employeeinfo.positions.Name);
					}
					if(data.employeeinfo.offices !== null){
						$('#jo_office').val(data.employeeinfo.offices.Name);
					}
					if(data.employeeinfo.divisions !== null){
						$('#jo_division').val(data.employeeinfo.divisions.Name);
					}
					$('#jo_assumption_to_duty').val(data.employeeinfo.assumption_date);

				}


				if(data.nonplantilla !== null){
					$('#newNonPlantilla').addClass('hidden');
					$('#editNonPlantilla').removeClass('hidden');
					$('#cancelNonPlantilla').removeClass('hidden');
					$('#nonplantilla_id').val(data.nonplantilla.id);
					daily_rate 	 = data.nonplantilla.daily_rate_amount;
					$('#jo_monthly_rate_amount').val(data.nonplantilla.monthly_rate_amount);
					$('#job_order_daily_rate').val(commaSeparateNumber(data.nonplantilla.daily_rate_amount));
					$('#jo_tax_policy_id').val(data.nonplantilla.tax_policy_id).trigger('change');
					$('#ewt_policy_id').val(data.nonplantilla.ewt_policy_id);
					$('#pwt_policy_id').val(data.nonplantilla.pwt_policy_id);
					$('#responsibility_id').val(data.nonplantilla.responsibility_id);
					$('#project_id').val(data.nonplantilla.project_id);
					$('#jo_atm_no').val(data.nonplantilla.atm_no);
					$('#jo_bank_id').val(data.nonplantilla.bank_id);
					threshhold_amount = (data.nonplantilla.threshhold_amount) ? commaSeparateNumber(parseFloat(data.nonplantilla.threshhold_amount).toFixed(2)) : '';
					jo_overtime_balance_amount = (data.nonplantilla.overtime_balance_amount) ? commaSeparateNumber(parseFloat(data.nonplantilla.overtime_balance_amount).toFixed(2)) : '';
					$('#threshhold_amount').val(threshhold_amount)
					$('#jo_overtime_balance_amount').val(jo_overtime_balance_amount)
					$('#jo_tax_id_number').val(data.nonplantilla.tax_id_number);
				}else{
					$('#jo_monthly_rate_amount').val(basic_amount);
					$('#job_order_daily_rate').val(commaSeparateNumber(parseFloat(_dailyRate).toFixed(2))).trigger('keyup');
					$('#jo_employeeinfo_id').val('');
					$('#newNonPlantilla').removeClass('hidden');
					$('#editNonPlantilla').addClass('hidden');
					$('#saveNonPlantilla').addClass('hidden');
					$('#cancelNonPlantilla').addClass('hidden');
				}
			}
		});
	});

//SUBMIT FORM
$(document).off('click',".submitme").on('click',".submitme",function(){
	btn = $(this);
	form = $(this).data('form');

		$('#'+form).ajaxForm({
			beforeSend:function(){

			},
			success:function(data){
				par  =  JSON.parse(data);

				if(par.status){

					swal({  title: par.response,
							text: '',
							type: "success",
							icon: 'success',

						}).then(function(){

							$('#benefit_info_id').val('')
							$('#laon_info_id').val('')
							$('#deduction_info_id').val('')
							$('#nonplantilla_id').val('')

							// if(par.employeeload){
								// window.location.href = base_url+module_prefix+module;
								$('.btn_cancel').trigger('click');
								$('.btnfilter').trigger('click');
							// }

							if(par.benefitinfo){

								// GENERATE TR FOR BENEFITS TAB

								generateBenefitInfoTable(par.benefitinfo);

							}// end of BenefitInfo

							if(par.loaninfo){
								// GENERATE TR FOR LOAN INFO TAB
								generateLoanInfoTable(par.loaninfo)

							}// end of Loaninfo

							if(par.deductioninfo){
								// GENERATE TR FOR DEDUCTION INFO TAB
								generateDeductionInfoTable(par.deductioninfo);

							}// end of Deductionifno

							if(par.salaryinfo){
								// GENERATE TR FOR SALARY INFO TAB
								var tSalary = $('#tbl_salaryinfo').DataTable();
								var sgjg = "";
								tSalary.clear().draw();

								$.each(par.salaryinfo,function(k,v){

									if(v.salarygrade_id){
										sgjg = 'SG '+v.salarygrade.Code;
									}else{
										sgjg = 'JG '+v.jobgrade.job_grade;
									}

									total = (parseFloat(par.salaryinfo[k].basic_amount_one) + parseFloat(par.salaryinfo[k].basic_amount_two));

									tSalary.row.add( [
							        	par.salaryinfo[k].salary_effectivity_date,
							        	par.salaryinfo[k].salary_description,
										sgjg,
										par.salaryinfo[k].basic_amount_one,
										par.salaryinfo[k].basic_amount_two,
										total,
							        ]).draw( false );

							        tSalary.rows(k).nodes().to$().attr("data-id", v.id);
							        tSalary.rows(k).nodes().to$().attr("data-employeeid", v.employee_id);
							        tSalary.rows(k).nodes().to$().attr("data-salarygradeid", v.salarygrade_id);
							        // tSalary.rows(k).nodes().to$().attr("data-jobgradeid", v.jobgrade_id);
							        tSalary.rows(k).nodes().to$().attr("data-description", v.salary_description);
							        tSalary.rows(k).nodes().to$().attr("data-basic_amount_one", v.basic_amount_one);
							        tSalary.rows(k).nodes().to$().attr("data-basic_amount_two", v.basic_amount_two);
							        tSalary.rows(k).nodes().to$().attr("data-effectivitydate", v.salary_effectivity_date);
							        tSalary.rows(k).nodes().to$().attr("data-year", v.year);
							        tSalary.rows(k).nodes().to$().attr("data-month", v.month);
							        tSalary.rows(k).nodes().to$().attr("data-btnnew", "newSalary");
							        tSalary.rows(k).nodes().to$().attr("data-btnsave", "saveSalary");
							        tSalary.rows(k).nodes().to$().attr("data-btnedit", "editSalary");
							        tSalary.rows(k).nodes().to$().attr("data-btncancel", "cancelSalary");
								});

								$('.newSalary').attr('disabled',true);
								$('#newSalary').removeClass('hidden');
								$('#saveSalary').addClass('hidden');
								$('#daysPresent').addClass('hidden');
								$('#hpRate').addClass('hidden');
								clear_form_elements("myform2");
								clear_form_elements("myform4");

							}// end of Salaryinfo

						});// end swal

				}else{

					swal({  title: par.response,
							text: '',
							type: "error",
							icon: 'error',

						});

				}// end of main IF STATUS

				btn.button('reset');
			},
			error:function(data){
				$error = data.responseJSON;
				/*reset popover*/
				$('input[type="text"], select').popover('destroy');

				/*add popover*/
				block = 0;
				$(".error-msg").remove();
				$.each($error.errors,function(k,v){
					var messages = v.join(', ');
					msg = '<div class="error-msg err-'+k+'" style="color:red;"><i class="fa fa-exclamation-triangle" style="color:rgb(255, 184, 0);"></i> '+messages+'</div>';
					$('input[name="'+k+'"], textarea[name="'+k+'"], select[name="'+k+'"]').after(msg).attr('data-content',messages);
					if(block == 0){
						$('html, body').animate({
					        scrollTop: $('.err-'+k).offset().top - 250
					    }, 500);
					    block++;
					}
				})
				$('.saving').replaceWith(btn);
			},
			always:function(){
				setTimeout(function(){
						$('.saving').replaceWith(btn);
					},300)
			}
		}).submit();

});
var status;
$(document).on('change', '#emp_status',function(){
	status = $(this).find(':selected').val();

	clear_form_elements('nonplantilla');
	clear_form_elements('myform');
	clear_form_elements('myform2');
	clear_form_elements('myform3');
	clear_form_elements('myform4');
	clear_form_elements('myform5');
	$('.newNonPlantilla').attr('disabled',true);
	$('#newNonPlantilla').removeClass('hidden');
	$('#saveNonPlantilla').addClass('hidden');
	$('#editNonPlantilla').addClass('hidden');
	$('#cancelNonPlantilla').addClass('hidden');
	$('.newSummary').attr('disabled',true);
	$('#newSummary').removeClass('hidden');
	$('#saveSummary').addClass('hidden');
	$('#editSummary').addClass('hidden');
	$('#cancelSummary').addClass('hidden');

	switch(status){
		case 'plantilla':
			$('#plantilla').removeClass('hidden');
			$('#nonplantilla').addClass('hidden');
		break;
		case 'nonplantilla':
			$('#nonplantilla').removeClass('hidden');
			$('#plantilla').addClass('hidden');
		break;
	}
	$('.btnfilter').trigger('click');
});

var timer;
$(document).on('click','.btnfilter',function(){
	// $('input.search').addClass('searchSpinner');
	tools  = $('#tools-form').serialize()

	category  	= $('#select_searchvalue :selected').val();
	empstatus   = $('#emp_status :selected').val();
	emp_type    = $('#emp_type :selected').val();
	searchby    = $('#searchby :selected').val();

	clearTimeout(timer);
	timer = setTimeout(
				function(){
					$.ajax({
					   type: "GET",
					   url: base_url+module_prefix+module+'/show',
					   data: {'category':category,'empstatus':empstatus,'emp_type':emp_type,'searchby':searchby },
					   beforeSend:function(){
					   		// $('#loading').removeClass('hidden');
					   },
					   complete:function(){
					   		// $('#loading').addClass('hidden');
					   },
					   success: function(res){
					      $(".sub-panelnamelist").html(res);
					      // $('input.search').removeClass('searchSpinner');
					   }
					});
				},500);
})


$(document).on('keyup','.search1',function(){
	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
			   type: "GET",
			   url: base_url+module_prefix+module+'/show',
			   data: {
			   	"q":$('.search1').val(),
			   	'limit':$(".limit").val(),
			   	'employee_status':status
			   },
			   beforeSend:function(){

			   },
			   success: function(res){
			      $(".sub-panelnamelist").html(res);

			   },
			   complete:function(){

			   }
			});
		},500);
});


// # DELETE BENEFIT INFO
$(document).on('click','#deleteBenefit',function(){
	if(!benefitinfo_id){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Delete Benefit?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.deleteBenefit();
			}else{
				return false;
			}
		});
	}
});

$.deleteBenefit = function(){
	$.ajax({
		url:base_url+module_prefix+module+'/deleteBenefitinfo',
		data:{
			'_token':'{{ csrf_token() }}',
			'benefit_info_id':benefitinfo_id,
			'employee_id':benefit_employee_id
		},
		type:'POST',
		dataType:'JSON',
		success:function(res){

			swal({
				title: 'Benefit Deleted',
				text: '',
				type: "success",
				icon: 'success',
			});

			generateBenefitInfoTable(res.data);
		}
	});
}
// # DELETE LOAN INFO
$(document).on('click','#deleteLoan',function(){
	if(!loaninfo_id){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Delete Loan?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.deleteLoanInfo();
			}else{
				return false;
			}
		});
	}
});

$.deleteLoanInfo = function(){
	$.ajax({
		url:base_url+module_prefix+module+'/deleteLoanInfo',
		data:{
			'_token':'{{ csrf_token() }}',
			'loan_info_id':loaninfo_id,
			'employee_id':loan_employee_id
		},
		type:'POST',
		dataType:'JSON',
		success:function(res){
			console.log(res);
			swal({
				title: 'Loan Deleted',
				text: '',
				type: "success",
				icon: 'success',
			});

			generateLoanInfoTable(res.data);
		}
	});
}

// # DELETE LOAN INFO
$(document).on('click','#deleteDeduct',function(){
	if(!loaninfo_id){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Delete Deduction?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.deleteLoanInfo();
			}else{
				return false;
			}
		});
	}
});

$.deleteLoanInfo = function(){
	$.ajax({
		url:base_url+module_prefix+module+'/deleteDeductInfo',
		data:{
			'_token':'{{ csrf_token() }}',
			'deduct_info_id':deduct_info_id,
			'employee_id':deduct_employee_id
		},
		type:'POST',
		dataType:'JSON',
		success:function(res){
			console.log(res);
			swal({
				title: 'Deduction Deleted',
				text: '',
				type: "success",
				icon: 'success',
			});

			generateDeductionInfoTable(res.data);
		}
	});
}

function generateBenefitInfoTable(benefitinfo){
	var tBenefit = $('#tbl_benefitinfo').DataTable();
	var code;
	tBenefit.clear().draw();

	$.each(benefitinfo,function(k,v){

		tBenefit.row.add( [
        	v.start_date,
        	v.benefits.name,
			v.benefit_amount,
			'<a class="btn btn-xs btn-danger delete_item" data-function_name="deleteBenefitinfo" data-id="'+v.id+'" data-employee_id="'+v.employee_id+'"><i class="fa fa-trash"></i> Delete</a>'
        ]).draw( false );
        tBenefit.rows(k).nodes().to$().attr("data-id", v.id);
        tBenefit.rows(k).nodes().to$().attr("data-employee_id", v.employee_id);
        tBenefit.rows(k).nodes().to$().attr("data-start_date", v.start_date);
        tBenefit.rows(k).nodes().to$().attr("data-name", v.benefits.name);
        tBenefit.rows(k).nodes().to$().attr("data-benefit_amount", v.benefit_amount);
        tBenefit.rows(k).nodes().to$().attr("data-benefit_id", v.benefit_id);
        tBenefit.rows(k).nodes().to$().attr("data-days_present", v.days_present);
        tBenefit.rows(k).nodes().to$().attr("data-benefit_rate", v.benefit_rate);
        tBenefit.rows(k).nodes().to$().attr("data-terminated", v.terminated);
        tBenefit.rows(k).nodes().to$().attr("data-date_terminated", v.date_terminated);
        tBenefit.rows(k).nodes().to$().attr("data-lp_basic_salary", v.lp_basic_salary);
        tBenefit.rows(k).nodes().to$().attr("data-lp_tranche", v.lp_tranche);
        tBenefit.rows(k).nodes().to$().attr("data-number_of_years", v.number_of_years);
        tBenefit.rows(k).nodes().to$().attr("data-btnnew", "newBenefit");
        tBenefit.rows(k).nodes().to$().attr("data-btnsave", "saveBenefit");
        tBenefit.rows(k).nodes().to$().attr("data-btnedit", "editBenefit");
        tBenefit.rows(k).nodes().to$().attr("data-btndelete", "deleteBenefit");
        tBenefit.rows(k).nodes().to$().attr("data-btncancel", "cancelBenefit");


	});

	$('.newBenefit').attr('disabled',true);
	$('#newBenefit').removeClass('hidden');
	$('#saveBenefit').addClass('hidden');
	$('#editBenefit').addClass('hidden');
	$('#deleteBenefit').addClass('hidden');
	$('#cancelBenefit').addClass('hidden');
	clear_form_elements("myform3");
}

// # loan Info Table
function generateLoanInfoTable(loaninfo){
	var tLoan = $('#tbl_loaninfo').DataTable();

	tLoan.clear().draw();

	$.each(loaninfo,function(k,v){

		tLoan.row.add( [
        	v.loans.name,
        	v.loan_total_amount,
			v.loan_total_balance,
			v.loan_amount,
			v.start_date,
			v.end_date,
			'<a class="btn btn-xs btn-danger delete_item" data-function_name="deleteLoanInfo" data-id="'+v.id+'" data-employee_id="'+v.employee_id+'"><i class="fa fa-trash"></i> Delete</a>'
        ]).draw( false );

        tLoan.rows(k).nodes().to$().attr("data-id", v.id);
        tLoan.rows(k).nodes().to$().attr("data-employee_id", v.employee_id);
        tLoan.rows(k).nodes().to$().attr("data-loan_total_amount", v.loan_total_amount);
        tLoan.rows(k).nodes().to$().attr("data-loan_total_balance", v.loan_total_balance);
        tLoan.rows(k).nodes().to$().attr("data-loan_amount", v.loan_amount);
        tLoan.rows(k).nodes().to$().attr("data-start_date", v.start_date);
        tLoan.rows(k).nodes().to$().attr("data-end_date", v.end_date);
        tLoan.rows(k).nodes().to$().attr("data-loan_id", v.loan_id);
        tLoan.rows(k).nodes().to$().attr("data-loan_date_granted", v.loan_date_granted);
        tLoan.rows(k).nodes().to$().attr("data-loan_date_terminated", v.loan_date_terminated);
        tLoan.rows(k).nodes().to$().attr("data-date_terminated", v.date_terminated);
        tLoan.rows(k).nodes().to$().attr("data-terminated", v.terminated);
        tLoan.rows(k).nodes().to$().attr("data-btnnew", "newLoan");
        tLoan.rows(k).nodes().to$().attr("data-btnsave", "saveLoan");
        tLoan.rows(k).nodes().to$().attr("data-btnedit", "editLoan");
        tLoan.rows(k).nodes().to$().attr("data-btncancel", "cancelLoan");
        tLoan.rows(k).nodes().to$().attr("data-btndelete", "deleteLoan");
	});

	$('.newLoan').attr('disabled',true);
	$('#newLoan').removeClass('hidden');
	$('#editLoan').addClass('hidden');
	$('#deleteLoan').addClass('hidden');
	$('#saveLoan').addClass('hidden');
	$('#cancelLoan').addClass('hidden');
	clear_form_elements("myform4");
}

function generateDeductionInfoTable(deductioninfo){
	var tDeduct = $('#tbl_deductioninfo').DataTable();

	tDeduct.clear().draw();

	$.each(deductioninfo,function(k,v){

		name = (v.deductions !== null) ? v.deductions.name : '';

		tDeduct.row.add( [
        	name,
        	v.deduction_amount,
			v.start_date,
			v.end_date,
			'<a class="btn btn-xs btn-danger delete_item" data-function_name="deleteDeductInfo" data-id="'+v.id+'" data-employee_id="'+v.employee_id+'"><i class="fa fa-trash"></i> Delete</a>'
        ]).draw( false );

        tDeduct.rows(k).nodes().to$().attr("data-id", v.id);
        tDeduct.rows(k).nodes().to$().attr("data-employee_id", v.employee_id);
        tDeduct.rows(k).nodes().to$().attr("data-deduction_id", v.deduction_id);
        tDeduct.rows(k).nodes().to$().attr("data-deduction_amount", v.deduction_amount);
        tDeduct.rows(k).nodes().to$().attr("data-start_date", v.start_date);
        tDeduct.rows(k).nodes().to$().attr("data-end_date", v.end_date);
        tDeduct.rows(k).nodes().to$().attr("data-date_terminated", v.date_terminated);
        tDeduct.rows(k).nodes().to$().attr("data-terminated", v.terminated);
        tDeduct.rows(k).nodes().to$().attr("data-btnnew", "newDeduct");
        tDeduct.rows(k).nodes().to$().attr("data-btnsave", "saveDeduct");
        tDeduct.rows(k).nodes().to$().attr("data-btnedit", "editDeduct");
        tDeduct.rows(k).nodes().to$().attr("data-btncancel", "cancelDeduct");
        tDeduct.rows(k).nodes().to$().attr("data-btndelete", "deleteDeduct");
	});

	$('.newDeduct').attr('disabled',true);
	$('#newDeduct').removeClass('hidden');
	$('#saveDeduct').addClass('hidden');
	$('#editDeduct').addClass('hidden');
	$('#deleteDeduct').addClass('hidden');
	$('#cancelDeduct').addClass('hidden');
	clear_form_elements("myform5");
}

// NON PLANTILLA
var daily_rate = 0;
var monthly_rate = 0;
var annual_rate = 0;
$(document).on('keyup','#job_order_daily_rate',function(){
	daily_rate = $(this).val().replace(/\,/g,'');
	jo_overtime_balance_amount = ((daily_rate*22)*12)/2;
	jo_overtime_balance_amount = (jo_overtime_balance_amount) ? commaSeparateNumber(parseFloat(jo_overtime_balance_amount).toFixed(2)) : '';
	$('#jo_overtime_balance_amount').val(jo_overtime_balance_amount)
	// $('#jo_no_ofdays_inamonth').trigger('keyup');

});


$(document).on('keyup','#jo_no_ofdays_inayear',function(){
	annual_rate = (parseFloat(monthly_rate.replace(',','')) * this.value) ;
	annual_rate = (annual_rate) ? (parseFloat(annual_rate)) : 0;
	annual_rate = (annual_rate) ? commaSeparateNumber(parseFloat(annual_rate).toFixed(2)) : '';
	$('#jo_annual_rate_amount').val(annual_rate);
});


$(document).on('change','#jo_bank_id',function(){
	branch_name = $(this).find(':selected').data('jobankbranch');
	$('#jo_bank_branch').val(branch_name);
});

// ======================================================= //
// ============ DELETE  FUNCTION ==================== //
// ===================================================== //

$(document).on('click','.delete_item',function(){
	id 	= $(this).data('id');
	employee_id = $(this).data('employee_id');
	function_name = $(this).data('function_name')

	if(id){
		swal({
			title: "Delete?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.ajax({
					url:base_url+module_prefix+module+'/'+function_name,
					data:{
						'id':id,
						'employee_id':employee_id,
						'_token':"{{ csrf_token() }}"
					},
					type:'post',
					dataType:'JSON',
					success:function(res){
						swal({
							  title: 'Deleted Successfully!',
							  type: "warning",
							  showCancelButton: false,
							  confirmButtonClass: "btn-warning",
							  confirmButtonText: "OK",
							  closeOnConfirm: false
						})

						switch(res.status){
							case 'loans':
								generateLoanInfoTable(res.data)
							break;
							case 'deductions':
								generateDeductionInfoTable(res.data)
							break;
							case 'benefits':
								generateBenefitInfoTable(res.data)
							break;
						}
					}

				})
			}else{
				return false;
			}
		});
	}
})

var _listId = [];
$(document).on('click','#check_all',function(){
	if ($(this).is(':checked')) {
        $('.emp_select').prop('checked', 'checked');
        $('.emp_select:checked').each(function(){
        	_listId.push($(this).val())
        });
    } else {
        $('.emp_select').prop('checked', false)
        _listId = [];
    }
});

$(document).on('click','.emp_select',function(){
	empid = $(this).val();

	index = $(this).data('key');
	if($(this).is(':checked')){
		_listId[index] =  empid;

	}else{
		delete _listId[index];
	}
});

$('#recompute').on('click',function(){
	$.ajax({
		url: base_url+module_prefix+module+'/computeEmployeeInfo',
		data:{
			'empid':_listId
		},
		type:'GET',
		beforeSend:function(){
        	$('#recompute').html('<i class="fa fa-spinner fa-spin"></i> Updating').prop('disabled',true);
        },
		success:function(data){
			par = JSON.parse(data);
        	if(par.status){
           		swal({
					  title: par.response,
					  type: "success",
					  showCancelButton: false,
					  confirmButtonClass: "btn-success",
					  confirmButtonText: "OK",
					  closeOnConfirm: false
				})
				$('#recompute').html('<i class="fa fa-cog"></i> Recompute').prop('disabled',false);
				$('.emp_select').prop('checked', false)
			}
		}
	})
})
})
</script>
@endsection
