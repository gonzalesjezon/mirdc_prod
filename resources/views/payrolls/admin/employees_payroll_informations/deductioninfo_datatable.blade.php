<div class="col-md-12">
	<table class="table table-responsive datatable" id="tbl_deductioninfo">
		<thead>
				<tr>
					<th>Deduction Name</th>
					<th>Amount</th>
					<th>Date Start</th>
					<th>Date End</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
	</table>

</div>
<script type="text/javascript">
$(document).ready(function(){

	 var table = $('#tbl_deductioninfo').DataTable({
	 	'dom':'<lf<t>pi>',
	 	"paging": false,
	 	// "scrollY":"250px",
   //      "scrollCollapse": true,
	 });

	$('#tbl_deductioninfo tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');

	        clear_form_elements('myForm5');

	        id 					= $(this).data('id');
			employee_id			= $(this).data('employee_id');
			deduction_id 		= $(this).data('deduction_id');
			deduction_amount 	= $(this).data('deduction_amount');
			start_date			= $(this).data('start_date');
			end_date			= $(this).data('end_date');
			date_terminated		= $(this).data('date_terminated');
			terminated			= $(this).data('terminated');


			$('#deduction_info_id').val(id)
			$('#deduction_id').val(deduction_id);
			$('#deduction_amount').val(deduction_amount);
			$('#deduction_start_date').val(start_date);
			$('#deduction_date_terminated').val(date_terminated);
			$('#deduction_end_date').val(end_date);

			if(terminated == 1){
				$('#chk_deduction_terminated').prop('checked',true);
			}else{
				$('#chk_deduction_terminated').prop('checked',false);
			}


			btnnew = $(this).data('btnnew');
			btnsave = $(this).data('btnsave');
			btnedit = $(this).data('btnedit');
			btncancel = $(this).data('btncancel');
			btndelete = $(this).data('btndelete');

			if(!$('#'+btnsave).is(':visible')){
				$('#'+btnedit).removeClass('hidden');
				$('#'+btncancel).removeClass('hidden');
				$('#'+btndelete).removeClass('hidden');
				$('#'+btnnew).addClass('hidden');
			}

	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	} );

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	} );


})
</script>
