<div class="col-md-12">
	<div class="box-1 button-style-wrapper" >
		<a class="btn btn-xs btn-info btn-savebg btn_new" id="newSalary" data-btnnew="newSalary" data-btnedit="editSalary" data-btnsave="saveSalary" data-btncancel="cancelSalary"><i class="fa fa-save"></i> New</a>
		<a class="btn btn-xs btn-success btn-editbg btn_edit hidden" data-btnnew="newSalary" data-btnedit="editSalary" data-btnsave="saveSalary" data-btncancel="cancelSalary" id="editSalary"><i class="fa fa-edit"></i> Edit</a>
		<a class="btn btn-xs btn-info btn-savebg btn_save submitme hidden" data-btnnew="newSalary" data-btnedit="editSalary" data-btnsave="saveSalary" data-btncancel="cancelSalary" data-form="form2" id="saveSalary"><i class="fa fa-save"></i> Save</a>
		<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newSalary" data-btnedit="editSalary" data-btnsave="saveSalary" data-btncancel="cancelSalary" data-form="myform2" id="cancelSalary"> Cancel</a>
	</div>
	<form method="POST" action="{{ url($module_prefix.'/'.$module.'/storeSalaryinfo')}}" onsubmit="return false" id="form2" class="myform2">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="employee_number" class="employee_number">
		<div class="col-md-7">
			<div class="formcontent" id="_salaryForm">
				<table class="table borderless">
					<tr>
						<td><span>Effectivity Date</span></td>
						<td>
							<input type="text" name="salary_effectivity_date" id="effective_salarydate" class="form-control font-style2 datepicker newSalary">
						</td>

					</tr>
					<tr>
						<td>Description</td>
						<td >
							<select name="salary_description" id="salary_description" class="form-control  font-style2 newSalary">
								<option value=""></option>
								<option value="Initial Salary">Initial Salary</option>
								<option value="Salary Adjustment">Salary Adjustment</option>
								<option value="Salary Increase">Salary Increase</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Basic 1</td>
						<td>
							<input type="text" name="basic_amount_one" id="basic_amount_one" class="form-control onlyNumber font-style2 newSalary">
						</td>
					</tr>
					<tr>
						<td>Basic 2</td>
						<td>
							<input type="text" name="basic_amount_two" id="basic_amount_two" class="form-control onlyNumber font-style2 newSalary">
						</td>
					</tr>
					<tr class="divsg">
						<td><span>Salary Grade</span></td>
						<td>
							@if ($errors->has('salary_grade'))
							    <span class="text-danger">{{ $errors->first('salary_grade') }}</span>
							@endif
							<select name="salarygrade_id" id="salarygrade_id" class="form-control font-style2 newSalary">
								<option value=""></option>
								@foreach($sg_data as $items)
									<option value="{{ $items->id }}">{{ $items->salary_grade }}</option>
								@endforeach
							</select>
						</td>
					</tr>
					<tr class="divjg hidden">
						<td><span>Job Grade</span></td>
						<td>
							@if ($errors->has('jobgrade_id'))
							    <span class="text-danger">{{ $errors->first('jobgrade_id') }}</span>
							@endif
							<select name="jobgrade_id" id="jobgrade_id" class="form-control font-style2 newSalary">
								<option value=""></option>
								@foreach($jg_data as $items)
									<option value="{{ $items->id }}">{{ $items->job_grade }}</option>
								@endforeach
							</select>
						</td>
					</tr>
					<tr class="divsg">
						<td><span>Step Increment</span></td>
						<td>
							<select name="step_inc" id="step_inc" class="form-control font-style2 newSalary">
								<option value=""></option>
								<option value="step1">Step 1</option>
								<option value="step2">Step 2</option>
								<option value="step3">Step 3</option>
								<option value="step4">Step 4</option>
								<option value="step5">Step 5</option>
								<option value="step6">Step 6</option>
								<option value="step7">Step 7</option>
								<option value="step8">Step 8</option>
							</select>
						</td>
					</tr>
					<tr class="divsg hidden">
						<td><span>Amount</span></td>
						<td>
							<input type="text" name="amount" id="amount" class="form-control font-style2"/>

						</td>
					</tr>
					<tr class="divjg hidden">
						<td><span>Step Increment</span></td>
						<td>
							<select name="jgstep_inc" id="jgstep_inc" class="form-control font-style2 newSalary">
								<option value=""></option>
								<option value="step1">Step 1</option>
								<option value="step2">Step 2</option>
								<option value="step3">Step 3</option>
								<option value="step4">Step 4</option>
								<option value="step5">Step 5</option>
								<option value="step6">Step 6</option>
								<option value="step7">Step 7</option>
								<option value="step8">Step 8</option>
							</select>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<input type="hidden" name="id" id="salary_info_id">
		<input type="hidden" name="employee_id" class="employee_id">
	</form>
</div>