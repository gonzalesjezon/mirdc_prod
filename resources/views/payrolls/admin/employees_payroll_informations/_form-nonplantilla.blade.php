<div class="tab-content">
	<div class="nonplantilla">
		<div class="col-md-12">
			<div class="button-wrapper" style="position: relative;top: 10px;left: 5px;" >
				<a class="btn btn-xs btn-info btn-savebg btn_new" id="newNonPlantilla" data-btnnew="newNonPlantilla" data-btncancel="cancelNonPlantilla" data-btnedit="editNonPlantilla" data-btnsave="saveNonPlantilla"><i class="fa fa-save"></i> New</a>

				<a class="btn btn-xs btn-info btn-editbg btn_edit hidden" id="editNonPlantilla" data-btnnew="newNonPlantilla" data-btncancel="cancelNonPlantilla" data-btnedit="editNonPlantilla" data-btnsave="saveNonPlantilla"><i class="fa fa-save"></i> Edit</a>

				<a class="btn btn-xs btn-info submitme hidden" data-form="form2" data-btnnew="newNonPlantilla" data-btncancel="cancelNonPlantilla" data-btnedit="editNonPlantilla" data-btnsave="saveNonPlantilla" id="saveNonPlantilla"><i class="fa fa-save"></i> Save</a>

				<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newNonPlantilla" data-btncancel="cancelNonPlantilla" data-form="myform4" data-btnedit="editNonPlantilla" data-btnsave="saveNonPlantilla"id="cancelNonPlantilla"> Cancel</a>
			</div>
			<form method="POST" action="{{ url($module_prefix.'/'.$module.'/storeNonPlantilla')}}" onsubmit="return false" id="form2" class="myform4">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="employee_number" class="employee_number">
			<input type="hidden" name="employee_id" class="employee_id">
			<input type="hidden" name="id" id="nonplantilla_id">
			<div class="col-md-4">
				<div class="border-style02">
					<table class="table borderless">
						<tr>
							<td>
								<span>Employee Status</span>
							</td>
							<td>
								<input type="text" name="jo_employee_status" id="jo_employee_status" class="form-control font-style2 "  readOnly>
							</td>
						</tr>
						<tr>
							<td>
								<span>Employee No.</span>
							</td>
							<td>
								<input type="text" name="jo_employee_no" id="jo_employee_no" class="form-control font-style2" readOnly>
							</td>
						</tr>
						<tr>
							<td>
								<span>BP No.</span>
							</td>
							<td>
								<input type="text" name="jo_bp_no" id="jo_bp_no" class="form-control font-style2 newNonPlantilla">
							</td>
						</tr>
					</table>
				</div>
				<div class="border-style02 margintop-25">
					<table class="table borderless">
						<tr>
							<td>
								<span>Position Item No.</span>
							</td>
							<td>
								<input type="text" name="jo_position_itemno" id="jo_position_itemno" class="form-control font-style2" readOnly>
							</td>
						</tr>
						<tr>
							<td>
								<span>Position</span>
							</td>
							<td>
								<input type="text" name="jo_position" id="jo_position" class="form-control font-style2" readOnly>
							</td>
						</tr>
						<tr>
							<td>
								<span>Office</span>
							</td>
							<td>
								<input type="text" name="jo_office" id="jo_office" class="form-control font-style2 _input" readOnly>
							</td>
						</tr>
						<tr>
							<td>
								<span>Division</span>
							</td>
							<td>
								<input type="text" name="jo_division" id="jo_division" class="form-control font-style2 _input" readOnly>
							</td>
						</tr>
						<tr>
							<td><span>Projects</span></td>
							<td>
								<select class="font-style2 form-control newNonPlantilla customselect" id="project_id" name="project_id">
								<option value=""></option>
								@foreach($projects as $value)
								<option  value="{{ $value->id }}">{{ $value->name }}</option>
								@endforeach
							</select>
							</td>
						</tr>
						<tr>
							<td><span>Responsiblity Center</span></td>
							<td>
								<select class="font-style2 form-control newNonPlantilla customselect" id="responsibility_id" name="responsibility_id">
								<option value=""></option>
								@foreach($rcenter as $value)
								<option  value="{{ $value->id }}">{{ $value->name }}</option>
								@endforeach
							</select>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="border-style2">
					<table class="table borderless">
						<tr class="text-center">
							<td>
								<span>Daily Rate</span>
							</td>
							<td>
								<input type="text" name="daily_rate_amount" class="form-control font-style2 onlyNumber newNonPlantilla" id="job_order_daily_rate" placeholder="0.00" />
							</td>
						</tr>
						<tr class="hidden">
							<td>
								<span>Monthly Rate</span>
							</td>
							<td>
								<input type="text" name="monthly_rate_amount" class="form-control font-style2 onlyNumber" id="jo_monthly_rate_amount" placeholder="0.00" readonly />
							</td>
						</tr >
						<tr class="hidden">
							<td>
								<span>Annual Rate</span>
							</td>
							<td>
								<input type="text" name="jo_annual_rate_amount" class="form-control font-style2 onlyNumber" id="jo_annual_rate_amount" placeholder="0.00" readonly />
							</td>
						</tr>
					</table>
				</div>
				<div class="border-style2" style="margin-top: -30px;">
					<table class="table borderless">
						<tr>
							<td colspan="2">
								<div class="row form-group" style="margin-right: -5px;margin-left: -5px;">
									<div class="col-sm-5">
										<span>Select Policy</span>
									</div>
									<div class="col-sm-7">
										<select id="jo_tax_policy_id" name="tax_policy_id" class="form-control font-style2 newNonPlantilla">
											<option></option>
											<option value="1">Inputted</option>
											<option value="2">Standard Policy</option>
											<option value="3">With sworn declaration</option>
										</select>
									</div>
								</div>
								<div class="row form-group tax_rate hidden" style="margin-right: -5px;margin-left: -5px;">
									<div class="col-sm-5">
										<span>Tax Rate (EWT)</span>
									</div>
									<div class="col-sm-7">
										<select class="font-style2 form-control newNonPlantilla customselect" id="ewt_policy_id" name="ewt_policy_id">
											<option value=""></option>
											@foreach($cos_tax_policy as $policy)
											<option data-taxrate="{{ $policy->job_grade_rate }}" value="{{ $policy->id }}">{{ $policy->policy_name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="row form-group tax_rate hidden" style="margin-right: -5px;margin-left: -5px;">
									<div class="col-sm-5">
										<span>Tax Rate (PWT)</span>
									</div>
									<div class="col-sm-7">
										<select class="font-style2 form-control newNonPlantilla customselect" id="pwt_policy_id" name="pwt_policy_id">
											<option value=""></option>
											@foreach($cos_tax_policy as $policy)
											<option data-taxrate="{{ $policy->job_grade_rate }}" value="{{ $policy->id }}">{{ $policy->policy_name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="row form-group inputted hidden" style="margin-right: -5px;margin-left: -5px;">
									<div class="col-sm-5">
										<span>Inputted</span>
									</div>
									<div class="col-sm-7">
										<input type="text" name="inputted_amount" id="jo_tax_amount" class="form-control newNonPlantilla font-style2 onlyNumber">
									</div>
								</div>
								<div class="row form-group sworn hidden" style="margin-right: -5px;margin-left: -5px;">
									<div class="col-sm-5">
										<span>Threshhold Amount</span>
									</div>
									<div class="col-sm-7">
										<input type="text" name="threshhold_amount" id="threshhold_amount" class="form-control newNonPlantilla font-style2 onlyNumber">
									</div>
								</div>
							</td>
						</tr>
					</table>
				</div>
				<div class="border-style2 margintop-25">
					<table class="table borderless">
						<tr>
							<td class="text-center" colspan="2"><span>For Job Order/Contractual</span></td>
							<td></td>
						</tr>
						<tr>
							<td>
								<span>Assumption to Duty</span>
							</td>
							<td>
								<input type="text" class="form-control font-style2 newNonPlantilla " id="jo_assumption_to_duty" name="jo_assumption_to_duty" readOnly />
							</td>
							<td></td>
						</tr>
						<tr>
							<td>
								<span>End of Contract</span>
							</td>
							<td>
								<input type="text" class="form-control font-style2 newNonPlantilla " id="jo_end_of_contract" name="jo_end_of_contract" readOnly />
							</td>
							<td></td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="border-style2 ">
					<table class="table borderless newNonPlantilla">
						<tr>
							<td><span>TIN</span></td>
							<td>
								<input type="text" name="tax_id_number" id="jo_tax_id_number" class="form-control font-style2 newNonPlantilla">
							</td>
						</tr>
						<tr>
							<td><span>ATM No</span></td>
							<td>
								<input type="text" name="atm_no" id="jo_atm_no" class="form-control font-style2 newNonPlantilla">
							</td>
						</tr>
						<tr>
							<td>
								<span>Bank</span>
							</td>
							<td>
								<select class="form-control font-style2 customselect newNonPlantilla"  id="jo_bank_id" name="bank_id">
									<option value=""></option>
									@foreach($bank as $value)
										<option data-jobankbranch="{{ $value->branch_name }}" value="{{ $value->id }}"  >{{ $value->name }}</option>
									@endforeach
								</select>
							</td>
						</tr>
						<tr>
							<td>
								<span>Bank Branch</span>
							</td>
							<td>
								<input type="text" class="form-control font-style2 customselect" id="jo_bank_branch" name="jo_bank_branch" readonly />
							</td>
						</tr>
					</table>
				</div>
				<div class="border-style2 margintop-25 newNonPlantilla">
					<table class="table borderless">
						<tr class="hidden">
							<td>
								<span>Overtime Balance</span>
							</td>
							<td>
								<input type="text" class="form-control font-style2 onlyNumber newNonPlantilla" id="jo_overtime_balance_amount" name="overtime_balance_amount" />
							</td>
						</tr>
					</table>
				</div>

			</div>
			</form>
		</div>
	</div>
</div>
