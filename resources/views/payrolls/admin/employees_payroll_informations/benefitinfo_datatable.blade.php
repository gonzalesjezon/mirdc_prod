<div class="col-md-12">
	<table class="table table-responsive datatable" id="tbl_benefitinfo">
		<thead class="text-center">
			<tr>
				<th>Start Date</th>
				<th>Benefits</th>
				<th>Amount</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody class="text-center">
		</tbody>
	</table>
</div>
<script type="text/javascript">
$(document).ready(function(){

	var table = $('#tbl_benefitinfo').DataTable({
	 	'dom':'<lf<t>pi>',
	 	"paging": false,
	 	// "scrollY":"250px",
   //      "scrollCollapse": true,
	 });

	$('#tbl_benefitinfo tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');

	        clear_form_elements('myform3');

	        id 					= $(this).data('id');
			employee_id 		= $(this).data('employeeid');
			benefit_amount 		= $(this).data('benefit_amount');
			benefitName			= $(this).data('name');
			benefit_id 			= $(this).data('benefit_id');
			start_date 	 		= $(this).data('start_date');
			end_date 	 		= $(this).data('end_date');
			days_present 		= $(this).data('days_present');
			benefit_rate 		= $(this).data('benefit_rate');
			date_terminated 	= $(this).data('date_terminated');
			terminated 			= $(this).data('terminated');
			number_of_years 	= $(this).data('number_of_years');
			lp_basic_salary 	= $(this).data('lp_basic_salary');
			lp_tranche 			= $(this).data('lp_tranche');


			if(terminated == 1){
				$('#chk_benefit_terminated').prop('checked',true);
			}else{
				$('#chk_benefit_terminated').prop('checked',false);
			}

			$('#daysPresent').addClass('hidden');
			switch(benefitName){
				case 'SALA':
					$('#daysPresent').removeClass('hidden');
					$('#hpRate').addClass('hidden');
					$('.lpRate').addClass('hidden');
				break;
				case 'HAZARD PAY':
					$('#hpRate').removeClass('hidden');
					$('#daysPresent').removeClass('hidden');
					$('.lpRate').addClass('hidden');
					$('#hp_rate').val(benefit_rate).trigger('change');
				break;
				case 'LONGEVITY PAY':
					$('.lpRate').removeClass('hidden');
					$('#daysPresent').addClass('hidden');
					$('#hpRate').addClass('hidden');
					$('#lp_rate').val(benefit_rate).trigger('change');
				break;
				default:
					$('.lpRate').addClass('hidden');
					$('#daysPresent').addClass('hidden');
					$('#hpRate').addClass('hidden');
					$('#benefit_info_id').val();
				break;
			}

			$('#benefit_amount').val(benefit_amount);
			$('#benefit_id').val(benefit_id).trigger('change');
			$('#benefit_info_id').val(id);
			$('#start_date').val(start_date);
			$('#end_date').val(end_date);
			$('#days_present').val(days_present);
			$('#number_of_years').val(number_of_years);
			$('#lp_basic_salary').val(lp_basic_salary);
			$('#lp_tranche').val(lp_tranche);
			$('#benefit_info_date_terminated').val(date_terminated);

			btnnew = $(this).data('btnnew');
			btnsave = $(this).data('btnsave');
			btnedit = $(this).data('btnedit');
			btncancel = $(this).data('btncancel');
			btndelete = $(this).data('btndelete');

			if(!$('#'+btnsave).is(':visible')){
				$('#'+btnedit).removeClass('hidden');
				$('#'+btncancel).removeClass('hidden');
				$('#'+btndelete).removeClass('hidden');
				$('#'+btnnew).addClass('hidden');
			}

	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	});

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	} );

})
</script>
