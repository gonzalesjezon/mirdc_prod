<div class="col-md-12">
		<div class="box-1 button-style-wrapper" >
				<a class="btn btn-xs btn-info btn-savebg btn_new" id="newDeduct" data-btnnew="newDeduct" data-btnedit="editDeduct" data-btnsave="saveDeduct" data-btncancel="cancelDeduct" data-btndelete="deleteDeduct"><i class="fa fa-save"></i> New</a>

				<a class="btn btn-xs btn-success btn-editbg btn_edit hidden" data-btnnew="newDeduct" data-btnedit="editDeduct" data-btnsave="saveDeduct" data-btncancel="cancelDeduct" data-btndelete="deleteDeduct" id="editDeduct"><i class="fa fa-edit"></i> Edit</a>

				<a class="btn btn-xs btn-info btn-savebg btn_save submitme hidden" data-btnnew="newDeduct" data-btnedit="editDeduct" data-btnsave="saveDeduct" data-btncancel="cancelDeduct" data-btndelete="deleteDeduct" data-form="form5" id="saveDeduct"><i class="fa fa-save"></i> Save</a>

				<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newDeduct" data-btnedit="editDeduct" data-btnsave="saveDeduct" data-btncancel="cancelDeduct" data-btndelete="deleteDeduct" id="cancelDeduct" data-form="myform5"> Cancel</a>
		</div>
		<form method="POST" action="{{ url($module_prefix.'/'.$module.'/storeDeductioninfo')}}" onsubmit="return false" id="form5" class="myform5">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="employee_number" class="employee_number">
			<div class="formcontent">
				<table class="table borderless">
					<tr>
						<td>Deduction Name</td>
						<td>
							<select class="form-control font-style2  newDeduct " name="deduction_id" id="deduction_id">
								<option value=""></option>
								@foreach($deductions as $value)
								<option value="{{ $value->id }}">{{ $value->name }}</option>
								@endforeach
							</select>
						</td>
						<td colspan="2"></td>
					</tr>
					<tr>
						<td>Date</td>
						<td>
							<div class="col-md-6">
								<input type="text" name="start_date" id="deduction_start_date" class="form-control font-style2  newDeduct datepicker" placeholder="Start Date">

							</div>
							<div class="col-md-6">
								<input type="text" name="end_date" id="deduction_end_date" class="form-control font-style2  newDeduct datepicker" placeholder="End Date">
							</div>
						</td>
						<td></td>
						<td class="text-right"><label class="font-style2" style="color:#000;font-weight: normal;    position: relative;top: 20px;">
								<input type="checkbox" name="chk_deduction_terminated" id="chk_deduction_terminated" >
							Terminated
							</label>
						</td>
					</tr>
					<tr>
						<td>Amount</td>
						<td>
							<input type="text" name="deduction_amount" class="form-control font-style2 onlyNumber newDeduct" id="deduction_amount">
						</td>
						<td class="text-right">Date Terminated</td>
						<td>
							<input type="text" name="date_terminated" id="deduction_date_terminated" class="form-control font-style2 datepicker" disabled>
						</td>
					</tr>
				</table>
			</div>
			<input type="hidden" name="id" id="deduction_info_id">
			<input type="hidden" name="employee_id" class="employee_id">
		</form>

	</div>