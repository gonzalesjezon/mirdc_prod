<div class="col-md-12">
	<div class="box-1 button-style-wrapper" >
		<a class="btn btn-xs btn-info btn-savebg btn_new" id="newLoan" data-btnnew="newLoan" data-btnedit="editLoan" data-btnsave="saveLoan" data-btncancel="cancelLoan" data-btndelete="deleteLoan"><i class="fa fa-save"></i> New</a>

			<a class="btn btn-xs btn-success btn-editbg btn_edit hidden" data-btnnew="newLoan" data-btnedit="editLoan" data-btnsave="saveLoan" data-btncancel="cancelLoan" data-btndelete="deleteLoan" id="editLoan"><i class="fa fa-edit"></i> Edit</a>

			<a class="btn btn-xs btn-info btn-savebg btn_save submitme hidden" data-btnnew="newLoan" data-btnedit="editLoan" data-btnsave="saveLoan" data-btncancel="cancelLoan" data-btndelete="deleteLoan" data-form="form4" id="saveLoan"><i class="fa fa-save"></i> Save</a>

			<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newLoan" data-btnedit="editLoan" data-btnsave="saveLoan" data-btncancel="cancelLoan" data-btndelete="deleteLoan"   id="cancelLoan" data-form="myform4"> Cancel</a>
	</div>
	<form method="POST" action="{{ url($module_prefix.'/'.$module.'/storeLoaninfo')}}" onsubmit="return false" id="form4" class="myform4">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="employee_number" class="employee_number">
		<div class="formcontent">
			<table class="table borderless">
				<tr>
					<td>Loan Name</td>
					<td colspan="2">
						<select class="form-control font-style2  newLoan " name="loan_id" id="loan_id">
							<option value=""></option>
							@foreach($loans as $value)
							<option value="{{ $value->id }}">{{ $value->name }}</option>
							@endforeach
						</select>
					</td>
					<td class="text-right">Date</td>
					<td>
						<div class="col-md-6">
							<input type="text" name="start_date" id="loan_start_date" class="form-control font-style2  newLoan datepicker" placeholder="Start Date">

						</div>
						<div class="col-md-6">
							<input type="text" name="end_date" id="loan_end_date" class="form-control font-style2  newLoan datepicker" placeholder="End Date">
						</div>
					</td>
				</tr>
				<tr>
					<td>Date Granted</td>
					<td colspan="2">
						<input type="text" name="loan_date_granted" id="loan_date_granted" class="form-control font-style2 datepicker newLoan" >
					</td>
					<td colspan="2"></td>
				</tr>
				<tr>
					<td>Total Loan Amount</td>
					<td colspan="2">
						<input type="text" name="loan_total_amount" id="loan_total_amount" class="form-control font-style2 onlyNumber newLoan" >
					</td>
					<td colspan="2"></td>
				</tr>
				<tr>
					<td>Total Loan Balance</td>
					<td colspan="2">
						<input type="text" name="loan_total_balance" id="loan_total_balance" class="form-control font-style2 onlyNumber newLoan">
					</td>
					<td></td>
					<td class="text-right" >
						<label class="font-style2" style="color:#000;font-weight: normal;    position: relative;top: 20px;">
							<input type="checkbox" name="chk_loan_terminated" id="chk_loan_terminated" >
							Terminated
						</label>
					</td>
				</tr>
				<tr>
					<td>Amortization</td>
					<td colspan="2">
						<input type="text" name="loan_amount" id="loan_amount" class="form-control font-style2 onlyNumber newLoan">
					</td>
					<td class="text-right">Date Terminated</td>
					<td>
						<input type="text" name="date_terminated" id="loan_info_date_terminated" class="form-control font-style2 datepicker" disabled>
					</td>
				</tr>
			</table>

		</div>
		<input type="hidden" name="id" id="loan_info_id">
		<input type="hidden" name="employee_id" class="employee_id">
	</form>
</div>