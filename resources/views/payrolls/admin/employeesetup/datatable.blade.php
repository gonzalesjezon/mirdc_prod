<br><br>
<div class="col-md-12">
	<table class="table table-responsive datatable" id="tbl_employee_status">
		<thead>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Position</th>
				<th>Division</th>
				<th>Office</th>
				<th>Department</th>
				<th>Employee Status</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
			@foreach($data as $value)

			<tr data-id="{{ $value->id }}" data-employee_id="{{  @$value->employees->id }}" data-lastname="{{ @$value->employees->lastname }}" data-firstname="{{ @$value->employees->firstname }}" data-status="{{ @$value->employees->active }}" data-middlename="{{ @$value->employees->middlename }}" data-position_id="{{ @$value->position_id }}" data-division_id="{{ @$value->division_id }}" data-office_id="{{ @$value->office_id }}" data-employeestatus_id="{{ @$value->employee_status_id }}" data-department_id="{{ @$value->department_id }}" data-assumption_date="{{ @$value->assumption_date }}" data-hired_date="{{ @$value->hired_date }}" data-start_date="{{ @$value->start_date }}" data-employee_id="{{ @$value->employee_id }}" data-end_date="{{ @$value->end_date }}" data-btnnew="newEmployeeStatus" data-btnedit="editEmployeeStatus" data-btnsave="saveEmployeeStatus" data-btncancel="cancelEmployeeStatus">
				<td>{{ @$value->id }}</td>
				<td>{{ @$value->employees->lastname }} {{ @$value->employees->firstname }} {{ @$value->employees->middlename }} {{ @$value->employees->extension_name }}</td>
				<td>{{ @$value->positions->name }}</td>
				<td>{{ @$value->divisions->name }}</td>
				<td>{{ @$value->offices->name }}</td>
				<td>{{ @$value->departments->name }}</td>
				<td>{{ @$value->employeestatus->name }}</td>
				<td>{{ ($value->employees->active == 1) ? 'Active' : 'In Active' }}</td>
			</tr>
			@endforeach
		</tbody>

	</table>
</div>
<script type="text/javascript">
$(document).ready(function(){

	 var table = $('#tbl_employee_status').DataTable({
	 	'dom':'<lf<t>pi>',
	 	"paging": false,
	 	"scrollY":"250px",
        "scrollCollapse": true,
	 });

	$('#tbl_employee_status tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');

	       	employee_information_id = $(this).data('id');
	       	employee_id 			= $(this).data('employee_id');
			firstname 				= $(this).data('firstname');
			lastname 				= $(this).data('lastname');
			middlename 				= $(this).data('middlename');
			active 					= $(this).data('status');
			position_id 			= $(this).data('position_id');
			division_id 			= $(this).data('division_id');
			office_id 				= $(this).data('office_id');
			employee_status_id 		= $(this).data('employeestatus_id');
			assumption_date 		= $(this).data('assumption_date');
			department_id 			= $(this).data('department_id');
			hired_date 				= $(this).data('hired_date');

			$('#employee_information_id').val(employee_information_id);
			$('#employee_id').val(employee_id);
			$('#firstname').val(firstname);
			$('#lastname').val(lastname);
			$('#middlename').val(middlename);
			$('#position_id').val(position_id);
			$('#division_id').val(division_id);
			$('#office_id').val(office_id);
			$('#employee_status_id').val(employee_status_id);
			$('#assumption_date').val(assumption_date);
			$('#hired_date').val(hired_date);
			$('#status').val(active);
			$('#department_id').val(department_id);

			btnnew = $(this).data('btnnew');
			btnsave = $(this).data('btnsave');
			btnedit = $(this).data('btnedit');
			btncancel = $(this).data('btncancel');

			if(!$('#'+btnsave).is(':visible')){
				$('#'+btnedit).removeClass('hidden');
				$('#'+btncancel).removeClass('hidden');
				$('#'+btnnew).addClass('hidden');
			}


	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	} );

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	});


})
</script>
