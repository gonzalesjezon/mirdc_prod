<div class="panel" style="padding: 20px;">
	<form action="{{ url($module_prefix.'/'.$module) }}" method="{{ $method }}" id="form">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="id" value="{{ @$access_module->id }}">

		<div class="row form-group">
			<label class="col-sm-2 text-left">Access Name <span style="color: red;">*</span></label>
			<div class="col-sm-3">
				<input type="text" name="access_name" value="{{ @$access_module->access_name }}" class="form-control form-control-sm" required>
			</div>
		</div>

		<div class="row form-group">
			<div class="col-sm-5">
				<table class="table">
					<thead class="text-center">
						<tr>
							<th>Module Name</th>
							<th>Allowed</th>
						</tr>
					</thead>
					<tbody>
						@foreach($modules as $key => $module)
						<tr>
							<td>{!! $module->description !!}</td>
							<td  style="width: 10px;" >
								<!-- <div class="col-12 col-sm- col-lg-6 "> -->
			                      <div class="switch-button switch-button-success switch-button-yesno">
			                      	@if(isset($access_right[$module->id]))
			                        <input type="checkbox" name="access_right[{{$module->id}}]" id="swt{{$key}}" checked><span>
				                     <input type="hidden" name="access_id[{{$module->id}}]" value="{{ $access_right[$module->id] }}">
			                        @else
			                        <input type="checkbox" name="access_right[{{$module->id}}]" id="swt{{$key}}"><span>
			                        @endif
			                        <label for="swt{{$key}}"></label></span>
			                      </div>
			                     <!-- </div> -->
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>

	</form>

	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-2 text-right">
			<a class="btn btn-sm btn-info submit btn-savebg"><i class="fa fa-save"></i>  Save</a>
		</div>
	</div>
</div>

<div>&nbsp;</div>
