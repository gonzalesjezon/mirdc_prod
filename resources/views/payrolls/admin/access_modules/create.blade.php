@extends('app-front')

@section('content')

<div style="height: 50px;"></div>

<div class="row">
	<div class="col-md-12">
		@include('payrolls.admin.access_modules._form', [
		        'method' => 'POST',
		    ])
	</div>
</div>

@endsection
