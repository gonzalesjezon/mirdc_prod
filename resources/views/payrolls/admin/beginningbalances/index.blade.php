@extends('app-front')
@section('content')
<div class="col-md-12">
	<div style="height: 40px;"></div>
	<div class="panel panel-default">
		<div class="panel-heading">&nbsp;</div>
		<div class="panel-body">
			<div class="col-md-3">
				<div >
					<input type="text" name="filter_search" class="form-control search" placeholder="Search Here">
				</div>
				<div style="height: 40px;"></div>
				<div class="sub-panel">
					{!! $controller->show() !!}
				</div>
			</div>
			<div class="col-md-9">
				<div class="form-group">
					<label style="float: right;position: relative;"><span id="employee_name"></span></label>
					<a class="btn btn-xs btn-info btn-savebg btn_new" id="myForm" data-btnnew="myForm" data-btncancel="cancelSummary" data-btnedit="editSummary" data-btnsave="saveSummary"><i class="fa fa-save"></i> New</a>

					<a class="btn btn-xs btn-info btn-editbg btn_edit hidden" id="editSummary" data-btnnew="myForm" data-btncancel="cancelSummary" data-btnedit="editSummary" data-btnsave="saveSummary"><i class="fa fa-save"></i> Edit</a>

					<a class="btn btn-xs btn-info btn-savebg btn_save submit hidden" data-form="form" data-btnnew="myForm" data-btncancel="cancelSummary" data-btnedit="editSummary" data-btnsave="saveSummary" id="saveSummary"><i class="fa fa-save"></i> Save</a>

					<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="myForm" data-btncancel="cancelSummary" data-form="myform" data-btnedit="editSummary" data-btnsave="saveSummary"id="cancelSummary"> Cancel</a>
				</div>
				<form method="POST" action="{{ url($module_prefix.'/'.$module)}}" class="myForm" id="form" onsubmit="return false">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="employee_id" id="employee_id">
					<input type="hidden" name="beginningbalances_id" id="beginningbalances_id">
					<div class="form-group">
						<label>As of Date</label>
						<input type="text" name="as_of_date" id="as_of_date" class="datepicker form-control" style="width: 250px;" >
					</div>
					<div class="col-md-6">
						<label><b>Contributions</b></label>
						<div class="form-group">
							<span>Premiums</span>
							<input type="text" name="premium_amount" id="premium_amount" class="form-control onlyNumber" placeholder="0.00">
						</div>
						<div class="form-group">
							<span>Taxwitheld</span>
							<input type="text" name="tax_witheld" id="tax_witheld" class="form-control onlyNumber" placeholder="0.00">
						</div>
						<label>Non Taxable Compensation</label>
						<div class="form-group">
							<span>Basic Pay</span>
							<input type="text" name="basic_pay" id="basic_pay" class="form-control onlyNumber" placeholder="0.00">
						</div>
						<div class="form-group">
							<span>Overtime Pay</span>
							<input type="text" name="overtime_pay" id="overtime_pay" class="form-control onlyNumber" placeholder="0.00">
						</div>
						<div class="form-group">
							<span>13 Month Pay</span>
							<input type="text" name="thirteen_month_pay" id="thirteen_month_pay" class="form-control onlyNumber" placeholder="0.00">
						</div>
						<div class="form-group">
							<span>Deminimis</span>
							<input type="text" name="deminimis" id="deminimis" class="form-control onlyNumber" placeholder="0.00">
						</div>
						<div class="form-group">
							<span>Other Salaries</span>
							<input type="text" name="other_salaries" id="other_salaries" class="form-control onlyNumber" placeholder="0.00">
						</div>
					</div>
					<div class="col-md-6">
						<label>Taxable Compensation</label>
						<div class="form-group">
							<span>Basic Pay</span>
							<input type="text" name="taxable_basic_pay" id="taxable_basic_pay" class="form-control onlyNumber" placeholder="0.00">
						</div>
						<div class="form-group">
							<span>Overtime</span>
							<input type="text" name="taxable_overtime_pay" id="taxable_overtime_pay" class="form-control onlyNumber" placeholder="0.00">
						</div>
						<div class="form-group">
							<span>13th Month</span>
							<input type="text" name="taxable_thirteen_month_pay" id="taxable_thirteen_month_pay" class="form-control onlyNumber" placeholder="0.00">
						</div>
						<div class="form-group">
							<span>Other Salaries</span>
							<input type="text" name="taxable_other_salaries" id="taxable_other_salaries" class="form-control onlyNumber" placeholder="0.00">
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>
	<br><br>
</div>
@endsection
@section('js-logic1')
<script type="text/javascript">
$(document).ready(function(){
	$('.datepicker').datepicker({
		dateFormat:'yy-mm-dd'
	});
	$('.myForm :input').attr('disabled',true)
	$('.btn_new').on('click',function(){
		$('#benefitinfo_id').val('');
		$('#deductinfo_id').val('');
		$('#loaninfo_id').val('');

		btnnew = $(this).data('btnnew');
		btnsave = $(this).data('btnsave');
		btncancel = $(this).data('btncancel');
		$('.'+btnnew+' :input').attr("disabled",false);
		$('.'+btnnew).attr('disabled',false);
		$('#'+btnnew).addClass('hidden');
		$('#'+btnsave).removeClass('hidden');
		$('#'+btncancel).removeClass('hidden');
	});

	$('.btn_edit').on('click',function(){
		btnnew = $(this).data('btnnew');
		btnsave = $(this).data('btnsave');
		btncancel = $(this).data('btncancel');
		btnedit = $(this).data('btnedit');
		$('.'+btnnew+' :input').attr("disabled",false);
		$('.'+btnnew).attr('disabled',false);
		$('#'+btnnew).addClass('hidden');
		$('#'+btnedit).addClass('hidden');
		$('#'+btnsave).removeClass('hidden');
		$('#'+btncancel).removeClass('hidden');
	});

	$('.btn_cancel').on('click',function(){
		$('#benefitinfo_id').val('');
		$('#deductinfo_id').val('');
		$('#loaninfo_id').val('');

		btnnew = $(this).data('btnnew');
		btnsave = $(this).data('btnsave');
		btncancel = $(this).data('btncancel');
		btnedit = $(this).data('btnedit');

		$('.'+btnnew+' :input').attr("disabled",true);
		$('.'+btnnew).attr('disabled',true);
		$('#'+btnnew).removeClass('hidden');
		$('#'+btnedit).addClass('hidden');
		$('#'+btnsave).addClass('hidden');
		$('#'+btncancel).addClass('hidden');
		$('.weekly').addClass('hidden');
		$('.semi-monthly').addClass('hidden');

		form = $(this).data('form');
		clear_form_elements(form)
		$('.error-msg').remove();

	});
	var as_of_date;
	$(document).on('change','#as_of_date',function(){
		as_of_date = $(this).val();
	});

	$(document).on('click','#namelist tr',function(){
		employee_id 	= $(this).data('empid');
		employee_name = $(this).data('empname');
		$('#employee_id').val(employee_id);
		$('#employee_name').text(employee_name);

		$.ajax({
			url:base_url+module_prefix+module+'/getBeginningBalances',
			data:{
				'id':employee_id,
			},
			type:'GET',
			dataType:'JSON',
			success:function(data){
				clear_form_elements('myForm');
				if(data !== null){
					$('.btn_new').addClass('hidden');
					$('.btn_save').addClass('hidden');
					$('.btn_edit').removeClass('hidden');
					$('.btn_cancel').removeClass('hidden');
					$('#beginningbalances_id').val(data.id);
					$('#employee_name').val('');
					$('#as_of_date').val(data.as_of_date);
					$('#premium_amount').val(data.premium_amount);
					$('#tax_witheld').val(data.tax_witheld);
					$('#basic_pay').val(data.basic_pay);
					$('#overtime_pay').val(data.overtime_pay);
					$('#thirteen_month_pay').val(data.thirteen_month_pay);
					$('#deminimis').val(data.deminimis);
					$('#other_salaries').val(data.other_salaries);
					$('#taxable_basic_pay').val(data.taxable_basic_pay);
					$('#taxable_overtime_pay').val(data.taxable_overtime_pay);
					$('#taxable_thirteen_month_pay').val(data.taxable_thirteen_month_pay);
					$('#taxable_other_salaries').val(data.taxable_other_salaries);

				}
			}
		})

	});

	$(".onlyNumber").keyup(function(){
		amount  = $(this).val();
		if(amount == 0){
			$(this).val('');
		}else{
			plainAmount = amount.replace(/\,/g,'')
			$(this).val(commaSeparateNumber(plainAmount));
		}
	})
})
</script>
@endsection