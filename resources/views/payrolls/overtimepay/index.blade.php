@extends('app-front')
@section('content')
<div class="col-md-12">
	<div class="col-md-3">
		<div style="height: 50px;"></div>
		<div>
			<h5 ><b>Filter Employee By</b></h5>
			<table class="table borderless" style="border:none;font-weight: bold">
				<tr class="text-left">
					<td><span>Transaction Period</span></td>
				</tr>
				<tr>
					<td >
						<div class="row" style="margin-right: -5px;margin-left: -5px;">
							@include('payrolls.includes._months-year')
						</div>
					</td>
				</tr>
				<tr>
					<td>Pay Period</td>
				</tr>
				<tr>
					<td>
						<select id="pay_period" class="form-control font-style2" name="pay_period">
							<option value=""></option>
							<option value="semimonthly">Semi Monthly</option>
							<option value="monthly">Monthly</option>
						</select>
					</td>
					<td >
						<select class="form-control font-style2 hidden" id="semi_pay_period" name="semi_pay_period">
							<option value=""></option>
							<option value="firsthalf">First Half</option>
							<option value="secondhalf">Second Half</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Employee Status</td>
				</tr>
				<tr>
					<td >
						<select class="form-control font-style2" id="status" name="status">
							<option value=""></option>
							<option value="plantilla">Plantilla</option>
							<option value="nonplantilla">Non Plantilla</option>
						</select>
					</td>
				</tr>
			</table>
			<div class="col-md-4">
					<a class="btn btn-xs btn-info btnfilter hidden"style="background-color: #164c8a;" style="float: left;line-height: 16px;" ><i class="fa fa-filter"></i>Filter</a>
				</div>
				<div class="col-md-8 text-right">
					<button class="btn btn-xs btn-info process_overtime" id="process_overtime" style="background-color: #164c8a;"><i class="far fa-save"></i>&nbsp;Process</button>
					<a class="btn btn-xs btn-danger" id="delete_overtime" ><i class="fas fa-minus-circle"></i>&nbsp;Delete</a>

				</div>
				<div class="search-btn">
					<div class="col-md-12">
						<span >Search</span>
						<span class="radiobut-style radio-inline " style="margin-left: 70px;">
							<input type="radio" name="chk_overtime" id="wovertime" value="wovertime">
							W/ OT
						</span>

						<span class="radiobut-style radio-inline ">
							<input type="radio" name="chk_overtime" id="woovertime" value="woovertime">
							W/Out OT
						</span>
					</div>
				</div>
			<div >
				<input type="text" name="filter_search" class="form-control _searchname">
			</div>
			<br>
			<br>
			<div class="namelist" style="position: relative;top: -25px;">
				{!! $controller->show() !!}
			</div>

		</div>
	</div>
	<div class="col-md-9">
		<div style="height: 50px;"></div>
		<div class="col-md-12">
			<div class="col-md-6">
				<div class="newSummary-name">
					<label id="d-name" style="text-transform: uppercase;"></label>
				</div>
			</div>
			<div class="col-md-6">

			</div>
		</div>
		<div class="tab-container">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#">Overtime</a></li>
			</ul>
		</div>
		<div class="tab-content">
			<div class="col-md-12">
				<div class="tab-pane">
					<div class="button-wrapper hidden" style="position: relative;top: 10px;left: 5px;" id="editForm">
						<a class="btn btn-xs btn-info btn-editbg btn_edit" id="editOvertime" data-btnnew="newOvertime" data-btncancel="cancelOvertime" data-btnedit="editOvertime" data-btnsave="saveOvertime"><i class="fa fa-save"></i> Edit</a>

						<a class="btn btn-xs btn-info btn-savebg btn_save save_overtime hidden" data-form="form" data-btnnew="newOvertime" data-btncancel="cancelOvertime" data-btnedit="editOvertime" data-btnsave="saveOvertime" id="saveOvertime"><i class="fa fa-save"></i> Save</a>
						<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newOvertime" data-btncancel="cancelOvertime" data-form="myform" data-btnedit="editOvertime" data-btnsave="saveOvertime"id="cancelOvertime"> Cancel</a>
					</div>
					<div class="border-style2">
						<table class="table borderless">
							<tr class="text-center">
								<td colspan="1">Previous Balance</td>
								<td colspan="2">Used</td>
								<td colspan="2">Available Balance</td>
							</tr>
							<tr>
								<td colspan="1" class="clear-input">
									<input type="text" name="previous_balance" id="previous_balance" class="form-control onlyNumber  editOvertime" placeholder="0.00" readonly>
								</td>
								<td colspan="2" class="clear-input">
									<input type="text" name="used_amount" id="used_amount" class="form-control editOvertime" placeholder="0.00" readonly>
								</td>
								<td colspan="2" class="clear-input">
									<input type="text" name="available_balance" id="available_balance" class="form-control editOvertime" placeholder="0.00" readonly>
								</td>
							</tr>
							<tr class="text-center">
								<td>OT Type</td>
								<td>Rate</td>
								<td>Actual</td>
								<td>Adjustment</td>
								<td>Total</td>
							</tr>
							<tr>
								<td >Regular Day</td>
								<td>
									<input type="text" name="rate_regularday" id="rate_regularday" class="form-control font-style2" readonly value="125%">
								</td>
								<td class="clear-input" >
									<input type="text" name="actual_regular_overtime" id="actual_regular_overtime" class="form-control font-style2 isNumber editOvertime" >
								</td>
								<td class="clear-input">
									<input type="text" name="adjust_regular_overtime" id="adjust_regular_overtime" class="form-control font-style2 isNumber editOvertime" >
								</td>
								<td class="clear-input">
									<input type="text" name="total_regular_amount" id="total_regular_amount" class="form-control font-style2 " readonly placeholder="0.00">
								</td>
							</tr>
							<tr>
								<td>Weekend</td>
								<td>
									<input type="text" name="rate_restday" id="rate_restday" class="form-control font-style2" readonly value="150%">
								</td>
								<td class="clear-input">
									<input type="text" name="actual_special_overtime" id="actual_special_overtime" class="form-control font-style2 isNumber editOvertime" >
								</td>
								<td class="clear-input">
									<input type="text" name="adjust_special_overtime" id="adjust_special_overtime" class="form-control font-style2 isNumber editOvertime" >
								</td>
								<td class="clear-input">
									<input type="text" name="total_special_amount" id="total_special_amount" class="form-control font-style2 " readonly placeholder="0.00">
								</td>
							</tr>
							<tr>
								<td>Regular Holiday</td>
								<td>
									<input type="text" name="rate_regularholiday" id="rate_regularholiday" class="form-control font-style2" readonly value="150%">
								</td>
								<td>
									<input type="text" name="actual_regular_holiday_overtime" id="actual_regular_holiday_overtime" class="form-control font-style2 isNumber editOvertime">
								</td>
								<td class="clear-input">
									<input type="text" name="adjust_regular_holiday_overtime" id="adjust_regular_holiday_overtime" class="form-control font-style2 isNumber editOvertime">
								</td>
								<td class="clear-input">
									<input type="text" name="total_regular_holiday_amount" id="total_regular_holiday_amount" class="form-control font-style2 " readonly placeholder="0.00">
								</td>
							</tr>
							<tr>
								<td colspan="4" class="text-right">
									<label>Overtime Pay</label>
								</td>
								<td><label id="net_overtime">0.00</label></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<input type="hidden" name="employee_id" id="employee_id">
<input type="hidden" name="overtime_id" id="overtime_id">
@endsection
@section('js-logic1')
<script type="text/javascript">
$(document).ready(function(){
var _Year;
var _Month;
var _bool = false;
$(document).on('change','#select_year',function(){
	_Year = "";
	_Year = $(this).find(':selected').val();
	if(_bool == true){
		$('.btnfilter').trigger('click');
	}
})
$(document).on('change','#select_month',function(){
	_Month = "";
	_Month = $(this).find(':selected').val();
	if(_bool == true){
		$('.btnfilter').trigger('click');
	}
})

$('.editOvertime').prop('readonly',true);
$('.select2').select2();

$('#select_year').trigger('change');
$('#select_month').trigger('change');

$('.isNumber').keypress(function (event) {
    return isNumber(event, this)
});
//  SEPARATE NUMBER IN COMMA
$(".onlyNumber").keyup(function(){
	amount  = $(this).val();
	if(amount == 0){
		$(this).val('');
	}else{
		plainAmount = amount.replace(/\,/g,'')
		$(this).val(commaSeparateNumber(plainAmount));
	}
});
var list_id = [];
// CHECK EMPLOYEE WITH OR W/ OUT OVERTIME
check_overtime = ""
 $('input[type=radio][name=chk_overtime]').change(function() {
 	if(!_Year){
 		swal({
		  	title: 'Select year and month first',
		  	type: "warning",
		  	showCancelButton: false,
		  	confirmButtonClass: "btn-warning",
		  	confirmButtonText: "OK",
		  	closeOnConfirm: false
		})
		$(this).prop('checked',false);
 	}else{
		if (this.value == 'wovertime') {
			$('#process_overtime').prop('disabled',true);
			$('#editForm').removeClass('hidden');
			_bool = true;
        	check_overtime = this.value;
        	list_id
        }
        else if (this.value == 'woovertime') {
        	$('#process_overtime').prop('disabled',false);
        	$('#editForm').addClass('hidden');
            check_overtime = this.value;
            list_id
        }
        $('.btnfilter').trigger('click');
 	}
});

var payPeriod;
var semiPayPeriod;
$(document).on('change','#pay_period',function(){
	payPeriod = $(this).find(':selected').val();
	switch(payPeriod){
		case 'semimonthly':
			$('#semi_pay_period').removeClass('hidden');
		break;
		default:
			$('#semi_pay_period').addClass('hidden');
		break;
	}
});

$(document).on('change','#semi_pay_period',function(){
	semiPayPeriod = $(this).find(':selected').val();
})

// GET SELECTED ID AND PUSH IT INTO ARRAY

$(document).on('click','.emp_select',function(){
	empid = $(this).val();
	index = $(this).data('key');
	if(!_Year && !_Month && !list_id){
		swal({
			  title: "Select year and month first",
			  type: "warning",
			  showCancelButton: false,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Yes",
			  closeOnConfirm: false

		});
		$('.emp_select').prop('checked',false);
	}else{
		if($(this).is(':checked')){
			list_id[index] =  empid;

		}else{
			delete list_id[index];
		}
	}
	console.log(list_id);
});
$(document).on('click','#check_all',function(){
	if(!_Year && !_Month){
		swal({
			  title: "Select year and month first",
			  type: "warning",
			  showCancelButton: false,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Yes",
			  closeOnConfirm: false

		});
		$('#check_all').prop('checked',false);
	}else{
		if ($(this).is(':checked')) {
	        $('.emp_select').prop('checked', 'checked');

	        $('.emp_select:checked').each(function(){
	        	list_id.push($(this).val())
	        });

	    } else {
	        $('.emp_select').prop('checked', false)
	        list_id = [];
	    }
	}
});


// GET EMPLOYEE OVERTIME PAY DETAILS
$(document).on('click','#namelist tr',function(){
	employee_id = $(this).data('empid');
	$('#d-name').text($(this).data('employee'));
	if(_Year && _Month){
		$.ajax({
			url:base_url+module_prefix+module+'/getOvertimeInfo',
			data:{
				'employee_id':employee_id,
				'year':_Year,
				'month':_Month,
			},
			type:'GET',
			dataType:'JSON',
			success:function(data){
				console.log(data);
				$('#net_overtime').text('')
				clear_form_elements('editOvertime');
				clear_form_elements('clear-input');
				$('#employee_id').val('');
				$('#overtime_id').val('');
				if(data.plantilla !== null){
					$('#employee_id').val(data.plantilla.employee_id);
					total_working_days = (data.workdays !== null) ? data.workdays : 0;
					total_hour_per_day = 8;
					overtime_balance_amount = (data.plantilla.overtime_balance_amount) ? commaSeparateNumber(parseFloat(data.plantilla.overtime_balance_amount).toFixed(2))  : '';
					previous_balance = overtime_balance_amount;
					$('#previous_balance').val(overtime_balance_amount).trigger('keyup');

				}

				if(data.nonplantilla !== null){
					$('#employee_id').val(data.nonplantilla.employee_id);
					total_working_days = (data.workdays !== null) ? data.workdays : 0;
					total_hour_per_day = 8;
					overtime_balance_amount = (data.nonplantilla.overtime_balance_amount) ? commaSeparateNumber(parseFloat(data.nonplantilla.overtime_balance_amount).toFixed(2))  : '';
					previous_balance = overtime_balance_amount;
					$('#previous_balance').val(overtime_balance_amount).trigger('keyup');
				}

				if(data.overtimepay){
					$('#overtime_id').val(data.overtimepay.id);
					$('#employee_id').val(data.overtimepay.employee_id);
					$('#previous_balance').val(data.overtimepay.previous_balance).trigger('keyup');
					$('#used_amount').val(data.overtimepay.used_amount);
					$('#available_balance').val(data.overtimepay.available_balance);
					$('#actual_regular_overtime').val(data.overtimepay.actual_regular_overtime).trigger('keyup');
					$('#adjust_regular_overtime').val(data.overtimepay.adjust_regular_overtime);
					$('#total_regular_amount').val(data.overtimepay.total_regular_amount);
					$('#actual_special_overtime').val(data.overtimepay.actual_special_overtime);
					$('#adjust_special_overtime').val(data.overtimepay.adjust_special_overtime);
					$('#total_special_amount').val(data.overtimepay.total_special_amount);
					$('#actual_regular_holiday_overtime').val(data.overtimepay.actual_regular_holiday_overtime);
					$('#adjust_regular_holiday_overtime').val(data.overtimepay.adjust_regular_holiday_overtime);
					$('#total_regular_holiday_amount').val(data.overtimepay.total_regular_holiday_amount);
					$('#net_overtime').text(data.overtimepay.total_overtime_amount)
				}
			}
		});
	}else{
		swal({
			title: 'Select year and month first',
			type: "warning",
			showCancelButton: false,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			closeOnConfirm: false
		});
	}
});

$(document).on('click','#process_overtime',function(){
	if(list_id.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Process Overtime?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.processOvertime();
			}else{
				return false;
			}
		});
	}
});

$(document).on('click','.save_overtime',function(){
	if(list_id.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Update Overtime?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.processOvertime();
			}else{
				return false;
			}
		});
	}
});

// SAVE COMPUTED OVERTIME PAY
$.processOvertime = function(){
	$.ajax({
		url:base_url+module_prefix+module+'/storeOvertimeInfo',
		type:'POST',
		data:{
			'_token':"{{ csrf_token() }}",
			'overtime_id':$('#overtime_id').val(),
			'employee_id':list_id,
			'empid':$('#employee_id').val(),
			'previous_balance':$('#previous_balance').val(),
			'used_amount':$('#used_amount').val(),
			'available_balance':$('#available_balance').val(),
			'actual_regular_overtime':$('#actual_regular_overtime').val(),
			'adjust_regular_overtime':$('#adjust_regular_overtime').val(),
			'total_regular_amount':$('#total_regular_amount').val(),
			'actual_special_overtime':$('#actual_special_overtime').val(),
			'adjust_special_overtime':$('#adjust_special_overtime').val(),
			'total_special_amount':$('#total_special_amount').val(),
			'actual_regular_holiday_overtime':$('#actual_regular_holiday_overtime').val(),
			'adjust_regular_holiday_overtime':$('#adjust_regular_holiday_overtime').val(),
			'total_regular_holiday_amount':$('#total_regular_holiday_amount').val(),
			'total_overtime_amount':$('#used_amount').val(),
			'pay_period':payPeriod,
			'sub_pay_period':semiPayPeriod,
			'year':_Year,
			'month':_Month,
			'status':status
		},
		beforeSend:function(){
        	$('#process_overtime').html('<i class="fa fa-spinner fa-spin"></i> Processing').prop('disabled',true);
        },
		success:function(data){
			par = JSON.parse(data)
			swal({
			  	title: par.response,
			  	type: "success",
			  	showCancelButton: false,
			  	confirmButtonClass: "btn-success",
			  	confirmButtonText: "Yes",
			  	closeOnConfirm: false
			});
			$('#process_overtime').html('<i class="fa fa-save"></i> Process').prop('disabled',false);
			clear_form_elements('editOvertime');
			$('#employee_id').val('');
			$('#overtime_id').val('');
			$('#editOvertime').removeClass('hidden');
			$('#saveOvertime').addClass('hidden');
			$('#cancelOvertime').addClass('hidden');
			$('.editOvertime').prop('readonly',true);
			list_id = [];
			$('.btnfilter').trigger('click')
		}
	});
}

var status;
$('#status').change(function(){
	status = $(this).find(':selected').val();
	$('.btnfilter').trigger('click');
})


// FILTER BY
var timer;
$(document).on('click','.btnfilter',function(){
	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
			   type: "GET",
			   url: base_url+module_prefix+module+'/show',
			   data: {
			   	'year':_Year,
			   	'month':_Month,
			   	'check_overtime':check_overtime,
			   	'status':status
			   },
			   success: function(res){
			      $(".namelist").html(res);
			   }
			});
		},500);
});

var previous_balance  ;
var available_balance ;
var total_working_days ;
var total_hour_per_day ;
var total_pay_per_day ;
var total_pay_per_hour ;
//  PASS THE VALUE OF PREVIOUS BALANCE TO AVAILABLE BALANCE
$(document).on('keyup','#previous_balance',function(){
	previous_balance = $(this).val().replace(',','');
	available_balance = previous_balance;
	$('#available_balance').val(commaSeparateNumber(previous_balance));
	total_pay_per_day = (parseFloat(previous_balance) / total_working_days);
	total_pay_per_hour = (parseFloat(total_pay_per_day) /total_hour_per_day);

});

var regular_day_rate = 1.25;
var sp_day_rate 	 = 1.50;
var rh_day_rate 	 = 1.50;
// COMPUTE OVERTIME PAY
function compute_overtime(rate_per_hour,rate_per_minute,rate){
	pay_per_minute = ((rate_per_hour / 60) * (rate_per_minute));
	overtime_pay = ((pay_per_minute) * rate);
	return overtime_pay;
}

function compute_net_overtime(rd_amount,sh_amount,rh_amount){
	rd_amount = (rd_amount) ? rd_amount : 0;
	sh_amount = (sh_amount) ? sh_amount : 0;
	rh_amount = (rh_amount) ? rh_amount : 0;

	net_overtime = (parseFloat(rd_amount) + parseFloat(sh_amount) + parseFloat(rh_amount));

	return net_overtime;
}

function compute_total_overtime(actual_amount,adjust_amount){
	actual_amount = (actual_amount) ? actual_amount : 0;
	adjust_amount = (adjust_amount) ? adjust_amount : 0;

	total_overtime = (parseFloat(actual_amount) + parseFloat(adjust_amount));

	return total_overtime;
}

var actual_rd_amount 	= 0;
var adjust_rd_amount 	= 0;
var total_rd_amount  	= 0;
var actual_sh_amount 	= 0;
var adjust_sh_amount 	= 0;
var total_sh_amount  	= 0;
var actual_rh_amount 	= 0;
var adjust_rh_amount 	= 0;
var total_rh_amount  	= 0;
var net_overtime_amount = 0;

$(document).on('keyup','#actual_regular_overtime',function(){
	rd_overtime = $(this).val();
	actual_rd_amount = compute_overtime(total_pay_per_hour,rd_overtime,regular_day_rate);

	total_rd_amount = compute_total_overtime(actual_rd_amount,adjust_rd_amount);
	if(!total_rd_amount){
		total_rd_amount = actual_rd_amount;
	}
	net_overtime = compute_net_overtime(total_rd_amount,total_sh_amount,total_rh_amount);
	available_balance = (parseInt(previous_balance) - net_overtime);
	net_overtime = (net_overtime) ? commaSeparateNumber(net_overtime.toFixed(2)) : '';
	total_regular_day_amount = (total_rd_amount) ? commaSeparateNumber(total_rd_amount.toFixed(2)) : '';
	$('#total_regular_amount').val(total_regular_day_amount);
	$('#net_overtime').text(net_overtime);
	$('#used_amount').val(net_overtime)
	$('#available_balance').val(commaSeparateNumber(available_balance.toFixed(2)));
});

$(document).on('keyup','#adjust_regular_overtime',function(){
	rd_overtime = $(this).val();
	adjust_rd_amount = compute_overtime(total_pay_per_hour,rd_overtime,regular_day_rate);
	total_rd_amount = compute_total_overtime(actual_rd_amount,adjust_rd_amount);
	if(!total_rd_amount){
		total_rd_amount = adjust_rd_amount;
	}
	net_overtime = compute_net_overtime(total_rd_amount,total_sh_amount,total_rh_amount);
	available_balance = (parseInt(previous_balance) - net_overtime);
	net_overtime = (net_overtime) ? commaSeparateNumber(net_overtime.toFixed(2)) : '';
	total_regular_day_amount = (total_rd_amount) ? commaSeparateNumber(total_rd_amount.toFixed(2)) : '';
	$('#total_regular_amount').val(total_regular_day_amount);
	$('#net_overtime').text(net_overtime);
	$('#used_amount').val(net_overtime)
	$('#available_balance').val(commaSeparateNumber(available_balance.toFixed(2)));
});


$(document).on('keyup','#actual_special_overtime',function(){
	sh_overtime = $(this).val();
	actual_sh_amount = compute_overtime(total_pay_per_hour,sh_overtime,sp_day_rate);
	total_sh_amount = compute_total_overtime(actual_sh_amount,adjust_rd_amount);
	if(!total_sh_amount){
		total_sh_amount = actual_sh_amount;
	}
	net_overtime = compute_net_overtime(total_rd_amount,total_sh_amount,total_rh_amount);
	available_balance = (parseFloat(previous_balance) - parseFloat(net_overtime));
	net_overtime = (net_overtime) ? commaSeparateNumber(net_overtime.toFixed(2)) : '';
	total_specialh_amount = (total_sh_amount) ? commaSeparateNumber(total_sh_amount.toFixed(2)) : '';
	$('#total_special_amount').val(total_specialh_amount);
	$('#net_overtime').text(net_overtime);
	$('#used_amount').val(net_overtime)
	$('#available_balance').val(commaSeparateNumber(available_balance.toFixed(2)));
});

$(document).on('keyup','#adjust_special_overtime',function(){
	sh_overtime = $(this).val();
	adjust_sh_amount = compute_overtime(total_pay_per_hour,sh_overtime,sp_day_rate);
	total_sh_amount = compute_total_overtime(adjust_sh_amount,adjust_rd_amount);
	if(!total_sh_amount){
		total_sh_amount = adjust_sh_amount;
	}
	net_overtime = compute_net_overtime(total_rd_amount,total_sh_amount,total_rh_amount);
	available_balance = (parseFloat(previous_balance) - parseFloat(net_overtime));
	net_overtime = (net_overtime) ? commaSeparateNumber(net_overtime.toFixed(2)) : '';
	total_specialh_amount = (total_sh_amount) ? commaSeparateNumber(total_sh_amount.toFixed(2)) : '';
	$('#total_special_amount').val(total_specialh_amount);
	$('#net_overtime').text(net_overtime);
	$('#used_amount').val(net_overtime)
	$('#available_balance').val(commaSeparateNumber(available_balance.toFixed(2)));
});

$(document).on('keyup','#actual_regular_holiday_overtime',function(){
	rh_overtime = $(this).val();
	actual_rh_amount = compute_overtime(total_pay_per_hour,rh_overtime,rh_day_rate);
	total_rh_amount = compute_total_overtime(actual_rh_amount,adjust_rd_amount);
	if(!total_rh_amount){
		total_rh_amount = actual_rh_amount;
	}
	net_overtime = compute_net_overtime(total_rd_amount,total_sh_amount,total_rh_amount);
	available_balance = (parseFloat(previous_balance) - parseFloat(net_overtime));
	net_overtime = (net_overtime) ? commaSeparateNumber(net_overtime.toFixed(2)) : '';
	total_resth_amount = (total_rh_amount) ? commaSeparateNumber(total_rh_amount.toFixed(2)) : '';
	$('#total_regular_holiday_amount').val(total_resth_amount);
	$('#net_overtime').text(net_overtime);
	$('#used_amount').val(net_overtime)
	$('#available_balance').val(commaSeparateNumber(available_balance.toFixed(2)));
});
$(document).on('keyup','#adjust_regular_holiday_overtime',function(){
	rh_overtime = $(this).val();
	adjust_rh_amount = compute_overtime(total_pay_per_hour,rh_overtime,rh_day_rate);
	total_rh_amount = compute_total_overtime(actual_rh_amount,adjust_rh_amount);
	if(!total_rh_amount){
		total_rh_amount = actual_rh_amount;
	}
	net_overtime = compute_net_overtime(total_rd_amount,total_sh_amount,total_rh_amount);
	available_balance = (parseFloat(previous_balance) - parseFloat(net_overtime));
	net_overtime = (net_overtime) ? commaSeparateNumber(net_overtime.toFixed(2)) : '';
	total_resth_amount = (total_rh_amount) ? commaSeparateNumber(total_rh_amount.toFixed(2)) : '';
	$('#total_regular_holiday_amount').val(total_resth_amount);
	$('#net_overtime').text(net_overtime);
	$('#used_amount').val(net_overtime)
	$('#available_balance').val(commaSeparateNumber(available_balance.toFixed(2)));
});

$(document).on('click','#delete_overtime',function(){
	if(list_id.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Delete Overtime?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.deleteOvertime();
			}else{
				return false;
			}
		});
	}
});

$.deleteOvertime = function(){
	$.ajax({
		url:base_url+module_prefix+module+'/deleteOvertime',
		data:{'empid':list_id,'year':_Year,'month':_Month,'_token':"{{ csrf_token() }}"},
		type:'post',
		beforeSend:function(){
        	$('#delete_overtime').html('<i class="fa fa-spinner fa-spin"></i> Processing').prop('disabled',true);
        },
		success:function(response){
			par = JSON.parse(response)
			if(par.status){
				swal({
				  title: par.response,
				  type: "success",
				  showCancelButton: false,
				  confirmButtonClass: "btn-success",
				  confirmButtonText: "OK",
				  closeOnConfirm: false
				});

				list_id = [];
				$('#delete_overtime').html('<i class="fa fa-save"></i> Process').prop('disabled',false);
				$('.btnfilter').trigger('click');
				clear_form_elements('myForm');
				$("#overtime_pay").text(0.00)
			}
		}
	})
}

$(document).on('keyup','._searchname',function(){
	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
			   type: "GET",
			   url: base_url+module_prefix+module+'/show',
			   data: {
			   	"q":$('._searchname').val(),
			   	'chk_overtime':check_overtime
			},
			   success: function(res){
			      $(".namelist").html(res);
			   }
			});
		},500);
});

$('.btn_edit').on('click',function(){
	btnnew = $(this).data('btnnew');
	btnsave = $(this).data('btnsave');
	btncancel = $(this).data('btncancel');
	btnedit = $(this).data('btnedit');
	$('.'+btnedit+' :input').attr("readonly",false);
	$('.'+btnedit).attr('readonly',false);
	$('#'+btnedit).addClass('hidden');
	$('#'+btnedit).addClass('hidden');
	$('#'+btnsave).removeClass('hidden');
	$('#'+btncancel).removeClass('hidden');
});

$('.btn_cancel').on('click',function(){
	$('#overtime_id').val('');
	btnnew = $(this).data('btnnew');
	btnsave = $(this).data('btnsave');
	btncancel = $(this).data('btncancel');
	btnedit = $(this).data('btnedit');

	$('.'+btnedit+' :input').attr("readonly",true);
	$('.'+btnedit).attr('readonly',true);
	// $('#'+btnnew).removeClass('hidden');
	$('#'+btnedit).removeClass('hidden');
	$('#'+btnsave).addClass('hidden');
	$('#'+btncancel).addClass('hidden');
	clear_form_elements('editOvertime');
	$('.error-msg').remove();

});

});
</script>
@endsection