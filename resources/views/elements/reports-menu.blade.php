
<?php
(strtolower($module) == 'payslips') ? $payslips = 'active' : $payslips = '';
(strtolower($module) == 'generalpayroll') ? $generalpayroll = 'active' : $generalpayroll = '';
(strtolower($module) == 'gsisremittances') ? $gsisremittances = 'active' : $gsisremittances = '';
(strtolower($module) == 'joborder') ? $joborder = 'active' : $joborder = '';
(strtolower($module) == 'contractofservice') ? $contractofservice = 'active' : $contractofservice = '';
(strtolower($module) == 'employeeentitledmidyearbonus') ? $employeeentitledmidyearbonus = 'active' : $employeeentitledmidyearbonus = '';
(strtolower($module) == 'employeeentitledpei') ? $employeeentitledpei = 'active' : $employeeentitledpei = '';
(strtolower($module) == 'employeeentitledriceallowance') ? $employeeentitledriceallowance = 'active' : $employeeentitledriceallowance = '';
(strtolower($module) == 'employeeentitledyearendbonus') ? $employeeentitledyearendbonus = 'active' : $employeeentitledyearendbonus = '';
(strtolower($module) == 'employeeentitledanniversarybonus') ? $employeeentitledanniversarybonus = 'active' : $employeeentitledanniversarybonus = '';
(strtolower($module) == 'employeeentitleduniformallowance') ? $employeeentitleduniformallowance = 'active' : $employeeentitleduniformallowance = '';
(strtolower($module) == 'gippayrollworksheet') ? $gippayrollworksheet = 'active' : $gippayrollworksheet = '';
(strtolower($module) == 'deductedloanreports') ? $deductedloanreports = 'active' : $deductedloanreports = '';
(strtolower($module) == 'loandetailsreports') ? $loandetailsreports = 'active' : $loandetailsreports = '';
(strtolower($module) == 'gsisloanreports') ? $gsisloanreports = 'active' : $gsisloanreports = '';
(strtolower($module) == 'gipdeductionreports') ? $gipdeductionreports = 'active' : $gipdeductionreports = '';
(strtolower($module) == 'specialpayrollreports') ? $specialpayrollreports = 'active' : $specialpayrollreports = '';
(strtolower($module) == 'cosgeneralpayrollreports') ? $cosgeneralpayrollreports = 'active' : $cosgeneralpayrollreports = '';
(strtolower($module) == 'cashadvancereports') ? $cashadvancereports = 'active' : $cashadvancereports = '';
(strtolower($module) == 'employeeentitledhonoraria') ? $employeeentitledhonoraria = 'active' : $employeeentitledhonoraria = '';

(strtolower($module) == 'certificate_of_employment') ? $coe = 'active' : $coe = '';
(strtolower($module) == 'coec_report') ? $coec = 'active' : $coec = '';
?>
<style type="text/css">
    nav>ul>li>a span{
        font-size: 12px;
    }
</style>
<nav class="nav-sidebar">
    <ul class="nav">

        <li class="{{ $payslips }}">
        	<a href="{{ url('payrolls/reports/payslips') }}"  >
				<span>PAYSLIP</span>
			</a>
        </li>
        <li class="{{ $generalpayroll }}">
            <a href="{{ url('payrolls/reports/generalpayroll') }}"  >
                <span>GENERAL PAYROLL</span>
            </a>
        </li>
        <li class="{{ $cosgeneralpayrollreports }}">
            <a href="{{ url('payrolls/reports/cosgeneralpayrollreports') }}"  >
                <span>CONTRACTUAL GENERAL PAYROLL</span>
            </a>
        </li>
        <li class="{{ $specialpayrollreports }}">
            <a href="{{ url('payrolls/reports/specialpayrollreports') }}"  >
                <span>OTHER PAYROLL</span>
            </a>
        </li>
        <li class="{{ $joborder }}">
            <a href="{{ url('payrolls/reports/joborder') }}"  >
                <span>SUMMARY OF DEDUCTIONS OF JOB ORDER</span>
            </a>
        </li>
        <li class="{{ $contractofservice }}">
            <a href="{{ url('payrolls/reports/contractofservice') }}"  >
                <span>SUMMARY OF DEDUCTIONS OF COS</span>
            </a>
        </li>
        <li class="{{ $employeeentitledmidyearbonus }}">
            <a href="{{ url('payrolls/reports/employeeentitledmidyearbonus') }}"  >
                <span>EMPLOYEE ENTITLED TO MID YEAR BONUS</span>
            </a>
        </li>
        <li class="{{ $employeeentitledyearendbonus }}">
            <a href="{{ url('payrolls/reports/employeeentitledyearendbonus') }}"  >
                <span>EMPLOYEE ENTITLED TO YEAR END BONUS</span>
            </a>
        </li>
        <li class="{{ $employeeentitledpei }}">
            <a href="{{ url('payrolls/reports/employeeentitledpei') }}"  >
                <span>EMPLOYEE ENTITLED TO PEI</span>
            </a>
        </li>
        <li class="{{ $employeeentitledriceallowance }}">
            <a href="{{ url('payrolls/reports/employeeentitledriceallowance') }}"  >
                <span>EMPLOYEE ENTITLED TO RICE ALLOWANCE</span>
            </a>
        </li>
        <li class="{{ $employeeentitleduniformallowance }}">
            <a href="{{ url('payrolls/reports/employeeentitleduniformallowance') }}"  >
                <span>EMPLOYEE ENTITLED TO UNIFORM ALLOWANCE</span>
            </a>
        </li>
        <li class="{{ $employeeentitledanniversarybonus }}">
            <a href="{{ url('payrolls/reports/employeeentitledanniversarybonus') }}"  >
                <span>EMPLOYEE ENTITLED TO ANNIVERSARY BONUS</span>
            </a>
        </li>
        <li class="{{ $employeeentitledhonoraria }}">
            <a href="{{ url('payrolls/reports/employeeentitledhonoraria') }}"  >
                <span>EMPLOYEE ENTITLED TO HONORARIA</span>
            </a>
        </li>
        <li class="{{ $gipdeductionreports }}">
            <a href="{{ url('payrolls/reports/gipdeductionreports') }}"  >
                <span>SUMMARY OF DEDUCTIONS FOR GIP</span>
            </a>
        </li>
        <li class="{{ $deductedloanreports }}">
            <a href="{{ url('payrolls/reports/deductedloanreports') }}"  >
                <span>DEDUCTED LOANS REPORT</span>
            </a>
        </li>
        <li class="{{ $loandetailsreports }}">
            <a href="{{ url('payrolls/reports/loandetailsreports') }}"  >
                <span>LOAN DETAILS REPORT</span>
            </a>
        </li>
        <li class="{{ $gsisloanreports }}">
            <a href="{{ url('payrolls/reports/gsisloanreports') }}"  >
                <span>GSIS REMITTANCE REPORT</span>
            </a>
        </li>
        <li class="{{ $gsisremittances }}">
            <a href="{{ url('payrolls/reports/remittances/gsisremittances') }}"  >
                <span>REMITTANCES</span>
            </a>
        </li>
        <li class="{{ $cashadvancereports }}">
            <a href="{{ url('payrolls/reports/cashadvancereports') }}"  >
                <span>CASH ADVANCE REPORTS</span>
            </a>
        </li>

        <li class="{{ $coe }}">
            <a href="{{ url('payrolls/reports/certificate_of_employment') }}"  >
                <span>CERTIFICATE OF EMPLOYMENT</span>
            </a>
        </li>

        <li class="{{ $coec }}">
            <a href="{{ url('payrolls/reports/coec_report') }}"  >
                <span>CERTIFICATE OF EMPLOYEE WITH COMPENSATION</span>
            </a>
        </li>


    </ul>
</nav>

<br>




