<?php
	(strtolower($module) == 'benefits') ? $benefits = 'active' : $benefits = '';
	(strtolower($module) == 'adjustments') ? $adjustments = 'active' : $adjustments = '';
	(strtolower($module) == 'deductions') ? $deductions = 'active' : $deductions = '';
	(strtolower($module) == 'loans') ? $loans = 'active' : $loans = '';
	(strtolower($module) == 'responsibilitiescenter') ? $responsibilitiescenter = 'active' : $responsibilitiescenter = '';
	(strtolower($module) == 'banks') ? $banks = 'active' : $banks = '';
	(strtolower($module) == 'bankbranches') ? $bankbranches = 'active' : $bankbranches = '';
	(strtolower($module) == 'philhealths') ? $philhealths = 'active' : $philhealths = '';
	(strtolower($module) == 'pagibig') ? $pagibig = 'active' : $pagibig = '';
	(strtolower($module) == 'gsis') ? $gsis = 'active' : $gsis = '';
	(strtolower($module) == 'providentfundpolicies') ? $providentfundpolicies = 'active' : $providentfundpolicies = '';
	(strtolower($module) == 'taxes') ? $taxes = 'active' : $taxes = '';
	(strtolower($module) == 'salariesgrade') ? $salariesgrade = 'active' : $salariesgrade = '';
	(strtolower($module) == 'wagerates') ? $wagerates = 'active' : $wagerates = '';
	(strtolower($module) == 'jobgrades') ? $jobgrades = 'active' : $jobgrades = '';
	(strtolower($module) == 'divisions') ? $divisions = 'active' : $divisions = '';
	(strtolower($module) == 'employeestatus') ? $employeestatus = 'active' : $employeestatus = '';
	(strtolower($module) == 'position_items') ? $positionitems = 'active' : $positionitems = '';
	(strtolower($module) == 'positions') ? $positions = 'active' : $positions = '';
	(strtolower($module) == 'offices') ? $offices = 'active' : $offices = '';
	(strtolower($module) == 'rates') ? $rates = 'active' : $rates = '';
	(strtolower($module) == 'travelrates') ? $travelrates = 'active' : $travelrates = '';
	(strtolower($module) == 'projects') ? $projects = 'active' : $projects = '';
?>

<div class="side-menu">
	<!-- <div class="row">
        <div class="col-sm-8"> -->
            <!-- <nav class="nav-sidebar">
                <ul class="nav">
                	<li style="border-bottom: 1px solid #eeeeee;margin-bottom: 10px;" class="text-center">
                		<label>COMPANY SETUP</label></li>
                    <li class="{{ $employeestatus }}">
						<a href="{{ url('payrolls/admin/filemanagers/employeestatus') }}" name="employeestatus" >
							<span>Employee Status</span>
						</a>
					</li>
					<li class="{{ $divisions }}">
						<a href="{{ url('payrolls/admin/filemanagers/divisions') }}" name="divisions"  >
							<span>Divisions</span>
						</a>
					</li>
					<li class="{{ $positionitems }}">
						<a href="{{ url('payrolls/admin/filemanagers/position_items') }}" name="positionitems"  >
							<span>Position Item</span>
						</a>
					</li>
					<li class="{{ $positions }}">
						<a href="{{ url('payrolls/admin/filemanagers/positions') }}" name="positions"  >
							<span>Position</span>
						</a>
					</li>
					<li class="{{ $offices }}">
						<a href="{{ url('payrolls/admin/filemanagers/offices') }}" name="offices"  >
							<span>Office</span>
						</a>
					</li>

                </ul>
            </nav> -->
<!--         </div>
    </div>
    <div class="row">
        <div class="col-sm-8"> -->
            <nav class="nav-sidebar">
                <ul class="nav">
                	<li style="border-bottom: 1px solid #eeeeee;margin-bottom: 10px;" class="text-center">
                		<label>MISCELLANEOUS</label></li>
                    <li class="{{ $benefits }}">
						<a href="{{ url('payrolls/admin/filemanagers/benefits') }}" name="benefits" >
							<span>Benefits</span>
						</a>
					</li>
					<li class="{{ $adjustments }}">
						<a href="{{ url('payrolls/admin/filemanagers/adjustments') }}" name="adjustments"  >
							<span>Adjustments</span>
						</a>
					</li>
					<li class="{{ $deductions }}">
						<a href="{{ url('payrolls/admin/filemanagers/deductions') }}" name="deductions"  >
							<span>Deductions</span>
						</a>
					</li>
					<li class="{{ $loans }}">
						<a href="{{ url('payrolls/admin/filemanagers/loans') }}" name="loans"  >
							<span>Loans</span>
						</a>
					</li>
					<li class="{{ $responsibilitiescenter }}">
						<a href="{{ url('payrolls/admin/filemanagers/responsibilitiescenter') }}" name="resp_center"  >
							<span>Responsibility Center</span>
						</a>
					</li>
					<li class="{{ $projects }}">
						<a href="{{ url('payrolls/admin/filemanagers/projects') }}" name="projects"  >
							<span>Projects</span>
						</a>
					</li>
					<li class="{{ $banks }}">
						<a href="{{ url('payrolls/admin/filemanagers/banks') }}" name="banks"  >
							<span>Banks</span>
						</a>
					</li>
					<li class="{{ $rates }}">
						<a href="{{ url('payrolls/admin/filemanagers/rates') }}" name="rates"  >
							<span>Rates</span>
						</a>
					</li>
					<li class="{{ $travelrates }}">
						<a href="{{ url('payrolls/admin/filemanagers/travelrates') }}" name="travelrates"  >
							<span>Trave Rates</span>
						</a>
					</li>
                    <!-- <li class="nav-divider"></li> -->

                </ul>
            </nav>
<!--         </div>
    </div>
    <div class="row">
        <div class="col-sm-8"> -->
            <nav class="nav-sidebar">
                <ul class="nav">
                	<li style="border-bottom: 1px solid #eeeeee;margin-bottom: 10px;" class="text-center">
                		<label style="font-size: 13px;" > TABLES & POLICIES</label></li>
					<li>
                   <li class="{{ $philhealths }}">
						<a href="{{ url('payrolls/admin/filemanagers/philhealths') }}" name="philhealth"  >
							<span>Philhealth Policies</span>
						</a>
					</li>
					<li class="{{ $pagibig }}">
						<a href="{{ url('payrolls/admin/filemanagers/pagibig') }}" name="pagibig"  >
							<span>Pagibig Policies</span>
						</a>
					</li>
					<li  class="{{ $gsis }}">
						<a href="{{ url('payrolls/admin/filemanagers/gsis') }}" name="gsis" >
							<span>GSIS Policies</span>
						</a>
					</li>
					<li class="{{ $providentfundpolicies }}">
						<a href="{{ url('payrolls/admin/filemanagers/providentfundpolicies') }}" name="provident_fund"  >
							<span>Provident Fund</span>
						</a>
					</li>
					<li class="{{ $taxes }}">
						<a href="{{ url('payrolls/admin/filemanagers/taxes') }}" name="tax"  >
							<span>Tax Table</span>
						</a>
					</li>
					<li class="{{ $wagerates }}">
						<a href="{{ url('payrolls/admin/filemanagers/wagerates') }}" name="wagerate"  >
							<span>Wage Rate</span>
						</a>
					</li>
					<li class="{{$salariesgrade}}">
						<a href="{{ url('payrolls/admin/filemanagers/salariesgrade') }}" name="salary"  class="{{ $salariesgrade }}">
							<span>Salary Grade</span>
						</a>
					</li>
		<!-- 			<li class="{{ $jobgrades }}">
						<a href="{{ url('payrolls/admin/filemanagers/jobgrades') }}" name="jobgrade"  >
							<span>Job Grade</span>
						</a>
					</li> -->
                    <!-- <li class="nav-divider"></li> -->

                </ul>
            </nav>
<!--         </div>
    </div> -->
</div>


