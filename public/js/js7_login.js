function calcHash(objval) {
   try {
      var hash_Variant = "SHA-1";
      var hash_InputType = "TEXT";
      var hash_Rounds = "3";

      var hashObj = new jsSHA(
         hash_Variant,
         hash_InputType,
         {numRounds: parseInt(hash_Rounds, 10)}
      );
      hashObj.update(objval);
      return hashObj.getHash("HEX");
   } catch(e) {
      return e.message;
   }
}

function loginNow() {
   var frm = document.formlogin;
   var valtoken = frm.token.value;
   frm.token.value = "dbgebm";
   frm.hToken.value = calcHash(valtoken);
   frm.submit();
}

function TCNow(kindEntry) {
   var frm = document.formlogin;
   var valtoken = frm.token.value;
   frm.token.value = "e2e";
   frm.hToken.value = valtoken;
   //frm.hToken.value = calcHash(valtoken);
   $.post("punchedIn.e2e.php",
   {
      val1:$("#txtEmpIDNO").val(),
      val2:$("[name='hToken']").val(),
      kindEntry:kindEntry,
      token:frm.token.value
   },
   function(data,status) {
      if (status == "success") {
         code = data;
         try {
            eval(code);
         } catch (e) {
             if (e instanceof SyntaxError) {
                 alert(e.message);
             }
         }
      }
      else {
         alert("Ooops Error : " + status + "[fn:TCNow]");
      }
   });
}

function newUser() {
   var frm = document.xForm;
   var valtoken1 = calcHash($("[name='char_Password1']").val());
   var valtoken2 = calcHash($("[name='char_Password2']").val());
   var pw2 = $("[name='char_Password2']").val();
   var pw1 = $("[name='char_Password1']").val();
   var errmsg = "";
   if (pw2.length<8) {
      alert("Password must be atleast 8 AphaNumeric Char");
      return false;
   }
   if ($("[name='char_LastName']").val() == "") errmsg += "LastName is Required\n";
   if ($("[name='char_FirstName']").val() == "") errmsg += "FirstName is Required\n";
   if ($("[name='char_UserName']").val() == "") errmsg += "UserName is Required\n";
   if ($("[name='char_SysGroup']").val() == "") errmsg += "System User Group is Required\n";
   if (pw1 == "") errmsg += "Password is Required\n";
   if (pw2 == "") errmsg += "Please Retype your Required\n";
   if (errmsg != ""){
      alert(errmsg);
      return false;
   }
   frm.char_Password1.value = "";
   frm.char_Password2.value = "";
   if (valtoken1==valtoken2) {
      frm.hNewToken.value = valtoken1;
      frm.char_hNewReToken.value = valtoken2;
      SaveRecord();
      return true;
   } else {
      alert("Password mismatched");
      frm.char_Password2.focus();
      return false;
   }
}
$(document).ready(function () {
   $("#btnChangeNow").click(function () {
      var newToken = "";
      var reToken = "";
      var currentToken = "";
      currentToken = calcHash($("#currentToken").val()),
      newToken = calcHash($("#newToken").val()),
      reToken = calcHash($("#reToken").val()),
      $("#currenToken").val("");
      $("#newToken").val("");
      $("#reToken").val("");
      if (newToken == reToken) {
         
         $.post("trn.e2e.php",
         {
            fn:"CompEmpChangeToken",
            cuToken:currentToken,
            newToken:newToken,
            hUser:$("#hUser").val(),
            EmpRefId:$("#hEmpRefId").val()
         },
         function(data,status) {
            if (status == "success") {
               try {
                  eval(data);   
               }
               catch (e) {
                  if (e instanceof SyntaxError) {
                     alert(e.message);
                  }
               }
             }
         });
      } else {
         $("#newToken").val(""); 
         $("#reToken").val("");
         $("#newToken").focus();
         alert("Mismacthed of New Password !!!");
         return false;
      }
   }); 
}); 
