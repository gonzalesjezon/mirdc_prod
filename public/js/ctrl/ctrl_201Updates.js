$(document).ready(function() {
   $("[name='txtEmpName']").click(function() {
      $("#DataEntry").slideToggle(300);
   });
   $("#201Tabs").change(function() {
      $("#dummyTable").val($(this).val());
      loadEmpDtl($(this).val());
   });
   $(function() {
      if ($("#hUserGroup").val() == "COMPEMP") {
         selectMe($("#hEmpRefId").val());
      }
   });
});

function loadEmpDtl(tbl) {
   if ($("[name='txtEmpName']").val() !== "") {
      var url = "ctrl_201Updates.e2e.php?";
      url += "task=LoadTabDtl";
      url += "&dbtable="+tbl;
      url += "&empRefId="+$("#hEmployeesRefId").val();
      url += "&hCompanyID="+$("#hCompanyID").val();
      url += "&hBranchID="+$("#hBranchID").val();
      $("#idTabDetails").html("");
      $("#idTabDetails").load(url, function(responseTxt, statusTxt, xhr){
         if(statusTxt == "error") {
            alert("Ooops Error: " + xhr.status + " : " + xhr.statusText);
            return false;
         }
      });
   }
}

function afterNewSave(newRefId) {
   var idx = $("#hCurrentIdx").val();
   $("#addNewEntry").html("");
   //alert("Added this transaction for Audit Trail " + newRefId);
   //alert("New Record Added.");
   $("#modalAddNew").modal("hide");
}

function getEmployeeInfo(EmpRefid) {
   $("#hEmployeesRefId").val(EmpRefid);
   $("#201Tabs").val("");
   $("#idTabDetails").html("");
   $("#201Tabs").prop("disabled",false);
   $.get("SystemAjax.e2e.php",
   {
      task:"getEmployeesInfo",
      emprefid:EmpRefid,
      hCompanyID:$("#hCompanyID").val(),
      hBranchID:$("#hBranchID").val(),
      hEmpRefId:$("#hEmpRefId").val(),
      hUser:$("[name='hUser']").val(),
      hUserRefId:$("#hUserRefId").val()
   },
   function(data,status) {
      var data = JSON.parse(data);
      try {
         $("[name='txtEmpName']").val(data.LastName + ", " + data.FirstName);
         $("[name='txtEmpId']").val(data.AgencyId);
      } catch (e) {
          if (e instanceof SyntaxError) {
              alert(e.message);
          }
      }
   });
}

function selectMe(empRefId) {
   $("#tableDtl").slideUp(100);
   getEmployeeInfo(empRefId);
}
