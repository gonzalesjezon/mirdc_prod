$(document).ready(function() {
   $(".chkIncComp--").each(function () {
      $(this).click(function (){
         var chkstr = $("[name='char_IncludeComputation']").val();
         var str = $(this).attr("string");
         if ($(this).is(":checked")) {
            if (chkstr.indexOf(str) < 0) {
               chkstr = chkstr + str;
               $("[name='char_IncludeComputation']").val(chkstr);
            }
         } else {
            $("[name='char_IncludeComputation']").val(chkstr.replace(str, ""));
         }
         alert ($("[name='char_IncludeComputation']").val());
      });
   });
});

function afterNewSave(LastRefId) {
   alert("New Record " + LastRefId + " Succesfully Inserted");
   gotoscrn ("pmsFileManager", setAddURL());
   return false;
}
function afterDelete() {
   alert("Record Succesfully Deleted");
   gotoscrn ("pmsFileManager", setAddURL());
   return false;
}
function afterEditSave(RefId){
   alert("Record " + RefId + " Succesfully Updated");
   gotoscrn ("pmsFileManager", setAddURL());
   return false;
}
