ALTER TABLE `pms_nonplantilla_transactions`
	CHANGE COLUMN `actual_workdays` `actual_workdays` DECIMAL(10,5) NULL DEFAULT NULL AFTER `gross_taxable_pay`,
	CHANGE COLUMN `actual_absences` `actual_absences` DECIMAL(10,5) NULL DEFAULT NULL AFTER `adjust_workdays`;