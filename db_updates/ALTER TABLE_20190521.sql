
-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table hrpms.pms_projects
DROP TABLE IF EXISTS `pms_projects`;
CREATE TABLE IF NOT EXISTS `pms_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table hrpms.pms_projects: ~10 rows (approximately)
/*!40000 ALTER TABLE `pms_projects` DISABLE KEYS */;
INSERT INTO `pms_projects` (`id`, `code`, `name`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(1, NULL, 'Pre-Commercialization Services of RTA and RHA for Hand Tractor', NULL, NULL, NULL, NULL),
	(2, NULL, 'Enhancing One-Stop Laboratory Services for Global Competitiveness (ONELAB)', NULL, NULL, NULL, NULL),
	(3, NULL, 'Technical and Economic Feasibility Study to Determine the Most Suitable Ironmaking Technology for the Value Adding of PH Magnetite Resources', NULL, NULL, NULL, NULL),
	(4, NULL, 'Capability Building for Enhancing the Competitiveness of Die and Mold Industry Through the Engagement of Foreign and Local Experts (DiMo GURU)', NULL, NULL, NULL, NULL),
	(5, NULL, 'Improvement of Gong Fabrication Process through S & T Invention at Mankayan, Benguet', NULL, NULL, NULL, NULL),
	(6, NULL, 'Development of Automatic Trash Rake for Malabon', NULL, NULL, NULL, NULL),
	(7, NULL, 'Strengthening the DOST Regional  Metrology Laboratory Services Phase II', NULL, NULL, NULL, NULL),
	(8, NULL, 'Establishing & Strenghtening of ICT  Infrastructure  ICT INFR & BOSS', NULL, NULL, NULL, NULL),
	(9, NULL, 'Performance Testing and Evaluation of Prototype Trainset - Y2', NULL, NULL, NULL, NULL),
	(10, '01', 'Contract of Service', '2019-05-21 02:54:06', '2019-05-21 02:54:06', 1, NULL);
/*!40000 ALTER TABLE `pms_projects` ENABLE KEYS */;

-- Dumping structure for table hrpms.pms_responsibility_center
DROP TABLE IF EXISTS `pms_responsibility_center`;
CREATE TABLE IF NOT EXISTS `pms_responsibility_center` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(225) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table hrpms.pms_responsibility_center: ~2 rows (approximately)
/*!40000 ALTER TABLE `pms_responsibility_center` DISABLE KEYS */;
INSERT INTO `pms_responsibility_center` (`id`, `code`, `name`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	(1, '01', 'Regular - MDS', 1, NULL, '2019-05-21 02:26:14', '2019-05-21 02:26:14'),
	(2, '02', 'TL', 1, NULL, '2019-05-21 02:26:42', '2019-05-21 02:26:42');
/*!40000 ALTER TABLE `pms_responsibility_center` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;


ALTER TABLE `pms_nonplantilla_employeeinfo`
	ADD COLUMN `project_id` INT(11) NULL DEFAULT NULL AFTER `responsibility_id`;

ALTER TABLE `pms_nonplantilla_transactions`
	ADD COLUMN `project_id` INT(11) NULL DEFAULT NULL AFTER `responsibility_id`;

ALTER TABLE `pms_nonplantilla_transactions`
  ADD COLUMN `overpayment_amount` DECIMAL(13,2) NULL DEFAULT NULL AFTER `aom_amount`,
  ADD COLUMN `underpayment_amount` DECIMAL(13,2) NULL DEFAULT NULL AFTER `overpayment_amount`,
  ADD COLUMN `other_deduction_amount` DECIMAL(13,2) NULL DEFAULT NULL AFTER `underpayment_amount`;