-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table hrpms.pms_annual_tax_rates
DROP TABLE IF EXISTS `pms_annual_tax_rates`;
CREATE TABLE IF NOT EXISTS `pms_annual_tax_rates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_number` varchar(225) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `basic_amount` decimal(13,2) DEFAULT NULL,
  `contribution_amount` decimal(13,2) DEFAULT NULL,
  `pera_amount` decimal(13,2) DEFAULT NULL,
  `rata_amount` decimal(13,2) DEFAULT NULL,
  `longevity_amount` decimal(13,2) DEFAULT NULL,
  `lwop_amount` decimal(13,2) DEFAULT NULL,
  `overtime_amount` decimal(13,2) DEFAULT NULL,
  `hp_amount` decimal(13,2) DEFAULT NULL,
  `tax_amount` decimal(13,2) DEFAULT NULL,
  `for_month` varchar(50) DEFAULT NULL,
  `for_year` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
