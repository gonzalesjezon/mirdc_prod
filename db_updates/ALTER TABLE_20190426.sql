ALTER TABLE `pms_nonplantilla_employeeinfo`
	CHANGE COLUMN `taxpolicy_id` `tax_policy_id` INT(11) NULL DEFAULT NULL AFTER `bank_id`,
	CHANGE COLUMN `taxpolicy_two_id` `ewt_policy_id` INT(11) NULL DEFAULT NULL AFTER `tax_policy_id`,
	ADD COLUMN `pwt_policy_id` INT(11) NULL DEFAULT NULL AFTER `ewt_policy_id`,
	ADD COLUMN `inputted_amount` DECIMAL(9,2) NULL DEFAULT NULL AFTER `overtime_balance_amount`;