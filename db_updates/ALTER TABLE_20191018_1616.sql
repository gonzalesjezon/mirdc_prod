-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table hrpms.pms_access_modules
DROP TABLE IF EXISTS `pms_access_modules`;
CREATE TABLE IF NOT EXISTS `pms_access_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_name` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- Dumping data for table hrpms.pms_access_modules: ~1 rows (approximately)
/*!40000 ALTER TABLE `pms_access_modules` DISABLE KEYS */;
INSERT INTO `pms_access_modules` (`id`, `access_name`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	(15, 'HR 1', 1, NULL, '2019-06-20 21:53:35', '2019-06-20 21:53:35');
/*!40000 ALTER TABLE `pms_access_modules` ENABLE KEYS */;

-- Dumping structure for table hrpms.pms_access_rights
DROP TABLE IF EXISTS `pms_access_rights`;
CREATE TABLE IF NOT EXISTS `pms_access_rights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_module_id` int(11) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `to_view` int(11) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

-- Dumping data for table hrpms.pms_access_rights: ~2 rows (approximately)
/*!40000 ALTER TABLE `pms_access_rights` DISABLE KEYS */;
INSERT INTO `pms_access_rights` (`id`, `access_module_id`, `module_id`, `to_view`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	(27, 15, 4, 1, 1, NULL, '2019-06-20 21:53:35', '2019-06-20 21:53:35'),
	(28, 15, 9, 1, NULL, 1, '2019-06-20 21:53:35', '2019-06-20 21:53:35');
/*!40000 ALTER TABLE `pms_access_rights` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
