-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table gcg.pms_annualtax_policy
DROP TABLE IF EXISTS `pms_annualtax_policy`;
CREATE TABLE IF NOT EXISTS `pms_annualtax_policy` (
  `id` bigint(50) NOT NULL AUTO_INCREMENT,
  `from_year` varchar(225) DEFAULT NULL,
  `to_year` varchar(225) DEFAULT NULL,
  `below_amount` decimal(12,2) DEFAULT NULL,
  `above_amount` decimal(12,2) DEFAULT NULL,
  `rate_percentage` decimal(12,2) DEFAULT NULL,
  `rate_amount` decimal(12,2) DEFAULT NULL,
  `excess_amount` decimal(12,2) DEFAULT NULL,
  `effectivity_date` date DEFAULT NULL,
  `remarks` varchar(300) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table gcg.pms_annualtax_policy: ~5 rows (approximately)
/*!40000 ALTER TABLE `pms_annualtax_policy` DISABLE KEYS */;
INSERT INTO `pms_annualtax_policy` (`id`, `from_year`, `to_year`, `below_amount`, `above_amount`, `rate_percentage`, `rate_amount`, `excess_amount`, `effectivity_date`, `remarks`, `created_at`, `updated_at`, `updated_by`, `created_by`) VALUES
	(1, '2018', '2022', 250000.00, 400000.00, 20.00, 0.00, 250000.00, '2018-10-19', NULL, '2018-10-19 04:19:26', '2018-10-19 04:19:26', NULL, 2),
	(2, '2018', '2022', 400000.00, 800000.00, 25.00, 30000.00, 400000.00, '2018-10-19', NULL, '2018-10-19 04:29:02', '2018-10-19 04:29:02', NULL, 2),
	(3, '2018', '2022', 800000.00, 2000000.00, 30.00, 130000.00, 800000.00, '2018-10-19', NULL, '2018-10-19 04:38:17', '2018-10-19 04:38:17', NULL, 2),
	(4, '2018', '2022', 2000000.00, 8000000.00, 32.00, 490000.00, 2000000.00, '2018-10-19', NULL, '2018-10-19 05:02:26', '2018-10-19 05:02:26', NULL, 2),
	(5, '2018', '2022', 8000000.00, 0.00, 35.00, 2041000.00, 8000000.00, '2018-10-19', NULL, '2018-10-19 05:07:05', '2018-10-19 05:07:05', NULL, 2);
/*!40000 ALTER TABLE `pms_annualtax_policy` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
