-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

USE `hrpms`;

-- Dumping structure for table hris.pms_posted_reports
DROP TABLE IF EXISTS `pms_posted_reports`;
CREATE TABLE IF NOT EXISTS `pms_posted_reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `signatory_id` int(11) DEFAULT NULL,
  `report_type` varchar(225) DEFAULT NULL,
  `code_1` varchar(225) DEFAULT NULL,
  `code_2` varchar(225) DEFAULT NULL,
  `pay_period` varchar(225) DEFAULT NULL,
  `year` varchar(225) DEFAULT NULL,
  `month` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=649 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table hris.pms_signatory
DROP TABLE IF EXISTS `pms_signatory`;
CREATE TABLE IF NOT EXISTS `pms_signatory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `signatory_one` varchar(225) DEFAULT NULL,
  `signatory_two` varchar(225) DEFAULT NULL,
  `signatory_three` varchar(225) DEFAULT NULL,
  `signatory_four` varchar(225) DEFAULT NULL,
  `signatory_five` varchar(225) DEFAULT NULL,
  `signatory_six` varchar(225) DEFAULT NULL,
  `add_degree` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

ALTER TABLE `pms_transactions`
  ADD COLUMN `posted` INT NULL DEFAULT '0' AFTER `month`;

ALTER TABLE `pms_specialpayroll_transactions`
  ADD COLUMN `posted` INT NULL DEFAULT '0' AFTER `month`;

ALTER TABLE `pms_posted_reports`
  ADD COLUMN `division_id` INT(11) NULL DEFAULT NULL AFTER `signatory_id`;

ALTER TABLE `pms_nonplantilla_transactions`
  ADD COLUMN `posted` INT NULL DEFAULT '0' AFTER `month`;

  ALTER TABLE `pms_posted_reports`
  ADD COLUMN `responsibility_id` INT(11) NULL DEFAULT NULL AFTER `division_id`;
-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
