ALTER TABLE `pms_benefitsinfo_transactions`
	ADD COLUMN `used_amount` DECIMAL(9,2) NULL DEFAULT NULL AFTER `no_of_months_entitled`;