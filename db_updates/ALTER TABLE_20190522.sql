-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table hrpms.pms_projects
DROP TABLE IF EXISTS `pms_projects`;
CREATE TABLE IF NOT EXISTS `pms_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table hrpms.pms_projects: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_projects` DISABLE KEYS */;
INSERT INTO `pms_projects` (`id`, `code`, `name`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(1, NULL, 'AMERIAL PROJECT', '2019-05-22 02:06:55', '2019-05-22 02:06:55', 1, NULL),
	(2, NULL, 'Development of Automatic Trash  Rake for Malabon', '2019-05-22 02:07:09', '2019-05-22 02:07:09', 1, NULL),
	(3, NULL, 'Improvement of Gong Fabrication Process through S & T Invention', '2019-05-22 02:07:23', '2019-05-22 02:07:23', 1, NULL),
	(4, NULL, 'ONELAB', '2019-05-22 02:07:35', '2019-05-22 02:07:35', 1, NULL),
	(5, NULL, 'PROJECT AMERIAL', '2019-05-22 02:07:52', '2019-05-22 02:07:52', 1, NULL),
	(6, NULL, 'PROJECT BUHAWI', '2019-05-22 02:08:11', '2019-05-22 02:08:11', 1, NULL),
	(7, NULL, 'RAPPID ADMATEC Proj. 2', '2019-05-22 02:08:23', '2019-05-22 02:08:23', 1, NULL),
	(8, NULL, 'Validation & Turnover of the Hybrid Electric Train', '2019-05-22 02:08:35', '2019-05-22 02:08:35', 1, NULL),
	(9, NULL, 'Establishing & Strenghtening of ICT  Infrastructure  ICT INFR & BOSS', '2019-05-22 02:09:11', '2019-05-22 02:09:11', 1, NULL),
	(10, NULL, 'CONTRACT OF SERVICE', '2019-05-22 02:09:32', '2019-05-22 02:09:32', 1, NULL);
/*!40000 ALTER TABLE `pms_projects` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
