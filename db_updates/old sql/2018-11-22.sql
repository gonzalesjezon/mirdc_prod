-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table mirdc.pms_benefitsinfo_transactions
DROP TABLE IF EXISTS `pms_benefitsinfo_transactions`;
CREATE TABLE IF NOT EXISTS `pms_benefitsinfo_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_number` varchar(225) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `benefit_id` int(11) DEFAULT NULL,
  `benefitinfo_id` int(11) DEFAULT NULL,
  `sala_absent` int(11) DEFAULT NULL,
  `sala_undertime` int(11) DEFAULT NULL,
  `sala_absent_amount` decimal(9,2) DEFAULT NULL,
  `sala_undertime_amount` decimal(9,2) DEFAULT NULL,
  `days_present` int(11) DEFAULT NULL,
  `hp_rate` decimal(9,2) DEFAULT NULL,
  `no_of_months_entitled` int(11) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `year` varchar(50) DEFAULT NULL,
  `month` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1942 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table mirdc.pms_deductioninfo_transactions
DROP TABLE IF EXISTS `pms_deductioninfo_transactions`;
CREATE TABLE IF NOT EXISTS `pms_deductioninfo_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_number` varchar(225) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `deduction_id` int(11) DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  `deduction_info_id` int(11) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `year` varchar(50) DEFAULT NULL,
  `month` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1111 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table mirdc.pms_loaninfo_transactions
DROP TABLE IF EXISTS `pms_loaninfo_transactions`;
CREATE TABLE IF NOT EXISTS `pms_loaninfo_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_number` varchar(225) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `loan_id` int(11) DEFAULT NULL,
  `loan_info_id` int(11) DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `year` varchar(50) DEFAULT NULL,
  `month` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1240 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table mirdc.pms_specialpayroll_transactions
DROP TABLE IF EXISTS `pms_specialpayroll_transactions`;
CREATE TABLE IF NOT EXISTS `pms_specialpayroll_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_number` varchar(225) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `benefit_info_id` int(11) DEFAULT NULL,
  `position_item_id` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  `office_id` int(11) DEFAULT NULL,
  `percentage` decimal(9,2) DEFAULT NULL,
  `cost_uniform_amount` decimal(9,2) DEFAULT NULL,
  `no_of_months_entitled` decimal(9,2) DEFAULT NULL,
  `cash_gift_amount` decimal(9,2) DEFAULT NULL,
  `deduction_amount` decimal(9,2) DEFAULT NULL,
  `tax_amount` decimal(9,2) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `year` varchar(50) DEFAULT NULL,
  `month` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=632 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
